function update_normalization(batch, cellids, modelnames)

global STACK XXX META MODULES;

n_models = length(modelnames);
n_cellids = length(cellids);
stacks = cell(n_models, n_cellids);
metas = cell(n_models, n_cellids);
x0s = cell(n_models, n_cellids);

old_test = [];
new_test = [];

dbopen;
for mi = 1:n_models
    model = modelnames{mi};
    
    for ci = 1:n_cellids
        cellid = cellids{ci};
        
        sql = ['SELECT * FROM NarfResults WHERE batch=' num2str(batch) ''];
        sql = [sql ' AND cellid="' cellid '"'];
        sql = [sql ' AND modelname="' model '"'];
        results = mysql(sql);  
                
        fprintf('Updating Model [%d/%d]\n', (mi-1)*n_cellids+ci, n_models*n_cellids);        
        if isempty(results)
            stacks{mi, ci} = {};
            metas{mi, ci} = {};
            x0s{mi, ci} = {};
            fprintf('Skipping because it doesn''t exist.\n');
            continue;
        elseif (length(results) > 1)
            error('Multiple DB Results found: for %s/%s\n', cellid, model);
        end
        old_test(end+1) = results(1).r_test;
        load_model(char(results(1).modelpath));
        calc_xxx(1);

        new_test(end+1) = XXX{end}.score_test_corr;        
        
        % save_model(META.modelpath, STACK, XXX, META);
        % keyboard;
    end
end

figure; 
plot([0,1],[0,1], 'k-.');
hold on;
plot(old_test, new_test, 'k.');

mean(old_test)
mean(new_test)

keyboard;