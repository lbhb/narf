function m = load_stim_resp_basic(args)
% LOAD_STIM_RESP_BASIC
% A module to load generic stimulus and response files. 
% Returns a function module which implements the MODULE interface.
% See documentation for more information about what a MODULE is.

% Module fields that must ALWAYS be defined
m = [];
m.mdl = @load_stim_resp_basic;
m.name = 'load_stim_resp_basic';
m.fn = @do_load_from_baphy;
m.pretty_name = 'Load stim+resp data in standard format';
m.editable_fields = {'raw_stim_fs', 'raw_resp_fs', 'include_prestim', ...
                    'response_format',...
                    'stimulus_format','stimulus_channel_count', ...
                    'output_stim', 'output_stim_time', ...
                    'output_resp', 'output_resp_time', 'output_respavg'};
m.isready_pred = @isready_always;

% Module fields that are specific to THIS MODULE
m.raw_stim_fs = 100000;
m.raw_resp_fs = 200;
m.include_prestim = 1;
m.pupil = 0;
m.exclude_target_phase = 1;
m.stream=0; % default use whole stimulus
            % if >0, use the stream known by baphy
m.zpad=0;  % pad with zpad bins at begining of each trial
m.stimulus_channel_count=0; % 0 should be 'autodetect'
m.stimulus_format = 'wav';  % Can be 'wav' or 'envelope'
m.response_format = 'spike';  % Can be 'spike' or 'mua' or 'lfp'
m.response_lfp_envelope=0;
m.response_lfp_lo=1;
m.response_lfp_hi=100;
m.output_stim = 'stim';
m.output_stim_time = 'stim_time';
m.output_resp = 'resp';
m.output_resp_time = 'resp_time';
m.output_respavg = 'respavg';
m.data_root = '';
m.data_format = 'baphy';
m.is_data_loader = true; % Special marker used by jackknifing routine
m.varname_stim='stim';
m.varname_resp='resp';
m.validate_subset_frac=0.15;

% Overwrite the default module fields with arguments 
if nargin > 0
    m = merge_structs(m, args);
end

% Optimize this module for tree traversal  
% Signal dependencies
m.required = {'cellid', 'training_set', 'test_set'};
% These signals are modified
m.modifies = {m.output_stim, m.output_stim_time, ...
              m.output_resp, m.output_resp_time ...
              m.output_respavg, 'trial_code'};


% Optional fields
m.plot_fns = {};
m.plot_fns{1}.fn = @do_plot_all_stim_channels;
m.plot_fns{1}.pretty_name = 'Stim Channels (All)';
m.plot_fns{2}.fn = @do_plot_single_stim_channel;
m.plot_fns{2}.pretty_name = 'Stim Channel (Single)';
% m.plot_fns{3}.fn = @(xx, stck) do_plot_channels_as_heatmap(xx, stck, m.output_stim);
% m.plot_fns{3}.pretty_name = 'Stim Channels (Heatmap)';
m.plot_fns{3}.fn = @do_plot_respavg;
m.plot_fns{3}.pretty_name = 'Response Average';
m.plot_fns{4}.fn = @do_plot_response_raster;
m.plot_fns{4}.pretty_name = 'Response Raster';
m.plot_fns{5}.fn = @do_plot_stim_log_spectrogram;
m.plot_fns{5}.pretty_name = 'Stimulus Log Spectrogram';
% m.plot_fns{7}.fn = @do_plot_spectro_and_raster;
% m.plot_fns{7}.pretty_name = 'Spectrogram + Raster';
m.plot_fns{6}.fn = @do_plot_channels_as_heatmap; 
m.plot_fns{6}.pretty_name = 'Output Channels (Heatmap)';
m.plot_gui_create_fn = @create_gui;

% ------------------------------------------------------------------------
% Define the 'methods' of this module, as if it were a class

function x = do_load_from_baphy(mdl, x)
    
    % Merge the training and test set names, which may overlap
    files_to_load = unique({x.training_set{:}, x.test_set{:}});
    
    % remove any problematic characters for field names
    for jj=1:length(x.training_set),
        x.training_set{jj}=file_string_cleanup(x.training_set{jj});
    end
    for jj=1:length(x.test_set),
        x.test_set{jj}=file_string_cleanup(x.test_set{jj});
    end
    
    % Create the 'dat' cell array and its entries, if it doesn't exist
    len = length(files_to_load);
    
    if ~isfield(x, 'dat')
        x.dat=struct();
    end
    
    for f_idx = 1:len;
        f = files_to_load{f_idx};
        
        % SVD 2013-03-08 - check for special est/val subsets
        if strcmpi(f((end-3):end),'_val'),
            datasubset=2;
            fname=f(1:(end-4));
        elseif strcmpi(f((end-3):end),'_est'),
            datasubset=1;
            fname=f(1:(end-4));
        elseif strcmpi(f((end-3):end),'_all'),
            datasubset=0;
            fname=f(1:(end-4));
        else
            datasubset=0;
            fname=f;
        end
        
        % remove special characters that aren't allowing in Matlab
        % field names
        f=file_string_cleanup(f);
        
        data=load(fname);
        resp=data.(m.varname_resp);
        stim=data.(m.varname_stim);
        
        stim=permute(stim,[2 3 1]);  % T x Stim X Chan
        resp=permute(resp,[1 3 2]);  % T x Stim X Rep
        
        if mdl.zpad,
           [~,S,R]=size(resp);
           resp=cat(1,nan.*zeros(mdl.zpad,S,R),resp);
           [~,S,C]=size(stim);
           stim=cat(1,zeros(mdl.zpad,S,C),stim);
        end
        
        % SVD 2013-03-08 - if specified, pull out either estimation (fit) or
        % validation (test) subset of the data
        if datasubset,
            
            vfrac=m.validate_subset_frac;
            
            repcount=squeeze(sum(~isnan(resp(1+mdl.zpad,:,:)),3));
            
            [ff,ii]=sort(repcount,'descend');
            ff=cumsum(ff)./sum(ff);
            validx=ii(1:min(find(ff>=vfrac)));
            
            if datasubset==2
                keepidx=validx;
            else
                keepidx=setdiff(find(repcount>0),validx);
            end
            
            stim=stim(:,keepidx,:);
            resp=resp(:,keepidx,:);
            if exist('pupil','var'),
                pupil=pupil(:,keepidx,:);
            end
        end
        
        training_count=length(x.training_set);
        test_count=length(x.test_set);
        cat_file_set=cat(2,x.training_set,x.test_set);
        if ~isfield(x,'filecodes') || isempty(x.filecodes),
           x.filecodes=repmat({''},[1 training_count+test_count]);
        elseif length(x.filecodes)<training_count,
           x.filecodes=repmat(x.file_codes(1),[1 training_count+test_count]);
        elseif length(x.filecodes)<training_count+test_count,
           x.filecodes=x.filecodes([1:training_count 1:test_count]);
        end
        x.unique_codes=unique(x.filecodes);
        
        % set up trial_code
        this_code=x.filecodes{strcmp(f,cat_file_set)};
        codeidx=find(strcmp(this_code,x.unique_codes));
        
        trial_code=ones(size(stim,2),1).*codeidx;
        
        nanstim=find(isnan(stim(:)));
        if ~isempty(nanstim),
            disp('WARNING: nan-valued stims are being set to zero');
            stim(nanstim)=0;
        end
        
        x.dat.(f).(mdl.output_stim) = stim;
        x.dat.(f).(mdl.output_resp) = resp;
        x.dat.(f).(mdl.output_respavg) = ...
            squeeze(nanmean(x.dat.(f).(mdl.output_resp),3));
        x.dat.(f).trial_code=trial_code;
        
        % Scale respavg so it is a spike rate in Hz
        x.dat.(f).(mdl.output_respavg) = ...
            (mdl.raw_resp_fs) .* x.dat.(f).(mdl.output_respavg);
        
        % Create time signals for later convenience
        [s1 s2 s3] = size(x.dat.(f).(mdl.output_stim));
        [r1 r2 r3] = size(x.dat.(f).(mdl.output_resp));
        [a1 a2]    = size(x.dat.(f).(mdl.output_respavg));
        
        % TODO: Check stim, resp, raw_time signal sizes match.       
        x.dat.(f).(mdl.output_stim_time) = (1/mdl.raw_stim_fs).*[1:s1]';
        x.dat.(f).(mdl.output_resp_time) = (1/mdl.raw_resp_fs).*[1:r1]';
        x.dat.(f).stim_freq=1:s2;
    end
    disp('load_stim_resp_basic: Finished loading files.');
    
end

% ------------------------------------------------------------------------
% Helper functions

    function ylab = what_is_ylabel(mdls)
        if strcmp(mdls{1}.stimulus_format, 'envelope')
            ylab = 'Envelope Magnitude [?]';
        elseif strcmp(mdls{1}.stimulus_format, 'wav')
            ylab = 'Volume [?]';
        else
            ylab = 'Unknown';
        end
    end

% ------------------------------------------------------------------------
% Plot functions

function do_plot_all_stim_channels(sel, stack, xxx)       
    %[mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));
    mdls = stack{end};
    xins = {xxx(1:end-1)};
    xouts = xxx(end);      
    sel=sel(1);
    sel.chan_idx = []; % when chan_idx is empty, do_plot plots all channels
    do_plot(xouts, mdls{1}.output_stim_time, mdls{1}.output_stim, ...
            sel, 'Time [s]', what_is_ylabel(mdls));
end

function do_plot_single_stim_channel(sel, stack, xxx)   
    %[mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end)); 
    mdls = stack{end};
    xins = {xxx(1:end-1)};
    xouts = xxx(end);
    do_plot(xouts, mdls{1}.output_stim_time, mdls{1}.output_stim, ...
            sel, 'Time [s]', what_is_ylabel(mdls)); 
end

function do_plot_respavg(sel, stack, xxx)
    %[mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end)); 
    mdls = stack{end};
    xins = {xxx(1:end-1)};
    xouts = xxx(end);
    [sel.chan_idx] = deal(1);    
    do_plot(xouts, mdls{1}.output_resp_time, mdls{1}.output_respavg, ...
            sel, 'Time [s]', 'Spike Rate Average [Hz]'); 
end

function do_plot_response_raster(sels, stack, xxx)    
    %[mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end)); 
    mdls = stack{end};
    xins = {xxx(1:end-1)};
    xouts = xxx(end);
    mdl = mdls{1};
    for j=1:length(sels)
        if(length(sels)==1)
            col=[0 0 0];
        else
            col=pickcolor(j);
        end
        dat = xouts{1}.dat.(sels(j).stimfile);
        [T, S, R] = size(dat.(mdl.output_resp));
        hold on;
        for r = 1:R
            [xs,ys] = find(dat.(mdl.output_resp)(:, sels(j).stim_idx, r) > 0);
            plot(dat.(mdl.output_resp_time)(xs), r*ys+(j-1)*.2, '.','Color',col);
        end
    end
    do_xlabel('Time [s]');
    do_ylabel('Trial #');
    axis([0 dat.(mdl.output_resp_time)(end) 0 R+1]);
    hold off;
end

% function do_plot_respavg_as_spikes(stack, xxx)
%     mdl = stack{end};    
%     x = xxx{end};
%     
%     % Read the GUI to find out the selected stim files
%     sf = popup2str(mdl.plot_gui.selected_stimfile_popup);
%     stim = popup2num(mdl.plot_gui.selected_stim_idx_popup);
%     dat = x.dat.(sf);
%     
%     [xs,ys] = find(dat.respavg(idx, :) > 0);
%     bar(dat.(mdl.output_resp_time)(ys), dat.respavg(idx,ys), 0.01, 'k-');
%     axis([0 dat.(mdl.output_resp_time)(end) 0 max(dat.respavg(:, stim))]);
% 
% end

function do_plot_stim_log_spectrogram(sel, stack, xxx)
    mdl = stack{end}{1};    
    x = xxx{end};
     
     if ~strcmp(mdl.stimulus_format, 'wav')
         text(0.35, 0.5, 'Cannot visualize non-wav stimulus as spectrogram');
         axis([0, 1, 0 1]);
         return;
     end
               
     % From 500Hz, 12 bins per octave, 4048 sample window w/half overlap
     logfsgram(x.dat.(sel.stimfile).(mdl.output_stim)(:, sel.stim_idx, sel.chan_idx), ...
               4048/2, mdl.raw_stim_fs, [], [], 200, 12); 
     caxis([-20,40]);  % TODO: use a 'smarter' caxis here
     axis tight;
 end
 
% function do_plot_spectro_and_raster(stack, xxx)
%     mdl = stack{end};    
%     x = xxx{end};
%     
%     if strcmp(mdl.stimulus_format, 'envelope')
%         text(0.35, 0.5, 'Cannot visualize envelope as spectrogram');
%         axis([0, 1, 0 1]);
%         return;
%     end
%     
%     % Read the GUI to find out the selected stim files
%     sf = popup2str(mdl.plot_gui.selected_stimfile_popup);
%     stim = popup2num(mdl.plot_gui.selected_stim_idx_popup);
%     dat = x.dat.(sf);
%     
%     hold on;
%     % From 500Hz, 12 bins per octave, 4048 sample window w/half overlap
%     logfsgram(dat.(mdl.output_stim)(:,stim)', 4048, mdl.raw_stim_fs, [], [], 500, 12); 
%     caxis([-20,40]);  % TODO: use a 'smarter' caxis here
%     h = get(gca, 'YLim');
%     d = h(2) - h(1);
%     axis tight;    
%     [T, S, R] = size(dat.(mdl.output_resp));
%     hold on;
%     for r = 1:R
%         [xs,ys] = find(dat.(mdl.output_resp)(:, stim, r) > 0);
%         plot(dat.(mdl.output_resp_time)(xs), ...
%              h(1) + (r/R)*d*(d/(d+d/R))*dat.(mdl.output_resp)(xs,stim,r), 'k.');
%     end    
%     hold off;
% end


function hs = create_gui(parent_handle, stack, xxx)
    pos = get(parent_handle, 'Position');
    w = pos(3) - 5;
    h = pos(4) - 5;
    hs = [];
    
    mdl = stack{end}{1};
    mod_idx = length(stack);
    x = xxx{end};
    
    % Create a popup which selects
    uicontrol('Parent', parent_handle, 'Style', 'text', 'Enable', 'on', ...
        'HorizontalAlignment', 'left',  'String', 'Stimfile:', ...
        'Units', 'pixels', 'Position', [5 (h-25) 100 25]);
    hs.add_stim_button = uicontrol('Parent', parent_handle, ...
        'Style', 'togglebutton', 'Enable', 'on', ...
        'String', '+', ...
        'Units', 'pixels', 'Position', [w-25 (h-25) 25 25], ...
        'Callback', @(h, evts, hdls) add_stim_callback());
    hsind=1;
    create_stim_selector(hsind)
    stim_sel=[];
    function create_stim_selector(ind)
       hs(ind).selected_stimfile_popup = uicontrol('Parent', parent_handle, ...
        'Style', 'popupmenu', 'Enable', 'on', 'String', 'NONE', ...
        'Units', 'pixels', 'Position', [5 (h-40) w-5 25], ...
        'Callback', @(a,b,c) selected_stimfile_popup_callback(ind));
    
    % Create a stimfile selector
    hs(ind).stimidx_txt=uicontrol('Parent', parent_handle, 'Style', 'text', 'Enable', 'on', ...
        'HorizontalAlignment', 'left', 'String', 'Idx:', ...
        'Units', 'pixels', 'Position', [5 (h-70) 50 25]);
    hs(ind).selected_stim_idx_popup = uicontrol('Parent', parent_handle, ...
        'Style', 'popupmenu', 'Enable', 'on', ...
        'String', 'NONE', ...
        'Units', 'pixels', 'Position', [45 (h-70) w-50 25], ...
        'Callback', @(a,b,c) selected_stim_idx_popup_callback());

    % Create a channel selector
    hs(ind).chantxt=uicontrol('Parent', parent_handle, 'Style', 'text', 'Enable', 'on', ...
        'HorizontalAlignment', 'left',  'String', 'Chan:', ...
        'Units', 'pixels', 'Position', [5 (h-95) 50 25]);
    hs(ind).selected_stim_chan_popup = uicontrol('Parent', parent_handle, ...
        'Style', 'popupmenu', 'Enable', 'on', 'String', 'NONE', ...
        'Units', 'pixels', 'Position', [45 (h-95) w-50 25], ...
        'Callback', @(a,b,c) selected_stim_chan_popup_callback());
    
    hs(ind).textbox = uicontrol('Parent', parent_handle, 'Style', 'text', 'Enable', 'on', ...
        'HorizontalAlignment', 'left',  'String', '', ...
        'Units', 'pixels', 'Position', [5 (h-125) w-5 h-100]);
    end
    
    % Two functions to populate the two popup menus
    function update_selected_stimfile_popup(ind)
        fns = fieldnames(x.dat);
        set(hs(ind).selected_stimfile_popup, 'String', char(fns));
        
    end
    
    function update_set_textbox(ind)
        % Print "TEST SET" or "TRAINING SET" in plot GUI as appropriate
        sf = popup2str(hs(ind).selected_stimfile_popup);
        is_test = any(strcmp(sf, x.test_set));
        is_training = any(strcmp(sf, x.training_set));
        
        if is_training && is_test
            str = 'Est&Val Set';
        elseif is_training
            str = 'Estimation Set';
            if isfield(x, 'training_codes') && ~isempty(x.training_codes)
                fc = x.training_codes(min(find(strcmp(sf, x.training_set))));
                str = [str ' [' sprintf('%s', fc) ']'];
            end
         elseif is_test
            str = 'Validation Set';
            if isfield(x, 'test_codes') && ~isempty(x.test_codes)
                fc = x.test_codes(min(find(strcmp(sf, x.test_set))));
                str = [str ' [' sprintf('%s', fc) ']'];
            end
        else
            str = 'Not in a set!?'
        end
        
        % Also append the filecode info
        if isfield(x, 'filecodes') && ~isempty(x.filecodes) && ~isempty(x.test_set)
            fc = x.filecodes(or(strcmp(sf, x.training_set), ...
                                strcmp(sf, x.test_set)));
            str = [str ' [' sprintf('%s', fc{1}) ']'];
        end
        
        set(hs(ind).textbox, 'String', str); 
    end
    
    function update_selected_stim_idx_popup(ind)
        sf = popup2str(hs(ind).selected_stimfile_popup);
        
        if isfield(x.dat, sf)
            [d1, d2, d3] = size(x.dat.(sf).(mdl.output_stim));
            d = {};
            for i = 1:d2
                d{i} = sprintf('%d',i);
            end
            previous_value=get(hs(ind).selected_stim_idx_popup, 'Value');            
            set(hs(ind).selected_stim_idx_popup, 'String', char(d));
            if(d2<previous_value)
                set(hs(ind).selected_stim_idx_popup, 'Value', 1);
            end
        else
            error('Selected stimulus file not found: %s', sf);
        end
    end
    
    function update_selected_stim_chan_popup(ind)
        sf = popup2str(hs(ind).selected_stimfile_popup);
        
        if isfield(x.dat, sf)
            [d1, d2, d3] = size(x.dat.(sf).(mdl.output_stim));
            d = {};
            for i = 1:d3
                d{i} = sprintf('%d',i);
            end
            previous_value=get(hs(ind).selected_stim_chan_popup, 'Value'); 
            set(hs(ind).selected_stim_chan_popup, 'String', char(d));
            if(d3<previous_value)
                set(hs(ind).selected_stim_chan_popup, 'Value', 1);
            end
        else
            error('Selected stimulus file not found: %s', sf);
        end
    end
    
    % Call the two update functions once to build the lists
    update_selected_stimfile_popup(hsind);
    update_selected_stim_idx_popup(hsind);
    update_selected_stim_chan_popup(hsind);
    update_set_textbox(hsind);
    
    % Define three callbacks, one for each popup.
    function selected_stimfile_popup_callback(ind)
        % Update the selected_stim_idx_popup string to reflect new choices
        update_selected_stim_idx_popup(ind);
        update_selected_stim_chan_popup(ind);
        update_set_textbox(ind);
        % Call the next popup callback to trigger a redraw
        selected_stim_idx_popup_callback();
    end
    
    function selected_stim_idx_popup_callback()
        global NARFGUI;
        % Call the plot function again via the plot_popup        
        hgfeval(get(NARFGUI{mod_idx}.plot_popup,'Callback'), mod_idx, []);
        drawnow;
    end
    
    function selected_stim_chan_popup_callback()
        global NARFGUI;
        % Call the plot function again via the plot_popup 
        hgfeval(get(NARFGUI{mod_idx}.plot_popup,'Callback'), mod_idx, []);
        drawnow;
    end
    
    function add_stim_callback()
        global NARFGUI;
        has_plot_gui=find(cellfun(@(x)isfield(x,'plot_gui'),NARFGUI));
        lsrfb_gui=has_plot_gui(cellfun(@(x)isequal(x.plot_gui,hs),NARFGUI(has_plot_gui)));
        ind=length(hs)+1;
        create_stim_selector(ind);
        NARFGUI{lsrfb_gui}.plot_gui=hs;
        if(ind==2)
            stim_sel(1) = uicontrol('Parent', parent_handle, ...
            'Style', 'radiobutton', 'Enable', 'on', ...
            'String', '1', ...
            'Units', 'pixels', 'Position', [60 (h-15) 30 15], ...
            'Callback', @(h, evts, hdls) sel_stim_callback(1));
        end
        stim_sel(ind) = uicontrol('Parent', parent_handle, ...
            'Style', 'radiobutton', 'Enable', 'on', ...
            'String', num2str(ind), ...
            'Units', 'pixels', 'Position', [60+(ind-1)*30 (h-15) 30 15], ...
            'Callback', @(h, evts, hdls) sel_stim_callback(ind),'Value',1);
        sel_stim_callback(ind);
        update_selected_stimfile_popup(ind);
        update_selected_stim_idx_popup(ind);
        update_selected_stim_chan_popup(ind);
        update_set_textbox(ind);
    end
    
    function sel_stim_callback(ind)
        make_invisible=setdiff(1:length(hs),ind);
        fns=fieldnames(hs);
        for i=make_invisible
            for j=1:length(fns)
                set(hs(i).(fns{j}),'Visible','off')
            end
            set(stim_sel(i),'Value',0)
        end
        for j=1:length(fns)
            set(hs(ind).(fns{j}),'Visible','on')
        end
        set(stim_sel(ind),'Value',1)
        
    end
end

% This module can be run if all necessary fields have been defined in the
% topmost part of the stack
function isready = isready_baphy(stack, xxx)
    mdl = stack{end};
    x = xxx{end};
    isready = (length(stack) == 1) && ...
              all(isfield(x, {'cellid', 'training_set', 'test_set'}));
end

end
