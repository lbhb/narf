function m = cond_neuron(args)
% conductance-based neural model that integrates the fir
% filter and allows an arbitrary number of input channels

% Module fields that must ALWAYS be defined
m = [];
m.mdl = @cond_neuron;
m.name = 'cond_neuron';
m.fn = @do_cond_neuron;
m.pretty_name = 'Conductance-based neuron';
m.editable_fields = {'input', 'time','output', 'Vrest', 'V0', 'gL',...
                     'rectify_inputs','input_threshold','input_gain','subsample'};
m.isready_pred = @isready_always;

% Module fields that are specific to THIS MODULE
m.num_coefs = 20;
m.num_dims = 2;
m.baseline = 0;
m.Vrest=0;
m.V0=[40 -20];
m.gL=25;
m.rectify_inputs=0;
m.input_threshold=[0 0];
m.input_gain=[1 1];
m.subsample=2;
m.input =  'stim';
m.filtered_input = 'stim_filtered';
m.time =   'stim_time';
m.output = 'stim';
m.init_fit_sig = 'respavg';
m.fit_constraints = {...
    struct('var', 'coefs', 'lower', -Inf, 'upper', Inf), ...
    struct('var', 'baseline', 'lower', -Inf, 'upper', Inf) ...
};

% Optional fields
m.plot_gui_create_fn = @create_chan_selector_gui;
m.auto_plot = @do_plot_fir_coefs_as_heatmap;
m.auto_init = @auto_init_cond_neuron;
m.plot_fns = {};
m.auto_plot = @do_plot_cond_output;
m.plot_fns{1}.fn = @do_plot_cond_output; 
m.plot_fns{1}.pretty_name = 'IFN Input/Output';
m.plot_fns{2}.fn = @do_plot_cond_input; 
m.plot_fns{2}.pretty_name = 'IFN Inputs';

% Overwrite the default module fields with arguments 
if nargin > 0
    m = merge_structs(m, args);
end

% Optimize this module for tree traversal  
m.required = {m.input, m.time, m.init_fit_sig};  % Signal dependencies
m.modifies = {m.output, m.filtered_input};       % These signals are modified

% ------------------------------------------------------------------------
% Methods

function mdl = auto_init_cond_neuron(stack, xxx)
    % NOTE: Unlike most plot functions, auto_init functions get a 
    % STACK and XXX which do not yet have this module or module's data
    % added to them.    
    if ~isfield(m, 'fit_fields') 
        mdl = m;
        return
    end
    
    % Init the coefs to have the right dimensionality
    mdl = m;
    x = xxx{end};
    fns = fieldnames(x.dat);
    sf = fns{1};
    [T, S, C] = size(x.dat.(sf).(mdl.input));               
    
    mdl.num_dims = C; 
    mdl.V0=linspace(40,-20,C);
    mdl.input_threshold=zeros(1,C);
    mdl.input_gain=ones(1,C);
end

function x = do_cond_neuron(mdl, x)    
    global STACK;
    
    fns = fieldnames(x.dat);
    fs=STACK{1}{1}.raw_stim_fs;
    
    Vrest=mdl.Vrest;
    V0=mdl.V0;
    gL=mdl.gL;
    for ii = 1:length(fns)
        sf=fns{ii};
        sf=fns{ii};
        
        % Compute the size of the filter
        [T, S, C] = size(x.dat.(sf).(mdl.input));
        if ~isfield(mdl,'input_gain'),
           mdl.input_gain=ones(1,C);
        end
        if ~isfield(mdl,'input_threshold'),
           mdl.input_threshold=zeros(1,C);
        end
        
        if S,
            if ~isequal(C, mdl.num_dims)
                error('Dimensions of (mdl.input) don''t match channel count.');
            end
            
            g = x.dat.(sf).(mdl.input);
            if mdl.rectify_inputs,
               for tt=1:size(g,3),
                  g(:,:,tt)=g(:,:,tt)-mdl.input_threshold(tt);
                  g(:,:,tt)=g(:,:,tt).*mdl.input_gain(tt);
               end
               g(g<0)=0;
            end
            
            % conductance-based part
            V = zeros(T, S);
            V(1,:)=Vrest;
            tV0=repmat([V0 Vrest],[S 1])';
            g(:,:,C+1)=-gL;
            
            for tt = 2:T
                tV=V(tt-1,:);
                tg=permute(g(tt,:,:),[3 2 1]);
                
                for jj=1:mdl.subsample,
                   dV=sum(tg .* (repmat(tV,[C+1 1])-tV0))./...
                      (fs./2)./mdl.subsample;
                    %dV=(sum(g(tt,:,:).*(repmat(tV,[1 C])-tV0),3)- ...
                   %    gL*(tV-Vrest))./(fs./2)./mdl.subsample;
                    %dV= (-gE(tt,:).*(tV-V0(1)) - gI(tt,:).*(tV-V0(2)) ...
                    %     - gL*(tV-Vrest))./(fs./2)./mdl.subsample;
                    tV=tV+dV;
                end
                V(tt,:)=tV;
            end
            
            % The output is the sum of the filtered channels
            x.dat.(sf).(mdl.filtered_input) = g;
            x.dat.(sf).(mdl.output) = V; 
            % The output is the sum of the filtered channels
            %if mdl.sum_channels
            %    x.dat.(sf).(mdl.output) = sum(tmp, 3) + mdl.baseline; 
            %    % tmp2 = sum(tmp, 3);
            %else
            %    x.dat.(sf).(mdl.output) = tmp + mdl.baseline; 
            %end
        else
            x.dat.(sf).(mdl.filtered_input) = x.dat.(sf).(mdl.input);
            x.dat.(sf).(mdl.output)=zeros(T,S,1);
        end

    end
    %keyboard
    
end

function do_plot_cond_output(sel, stack, xxx)
    [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end)); 
    sel.chan_idx = []; % when chan_idx is empty, do_plot plots all channels
    do_plot(xouts, mdls{1}.time, ...
            {mdls{1}.input mdls{1}.output}, ...
            sel, 'Time [s]', 'COND Output [-]');
end

function do_plot_cond_input(sel, stack, xxx)
    [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end)); 
    sel.chan_idx = []; % when chan_idx is empty, do_plot plots all channels
    
    do_plot(xouts, mdls{1}.time, {mdls{1}.filtered_input} , ...
            sel, 'Time [s]', 'COND Inputs [-]');
end

end
