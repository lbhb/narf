function m = load_stim_resps_from_baphy(args)
% LOAD_STIM_RESPS_FROM_BAPHY
% A module to load stimulus and response files from BAPHY. 
% Returns a function module which implements the MODULE interface.
% See documentation for more information about what a MODULE is.

% Module fields that must ALWAYS be defined
m = [];
m.mdl = @load_stim_resps_from_baphy;
m.name = 'load_stim_resps_from_baphy';
m.fn = @do_load_from_baphy;
m.pretty_name = 'Load stim+resp from BAPHY';
m.editable_fields = {'raw_stim_fs', 'raw_resp_fs', 'include_prestim', ...
                    'response_format',...
                    'stimulus_format','stimulus_channel_count', ...
                    'output_stim', 'output_stim_time', ...
                    'output_resp', 'output_resp_time', 'output_respavg'};
m.isready_pred = @isready_always;

% Module fields that are specific to THIS MODULE
m.raw_stim_fs = 100000;
m.raw_resp_fs = 200;
m.include_prestim = 1;
m.pupil = 0;
m.exclude_target_phase = 1;
m.stream=0; % default use whole stimulus
            % if >0, use the stream known by baphy
m.zpad=0;  % pad with zpad bins at begining of each trial
m.stimulus_channel_count=0; % 0 should be 'autodetect'
m.stimulus_format = 'wav';  % Can be 'wav' or 'envelope'
m.response_format = 'spike';  % Can be 'spike' or 'mua' or 'lfp'
m.response_lfp_envelope=0;
m.response_lfp_lo=1;
m.response_lfp_hi=100;
m.output_stim = 'stim';
m.output_stim_time = 'stim_time';
m.output_resp = 'resp';
m.output_resp_time = 'resp_time';
m.output_respavg = 'respavg';
m.data_root = '';
m.data_format = 'baphy';
m.is_data_loader = true; % Special marker used by jackknifing routine

% Overwrite the default module fields with arguments 
if nargin > 0
    m = merge_structs(m, args);
end

% Optimize this module for tree traversal  
% Signal dependencies
m.required = {'cellid', 'training_set', 'test_set'};
% These signals are modified
m.modifies = {m.output_stim, m.output_stim_time, ...
              m.output_resp, m.output_resp_time ...
              m.output_respavg, 'trial_code'};


% Optional fields
m.plot_fns = {};
m.plot_fns{1}.fn = @do_plot_all_stim_channels;
m.plot_fns{1}.pretty_name = 'Stim Channels (All)';
m.plot_fns{2}.fn = @do_plot_single_stim_channel;
m.plot_fns{2}.pretty_name = 'Stim Channel (Single)';
% m.plot_fns{3}.fn = @(xx, stck) do_plot_channels_as_heatmap(xx, stck, m.output_stim);
% m.plot_fns{3}.pretty_name = 'Stim Channels (Heatmap)';
m.plot_fns{3}.fn = @do_plot_respavg;
m.plot_fns{3}.pretty_name = 'Response Average';
m.plot_fns{4}.fn = @do_plot_response_raster;
m.plot_fns{4}.pretty_name = 'Response Raster';
m.plot_fns{5}.fn = @do_plot_stim_log_spectrogram;
m.plot_fns{5}.pretty_name = 'Stimulus Log Spectrogram';
% m.plot_fns{7}.fn = @do_plot_spectro_and_raster;
% m.plot_fns{7}.pretty_name = 'Spectrogram + Raster';
m.plot_fns{6}.fn = @do_plot_channels_as_heatmap; 
m.plot_fns{6}.pretty_name = 'Output Channels (Heatmap)';
m.plot_gui_create_fn = @create_gui;

% ------------------------------------------------------------------------
% Define the 'methods' of this module, as if it were a class

function x = do_load_from_baphy(mdl, x)
    
    if ~isfield(mdl,'response_format'),
        mdl.response_format='spike';
    end
    if ~isfield(mdl,'data_format'),
        mdl.data_format='baphy';
    end
    if mdl.include_prestim && length(mdl.include_prestim)==1,
        mdl.include_prestim=1;
    end
    if ~isfield(mdl,'stream'),
        mdl.stream=0;
    end
    if ~isfield(mdl,'zpad'),
        mdl.zpad=0;
    end
    if ~isfield(mdl,'pupil'),
        mdl.pupil=0;
    end
    mpupil=0;
    
    % Merge the training and test set names, which may overlap
    files_to_load = unique({x.training_set{:}, x.test_set{:}});
    
    % remove any problematic characters for field names
    for jj=1:length(x.training_set),
        x.training_set{jj}=file_string_cleanup(x.training_set{jj});
    end
    for jj=1:length(x.test_set),
        x.test_set{jj}=file_string_cleanup(x.test_set{jj});
    end
    
    % get celldb-specific data
    if strcmp(mdl.data_format,'baphy'),
        % Returns a list of raw files with this cell ID
        [cfd, cellids, cellfileids] = dbgetscellfile('cellid', [x.cellid,'^']);
        x.cfd = cfd;
        
        % If there is not exactly one cell file returned, throw an error.
        if ~isequal(length(cellids), 1)
            error('BAPHY gave %d cellids yet I want only 1.', length(cellids));
        end
    elseif strcmp(mdl.data_format,'bizley')
        x.cfd=struct();
        ff=files_to_load;
        for ii=1:length(ff),
            ff{ii}=strrep(ff{ii},'_est','');
            ff{ii}=strrep(ff{ii},'_val','');
        end
        ff=unique(ff);
        for ii=1:length(ff),
            cfd(ii).stimpath=mdl.data_root;
            ri=findstr('_SU',ff{ii})-1;
            cfd(ii).stimfile=['bData-',ff{ii}(1:ri(1)),'.mat'];
            cfd(ii).path=mdl.data_root;
            cfd(ii).respfile=ff{ii};
        end
    end
    
    % Create the 'dat' cell array and its entries, if it doesn't exist
    len = length(files_to_load);
    %x.dat = cell(1, len);
    if ~isfield(x, 'dat')
        x.dat=struct();
    end
    for f_idx = 1:len;
        f = files_to_load{f_idx};
        
        % SVD 2013-03-08 - check for special est/val subsets
        if strcmpi(f((end-3):end),'_val'),
            datasubset=2;
            fname=f(1:(end-4));
        elseif strcmpi(f((end-3):end),'_est'),
            datasubset=1;
            fname=f(1:(end-4));
        elseif strcmpi(f((end-3):end),'_all'),
            datasubset=0;
            fname=f(1:(end-4));
        else
            datasubset=0;
            fname=f;
        end
        
        % figure out if this file belongs to the estimation set
        % (dealt with differently in some special RDT situations)
        if ~isempty(find(strcmp(f,x.training_set))),
            est_file=1;
        else
            est_file=0;
        end
        % remove special characters that aren't allowing in Matlab
        % field names
        f=file_string_cleanup(f);
        
        % Find the index number of cfd corresponding to the file
        idx = 0;
        for j = 1:length(cfd)
            if strcmp(cfd(j).stimfile, fname)
                idx = j;
            end
            if strcmp(cfd(j).respfile, fname)
                idx = j;
            end
        end
        parms=dbReadData(cfd(idx).rawid);
        if idx == 0
            error('Not found in cfd: %s\n', fname); 
        end
        
        if strcmpi(mdl.data_format,'baphy'),
            % special case kludge to deal with messed up TSP files
            loadbytrial=0;
            a=regexp(fname,'por(\d*).*TSP.*$','tokens');
            if ~isempty(a),
                a=str2num(a{1}{1});
                if a>=31 && a<=65,
                    loadbytrial=1;
                end
            end
            
            if ~isempty(findstr('RDT',fname)) || ~isempty(findstr('SNS',fname)),
                loadbytrial=1;
                RDT=1;

                % Randomly shuffle samples between the two streams
                SHUFFLE_STREAMS = mdl.stream < 0;
                % A perfect example of why 0-based indexing is nice.
                stream=mod(abs(mdl.stream)-1,3)+1;
                if est_file,
                    % force both streams for fit data -- BNB: WHY?  
                    % This is something of a
                    % hack that controls normalization in the model,
                    % need to make sure that it matches the way the
                    % data were processed during fitting
                    stream=3;
                end
                % Hack to remove the target phase
                if abs(mdl.stream)>3,
                   REMOVE_TAR_PHASE=0;
                else
                   REMOVE_TAR_PHASE=1;
                end
            else
                RDT=0;
            end
            
            % Load the raw_stim part of the data structure
            stimfile = [cfd(idx).stimpath cfd(idx).stimfile];
            fprintf('Loading stimulus: %s\n', stimfile);
            if loadbytrial,
                options=[];
                options.filtfmt=mdl.stimulus_format;
                options.fsout=mdl.raw_stim_fs;
                options.chancount=mdl.stimulus_channel_count;
                [stim,stimparam] = loadstimbytrial(stimfile,options);
                
                if RDT,
                    % Shape of stim is (frequency, time, trial, stream) where
                    % stream 1 is target stream only, stream 2 is background
                    % stream only and stream 3 is stream 1 + 2.
                    disp('special stimulus processing for RDT');
                    if stream==2,
                        emptyidx=find(~nansum(nansum(stim(:,:,:,stream),1),2));
                        %stim(:,:,emptyidx,stream)=stim(:,:,emptyidx,1);
                        stim(:,:,emptyidx,stream)=0;
                    end
                    if SHUFFLE_STREAMS
                        % Ensure that shuffling is reproducible for each experiment.
                        randgen = RandStream('mlfg6331_64', 'Seed', 42);
                        for trial = 1:size(stim, 3)
                            % Check to see if all values in stream 2 for the
                            % given trial are (square first to ensure that
                            % negative values don't cancel out positive
                            % values).
                            if nansum(nansum(stim(:,:,trial,2).^2)) ~= 0
                                for time = 1:size(stim, 2)
                                    if rand(randgen) > 0.5
                                        stim(:,time,trial,[1,2]) = stim(:,time,trial,[2,1]);
                                    end
                                end
                            end
                        end
                    end

                    if stream==3,
                        stim=stim(:,:,:,1)+stim(:,:,:,2);
                    else
                        stim=stim(:,:,:,stream);
                    end
                end
            elseif ~isempty(x.filecodes) && strcmpi(x.filecodes{f_idx},'CN1'),
               % SVD 2016-10-07
               % need to load reference and target ("Reference1" and
               % "Reference2" tags)
               [stim1,stimparam] = loadstimfrombaphy(...
                  stimfile, [], [], ...
                  mdl.stimulus_format, mdl.raw_stim_fs, ...
                  mdl.stimulus_channel_count, 0, mdl.include_prestim);
               [stim2] = loadstimfrombaphy(...
                  stimfile, [], [], ...
                  mdl.stimulus_format, mdl.raw_stim_fs, ...
                  mdl.stimulus_channel_count, 0, mdl.include_prestim,...
                  'TargetHandle');
               stim=cat(3,stim1,stim2);
            else
                [stim,stimparam] = loadstimfrombaphy(...
                    stimfile, [], [], ...
                    mdl.stimulus_format, mdl.raw_stim_fs, ...
                    mdl.stimulus_channel_count, 0, mdl.include_prestim);
            end
            if strcmp(mdl.stimulus_format, 'wav')
                stim = squeeze(stim);
            end
            
            % TODO: Right now the envelope returned by baphy is not necessarily
            % positive semidefinite, and when run through a square root
            % compressor results in complex numbers being developed. This
            % should be fixed on the baphy side, but for now let's just
            % add a workaround here.
            if strcmp(mdl.stimulus_format, 'wav')
                stim = squeeze(stim);
            elseif strcmp(mdl.stimulus_format, 'envelope') ||...
                    strcmpi(mdl.stimulus_format, 'gamma') ||...
                    strcmpi(mdl.stimulus_format, 'gamma264') ||...
                    strcmpi(mdl.stimulus_format, 'ozgf') ||...
                    strcmpi(mdl.stimulus_format, 'parm') ||...
                    strcmpi(mdl.stimulus_format, 'specgram') ||...
                    strcmpi(mdl.stimulus_format, 'specgramv'),
                stim = permute(stim, [2 3 1]);
                stim = abs(stim);
            end
            
            % If you ever need arbitrary dimensions, I started planning
            % out how that could be accomplished...but it doesn't help plotting
            % See documentation in narf/doc/
            % stim = define_dims(stim, {'time', 'stim', 'chan'});
            
            % calculate min and max of all files in the training
            % set... used by depression_filter_bank
            stimminmax=[squeeze(nanmin(nanmin(stim,[],1),[],2))';
                        squeeze(nanmax(nanmax(stim,[],1),[],2))'];
            if f_idx==1,
                x.stimminmax=stimminmax;
            elseif f_idx<=length(x.training_set),
                x.stimminmax=[nanmin(x.stimminmax(1,:),stimminmax(1,:));
                              nanmax(x.stimminmax(2,:),stimminmax(2,:))];
            end
            
            % Load the raw_resp part of the data structure
            options = [];
            options.includeprestim = mdl.include_prestim;
            options.unit = cfd(idx).unit;
            options.channel  = cfd(idx).channum;
            options.rasterfs = mdl.raw_resp_fs;
            if ~isempty(cfd(idx).goodtrials),
                options.trialrange=eval(['[' cfd(idx).goodtrials ']']);
                %keyboard
            end
            respfile = [cfd(idx).path, cfd(idx).respfile];
            fprintf('Loading response: %s\n', respfile);
            
            if loadbytrial && RDT,
                % bit redundant? If RDT then loadbytrial must be 1?
                
                [resp,rparms]=load_RDT_by_trial(stimfile,respfile,options);
                resp=permute(resp,[1 3 2])./options.rasterfs;
                % resp already only has correct trials.  Need to select
                % that subset of stim trials
                CorrectTrials=rparms.CorrectTrials;
                stim=stim(:,CorrectTrials,:);
                TargetStartBin=rparms.TargetStartBin(rparms.CorrectTrials);
                ThisTarget=rparms.ThisTarget(rparms.CorrectTrials);
                BigSequenceMatrix=rparms.BigSequenceMatrix(:,:,rparms.CorrectTrials);
                TarRepCount=rparms.SamplesPerTrial-max(TargetStartBin)+1;
                TarDur=rparms.PreStimSilence+TarRepCount.*rparms.SampleDur+...
                       rparms.PostStimSilence;
                TarBins=round(rparms.rasterfs.*TarDur);
                mr=1;
                ucat=zeros(size(TargetStartBin));
                for tt=1:length(TargetStartBin),
                    if TargetStartBin(tt)>0 && REMOVE_TAR_PHASE,
                        refend=TargetStartBin(tt).*rparms.SampleDur+...
                               rparms.PreStimSilence;
                        refend=round(refend.*rparms.rasterfs)-1;
                        resp((refend+1):end,tt)=nan;
                        mr=max(refend,mr);
                    else
                        mr=max(mr,max(find(~isnan(resp(:,tt)))));
                    end
                    match=0;
                    ttr=1;
                    
                    while match==0 && ttr<tt,
                        if ~sum(sum(abs(BigSequenceMatrix(:,:,tt)- ...
                                        BigSequenceMatrix(:,:,ttr)))),
                            match=ttr;
                        end
                        ttr=ttr+1;
                    end
                    if match,
                        ucat(tt)=ucat(match);
                    else
                        ucat(tt)=max(ucat)+1;
                    end
                end
                if mr>size(stim,1),
                    fprintf('mr=%d trimmed to %d\n',mr,size(stim,1));
                    mr=size(stim,1);
                end
                oldresp=resp(1:mr,:,:);
                stim=stim(1:mr,:,:);
                umap=zeros(size(unique(ucat)));
                maxreps=max(hist(ucat,unique(ucat)));
                resp=zeros(size(oldresp,1),...
                           maxreps,length(umap)).*nan;
                for tt=unique(ucat)',
                    ttr=find(ucat==tt);
                    resp(:,1:length(ttr),tt)=oldresp(:,:,ttr);
                    umap(tt)=ttr(1);
                end
                x.dat.(f).trial2sequence=ucat;
                stim=stim(:,umap,:);
                
                if ~mdl.include_prestim,
                    startbin=round(rparms.PreStimSilence.* mdl.raw_resp_fs);
                    resp=resp(startbin:end,:,:);
                    stim=stim(startbin:end,:,:);
                end
                
            elseif loadbytrial,
                options.tag_masks={'SPECIAL-TRIAL'};
                [resp, tags,trialset] = loadspikeraster(respfile, options);
                resp=permute(resp,[1 3 2]);
                
                % only keep correct trials
                stim=stim(:,trialset,:);
                
                % mask out responses that aren't in valid stimulus
                % range (ie, responses during & after targets)
                for trialidx=1:size(stim,2),
                    gg=find(~isnan(stim(:,trialidx,1)));
                    resp((gg(end)+1):end,1,trialidx)=nan;
                end
                
                % find identical trials and call them repeats
                CC=zeros(size(stim,2));
                for dd=1:size(stim,3),
                    ts=stim(:,:,dd);
                    ts(isnan(ts))=0;
                    CC=CC+corrcoef(ts)./size(stim,3);
                end
                CC=CC-triu(CC);
                [t2,t1]=find(CC==1);
                keepidx=1:size(stim,2);
                for ii=unique(t1)',
                    mm=t2(find(t1==ii & t2>0));
                    tr=squeeze(resp(:,1,mm));
                    repcount=size(tr,2)+1;
                    if repcount>size(resp,2),
                        resp=cat(2,resp,nan*ones(size(resp,1),...
                                                 repcount-size(resp,2),size(resp,3)));
                    end
                    resp(:,2:repcount,ii)=tr;
                    keepidx=setdiff(keepidx,mm);
                    t2(ismember(t2,mm))=0;
                end
                resp=resp(:,:,keepidx);
                stim=stim(:,keepidx,:);
                
            elseif strcmpi(mdl.response_format,'spike') && ...
                  ~isempty(x.filecodes) && strcmpi(x.filecodes{f_idx},'CN1'),
               % SVD 2016-10-07 -
                options.tag_masks={'Reference1'};
                [resp1, tags1, trialset1] = loadspikeraster(respfile, options);
                options.tag_masks={'Reference2'};
                [resp2, tags2, trialset2] = loadspikeraster(respfile, options);
                resp=cat(3,resp1,resp2);
                tags=cat(2,tags1,tags2);
                trialset=cat(2,trialset1,trialset2);
                runidx=[ones(length(tags1),1);2*ones(length(tags2),1)];
            elseif strcmpi(mdl.response_format,'spike') && ...
                  strcmp(parms.ReferenceClass,'SpNoiseStream') && length(parms.Ref_LowFreq)>1,
               % LAS 207-5-01 -
                options.tag_masks={'Reference'};
                [resp, tags, trialset] = loadspikeraster(respfile, options);
                rmi=cellfun(@isempty,tags);
                 resp(:,:,rmi)=[];
                 tags(rmi)=[];
                 stim(:,rmi,:)=[];
                 Sp=NaN(length(tags),2);
                 Lev=NaN(length(tags),2);
                 for i=1:length(tags);
                    t=strsep(tags{i});
                    tagr{i}=t{3};
                    t=strsep(tagr{i},'+');
                    tagr_{i}=t;
                    if length(tagr_{i})==3
                        if strcmp(tagr_{i}{1},'BNB')
                            t=strsep(tagr_{i}{2},':');
                            Sp(i,1)=str2double(t{1}(3:end));
                            Lev(i,1)=t{2};
                        else
                            t=strsep(tagr_{i}{3},':');
                            Sp(i,2)=str2double(t{1}(3:end));
                            Lev(i,2)=t{2};
                        end
                    else
                         t=strsep(tagr_{i}{2},':');
                            Sp(i,1)=str2double(t{1}(3:end));
                            Lev(i,1)=t{2};
                        t=strsep(tagr_{i}{4},':');
                            Sp(i,2)=str2double(t{1}(3:end));
                            Lev(i,2)=t{2};
                    end
                 end
                runidx=3*ones(length(tags),1);
                runidx(isnan(Sp(:,1)) | isnan(Sp(:,2)))=1;
                runidx(Sp(:,1) == Sp(:,2))=2;
                % 1 - single stream
                % 2 - coherent
                % 3 - incoherent
            elseif strcmpi(mdl.response_format,'spike'),
               if isfield(parms,'TrialObjectClass') && ...
                     strcmpi(parms.TrialObjectClass,'InterleaveSounds'),
                  options.tag_masks={'Reference1'};
               else
                  options.tag_masks={'Reference'};
               end
               [resp, tags, trialset] = loadspikeraster(respfile, options);
               if ~sum(~isnan(resp(:))),
                  options.tag_masks={'Ref'};
                  [resp, tags, trialset] = loadspikeraster(respfile, options);
               end
            elseif strcmpi(mdl.response_format,'lfp'),
                options.tag_masks={'Reference'};
                options.lfp=1;
                if mdl.response_lfp_envelope,
                    [resp, tags] = ...
                        loadgammaraster(respfile, options.channel,...
                                        options.rasterfs,options.includeprestim,...
                                        options.tag_masks,-1,...
                                        mdl.response_lfp_lo,mdl.response_lfp_hi,...
                                        mdl.response_lfp_envelope);
                    resp=resp*10000;
                else
                    [resp, tags] = loadevpraster(respfile, options);
                    resp=resp./256;
                end
                
            elseif strcmpi(mdl.response_format,'lfp+pupil'),
                options.tag_masks={'Reference'};
                options.lfp=1;
                [resp, tags] = loadevpraster(respfile, options);
                resp=resp./256;
                
                options.lfp=4;
                pupil = loadevpraster(respfile, options);
            end
            
        elseif strcmpi(mdl.data_format,'bizley'),
            stimfile = [cfd(idx).stimpath, cfd(idx).stimfile];
            respfile = [cfd(idx).path, cfd(idx).respfile];
            
            [resp,stim]=biz_read_data(respfile,stimfile);
            resp=permute(resp,[1 3 2]);
            stim = permute(stim, [2 3 1]);
            stim = abs(stim);
            
        end
        
        % SVD pad response with nan's in case reference responses
        % were truncated because of target overlap during behavior.
        % this is a kludge that may be fixed some day in loadspikeraster
        % 2013-05-31: Ivar wraps an if statement around this because this
        % destroyed our ability to sample stim and resp at different rates
        if (mdl.raw_resp_fs == mdl.raw_stim_fs)
            if size(resp,1)<size(stim,1),
                resp((end+1):size(stim,1),:,:)=nan;
            elseif size(resp,1)>size(stim,1),
                resp=resp(1:size(stim,1),:,:);
            end
        end
        
        if mdl.zpad,
           [T,R,S]=size(resp);
           resp=cat(1,nan.*zeros(mdl.zpad,R,S),resp);
           [T,S,C]=size(stim);
           stim=cat(1,zeros(mdl.zpad,S,C),stim);
        end
        
        % SVD 2013-10-14 for special case of vocalizations (subset 2) the
        % number of stimuli changed for quirky reasons. if stim has
        % more stimidx values than resp, truncate stim
        if size(resp,3)<size(stim,2),
            disp('special case:  truncating stim to match resp stimcount');
            stim=stim(:,1:size(resp,3),:);
        end
        
        % Special pupil coding stuff
        if mdl.pupil==1,
           disp('pupil processing...');
           poptions=options;
           poptions.pupil=1;
           poptions.verbose=0;
           poptions.pupil_median=1;
           if ~isempty(x.filecodes) && strcmpi(x.filecodes{f_idx},'CN1'),
                poptions.tag_masks={'Reference1'};
                [pupil1, ptags1, ptrialset1] = loadevpraster(stimfile, poptions);
                poptions.tag_masks={'Reference2'};
                [pupil2, ptags2, ptrialset2] = loadevpraster(stimfile, poptions);
                pupil=cat(3,pupil1,pupil2);
                ptags=cat(2,ptags1,ptags2);
                ptrialset=cat(2,ptrialset1,ptrialset2);
           else
              [pupil,ptags,ptrialset] = loadevpraster(stimfile, poptions);
           end
           
           if size(pupil,1)<size(resp,1),
              pupil((end+1):size(resp,1),:,:)=nan;
           elseif size(pupil,1)>size(resp,1),
              pupil=pupil(1:size(resp,1),:,:);
           end
           
           % determine boundaries (currently hard-coded)
           switch basename(stimfile),
             case 'Mouse169_2013_10_23_TOR_8_cell2',
               bdset=[25 34 39 43 47 70];
             case 'Mouse167_2013_10_23_TOR_9_cell1',
               bdset=[28 31 34 38 45 70];
             case 'Mouse167_2013_10_24_TOR_5',
               bdset=[0 25 28 32 36 60];
             case 'Mouse160_2013_11_25_TOR_1',
               bdset=[0 16.7 25 30.5 39 56];
             otherwise
               if ~mpupil,
                   mpupil=nanmedian(pupil(:));
               end
               bdset=[0 mpupil 150];
               %bdset=[bdset;bdset+[diff(bdset)./2 0]];
               %bdset=bdset(1:(end-1));
           end
           x.filecodes={};
           for bdidx=1:(length(bdset)-1)
               x.filecodes{bdidx}=sprintf('P%d',bdidx);
           end
           
           % find mean pupil diameter at begining of each trial
           trialcount=max(trialset(:));
           
           pupil0=pupil;
           pupil=resp.*nan;
           for trialidx=1:trialcount,
              pb=find(ptrialset==trialidx);
              rb=find(trialset==trialidx);  % (rep,stimidx)
              pupil(:,rb)=pupil0(:,pb);
           end
        elseif mdl.pupil==2,
           disp('Loading pupil trace...');
           rand(1000,1);
           poptions=options;
           poptions.pupil=1;
           poptions.verbose=0;
           poptions.pupil_offset=1;
           poptions.pupil_smooth=1;
           poptions.pupil_deblink=1;
           [pupil,ptags,ptrialset] = loadevpraster(stimfile, poptions);
           if size(pupil,1)<size(resp,1),
              pupil((end+1):size(resp,1),:,:)=nan;
           elseif size(pupil,1)>size(resp,1),
              pupil=pupil(1:size(resp,1),:,:);
           end
           pupil=pupil./100;
        end
        
        % SVD 2013-03-08 - if specified, pull out either estimation (fit) or
        % validation (test) subset of the data
        if datasubset,
            repcount=squeeze(sum(~isnan(resp(1+mdl.zpad,:,:)),2));
            global META
            if exist('tags1','var')
                Ltags1=length(tags1);
            else
                Ltags1=[];
            end
            if isempty(x.filecodes)
                fcs=[];
            else
                fcs=x.filecodes{f_idx};
            end
            keepidx=get_fit_test_subset_indicies(repcount,datasubset,cfd(idx),META.batch,fcs,length(tags),Ltags1);
          
            stim=stim(:,keepidx,:);
            resp=resp(:,:,keepidx);
            if exist('pupil','var'),
                pupil=pupil(:,:,keepidx);
            end
        end
        
        training_count=length(x.training_set);
        test_count=length(x.test_set);
        cat_file_set=cat(2,x.training_set,x.test_set);
        if ~isfield(x,'filecodes') || isempty(x.filecodes),
           x.filecodes=repmat({''},[1 training_count+test_count]);
        elseif length(x.filecodes)<training_count,
           x.filecodes=repmat(x.file_codes(1),[1 training_count+test_count]);
        elseif length(x.filecodes)<training_count+test_count,
           x.filecodes=x.filecodes([1:training_count 1:test_count]);
        end
        x.unique_codes=unique(x.filecodes);
        
        % Special pupil coding stuff
        if mdl.pupil==1,
           
           % find mean pupil diameter at begining of each trial
           mp=squeeze(mean(pupil(1:50,:,:)));
           
           % create two copies of stim and resp, preserving only
           % components of resp that belong in respective pupil bins
           bincount=length(bdset)-1;
           stim=repmat(stim,[1 bincount 1]);
           resp0=resp;
           resp=nan(size(resp0,1),size(resp0,2),size(resp0,3)*bincount);
           for bb=1:length(bdset)-1,
              ff=find(mp>bdset(bb) & mp<=bdset(bb+1));
              boffset=(bb-1)*size(resp0,3)*size(resp0,2);
              for ii=ff(:)',
                 resp(:,ff+boffset)=resp0(:,ff);
              end
           end
           trial_code=ones(size(resp0,3),1)*(1:bincount);
           trial_code=trial_code(:);
           %if f_idx==1,
           %    mr=nanmin(resp(:));
           %end
           %resp=resp-mr;
        elseif ~isempty(x.filecodes) && strcmpi(x.filecodes{f_idx},'CN1'),
           % SVD 2016-10-07
           % set up trial_code for interleaved file!
           x.unique_codes={'C1','N1'};
           parms=dbReadData(cfd(idx).rawid);
           %LAS 2016-12-23 construct trial code from number of stimuli for 
           % clean and noisy explicitly, in case their lengths are unequal
           trial_code=runidx(keepidx); 
           if parms.Ref_SNR>=100,
              %do nothing
           else
              %flip trial code because "reference" was noisy, "target" was noisy
              trial_code=3-trial_code;
           end
        elseif strcmpi(mdl.response_format,'spike') && ...
                          strcmp(parms.ReferenceClass,'SpNoiseStream') && length(parms.Ref_LowFreq)>1,
                       % LAS 207-5-01 -
           x.unique_codes={'S1','C1','I1'};
          trial_code=runidx(keepidx); 
        else
            % set up trial_code
            this_code=x.filecodes{strcmp(f,cat_file_set)};
            codeidx=find(strcmp(this_code,x.unique_codes));
            
            trial_code=ones(size(stim,2),1).*codeidx;
        end
        if mdl.pupil==2,
           stim=repmat(stim,[1 size(resp,2) 1]);
           trial_code=repmat(trial_code,[size(resp,2) 1]);
           resp=resp(:,:);
           pupil=pupil(:,:);
           kk=find(~isnan(resp(1,:)));
           stim=stim(:,kk,:);
           resp=resp(:,kk);
           pupil=pupil(:,kk);
           trial_code=trial_code(kk);
           disp('Smoothing single-trial responses');
           sm=[0 0 0 1 0.875 0.75 0.625 0.5 0.385 0.25 0.125]';
           sm=sm./sum(sm);
           resp=rconv2(resp,sm);
           %resp=gsmooth(sqrt(resp),[1 0.0001]);
        else
           resp=permute(resp,[1 3 2]);
        end
        
        nanstim=find(isnan(stim(:)));
        if ~isempty(nanstim),
            disp('WARNING: nan-valued stims are being set to zero');
            stim(nanstim)=0;
        end
        
        x.dat.(f).(mdl.output_stim) = stim;
        x.dat.(f).(mdl.output_resp) = resp;
        x.dat.(f).(mdl.output_respavg) = ...
            squeeze(nanmean(x.dat.(f).(mdl.output_resp),3));
        x.dat.(f).trial_code=trial_code;
        if mdl.pupil,
           x.dat.(f).pupil=pupil;
        end
        % Scale respavg so it is a spike rate in Hz
        x.dat.(f).(mdl.output_respavg) = ...
            (mdl.raw_resp_fs) .* x.dat.(f).(mdl.output_respavg);
        
        % Create time signals for later convenience
        [s1 s2 s3] = size(x.dat.(f).(mdl.output_stim));
        [r1 r2 r3] = size(x.dat.(f).(mdl.output_resp));
        [a1 a2]    = size(x.dat.(f).(mdl.output_respavg));
        
        % TODO: Check stim, resp, raw_time signal sizes match.       
        x.dat.(f).(mdl.output_stim_time) = (1/mdl.raw_stim_fs).*[1:s1]';
        x.dat.(f).(mdl.output_resp_time) = (1/mdl.raw_resp_fs).*[1:r1]';
        x.dat.(f).stim_freq=stimparam.ff;
    end
    disp('load_stim_resps_from_baphy: Finished loading files.');
    
end

% ------------------------------------------------------------------------
% Helper functions

    function ylab = what_is_ylabel(mdls)
        if strcmp(mdls{1}.stimulus_format, 'envelope')
            ylab = 'Envelope Magnitude [?]';
        elseif strcmp(mdls{1}.stimulus_format, 'wav')
            ylab = 'Volume [?]';
        else
            ylab = 'Unknown';
        end
    end

% ------------------------------------------------------------------------
% Plot functions

function do_plot_all_stim_channels(sel, stack, xxx)       
    %[mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));
    mdls = stack{end};
    xins = {xxx(1:end-1)};
    xouts = xxx(end);      
    sel=sel(1);
    sel.chan_idx = []; % when chan_idx is empty, do_plot plots all channels
    do_plot(xouts, mdls{1}.output_stim_time, mdls{1}.output_stim, ...
            sel, 'Time [s]', what_is_ylabel(mdls));
end

function do_plot_single_stim_channel(sel, stack, xxx)   
    %[mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end)); 
    mdls = stack{end};
    xins = {xxx(1:end-1)};
    xouts = xxx(end);
    do_plot(xouts, mdls{1}.output_stim_time, mdls{1}.output_stim, ...
            sel, 'Time [s]', what_is_ylabel(mdls)); 
end

function do_plot_respavg(sel, stack, xxx)
    %[mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end)); 
    mdls = stack{end};
    xins = {xxx(1:end-1)};
    xouts = xxx(end);
    [sel.chan_idx] = deal(1);    
    do_plot(xouts, mdls{1}.output_resp_time, mdls{1}.output_respavg, ...
            sel, 'Time [s]', 'Spike Rate Average [Hz]'); 
end

function do_plot_response_raster(sels, stack, xxx)    
    %[mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end)); 
    mdls = stack{end};
    xins = {xxx(1:end-1)};
    xouts = xxx(end);
    mdl = mdls{1};
    for j=1:length(sels)
        if(length(sels)==1)
            col=[0 0 0];
        else
            col=pickcolor(j);
        end
        dat = xouts{1}.dat.(sels(j).stimfile);
        [T, S, R] = size(dat.(mdl.output_resp));
        hold on;
        for r = 1:R
            [xs,ys] = find(dat.(mdl.output_resp)(:, sels(j).stim_idx, r) > 0);
            plot(dat.(mdl.output_resp_time)(xs), r*ys+(j-1)*.2, '.','Color',col);
        end
    end
    do_xlabel('Time [s]');
    do_ylabel('Trial #');
    axis([0 dat.(mdl.output_resp_time)(end) 0 R+1]);
    hold off;
end

% function do_plot_respavg_as_spikes(stack, xxx)
%     mdl = stack{end};    
%     x = xxx{end};
%     
%     % Read the GUI to find out the selected stim files
%     sf = popup2str(mdl.plot_gui.selected_stimfile_popup);
%     stim = popup2num(mdl.plot_gui.selected_stim_idx_popup);
%     dat = x.dat.(sf);
%     
%     [xs,ys] = find(dat.respavg(idx, :) > 0);
%     bar(dat.(mdl.output_resp_time)(ys), dat.respavg(idx,ys), 0.01, 'k-');
%     axis([0 dat.(mdl.output_resp_time)(end) 0 max(dat.respavg(:, stim))]);
% 
% end

function do_plot_stim_log_spectrogram(sel, stack, xxx)
    mdl = stack{end}{1};    
    x = xxx{end};
     
     if ~strcmp(mdl.stimulus_format, 'wav')
         text(0.35, 0.5, 'Cannot visualize non-wav stimulus as spectrogram');
         axis([0, 1, 0 1]);
         return;
     end
               
     % From 500Hz, 12 bins per octave, 4048 sample window w/half overlap
     logfsgram(x.dat.(sel.stimfile).(mdl.output_stim)(:, sel.stim_idx, sel.chan_idx), ...
               4048/2, mdl.raw_stim_fs, [], [], 200, 12); 
     caxis([-20,40]);  % TODO: use a 'smarter' caxis here
     axis tight;
 end
 
% function do_plot_spectro_and_raster(stack, xxx)
%     mdl = stack{end};    
%     x = xxx{end};
%     
%     if strcmp(mdl.stimulus_format, 'envelope')
%         text(0.35, 0.5, 'Cannot visualize envelope as spectrogram');
%         axis([0, 1, 0 1]);
%         return;
%     end
%     
%     % Read the GUI to find out the selected stim files
%     sf = popup2str(mdl.plot_gui.selected_stimfile_popup);
%     stim = popup2num(mdl.plot_gui.selected_stim_idx_popup);
%     dat = x.dat.(sf);
%     
%     hold on;
%     % From 500Hz, 12 bins per octave, 4048 sample window w/half overlap
%     logfsgram(dat.(mdl.output_stim)(:,stim)', 4048, mdl.raw_stim_fs, [], [], 500, 12); 
%     caxis([-20,40]);  % TODO: use a 'smarter' caxis here
%     h = get(gca, 'YLim');
%     d = h(2) - h(1);
%     axis tight;    
%     [T, S, R] = size(dat.(mdl.output_resp));
%     hold on;
%     for r = 1:R
%         [xs,ys] = find(dat.(mdl.output_resp)(:, stim, r) > 0);
%         plot(dat.(mdl.output_resp_time)(xs), ...
%              h(1) + (r/R)*d*(d/(d+d/R))*dat.(mdl.output_resp)(xs,stim,r), 'k.');
%     end    
%     hold off;
% end


function hs = create_gui(parent_handle, stack, xxx)
    pos = get(parent_handle, 'Position');
    w = pos(3) - 5;
    h = pos(4) - 5;
    hs = [];
    
    mdl = stack{end}{1};
    mod_idx = length(stack);
    x = xxx{end};
    
    % Create a popup which selects
    uicontrol('Parent', parent_handle, 'Style', 'text', 'Enable', 'on', ...
        'HorizontalAlignment', 'left',  'String', 'Stimfile:', ...
        'Units', 'pixels', 'Position', [5 (h-25) 100 25]);
    hs.add_stim_button = uicontrol('Parent', parent_handle, ...
        'Style', 'togglebutton', 'Enable', 'on', ...
        'String', '+', ...
        'Units', 'pixels', 'Position', [w-25 (h-25) 25 25], ...
        'Callback', @(h, evts, hdls) add_stim_callback());
    hsind=1;
    create_stim_selector(hsind)
    stim_sel=[];
    function create_stim_selector(ind)
       hs(ind).selected_stimfile_popup = uicontrol('Parent', parent_handle, ...
        'Style', 'popupmenu', 'Enable', 'on', 'String', 'NONE', ...
        'Units', 'pixels', 'Position', [5 (h-40) w-5 25], ...
        'Callback', @(a,b,c) selected_stimfile_popup_callback(ind));
    
    % Create a stimfile selector
    hs(ind).stimidx_txt=uicontrol('Parent', parent_handle, 'Style', 'text', 'Enable', 'on', ...
        'HorizontalAlignment', 'left', 'String', 'Idx:', ...
        'Units', 'pixels', 'Position', [5 (h-70) 50 25]);
    hs(ind).selected_stim_idx_popup = uicontrol('Parent', parent_handle, ...
        'Style', 'popupmenu', 'Enable', 'on', ...
        'String', 'NONE', ...
        'Units', 'pixels', 'Position', [45 (h-70) w-50 25], ...
        'Callback', @(a,b,c) selected_stim_idx_popup_callback());

    % Create a channel selector
    hs(ind).chantxt=uicontrol('Parent', parent_handle, 'Style', 'text', 'Enable', 'on', ...
        'HorizontalAlignment', 'left',  'String', 'Chan:', ...
        'Units', 'pixels', 'Position', [5 (h-95) 50 25]);
    hs(ind).selected_stim_chan_popup = uicontrol('Parent', parent_handle, ...
        'Style', 'popupmenu', 'Enable', 'on', 'String', 'NONE', ...
        'Units', 'pixels', 'Position', [45 (h-95) w-50 25], ...
        'Callback', @(a,b,c) selected_stim_chan_popup_callback());
    
    hs(ind).textbox = uicontrol('Parent', parent_handle, 'Style', 'text', 'Enable', 'on', ...
        'HorizontalAlignment', 'left',  'String', '', ...
        'Units', 'pixels', 'Position', [5 (h-125) w-5 h-100]);
    end
    
    % Two functions to populate the two popup menus
    function update_selected_stimfile_popup(ind)
        fns = fieldnames(x.dat);
        set(hs(ind).selected_stimfile_popup, 'String', char(fns));
        
    end
    
    function update_set_textbox(ind)
        % Print "TEST SET" or "TRAINING SET" in plot GUI as appropriate
        sf = popup2str(hs(ind).selected_stimfile_popup);
        is_test = any(strcmp(sf, x.test_set));
        is_training = any(strcmp(sf, x.training_set));
        
        if is_training && is_test
            str = 'Est&Val Set';
        elseif is_training
            str = 'Estimation Set';
            if isfield(x, 'training_codes') && ~isempty(x.training_codes)
                fc = x.training_codes(min(find(strcmp(sf, x.training_set))));
                str = [str ' [' sprintf('%s', fc) ']'];
            end
         elseif is_test
            str = 'Validation Set';
            if isfield(x, 'test_codes') && ~isempty(x.test_codes)
                fc = x.test_codes(min(find(strcmp(sf, x.test_set))));
                str = [str ' [' sprintf('%s', fc) ']'];
            end
        else
            str = 'Not in a set!?'
        end
        
        % Also append the filecode info
        if isfield(x, 'filecodes') && ~isempty(x.filecodes) && ~isempty(x.test_set)
            fc = x.filecodes(or(strcmp(sf, x.training_set), ...
                                strcmp(sf, x.test_set)));
            str = [str ' [' sprintf('%s', fc{1}) ']'];
        end
        
        set(hs(ind).textbox, 'String', str); 
    end
    
    function update_selected_stim_idx_popup(ind)
        sf = popup2str(hs(ind).selected_stimfile_popup);
        
        if isfield(x.dat, sf)
            [d1, d2, d3] = size(x.dat.(sf).(mdl.output_stim));
            d = {};
            for i = 1:d2
                d{i} = sprintf('%d',i);
                if(~isempty(x.unique_codes))
                    d{i}=[d{i},' ',x.unique_codes{x.dat.(sf).trial_code(i)}];
                end
            end
            previous_value=get(hs(ind).selected_stim_idx_popup, 'Value');            
            set(hs(ind).selected_stim_idx_popup, 'String', char(d));
            if(d2<previous_value)
                set(hs(ind).selected_stim_idx_popup, 'Value', 1);
            end
        else
            error('Selected stimulus file not found: %s', sf);
        end
    end
    
    function update_selected_stim_chan_popup(ind)
        sf = popup2str(hs(ind).selected_stimfile_popup);
        
        if isfield(x.dat, sf)
            [d1, d2, d3] = size(x.dat.(sf).(mdl.output_stim));
            d = {};
            for i = 1:d3
                d{i} = sprintf('%d',i);
            end
            previous_value=get(hs(ind).selected_stim_chan_popup, 'Value'); 
            set(hs(ind).selected_stim_chan_popup, 'String', char(d));
            if(d3<previous_value)
                set(hs(ind).selected_stim_chan_popup, 'Value', 1);
            end
        else
            error('Selected stimulus file not found: %s', sf);
        end
    end
    
    % Call the two update functions once to build the lists
    update_selected_stimfile_popup(hsind);
    update_selected_stim_idx_popup(hsind);
    update_selected_stim_chan_popup(hsind);
    update_set_textbox(hsind);
    
    % Define three callbacks, one for each popup.
    function selected_stimfile_popup_callback(ind)
        % Update the selected_stim_idx_popup string to reflect new choices
        update_selected_stim_idx_popup(ind);
        update_selected_stim_chan_popup(ind);
        update_set_textbox(ind);
        % Call the next popup callback to trigger a redraw
        selected_stim_idx_popup_callback();
    end
    
    function selected_stim_idx_popup_callback()
        global NARFGUI;
        % Call the plot function again via the plot_popup        
        hgfeval(get(NARFGUI{mod_idx}.plot_popup,'Callback'), mod_idx, []);
        drawnow;
    end
    
    function selected_stim_chan_popup_callback()
        global NARFGUI;
        % Call the plot function again via the plot_popup 
        hgfeval(get(NARFGUI{mod_idx}.plot_popup,'Callback'), mod_idx, []);
        drawnow;
    end
    
    function add_stim_callback()
        global NARFGUI;
        has_plot_gui=find(cellfun(@(x)isfield(x,'plot_gui'),NARFGUI));
        lsrfb_gui=has_plot_gui(cellfun(@(x)isequal(x.plot_gui,hs),NARFGUI(has_plot_gui)));
        ind=length(hs)+1;
        create_stim_selector(ind);
        NARFGUI{lsrfb_gui}.plot_gui=hs;
        if(ind==2)
            stim_sel(1) = uicontrol('Parent', parent_handle, ...
            'Style', 'radiobutton', 'Enable', 'on', ...
            'String', '1', ...
            'Units', 'pixels', 'Position', [60 (h-15) 30 15], ...
            'Callback', @(h, evts, hdls) sel_stim_callback(1));
        end
        stim_sel(ind) = uicontrol('Parent', parent_handle, ...
            'Style', 'radiobutton', 'Enable', 'on', ...
            'String', num2str(ind), ...
            'Units', 'pixels', 'Position', [60+(ind-1)*30 (h-15) 30 15], ...
            'Callback', @(h, evts, hdls) sel_stim_callback(ind),'Value',1);
        sel_stim_callback(ind);
        update_selected_stimfile_popup(ind);
        update_selected_stim_idx_popup(ind);
        update_selected_stim_chan_popup(ind);
        update_set_textbox(ind);
    end
    
    function sel_stim_callback(ind)
        make_invisible=setdiff(1:length(hs),ind);
        fns=fieldnames(hs);
        for i=make_invisible
            for j=1:length(fns)
                set(hs(i).(fns{j}),'Visible','off')
            end
            set(stim_sel(i),'Value',0)
        end
        for j=1:length(fns)
            set(hs(ind).(fns{j}),'Visible','on')
        end
        set(stim_sel(ind),'Value',1)
        
    end
end

% This module can be run if all necessary fields have been defined in the
% topmost part of the stack
function isready = isready_baphy(stack, xxx)
    mdl = stack{end};
    x = xxx{end};
    isready = (length(stack) == 1) && ...
              all(isfield(x, {'cellid', 'training_set', 'test_set'}));
end

end
