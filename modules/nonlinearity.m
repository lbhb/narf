function m = nonlinearity(args)
% Applies a general nonlinear function to the input. 
%
% The nonlinear function (NLFN) must be a function of two arguments:
%
%    NLFN(PHI, X) -> Y
%
% where
%    PHI is NLFN's parameter vector, such as polynomial coefficients
%    X is the matrix that the function works on
%    Y is the output of NLFN
%    
% Good choices for NLFN are 
%    polyval         (See matlab documentation)
%    nl_log          
%    nl_root         
%    nl_sigmoid      
%    nl_exponential  
%    nl_zerothresh   
%    nl_*            (Pretty much anything that matches utils/nl_*)
%
% You may of course define your own functions. 

% LAS: added ability to control range of inputs over which to plot
%   linearity, and plot attributes 7/26/16

% Module fields that must ALWAYS be defined
m = [];
m.mdl = @nonlinearity;
m.name = 'nonlinearity';
m.fn = @do_nonlinearity;
m.pretty_name = 'Generic Nonlinearity';
m.editable_fields = {'input_stim', 'input_resp', 'time', 'output', 'nlfn', 'phi'};
m.isready_pred = @isready_always;

% Module fields that are specific to THIS MODULE
m.input_stim = 'stim';
m.input_resp = 'respavg';
m.time = 'stim_time';
m.output = 'stim';
m.nlfn = @polyval;
m.phi = [1 0];   % Default is to pass the signal through untouched
m.do_scale = false; % this switch toggles the scaling of outputs. Off by default

% Optional fields
m.auto_plot = @do_plot_smooth_scatter_and_nonlinearity;
m.plot_fns = {};

m.plot_fns{1}.fn = @do_plot_all_default_outputs;
m.plot_fns{1}.pretty_name = 'Output Channels (All)';

m.plot_fns{2}.fn = @do_plot_single_default_output;
m.plot_fns{2}.pretty_name = 'Output Channel (Single)';

m.plot_fns{3}.fn = @do_plot_scatter_and_nonlinearity; 
m.plot_fns{3}.pretty_name = 'Stim/Resp Scatter';

m.plot_fns{4}.fn = @do_plot_smooth_scatter_and_nonlinearity; 
m.plot_fns{4}.pretty_name = 'Stim/Resp Smooth Scatter';

m.plot_fns{5}.fn = @do_plot_channels_as_heatmap; 
m.plot_fns{5}.pretty_name = 'Output Channels (Heatmap)';

%m.plot_fns{1}.fn = @(stack, xxx) do_plot_nonlinearity(stack, xxx, stack{end}.input_stim, @(x) stack{end}.nlfn(stack{end}.phi, x), false);
%m.plot_fns{1}.pretty_name = 'Nonlinearity';

%m.plot_fns{4}.fn = @(stack, xxx) do_plot_nonlinearity(stack, xxx, stack{end}.input_stim, @(x) stack{end}.nlfn(stack{end}.phi, x), true);
%m.plot_fns{4}.pretty_name = 'Nonlinearity + Histogram';

% Overwrite the default module fields with arguments 
if nargin > 0
    m = merge_structs(m, args);
end

% Optimize this module for tree traversal  
m.required = {m.input_stim, m.input_resp, m.time};   % Signal dependencies
m.modifies = {m.output};          % These signals are modified

    function [min_val, max_val] = get_firing_rate_boundaries(mdl, x)
        stimulus = x.training_set{1}; % TODO: should consider all inputs, not just the first
        base_stim = x.dat.(stimulus).(mdl.input_resp);
        sound_onset = 40; % TODO: should get the real onset, or at least scale according to the sampling rate
        base_stim = base_stim(5:sound_onset,:); % TODO: do I need 5 here?
        min_val = mean(mean(base_stim));
        max_val = mean(max(x.dat.(stimulus).(mdl.input_resp)));
    end

function x = do_nonlinearity(mdl, x)

    for sf = fieldnames(x.dat)', sf=sf{1};
        [T, S, C] = size(x.dat.(sf).(mdl.input_stim));
        input = reshape(x.dat.(sf).(mdl.input_stim), T*S, C);

        if size(mdl.phi, 1) > 1
            if C ~= size(mdl.phi, 1)
                error('Nonlinearity implicit channel count does not match data.');
            end
            y = zeros(T*S, C);        
            for c = 1:C
                y(:,c) = mdl.nlfn(mdl.phi(c,:), input(:,c));
            end
            y = reshape(y, T,S,C);
        else                
            y = mdl.nlfn(mdl.phi, x.dat.(sf).(mdl.input_stim));      
        end
        
        % TODO: Find a better solution than this hacky way of zeroing nans
        % so that optimization continue in the presence of singularities
        y(isnan(y)) = 0;
        y(isinf(y)) = 10^6;
        
        
        % % % Added by Jean
        % % % This is an experiment to scale the firing rate according to
        % the min/max of the firing rate (in training_set)
        if isfield(mdl, 'do_scale') && mdl.do_scale
            [min_val, max_val] = get_firing_rate_boundaries(mdl, x);
            if min_val ~= max_val
                y = min_val + y * (max_val - min_val);
            end
        end
        
        x.dat.(sf).(mdl.output) = y;

    end
end



function help_plot_nonlinearity(sel, mdls, xins, xouts,xlims,varargin)     
    if(any(strcmp(varargin,'XLim')))
        xlims=varargin{find(strcmp(varargin,'XLim'))+1};
    else
        xlims = xlim();  
    end
    xs = linspace(xlims(1), xlims(2), 100);   
    
    for ii = 1:length(mdls)    
        ys = mdls{ii}.nlfn(mdls{ii}.phi, xs);
        if isfield(mdls{ii}, 'do_scale') && mdls{ii}.do_scale
            [min_val, max_val] = get_firing_rate_boundaries(mdls{ii}, xins{1});
            if min_val ~= max_val
                ys = min_val + ys * (max_val - min_val);
            end
        end

        xouts{ii}.dat.(sel(1).stimfile).input = xs';
        xouts{ii}.dat.(sel(1).stimfile).output = ys';
    end
    
    hold on;
    sel(1).stim_idx=1; sel(1).chan_idx=1;%Luke added otherwise if 2 was selected, error because size(output,2)=1
        do_plot(xouts, 'input', 'output', ...
            sel(1), 'Nonlinearity Input [-]', 'Prediction [Hz]');   
	hold off;
    
end

function do_plot_scatter_and_nonlinearity(sels, stack, xxx,varargin)
    [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));    
    varargin2=varargin;
    if(any(strcmp(varargin,'XLim')))
        varargin2(find(strcmp(varargin,'XLim'))+[0:1])=[];
    end
    
    in_stims=cellfun(@(x)x.input_stim,mdls,'Uni',0); %mod to get plots for split models
    if length(unique(in_stims))>1 && length(sels)>1
        for k=1:length(in_stims)
            if(size(xouts{k}.dat.(sels(k).stimfile).(in_stims{k}),3)<sels(k).chan_idx)
                sels(k).chan_idx=1;
            end
            do_plot_scatter(sels(k), xins, in_stims{k}, mdls{1}.input_resp,nan,'Color',pickcolor(k),varargin2{:});
        end
    else
        do_plot_scatter(sels, xins, mdls{1}.input_stim, mdls{1}.input_resp,nan,varargin2{:});  
    end
    help_plot_nonlinearity(sels, mdls, xins, xouts,varargin{:});     
end

function do_plot_smooth_scatter_and_nonlinearity(sels, stack, xxx,varargin) 
    %Luke added varargin
    % varargin will be passed to plot, can be used to add desired color, marker, markersize, etc
    [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));    
     varargin2=varargin;
    if(any(strcmp(varargin,'XLim')))
        varargin2(find(strcmp(varargin,'XLim'))+[0:1])=[];
    end
    
    in_stims=cellfun(@(x)x.input_stim,mdls,'Uni',0); %mod to get plots for split models
    un_in_stims=unique(in_stims);
    ko=1:length(un_in_stims);
    if(length(sels)>1 && length(un_in_stims)==1)
        un_in_stims=repmat(un_in_stims,length(sels),1);
        ko=ones(length(sels),1);
        for k=1:length(sels)
            sels(k).trial_code=xouts{ko(k)}.dat.(sels(k).stimfile).trial_code(sels(k).stim_idx);
        end
    end
    if length(un_in_stims)>1 && length(sels)>1
        for k=1:length(un_in_stims)
            if(size(xouts{ko(k)}.dat.(sels(k).stimfile).(un_in_stims{k}),3)<sels(k).chan_idx)
                sels(k).chan_idx=1;
            end
            do_plot_scatter(sels(k), xins, un_in_stims{k}, mdls{1}.input_resp,100,'Color',pickcolor(k),varargin2{:});
        end
    else
        do_plot_scatter(sels, xins, mdls{1}.input_stim, mdls{1}.input_resp,100,varargin2{:});  
    end
    help_plot_nonlinearity(sels, mdls, xins, xouts,varargin{:}); 
end

function do_plot_channels_as_heatmap(sel, stack, xxx)
    [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));  
    ii=1;  % assuming just a single data set for now...
    sel=sel(1);
    h = imagesc(xouts{ii}.dat.(sel.stimfile).(mdls{1}.time)(:),...
                1:size(xouts{ii}.dat.(sel.stimfile).(mdls{1}.output),3),...
                squeeze(xouts{ii}.dat.(sel.stimfile).(mdls{1}.output)(:, sel.stim_idx, :))');
    do_xlabel('Time [s]');
    do_ylabel('Output [-]');
    
    set(gca,'YDir','normal');
    axis xy tight;
end

end
