function m = combine_fields(args)
% Generic interface to combine multiple fields together, using a predefined
% function (m.combination_fn) parameterized with a matrix of real values
% (m.combination_p) which is bounded (m.combination_p_lower and
% combination_p.upper).
%
% There is also an option to parse the STACK structure in order to
% duplicate modules after split_stim which then treat one single input
% e.g., if m.inputs is {'stim1', 'stim2'} (the default), then all modules
% after split_stim are duplicated with one half treating 'stim1' and the
% other half treating 'stim2'


% Module fields that must ALWAYS be defined
m = [];
m.mdl = @combine_fields;
m.name = 'combine_fields';
m.fn = @do_combine_fields;
m.pretty_name = 'Combine Fields';
m.editable_fields = {'inputs', 'time', 'output', ...
    'combination_fn', 'combination_p', 'combination_p_lower', 'combination_p_upper'};
m.isready_pred = @isready_always;

% Module fields that are specific to THIS MODULE
m.inputs = {'stim1', 'stim2'};
m.time  = 'stim_time';
m.output = 'stim';
m.combination_fn = 'comb_poly'; % should accept (params, ....) - see comb_poly.m
m.combination_p = [1 1 0; 1 0 1]; % default is a simple sum between the two inputs
m.combination_p_lower = -Inf(size(m.combination_p)); % does not enforce any boundaries by default
m.combination_p_upper = Inf(size(m.combination_p));
m.duplicate_if_necessary = 1;

% Optional fields
m.auto_init = @auto_init_combine_fields;
m.auto_plot = @do_plot_all_default_outputs;
m.plot_fns = {};
m.plot_fns{1}.fn = @do_plot_all_default_outputs;
m.plot_fns{1}.pretty_name = 'Output vs Time';

% Overwrite the default module fields with arguments
if nargin > 0
    m = merge_structs(m, args);
end

% Optimize this module for tree traversal
m.required = {m.inputs{:}, m.time};   % Signal dependencies
m.modifies = {m.output};          % These signals are modified


    function mdl = auto_init_combine_fields(stack, xxx)
        global STACK
        
        mdl = m;
        
%         [mods, mod_idxs] = find_modules(stack, 'split_stim', false);
%         
%         if ~isempty(mods)
%             mod = mods{end}{1};
%             start_idx = mod_idxs{end}+1;
%             end_idx = length(stack);
%             
%             start_idx2 = start_idx;
%             end_idx2 = end_idx;
%             nb_modules = end_idx - start_idx + 1;
%             for ii = 1:numel(mod.outputs)
%                 if ii>1
%                     % need to duplicate the last bunch of modules
%                     %                 STACK = [STACK(1:end_idx2), STACK(start_idx:end_idx)];
%                     for jj = 1:nb_modules
%                         append_module(STACK{start_idx+jj-1}{1});
%                     end
%                     start_idx2 = start_idx2 + nb_modules;
%                     end_idx2 = end_idx2 + nb_modules;
%                 end
%                 for jj = 1:nb_modules
%                     STACK{start_idx2+jj-1}{1}.input = mod.outputs{ii};
%                     STACK{start_idx2+jj-1}{1}.input_stim = mod.outputs{ii};
%                     STACK{start_idx2+jj-1}{1}.output = mod.outputs{ii};
%                 end
%             end
%             
%             mdl.inputs = mod.outputs;
%         end
    end



    function x = do_combine_fields(mdl, x)
        
        % validate constraints
        for i=1:size(mdl.combination_p,1)
            for j=1:size(mdl.combination_p,2)
                if mdl.combination_p(i,j) < mdl.combination_p_lower(i,j)
                    mdl.combination_p(i,j) = mdl.combination_p_lower(i,j);
                elseif mdl.combination_p(i,j) > mdl.combination_p_upper(i,j)
                    mdl.combination_p(i,j) = mdl.combination_p_upper(i,j);
                end
            end
        end
        
        for sf = fieldnames(x.dat)', sf=sf{1};
            % Combine the fields together
            varargins = '';
            if length(mdl.inputs) == 1
                % we combine channels
                [T, S, C] = size(x.dat.(sf).(mdl.inputs{1}));
                for idx = 1:C
                    varargins = [varargins ', x.dat.(sf).(mdl.inputs{1})(:,:,' num2str(idx) ')'];
                end
            else
                % we combine stimuli
                for idx = 1:length(mdl.inputs)
                    if isfield(x.dat.(sf), mdl.inputs{idx})
                        varargins = [varargins ', x.dat.(sf).(mdl.inputs{' num2str(idx) '})'];
                    else
                        if mdl.duplicate_if_necessary == 1
                            %                         fprintf('\n! Warning !\n The first entry was duplicated in combine_fields.\n');
                            varargins = [varargins ', x.dat.(sf).(mdl.inputs{' num2str(1) '})'];
                        end
                    end
                end
            end
            x.dat.(sf).(mdl.output) = eval([mdl.combination_fn '(mdl.combination_p' varargins ')']);
        end
    end

    function do_plot_all_default_outputs(sel, stack, xxx)
%         mdls = stack{end};
%         [mdls, xins, xouts] = calc_paramsets(stack(1:end-2), xxx(1:end-2));
        [mdls_c, xins_c, xouts_c] = calc_paramsets(stack, xxx);
        for sf = fieldnames(xxx{end}.dat)', sf=sf{1};
%             xins{1}.dat.(sf).combined = xouts_c{1}.dat.(sf).(mdls_c{1}.output);
            xins_c{1}.dat.(sf).combined = xouts_c{1}.dat.(sf).(mdls_c{1}.output);
        end
        sel.chan_idx = []; % when chan_idx is empty, do_plot plots all channels
        do_plot(xins_c, mdls_c{1}.time, ...
            {mdls_c{1}.output 'combined'}, ...
            sel, 'Time [s]', 'Output [-]');
    end

end
