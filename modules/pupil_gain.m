function m = pupil_gain(args)
% Applies a 2D nonparametric static nonlinear function to the inputs

% Module fields that must ALWAYS be defined
m = [];
m.mdl = @pupil_gain;
m.name = 'pupil_gain';
m.fn = @do_pupil_gain;
m.pretty_name = 'Pupil Gain Scaling';
m.editable_fields = {'input_stim', 'input_pupil', 'input_resp', 'time',...
                     'output', 'gain','offset','smoothwindow'};
m.isready_pred = @isready_always;

% Module fields that are specific to THIS MODULE
m.input_stim = 'stim';   % base input
m.input_pupil = 'pupil';   % input to gain scale term
m.input_resp = 'respavg';
m.time = 'stim_time';
m.output = 'stim';
m.bincount = 20;
m.smoothwindow=1;
m.gain=[0 0];
m.offset=[0 0];

% Optional fields
m.plot_fns = {};
m.auto_plot = @do_plot_gain_dc;
m.plot_fns{1}.fn = @do_plot_gain_dc;
m.plot_fns{1}.pretty_name = 'Gain/DC plot';
m.plot_fns{2}.fn = @do_plot_smooth_scatter_and_nonlinearity;
m.plot_fns{2}.pretty_name = 'Stim/Resp Smooth Scatter';
m.plot_fns{3}.fn = @do_plot_all_default_outputs;
m.plot_fns{3}.pretty_name = 'Output Channels (All)';
m.plot_fns{4}.fn = @do_plot_single_default_output;
m.plot_fns{4}.pretty_name = 'Output Channel (Single)';

% Overwrite the default module fields with arguments 
if nargin > 0
    m = merge_structs(m, args);
end

% Optimize this module for tree traversal  
m.required = {m.input_stim, m.input_pupil, m.input_resp, m.time};   % Signal dependencies
m.modifies = {m.output};          % These signals are modified

% ------------------------------------------------------------------------
% Methods

function x = do_pupil_gain(mdl, x)    
    
    gain=mdl.gain;
    offset=mdl.offset;
    
    fns = fieldnames(x.dat);
    for ii = 1:length(fns)
        sf = fns{ii};
        s1=x.dat.(sf).(mdl.input_stim)-gain(1);
        s2=x.dat.(sf).(mdl.input_pupil)-offset(1);
        
        if size(s1,3)>size(s2,3),
           x.dat.(sf).(mdl.output)=s1;
           for jj=1:size(s1,3),
              x.dat.(sf).(mdl.output)(:,:,jj) = gain(1) + ...
                 s1(:,:,jj).*(1+s2.*gain(2)) + offset(2).*s2;
           end
        else
           x.dat.(sf).(mdl.output) = gain(1) + ...
              s1.*(1+s2.*gain(2)) + offset(2).*s2;
        end
    end
end

function help_plot_npnl(sel, mdls, xins, xouts)
    for ii = 1:length(mdls)
        [phi,outbinserr] = init_nonparm_gain(mdls{ii}, xins{ii}{end});
        xouts{ii}.dat.(sel.stimfile).npnlstim = phi{1};
        xouts{ii}.dat.(sel.stimfile).npnlpred = phi{2};
        xouts{ii}.dat.(sel.stimfile).temperr  = outbinserr;
    end
    
    hold on;
    tsel=sel;
    tsel.stim_idx=1;
    do_plot(xouts, 'npnlstim', 'npnlpred', ...
            tsel, 'NPNL Input [-]', 'RespAvg Prediction [Hz]');   
    hold off;
end

function do_plot_gain_dc(sel, stack, xxx)    
    [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));    
    plot([mdls{1}.gain(:) mdls{1}.offset(:)]);
    
end

function do_plot_scatter_and_nonlinearity(sel, stack, xxx)    
    [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));    
    do_plot_scatter(sel, xins, mdls{1}.input_stim2, mdls{1}.input_resp);  
    help_plot_npnl(sel, mdls, xins, xouts);
end

function do_plot_smooth_scatter_and_nonlinearity(sel, stack, xxx)
    [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end)); 
    %do_plot_scatter(sel, xins, mdls{1}.input_stim2, mdls{1}.input_resp, 100); 
    help_plot_npnl(sel, mdls, xins, xouts);
end


end
