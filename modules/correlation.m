function m = correlation(args)
% A function to linearly correlate two signals. 
% 
% The signals are assumed to be two dimensional matrices:
%  Dimension 1 is time
%  Dimension 2 is the stimuli #
%
% Returns a function module 'm' which implements the MODULE interface.
% See docs/modules.org for more information.

% Module fields that must ALWAYS be defined
m = [];
m.mdl = @correlation;
m.name = 'correlation';
m.fn = @do_correlation;
m.pretty_name = 'Correlation';
m.editable_fields = {'input1', 'input2', 'nfft_coherence', 'time', 'train_score', 'test_score'};
m.isready_pred = @isready_always;

% Module fields that are specific to THIS MODULE
m.input1 = 'stim';
m.input2 = 'respavg';
m.input_resp = 'resp';
m.time   = 'stim_time';
m.nfft_coherence   = 64;
m.train_score = 'score_train_corr';
m.test_score = 'score_test_corr';
m.test_r_ceiling = 'score_test_ceilingcorr';
m.test_r_floor = 'score_test_floorcorr';
m.output = 'corr_score';  

% Overwrite the default module fields with arguments 
if nargin > 0
    m = merge_structs(m, args);
end

% Optimize this module for tree traversal  
m.required = {m.input1, m.input2, m.input_resp, m.time};   % Signal dependencies
m.modifies = {m.output, m.train_score, m.test_score, m.test_r_ceiling, m.test_r_floor};  % These signals are modified

% Optional fields
m.plot_fns = {};
m.auto_plot = @do_plot_correlation_inputs;
m.plot_fns{1}.fn = @do_plot_correlation_inputs;
m.plot_fns{1}.pretty_name = 'Correlation';
m.plot_fns{2}.fn = @do_plot_coherence;
m.plot_fns{2}.pretty_name = 'Coherence';
m.plot_fns{3}.fn = @do_plot_coherence_phase;
m.plot_fns{3}.pretty_name = 'Coherence Phase';

function x = do_correlation(mdl, x)
    
    global XXX FLATXXX
    % Compute the training set correlation, ignoring nans
    
    p = flatten_field(x.dat, x.training_set, mdl.input1);
    q = flatten_field(x.dat, x.training_set, mdl.input2);
    
    R = corrcoef(excise([p q]));
    if isnan(R)
        x.(mdl.train_score) = NaN;
        x.(mdl.output) = -2;
    else
        x.(mdl.train_score) = R(2,1);
        x.(mdl.output) = 1 - R(2,1);
    end
    
    % Compute the test set correlation, ignoring nans
    ptest = flatten_field(x.dat, x.test_set, mdl.input1);
    qtest = flatten_field(x.dat, x.test_set, mdl.input2);
    R = corrcoef(excise([ptest qtest]));
    if isnan(R)
        x.(mdl.test_score) = NaN;
    else
        x.(mdl.test_score) = R(2,1);if(~exist('iscrossed','var'))
            iscrossed=false;
        end
    end
    
    if ~isfield(mdl, 'test_r_ceiling')
        mdl.test_r_ceiling = 'score_test_ceilingcorr';
    end
    if ~isfield(mdl, 'test_r_floor')
        mdl.test_r_floor = 'score_test_floorcorr';
    end
    
    disp('starting do_correlation...');
   
    
    % TODO: 
    %  Come back and make this more general so that we compute respavg from
    %  resp and this module goes back to having just two inputs. (a vector
    %  and a scalar)
    
    % Extract resps as a matrix, not a vector    
    %if isfield(x.dat., 'resp')
        
    respmat = [];
    for ii = 1:length(x.test_set),
       [A, B, C] = size(x.dat.(x.test_set{ii}).resp);
       M = reshape(x.dat.(x.test_set{ii}).resp, A*B, C);
       if ~isempty(respmat) && C > size(respmat, 2)
          respmat = cat(1, [respmat nan(size(respmat,1), size(M,2)-size(respmat,2))] , M);
       elseif C < size(respmat, 2)
          respmat = cat(1, respmat, [M nan(size(M,1), size(respmat,2) - size(M,2))]);
       else
          respmat = cat(1, respmat, M);
       end
    end
    
    % Remove rows with all NaNs
    idxs = find(~isnan(qtest) & ~isnan(ptest) & sum(~isnan(respmat),2)>=size(respmat,2)./2);
    tmp = [ptest qtest respmat];
    tmp = tmp(idxs, :);
    tmp = excise(tmp')';
    
    pe = tmp(:,1);
    qe = tmp(:,2);
    re = tmp(:,3:end);
    
    disp('calcing rceiling...');
    
    N=min(500,ceil(1000000./size(pe,1)));
    x.(mdl.test_r_ceiling) = rceiling(pe, re, N);
    disp('calcing rfloor...');
    [~, x.(mdl.test_r_floor)] = rfloor(pe, qe, N);
        
%     else
%         tmp = excise([ptest qtest]);
%         pe = tmp(:,1);
%         qe = tmp(:,2);        
%         
%         x.(mdl.test_r_ceiling) = NaN;
%         [~, x.(mdl.test_r_floor)] = rfloor(pe, qe);
%     end

    disp('calcing correlation for active...');

    % special case-- if we have active vs. passive file codes,
    % compute correlation for only the active test data as well
    if length(x.filecodes)>1 && ...
            sum(ismember(x.filecodes(1:length(x.test_set)),{'A1','A2','A3','A4','A5'}))>0,
        activefiles=find(ismember(x.filecodes(1:length(x.test_set)),...
                                  {'A1','A2','A3','A4','A5'}));
        ptest = flatten_field(x.dat, x.test_set(activefiles), mdl.input1);
        qtest = flatten_field(x.dat, x.test_set(activefiles), mdl.input2); 
        R = corrcoef(excise([ptest qtest]));
        x.score_test_corr_active=R(2,1);
    end
    
    disp('calcing correlation per condition...');
    % LAS 8/1/16:
    num_conditions=length(x.unique_codes);
    % correlation separately by condition
    for k=1:num_conditions
        p = flatten_field(x.dat, x.training_set, mdl.input1,k);
        q = flatten_field(x.dat, x.training_set, mdl.input2,k); 
        
        R = corrcoef(excise([p q]));
        if isnan(R)
            x.([mdl.train_score,'_byC'])(k) = NaN; 
        else
            x.([mdl.train_score,'_byC'])(k) = R(2,1);
        end

        % Compute the test set correlation, ignoring nans
        ptest = flatten_field(x.dat, x.test_set, mdl.input1, k);
        qtest = flatten_field(x.dat, x.test_set, mdl.input2, k); 
        R = corrcoef(excise([ptest qtest]));
        if isnan(R)
            x.([mdl.test_score,'_byC'])(k) = NaN;
        else
            x.([mdl.test_score,'_byC'])(k) = R(2,1);
        end
    end
    
    %LAS 8/1/16:
    compute_crossed_condition_correlations=true;
    if(isfield(mdl,'options'))
        if(isfield(mdl.options,'compute_crossed_condition_correlations'))
            compute_crossed_condition_correlations=mdl.options.compute_crossed_condition_correlations;
        end
    end
    s=dbstack;
    global STACK
    if(compute_crossed_condition_correlations && sum(arrayfun(@(x)strcmp(x.name,'correlation/do_correlation'),s))==1 && max(cellfun(@(x)length(x),STACK))>1 )
        %if compute_crossed_codition_correlations and this isn't already a
        %crossed computation. This is better than a global or persistent
        %variable because it will do the right thing even if there is an error.
        if(sum(arrayfun(@(x)strcmp(x.name,'narf_modelpane/replot_from_depth'),s))>0 )
            %this gets re-called every time a re-plot occurs, no need to recalculate
            x.([mdl.train_score,'_byC_all'])=XXX{end}.([mdl.train_score,'_byC_all']);
            x.([mdl.test_score,'_byC_all'])=XXX{end}.([mdl.test_score,'_byC_all']);
        else
            global XXX_FLIP
            cross_calcs_stop_ind=find(cellfun(@(x)strcmp(x{1}.name,'correlation'),STACK));
            uncrossed_flatxxx=FLATXXX;
            uncrossed_xxx=XXX;
            uncrossed_x=x;
            XXX(2:end)=[];
            uncrossed_x.([mdl.train_score,'_byC_all'])=nan(num_conditions);
            uncrossed_x.([mdl.test_score,'_byC_all'])=nan(num_conditions);
            uncrossed_x.([mdl.test_score,'_byC_all'])(1:num_conditions+1:end)=uncrossed_x.([mdl.test_score,'_byC']);
            uncrossed_x.([mdl.train_score,'_byC_all'])(1:num_conditions+1:end)=uncrossed_x.([mdl.train_score,'_byC']);
            for crossed_state=1:num_conditions-1
                if(1)
                    XXX=uncrossed_flatxxx(1:2);
                    fns=fieldnames(XXX{2}.dat);
                    for i=1:length(fns)
                        tc=mod(XXX{2}.dat.(fns{i}).trial_code+crossed_state,num_conditions);
                        tc(tc==0)=num_conditions;
                        XXX{2}.dat.(fns{i}).trial_code=tc;
                    end
                    FLATXXX=XXX;
                    calc_xxx(2,cross_calcs_stop_ind);
                else
                    XXX=uncrossed_xxx(1);
                    XXX{1}.filecodes=circshift(XXX{1}.filecodes,[0 crossed_state]);
                    calc_xxx(1,cross_calcs_stop_ind);
                end
                XXX_FLIP{crossed_state}=XXX;
                [ind]=sub2ind(num_conditions([1 1]),circshift(1:num_conditions,[0 crossed_state]),1:num_conditions);
                uncrossed_x.([mdl.train_score,'_byC_all'])(ind)=FLATXXX{end}.([mdl.train_score,'_byC']);
                uncrossed_x.([mdl.test_score,'_byC_all'])(ind)=FLATXXX{end}.([mdl.test_score,'_byC']);
                % Row: model, Column: filecode (stimulus)
            end
            FLATXXX=uncrossed_flatxxx;
            XXX=uncrossed_xxx;
            x=uncrossed_x;
        end
    end
    
end


    function do_plot_correlation_inputs(sel, stack, xxx)
    [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end)); 
    xout = xxx{end};
    mdl = mdls{1};
    
    do_plot_scatter(sel, xins, mdls{1}.input1, mdls{1}.input2, 500);
    
    if ~isfield(mdl, 'test_r_ceiling')
        mdl.test_r_ceiling = 'score_test_ceilingcorr';
    end
    if ~isfield(mdl, 'test_r_floor')
        mdl.test_r_floor = 'score_test_floorcorr';
    end    
    textLoc(sprintf(' Train r: %f\n Test r : %f ({\\pm}%f)\n Ceiling r: %f', ...
                    xout.(mdl.train_score), xout.(mdl.test_score),...
                    xout.(mdl.test_r_floor), xout.(mdl.test_r_ceiling)),...
            'NorthWest');    
    
    do_xlabel('Prediction [Hz]');
    do_ylabel('Actual Response [Hz]');
    
end

function do_plot_coherence(sel, stack, xxx)
    [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end)); 
    xout = xouts{1};
    mdl = mdls{1};
    
    train1 = flatten_field(xout.dat, xout.training_set, (mdls{1}.input1));
    train2 = flatten_field(xout.dat, xout.training_set, (mdls{1}.input2));       
    test1 = flatten_field(xout.dat, xout.test_set, (mdls{1}.input1));
    test2 = flatten_field(xout.dat, xout.test_set, (mdls{1}.input2));
    
    tim  = xout.dat.(xout.training_set{1}).(mdls{1}.time)(:);
    fs = 1 / (tim(2) - tim(1));
    n_fft = mdl.nfft_coherence;
    n_overlap = n_fft/2;
    
    % Slow calculation: takes 0.75 seconds
    tic
    [Ptest, Ftest] = mscohere(test1, test2, hanning(n_fft), n_overlap, n_fft, fs);
    [Ptrain, Ftrain] = mscohere(train1, train2, hanning(n_fft), n_overlap, n_fft, fs);
    toc
    plot(Ftest, Ptest, 'r-', Ftrain, Ptrain, 'b-'); 
    do_xlabel('Frequency [Hz]');
    do_ylabel('Magnitude-Squared Coherence');
    legend('Val', 'Est');
        
    textLoc(sprintf('Est Mean = %0.3f\nVal Mean =%0.3f', ...
        mean(Ptrain(:)), mean(Ptest(:))), 'North', 'Interpreter', 'none');
    

end

function do_plot_coherence_phase(sel, stack, xxx)
    [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end)); 
    xout = xouts{1};
    mdl = mdls{1};
    
    train1 = flatten_field(xout.dat, xout.training_set, (mdls{1}.input1));
    train2 = flatten_field(xout.dat, xout.training_set, (mdls{1}.input2));       
    test1 = flatten_field(xout.dat, xout.test_set, (mdls{1}.input1));
    test2 = flatten_field(xout.dat, xout.test_set, (mdls{1}.input2));
    
    tim  = xout.dat.(xout.training_set{1}).(mdls{1}.time)(:);
    fs = 1 / (tim(2) - tim(1));
    n_fft = mdl.nfft_coherence;
    n_overlap = n_fft/2;
    
    % Getting the phase (cross power spectral density)
    % Slow calculation: Takes 0.3 seconds
    [Ctest, Ftest] = cpsd(test1,test2, hamming(n_fft), n_overlap, n_fft, fs);    
    [Ctrain, Ftrain] = cpsd(train1,train2, hamming(n_fft), n_overlap, n_fft, fs);       
    plot(Ftest, -angle(Ctest), 'r-', Ftrain, -angle(Ctrain), 'b-');
    legend('Val', 'Est');
    do_ylabel('Phase Angle [Rad]');
    do_xlabel('Frequency [Hz]');

end

end
