function m = normalize_output(args)
% Scale the output value of a module to be in [0,1], using the whole
% training dataset.
% TODO 1: make it work for several channels
% TODO 2: make a nice visualization

% Module fields that must ALWAYS be defined
m = [];
m.mdl = @normalize_output;
m.name = 'normalize_output';
m.fn = @do_normalize_output;
m.pretty_name = 'Normalize Output';
m.editable_fields = {'input', 'time', 'output'};
m.isready_pred = @isready_always;

% Module fields that are specific to THIS MODULE
m.input = 'stim';
m.time  = 'stim_time';
m.output = 'stim';

% Optional fields
%m.plot_gui_create_fn = @create_chan_selector_gui;
m.plot_fns = {};
m.plot_fns{1}.fn = @do_plot_scatter_normalization;
m.plot_fns{1}.pretty_name = 'Stim/Resp Scatter';

% Overwrite the default module fields with arguments
if nargin > 0
    m = merge_structs(m, args);
end

% Optimize this module for tree traversal
m.required = {m.input, m.time};   % Signal dependencies
m.modifies = {m.output};          % These signals are modified

    function [min_val, max_val] = get_norm_boundaries(mdl, x, sets)
        min_val = -Inf;
        max_val = Inf;
        for stimulus=sets
            stimulus = stimulus{1};
            base_stim = x.dat.(stimulus).(mdl.input);
            %             sound_onset = find(sum(sum(x.dat.(stimulus).(mdl.input),2),3),1);
            %             if sound_onset < 5 % weird case
            %                 x.dat.(stimulus).(mdl.output) = 0.5 * ones(size(x.dat.(stimulus).(mdl.input)));
            %                 fprintf(1,'oh weird\n');
            %                 continue
            %             end
            sound_onset = 40;
            base_stim = base_stim(5:sound_onset,:);
            min_val = mean(mean(base_stim));
            max_val = max(max(x.dat.(stimulus).(mdl.input)));
            
            %             min_val = min(min(x.dat.(stimulus).(mdl.input)));
            %             max_val = max(max(x.dat.(stimulus).(mdl.input)));
            %             min_val = min(mean(x.dat.(stimulus).(mdl.input)));
            %             max_val = max(mean(x.dat.(stimulus).(mdl.input)));
            %             min_val = mean(prctile(x.dat.(stimulus).(mdl.input),5));
            %             max_val = mean(prctile(x.dat.(stimulus).(mdl.input),95));
            
%             fprintf(1,'Scaling from %f to %f\n',min_val,max_val);
        end
    end


    function x = do_normalize_output(mdl, x)
        [min_val, max_val] = get_norm_boundaries(mdl, x, x.training_set);
        fns = fieldnames(x.dat);
        for ii = 1:length(fns)
            stimulus=fns{ii};
            
            % relative normalization
%                     [min_val, max_val] = get_norm_boundaries(mdl, x, {stimulus});
            
            if max_val ~= min_val
                x.dat.(stimulus).(mdl.output) = (x.dat.(stimulus).(mdl.input) - min_val) / (max_val-min_val);
            else % handle divide by zero error
                x.dat.(stimulus).(mdl.output) = 0.5 * ones(size(x.dat.(stimulus).(mdl.input)));
            end
        end
    end

    function do_plot_scatter_normalization(sel, stack, xxx)
        [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));
        mdls{1}.output = 'scatterplot';
        xins = {do_normalize_output(mdls{1}, xins{1})};
        do_plot_scatter(sel, xins, mdls{1}.input, mdls{1}.output);
        hold on;
        refline(0,1);
        refline(0,0);
        axis auto;
        %         yL = get(gca, 'YLim');
        %         [min_val, max_val] = get_norm_boundaries(mdls{1}, xins{1});
        %         line([min_val min_val], yL);
        %         line([max_val max_val], yL);
        hold off;
        %     help_plot(sel, mdls, xins, xouts);
    end


end
