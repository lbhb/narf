function m = fir_filter(args)
% A Single N-dimensional FIR that spans the input space.
%
% DOCUMENTATION TODO
%
% The total number of filter coefficients = num_coefs * num_dims * the
% number of groupings of respfiles
%
% NUM_DIMS should always equal the size of the 'channel' input dimension.
%

% Module fields that must ALWAYS be defined
m = [];
m.mdl = @fir_filter;
m.name = 'fir_filter';
m.fn = @do_fir_filtering;
m.pretty_name = 'FIR Filter';
m.editable_fields = {'coefs', 'global_gain','baseline', ...
    'num_coefs', 'num_dims', 'sum_channels','phi','phifn',...
    'input', 'filtered_input', 'time', 'output'};
m.isready_pred = @isready_always;

% Module fields that are specific to THIS MODULE
m.phi = [];
m.phifn = [];  % Applied to phi, this is used to generate coefs
m.num_coefs = 20;
m.num_dims = 2;
m.baseline = 0;
m.sum_channels=1;
m.rectify_channels=0;
m.coefs = zeros(m.num_dims, m.num_coefs);
m.global_gain = 1;
m.reset_filter_during_fit=0;
m.input =  'stim';
m.filtered_input = 'stim_filtered';
m.time =   'stim_time';
m.output = 'stim';
m.init_fit_sig = 'respavg';
m.fit_constraints = {...
    struct('var', 'coefs', 'lower', -Inf, 'upper', Inf), ...
    struct('var', 'baseline', 'lower', -Inf, 'upper', Inf) ...
    };


% Optional fields
m.plot_gui_create_fn = @create_chan_selector_gui;
m.auto_plot = @do_plot_fir_coefs_as_heatmap;
m.auto_init = @auto_init_fir_filter;
m.plot_fns = {};
m.plot_fns{1}.fn = @do_plot_fir_coefs_as_heatmap;
m.plot_fns{1}.pretty_name = 'FIR Coefs (Heat map)';
m.plot_fns{2}.fn = @do_plot_fir_coefs;
m.plot_fns{2}.pretty_name = 'FIR Coefficients (Line)';
m.plot_fns{3}.fn = @do_plot_all_filtered_channels;
m.plot_fns{3}.pretty_name = 'Filtered Channels (All)';
m.plot_fns{4}.fn = @do_plot_single_filtered_channel;
m.plot_fns{4}.pretty_name = 'Filtered Channels (Single)';
m.plot_fns{5}.fn = @do_plot_filter_output;
m.plot_fns{5}.pretty_name = 'FIR Output';

% Overwrite the default module fields with arguments
if nargin > 0
    m = merge_structs(m, args);
end

% Optimize this module for tree traversal
m.required = {m.input, m.time, m.init_fit_sig};  % Signal dependencies
m.modifies = {m.output, m.filtered_input};       % These signals are modified

% Reset the FIR filter coefficients if its size doesn't match num_coefs
if ~isequal([m.num_dims m.num_coefs], size(m.coefs))
    m.coefs = zeros(m.num_dims, m.num_coefs);
    % and update the fit constraints
end

% ------------------------------------------------------------------------
% INSTANCE METHODS

    function mdl = auto_init_fir_filter(stack, xxx)
        % NOTE: Unlike most plot functions, auto_init functions get a
        % STACK and XXX which do not yet have this module or module's data
        % added to them.
        if ~isfield(m, 'fit_fields')
            mdl = m;
            return
        end
        
        % Init the coefs to have the right dimensionality
        mdl = m;
        x = xxx{end};
        fns = fieldnames(x.dat);
        sf = fns{1};
        [T, S, C] = size(x.dat.(sf).(mdl.input));
        
        mdl.num_dims = C;
        mdl.coefs = zeros(mdl.num_dims, mdl.num_coefs);
        % update the fit constraint to expand the coefs constraints
        [i, con] = get_constraint_index(mdl.fit_constraints, 'coefs');
        mdl.fit_constraints(i) = {...
            struct('var', 'coefs', 'lower', con.lower*ones(mdl.num_dims, mdl.num_coefs), 'upper', con.upper*ones(mdl.num_dims, mdl.num_coefs)) ...
            };
    end

    function x = do_fir_filtering(mdl, x)
        % Apply the FIR filter across every stimfile
        fns = fieldnames(x.dat);
        
        if ~isfield(mdl, 'sum_channels')
            mdl.sum_channels=1;
        end
        if ~isfield(mdl, 'rectify_channels')
            mdl.rectify_channels=0;
        end
        if ~isfield(mdl, 'global_gain')
            mdl.global_gain=1;
        end
        if isfield(mdl, 'phi') && isfield(mdl, 'phifn') && ~isempty(mdl.phifn)
            mdl.coefs = mdl.phifn(mdl.phi, [mdl.num_dims mdl.num_coefs]);
        end
        
        coefs = mdl.coefs.*mdl.global_gain;
        
        % Have a space allocated for computing initial filter conditions
        init_data_space = ones(size(coefs, 2) * 2, 1);
        
        for ii = 1:length(fns)
            sf=fns{ii};
            
            % Compute the size of the filter
            [T, S, C] = size(x.dat.(sf).(mdl.input));
            
            if S,
                if ~isequal(C, mdl.num_dims)
                    error('Dimensions of (mdl.input) don''t match channel count.');
                end
                
                zf = {};
                for c = 1:C
                    % Find proper initial conditions for the filter
                    [~, zftmp] = filter(coefs(c,:)', [1], ...
                        init_data_space .* x.dat.(sf).(mdl.input)(1, 1, c));
                    zf{c} = zftmp;
                end
                
                %          % Old way of filtering
                %          fprintf('Old Way:');
                %          tmp = zeros(T, S, C);
                %          for s = 1:S
                %              for c = 1:C,
                %
                %                  [tmp(:, s, c), zftmp] = filter(coefs(c,:)', [1], ...
                %                      x.dat.(sf).(mdl.input)(:, s, c), ...
                %                      zf{c});
                %                  zf{c} = zftmp;
                %              end
                %          end
                
                % Tests revealed that below is 2-5x faster than the above:
                
                pad_zeros_between_stimuli=true;
                if(pad_zeros_between_stimuli)
                    fs=1/diff(x.dat.(sf).stim_time(1:2));
                    num_pad=round(fs*.3);%300 ms of silence = 30 samples at fs=100, bigger than the FIR length
                    dd = reshape([zeros(num_pad,S,C);x.dat.(sf).(mdl.input)], (T+num_pad)*S, C); % 210000x3
                    tmp = zeros((T+num_pad)*S, C);
                else
                    dd = reshape(x.dat.(sf).(mdl.input), T*S, C); % 210000x3
                    tmp = zeros(T*S, C);
                end
                for c = 1:C,
                    tmp(:, c) = filter(coefs(c,:)', [1], dd(:,c), zf{c}, 1);
                end
                if(pad_zeros_between_stimuli)
                    tmp = reshape(tmp,T+num_pad, S, C);
                    tmp(1:num_pad,:,:)=[];
                else
                    tmp = reshape(tmp, T, S, C);
                end
                x.dat.(sf).(mdl.filtered_input) = tmp;
                
                % The output is the sum of the filtered channels
                if mdl.rectify_channels
                   
                end
                
                if mdl.sum_channels
                   if mdl.rectify_channels,
                      for ttt=1:size(tmp,3),
                         tmp(:,:,ttt)=tmp(:,:,ttt)+mdl.baseline(ttt);
                      end
                      tmp(tmp<0)=0;
                      x.dat.(sf).(mdl.output) = sum(tmp, 3);
                   else
                      x.dat.(sf).(mdl.output) = sum(tmp, 3) + mdl.baseline(1);
                      % tmp2 = sum(tmp, 3);
                   end
                elseif length(mdl.baseline)<size(tmp,3),
                    x.dat.(sf).(mdl.output) = tmp + mdl.baseline;
                else
                    for ttt=1:size(tmp,3),
                        tmp(:,:,ttt)=tmp(:,:,ttt)+mdl.baseline(ttt);
                    end
                    x.dat.(sf).(mdl.output) = tmp;
                end
            else
                x.dat.(sf).(mdl.filtered_input) = x.dat.(sf).(mdl.input);
                x.dat.(sf).(mdl.output)=zeros(T,S,1);
            end
        end
    end

% ------------------------------------------------------------------------
% Plot methods

    function do_plot_fir_coefs_as_heatmap(sel, stack, xxx)
        
        % LAS: xxx is now not needed so you can plot weights without
        % recalculating xxx 7/28/16
        mdls = stack{end};
        
        % Find the min and max values so colors are scaled appropriately
        c_max = 0;
        for ii = 1:length(mdls)
            mdl=mdls{ii};
            if isfield(mdl, 'phi') && isfield(mdl, 'phifn') && ~isempty(mdl.phifn)
                mdls{ii}.coefs = mdl.phifn(mdl.phi, [mdl.num_dims mdl.num_coefs]);
            end
            c_max = max(c_max, max(abs(mdls{ii}.coefs(:))));
        end
        
        %  Plot all parameter sets' coefficients. Separate them by white pixels.
        
        [weight_mdl, idxs]=find_modules(stack,'weight_channels');
        if ~isempty(weight_mdl),
            weight_mdl=weight_mdl{1};
            idx = idxs{end};
            if stack{1}{1}.stimulus_channel_count>0,
               [~, wts] = weight_mdl{1}.fn(weight_mdl{1},stack{1}{1}.stimulus_channel_count);
               if size(wts,2) ~= size(mdls{1}.coefs,1),
                  % size mismatch (b/c intervening volterra expansion?)
                  weight_mdl={};
               end
            else
               weight_mdl={};
            end
            
        end
        
        wmat=[];
        num_images=max(length(weight_mdl),length(mdls));
        if ~isempty(weight_mdl)% && length(mdls)<2,
            for ii = 1:num_images
                mdl_i=ii;
                if(ii>length(mdls))
                    mdl_i=1;
                end
                % Get the extra argument to calc the weights
                if length(weight_mdl)>1,
                    [~, wts] = weight_mdl{ii}.fn(weight_mdl{ii},stack{1}{1}.stimulus_channel_count);
                else
                    [~, wts] = weight_mdl{1}.fn(weight_mdl{1},stack{1}{1}.stimulus_channel_count);
                end
                if(isfield(xxx{end},'scale'))
                    %LAS 12/15/2016: use scale factors from previous
                    %normalization stage instead of std of the spectral
                    %weights (which leads to very small sf for wide
                    %channels)
                    sw=xxx{end}.scale;
                else
                    sw=std(wts,0,1);
                    sw(sw<eps)=1;
                end
                wcoefs = mdls{mdl_i}.coefs;
                if size(wts,2)==size(wcoefs,1),
                   wts=wts./repmat(sw,[size(wts,1) 1]);
                    if isfield(mdls{mdl_i}, 'sum_channels') && mdls{mdl_i}.sum_channels == 0
                        % Special handling when we do not sum channels
                        for jj = 1:size(wts,2)
                            wmat=cat(2, wmat, [wts(:,jj)*wcoefs(jj,:); wcoefs(jj,:)]);
                        end
                        wmat = wmat / max(max(wmat));
                        h = size(wmat,1);
                        w = size(wmat,2);
                        hc = nan;
                        n_chans = size(wts,2);
                    else
                        % Usual way to get the heatmap of coeffs to plot
                        coefs = wts*wcoefs;
                        wcoefs=wcoefs./max(abs(wcoefs(:))).*max(abs(coefs(:)));
                        wts = wts./max(abs(wts(:))).*max(abs(coefs(:)));
                        [wc, hc] = size(coefs');
                        [w, h] = size([wts coefs; zeros(size(wts,2)) wcoefs]');
                        n_chans = size(wts,2);
                        wmat=cat(2, wmat, [wts coefs; zeros(n_chans) wcoefs]);
                    end
                else
                    % this code never executes, right?
                    coefs=[];
                    [wc, hc] = size(coefs');
                    [w, h] = size(wts');
                    n_chans = size(wts,2);
                    wts = wts./max(abs(wts(:)));
                    wmat=cat(2, wmat, wts);
                end
                
            end
        else
            for ii = 1:length(mdls)
                mdl=mdls{ii};
                coefs = mdl.coefs;
                [wc, hc] = size(coefs');
                [w, h] = size(coefs');
                wmat=cat(2, wmat, coefs);
            end
            n_chans=size(coefs,1);
        end
        imagesc(wmat);
        
        hold on;
        for ii=1:num_images;
            mdl_i=ii;
            if(ii>length(mdls))
                mdl_i=1;
            end
            if ii<num_images,
                plot([1 1].*w.*ii+0.5, [0.5 h+0.5],'w-','LineWidth',2);
                plot([1 1].*w.*ii+n_chans+0.5, [0.5 h+0.5],'w-','LineWidth',2);
            end
            
            if isfield(mdls{mdl_i}, 'sum_channels') && mdls{mdl_i}.sum_channels == 0
                for jj = 1:(n_chans-1)
                    plot([jj*w/n_chans+0.5 jj*w/n_chans+0.5], [0 h+1],'w-','LineWidth',2);
                end
                plot([0.5 w+0.5],[h-1+0.5 h-1+0.5],'w-','LineWidth',2);
            else
                if hc<h,
                    plot([0.5 size(wmat,2)+0.5],[hc+0.5 hc+0.5],'w-','LineWidth',2);
                    plot([n_chans+0.5 n_chans+0.5], [0 h],'w-','LineWidth',2);
                end
            end
            coefs = mdls{mdl_i}.coefs;
            if isfield(xxx{end},'unique_codes') && ...
                    length(xxx{end}.unique_codes)>=ii,
                text(w.*ii, 1, sprintf('%s\nSp: %0.3g\nSm: %0.3g\nBl: %0.3g', ...
                    xxx{end}.unique_codes{ii},...
                    sparsity_metric(coefs),smoothness_metric(coefs),mdls{mdl_i}.baseline),...
                    'VerticalAlign','bottom','HorizontalAlign','right');
            else
                text(w.*ii, 1, sprintf('Sp: %f\nSm: %f', ...
                    sparsity_metric(coefs),smoothness_metric(coefs)),...
                    'VerticalAlign','bottom','HorizontalAlign','right');
            end
        end
        hold off;
        set(gca,'YDir','normal');
        
        ca = caxis;
        lim = max(abs(ca));
        caxis([-lim, +lim]);
        axis tight;
        do_xlabel('Coef Time Index');
        do_ylabel('Coef Channel Index');
    end

    function do_plot_all_filtered_channels(sel, stack, xxx)
        [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));
        [sel.chan_idx] = deal([]); % when chan_idx is empty, do_plot plots all channels
        do_plot(xouts(1), mdls{1}.time, mdls{1}.filtered_input, ...
            sel, 'Time [s]', 'Filtered Channel [-]');
    end

    function do_plot_single_filtered_channel(sel, stack, xxx)
        [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));
        if(strcmp(mdls{1}.output,'stim01'))
            mdls{1}.output='stim';
        end
        do_plot(xouts, mdls{1}.time, mdls{1}.output, ...
            sel, 'Time [s]', 'Filtered Channel [-]');
    end

    function do_plot_filter_output(sels, stack, xxx)
        [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));
        [sels.chan_idx] = deal([]); % when chan_idx is empty, do_plot plots all channels
        outputs=cellfun(@(x)x.output,mdls,'Uni',0);
        if length(unique(outputs))>1 && length(sels)>1
            for k=1:length(mdls)
                inds=xouts{k}.dat.(sels(k).stimfile).trial_code==k;
                sels(k).stim_idx=sum(inds(1:sels(k).stim_idx));
                %[h(k),leg(k)]=do_plot(xouts(k), mdls{k}.time, mdls{k}.output,sels(k), 'Time [s]', 'FIR Output [-]','Color',pickcolor(k));
                [ht,legt]=do_plot(xouts(k), mdls{k}.time, mdls{k}.output,sels(k), 'Time [s]', 'FIR Output [-]','Color',pickcolor(k));
                h(k)=ht(1);
                leg(k)=legt(1);
            end
            lh = legend(h, leg, 'Location', 'NorthWest');
            set(lh,'Interpreter', 'none');
        else
            do_plot(xouts, mdls{1}.time, mdls{1}.output,sels, 'Time [s]', 'FIR Output [-]');
        end
        
    end

    function do_plot_fir_coefs(sel, stack, xxx)
        %leg_location='NorthWest';
        leg_location='Best';
        mdls = stack{end};
        
        % Plot all parameter sets' coefficients. Separate them by white pixels
        hold on;
        handles = [];
        names = {};
        n_mdls = length(mdls);
        for ii = 1:n_mdls
            mdl = mdls{ii};
            if isfield(mdl, 'phi') && isfield(mdl, 'phifn') && ~isempty(mdl.phifn)
                mdl.coefs = mdl.phifn(mdl.phi, [mdl.num_dims mdl.num_coefs]);
            end
            coefs = mdl.coefs;
            [w, h] = size(coefs);
            for c = 1:w
                handles(end+1) = plot((0.3*(ii/n_mdls))+(1:mdl.num_coefs), coefs(c, :), 'Color', pickcolor(c), 'LineStyle', pickline(ii));
                names{end+1} = ['PS' num2str(ii) '/CH' num2str(c)];
            end
        end
        hold off;
        axis tight;
        do_xlabel('Coef Time Index');
        do_ylabel('Coef Magnitude');
        legend(handles, names{:}, 'Location', leg_location);
    end

end


