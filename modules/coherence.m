function m = coherence(args)
% A function to compute the coherence between two signals
%
% Returns a function module 'm' which implements the MODULE interface.
% See docs/modules.org for more information.

% Module fields that must ALWAYS be defined
m = [];
m.mdl = @coherence;
m.name = 'coherence';
m.fn = @do_coherence;
m.pretty_name = 'Coherence';
m.editable_fields = {'input1', 'input2', 'nfft_coherence', 'time', 'train_score', 'test_score', 'output'};
m.isready_pred = @isready_always;

% Module fields that are specific to THIS MODULE
m.input1 = 'stim';
m.input2 = 'respavg';
m.time   = 'stim_time';
m.nfft_coherence   = 256;
m.train_score = 'score_train_coherence';
m.test_score = 'score_test_coherence';
m.output = 'score_train_coherence';

% Overwrite the default module fields with arguments 
if nargin > 0
    m = merge_structs(m, args);
end

% Optimize this module for tree traversal  
m.required = {m.input1, m.input2, m.time};   % Signal dependencies
m.modifies = {m.output, m.train_score, m.test_score};  % These signals are modified

% Optional fields
m.plot_fns = {};
m.auto_plot = @do_plot_coherence;
m.plot_fns{1}.fn = @do_plot_coherence;
m.plot_fns{1}.pretty_name = 'Coherence';
m.plot_fns{2}.fn = @do_plot_coherence_phase;
m.plot_fns{2}.pretty_name = 'Coherence Phase';

function x = do_coherence(mdl, x)      
    
    tim  = x.dat.(x.training_set{1}).(mdl.time)(:);
    fs = 1 / (tim(2) - tim(1));    
    
    % Compute the training set coherence, ignoring nans
        
    p = flatten_field(x.dat, x.training_set, mdl.input1);
    q = flatten_field(x.dat, x.training_set, mdl.input2); 
    gamma_train= mscohere(p, q, hanning(mdl.nfft_coherence), mdl.nfft_coherence/2, mdl.nfft_coherence, fs);        
    x.(mdl.train_score) = mean(gamma_train);
    
    % Compute the test set coherence, ignoring nans
    if ~isempty(x.test_set)
        ptest = flatten_field(x.dat, x.test_set, mdl.input1);
        qtest = flatten_field(x.dat, x.test_set, mdl.input2);     
        if(isempty(qtest) && isempty(ptest))
            gamma_test=NaN;
        else
            gamma_test = mscohere(ptest, qtest, hanning(mdl.nfft_coherence), mdl.nfft_coherence/2, mdl.nfft_coherence, fs);
        end
        x.(mdl.test_score) = mean(gamma_test);
    else
        x.(mdl.test_score) = NaN;   
    end    
end    

function do_plot_coherence(sel, stack, xxx)
    [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end)); 
    xout = xouts{1};
    mdl = mdls{1};
    
    train1 = flatten_field(xout.dat, xout.training_set, (mdls{1}.input1));
    train2 = flatten_field(xout.dat, xout.training_set, (mdls{1}.input2));       
    test1 = flatten_field(xout.dat, xout.test_set, (mdls{1}.input1));
    test2 = flatten_field(xout.dat, xout.test_set, (mdls{1}.input2));
    
    tim  = xout.dat.(xout.training_set{1}).(mdls{1}.time)(:);
    fs = 1 / (tim(2) - tim(1));
    n_fft = mdl.nfft_coherence;
    n_overlap = n_fft/2;
    
    % Slow calculation: takes 0.75 seconds
    [Ptest, Ftest] = mscohere(test1, test2, hanning(n_fft), n_overlap, n_fft, fs);
    [Ptrain, Ftrain] = mscohere(train1, train2, hanning(n_fft), n_overlap, n_fft, fs);
    
    plot(Ftest, Ptest, 'r-', Ftrain, Ptrain, 'b-'); 
    do_xlabel('Frequency [Hz]');
    do_ylabel('Magnitude-Squared Coherence');
    legend('Val', 'Est');
        
    textLoc(sprintf('Est Mean = %0.3f\nVal Mean =%0.3f', ...
        mean(Ptrain(:)), mean(Ptest(:))), 'North', 'Interpreter', 'none');
    

end

function do_plot_coherence_phase(sel, stack, xxx)
    [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end)); 
    xout = xouts{1};
    mdl = mdls{1};
    
    train1 = flatten_field(xout.dat, xout.training_set, (mdls{1}.input1));
    train2 = flatten_field(xout.dat, xout.training_set, (mdls{1}.input2));       
    test1 = flatten_field(xout.dat, xout.test_set, (mdls{1}.input1));
    test2 = flatten_field(xout.dat, xout.test_set, (mdls{1}.input2));
    
    tim  = xout.dat.(xout.training_set{1}).(mdls{1}.time)(:);
    fs = 1 / (tim(2) - tim(1));
    n_fft = mdl.nfft_coherence;
    n_overlap = n_fft/2;
    
    % Getting the phase (cross power spectral density)
    % Slow calculation: Takes 0.3 seconds
    [Ctest, Ftest] = cpsd(test1,test2, hamming(n_fft), n_overlap, n_fft, fs);    
    [Ctrain, Ftrain] = cpsd(train1,train2, hamming(n_fft), n_overlap, n_fft, fs);       
    plot(Ftest, -angle(Ctest), 'r-', Ftrain, -angle(Ctrain), 'b-');
    legend('Val', 'Est');
    do_ylabel('Phase Angle [Rad]');
    do_xlabel('Frequency [Hz]');

end

end
