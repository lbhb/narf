function cellfiledata=dbgettspfiles(cellid,batch)

    global NARF_DEBUG
    
% find a file that matches specific behavioral selection
% criteria for this batch
[batchactivefiles,~,params]=dbbatchcells(batch,cellid);
firstactiveidx=[];
params.activeonly=getparm(params,'activeonly',0);
batchrawids=cat(1,batchactivefiles.rawid);

cellfiledata=dbgetscellfile('cellid',cellid,'runclassid',[42 8 103]);
filecount=length(cellfiledata);

% special case, find only matching behavior sets
%runclassid=cat(2,cellfiledata.runclassid);
%stimspeedid=cat(2,cellfiledata.stimspeedid);
activefile=zeros(1,length(cellfiledata));
batchfile=zeros(1,length(cellfiledata));
stimprint=zeros(filecount,5);

for ii=1:length(cellfiledata),
    [parms, ~]=dbReadData(cellfiledata(ii).rawid);
    if cellfiledata(ii).rawid==batchrawids(1),
        % first active that matches criteria for this batch
        firstactiveidx=ii;
    end
    
    if strcmpi(cellfiledata(ii).behavior,'active'),
        activefile(ii)=1;
    end
    if ismember(cellfiledata(ii).rawid,batchrawids),
       batchfile(ii)=1;
    end
    % "fingerprint" is subset and frequency range.  Need to match
    % between passive and active
    %if params.activeonly,
    %    thisprint=[parms.Ref_Subsets parms.Ref_LowFreq ...
    %               parms.Ref_HighFreq activefile(ii)];
    %else
        thisprint=[parms.Ref_Subsets parms.Ref_LowFreq parms.Ref_HighFreq];
    %end
    stimprint(ii,1:length(thisprint))=thisprint;
end
if strcmpi(params.specialbatch,'tsp-onbf') || ...
      strcmpi(params.specialbatch,'tsp-offbf') ||...
      strcmpi(params.specialbatch,'tsp-onbfpas') || ...
      strcmpi(params.specialbatch,'tsp-offbfpas')|| ...
      strcmpi(params.specialbatch,'tsp-pass'),
    % first look for pre-passive:
    printmatch=~activefile(:) & double(sum(abs(...
        stimprint-repmat(stimprint(firstactiveidx,:),filecount,1)),2)==0);
    prepas=find(printmatch(1:firstactiveidx), 1, 'last' );
    % SVD 2015-01-18: only include post-passive if pre missing
    % SVD 2018-05 : always include pospas now, for NEMS
    %if isempty(prepas)
        pospas=find(printmatch(firstactiveidx:end), 1 )+firstactiveidx-1;
    %else
    %    pospas=[];
    %end
    
    stimprint=cat(2,stimprint,zeros(length(cellfiledata),1));
    stimprint([prepas pospas find(batchfile)],end)=1;
end

if isempty(firstactiveidx),
    error('no active TSP data for this cell');
end
printmatch=double(sum(abs(...
    stimprint-repmat(stimprint(firstactiveidx,:),filecount,1)),2)==0);
useidx=find(printmatch);
if strcmpi(params.specialbatch,'tsp-onbfpas') || ...
      strcmpi(params.specialbatch,'tsp-offbfpas'),
   % special case, only want the passive files that would have been used in
   % tsp-onbf -offbf conditions
   useidx=setdiff(useidx,find(activefile));
end
if NARF_DEBUG,
    iso=cat(1,cellfiledata.isolation);
    useidx=find(printmatch & iso>params.miniso);
end

cellfiledata=cellfiledata(useidx);

 
