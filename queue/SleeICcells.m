function [c,r,g]=SleeICcells(reload,updatescellfile)

persistent ICcellids
persistent rawids
persistent goodreps

if exist('reload','var') && reload
   ICcellids={};
end
if ~exist('updatescellfile','var')
   updatescellfile=0;
end

if isempty(ICcellids)
   
   disp('finding matches in Slee database')
   metapath='/auto/users/svd/data/slee/Aim7Analysis/IC_DataBase3';
   metapath2='/auto/users/svd/data/slee/Aim7Analysis/MultiTarDataBase';
   rawids=[];
   goodreps={};
   ICcellids={};
   for jj=1:27
      %----------------------------
      %load analysis file
      fname = sprintf('%s/IC_Data_File_%d',metapath2,jj);
      fprintf('%s\n',fname);
      load(fname);
      
      sql=['SELECT * FROM gDataRaw',...
         ' WHERE parmfile like "',ICDataFile.PreFileName,'%"'];
      rawdata=mysql(sql);
      rawids=cat(2,rawids,rawdata.id);
      goodreps{end+1}=ICDataFile.PreRepRng;

      sql=['SELECT * FROM gDataRaw',...
         ' WHERE parmfile like "',ICDataFile.OnBFTarFileName,'%"'];
      rawdata=mysql(sql);
      rawids=cat(2,rawids,rawdata.id);
      goodreps{end+1}=ICDataFile.OnBFTarRepRng;

      sql=['SELECT * FROM gDataRaw',...
         ' WHERE parmfile like "',ICDataFile.OffBFTarFileName,'%"'];
      rawdata=mysql(sql);
      rawids=cat(2,rawids,rawdata.id);
      goodreps{end+1}=ICDataFile.OffBFTarRepRng;
      if ~isempty(ICDataFile.PostFileName)
         sql=['SELECT * FROM gDataRaw',...
            ' WHERE parmfile like "',ICDataFile.PostFileName,'%"'];
         rawdata=mysql(sql);
         rawids=cat(2,rawids,rawdata.id);
         goodreps{end+1}=ICDataFile.PostRepRng;
      end
      
      siteid=ICDataFile.datID(1:min(findstr(ICDataFile.datID,'Ch')-1));
      channel=char(str2num(ICDataFile.datID(end-2))-1+'a');
      unit=(ICDataFile.datID(end));
      cellid=lower([siteid '-' channel unit]);
      if strcmpi(cellid,'btn25b-a1'), cellid='btn025b-a1'; end
      if strcmpi(cellid,'btn26a-a1'), cellid='btn026a-a1'; end
      if strcmpi(cellid,'btn33a-a1'), cellid='btn033a-a1'; end
      if strcmpi(cellid,'btn33b-a1'), cellid='btn033b-a1'; end
      if strcmpi(cellid,'btn33c-a1'), cellid='btn033c-a1'; end
      if strcmpi(cellid,'btn086cd-a1'), cellid='btn086d-a1'; end
      if strcmpi(cellid,'btn0111b-b1'), cellid='btn111b-b1'; end
      ICcellids{end+1} = cellid;
   end
   % sean IC data, screen for matches to his onBF list
   for jj=1:95;
      
      %----------------------------
      %load analysis file
      fname = sprintf('%s/IC_Data_File_%d',metapath,jj);
      fprintf('%s\n',fname);
      load(fname);
      
      siteid=ICDataFile.datID(1:min(findstr(ICDataFile.datID,'Ch')-1));
      channel=char(str2num(ICDataFile.datID(end-2))-1+'a');
      unit=(ICDataFile.datID(end));
      cellid=lower([siteid '-' channel unit]);
      if strcmpi(cellid,'btn25b-a1'), cellid='btn025b-a1'; end
      if strcmpi(cellid,'btn26a-a1'), cellid='btn026a-a1'; end
      if strcmpi(cellid,'btn33a-a1'), cellid='btn033a-a1'; end
      if strcmpi(cellid,'btn33b-a1'), cellid='btn033b-a1'; end
      if strcmpi(cellid,'btn33c-a1'), cellid='btn033c-a1'; end
      if strcmpi(cellid,'btn0111b-b1'), cellid='btn111b-b1'; end
      
      if sum(strcmpi(cellid,ICcellids))==0
         sql=['SELECT * FROM gDataRaw',...
            ' WHERE parmfile like "',ICDataFile.PreFileName,'%"'];
         rawdata=mysql(sql);
         rawids=cat(2,rawids,rawdata.id);
         goodreps{end+1}=ICDataFile.PreRepRng;
         
         sql=['SELECT * FROM gDataRaw',...
            ' WHERE parmfile like "',ICDataFile.OnBFTarFileName,'%"'];
         rawdata=mysql(sql);
         rawids=cat(2,rawids,rawdata.id);
         goodreps{end+1}=ICDataFile.OnBFTarRepRng;
         
         ICcellids{end+1} = cellid;
      else
         fprintf('cellid %s already in on-off set, skipping\n',cellid);
      end
   end
end

if updatescellfile
   for ridx=1:length(rawids)
      cfd=dbgetscellfile('rawid',rawids(ridx));
      respfile=[cfd(1).path cfd(1).respfile];
      options=struct('channel',cfd(1).channum,'unit',cfd(1).unit,...
         'rasterfs',100,'tag_masks',{{'Ref'}});
      [resp,tags,trialset]=loadspikeraster(respfile,options);
      tmin=min(min(trialset(goodreps{ridx},:)));
      tmax=max(max(trialset(goodreps{ridx},:)));
      if tmin>min(min(trialset)) || tmax<max(max(trialset))
         sql=sprintf('UPDATE sCellFile set goodtrials = "%d:%d" WHERE rawid=%d',...
            tmin,tmax,rawids(ridx))
         mysql(sql);
      end
   end   
end

c=ICcellids;
r=rawids;
g=goodreps;
