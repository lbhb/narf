% function r=dbkillqueue(queueidx,completevalue[=2])
% 
% queueid is the id of an unstarted job (complete=0) in tQueue
% queueid of -1 updates all jobs in queue (matching current user)
%
% completevalue 1 - mark as completed
% completevalue 2 - mark as dead
%
function dbmarkdead(queueidx,completevalue,user)

dbopen;

global DBUSER
if ~exist('user','var') | isempty(user),
   user=DBUSER;
end

if ~exist('completevalue','var'),
   completevalue=2;
end

if queueidx==-1,
   
   yn=input('mark ALL unstarted queue entries as dead (y/[n])??? ','s');
   if strcmp(yn(1),'y'),
      
      sql=['UPDATE tQueue set complete=',num2str(completevalue),...
           ' WHERE complete=0',...
           ' AND user like "',user,'"'];
      mysql(sql);
      
      dbqueue(-1);
   end
   return
end

sql=['SELECT * FROM tQueue WHERE id=',num2str(queueidx),...
     ' AND user like "',user,'"'];
queuedata=mysql(sql);

if isempty(queuedata),
   fprintf('ERROR: queueidx=%d not listed in tQueue.\n',queueidx);
elseif queuedata.complete~=0,
   fprintf('ERROR: queueidx=%d already started.\n',queueidx);
else
   sql=['UPDATE tQueue set complete=',num2str(completevalue),...
        ' where complete=0 AND id=',num2str(queueidx)];
   mysql(sql);
end


