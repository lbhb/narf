function export_batch_data(batchid)

RDT=0;

dbopen(1);

if batchid==296
   celldata=mysql(['SELECT * FROM sRunData WHERE cellid like "gus%" AND batch=',num2str(batchid)]);
else
   celldata=mysql(['SELECT * FROM sRunData WHERE batch=',num2str(batchid)]);
end

fs=200;
%fs=100;
all_cells=0;
per_trial=0;

% set export parameters specific to batch
if ismember(batchid,[259,283])
   stimfmt='envelope';
   chancount=0;
   fs=100;
   
elseif ismember(batchid,[230,298,301,302,303,304])
   stimfmt='parm';
   chancount=0;
   if ismember(batchid,[301 303])
       fs=100;
   else
       fs=100;
   end
   per_trial=0;
   
elseif batchid==296
   stimfmt='envelope';
   chancount=0;
   fs=100;
   
elseif batchid==294
   stimfmt='ozgf';
   chancount=24;
   fs=100;
   
elseif batchid==293
   stimfmt='parm';
   chancount=16;
   
elseif batchid==299
   stimfmt='none';
   chancount=0;
   fs=100;
   all_cells=1;
   per_trial=1;
   
elseif ismember(batchid,[263,271,272,291,289])
   %stimfmt='none';
   %chancount=0;
   
   stimfmt='ozgf';
   %chancount=93;
   chancount=18;
   fs=100;
elseif ismember(batchid,[259,278,283])
   stimfmt='envelope';
   chancount=0;
   fs=100;
elseif batchid==301
   stimfmt='ozgf';
   fs=100;
   chancount=18;
   
elseif ismember(batchid,[269,273,284,285]),
   stimfmt='ozgf';
   fs=100;
   chancount=18;
   per_trial=1;
   RDT=1;
else
   stimfmt='ozgf';
   %chancount=24;
   chancount=18;
   fs=100;
end

uselsr=0;
EXPORTFORMAT='full';
%EXPORTFORMAT='demo';

if uselsr && ~RDT
   % new way, just cache loadspikeraster data:
   cfd=dbbatchcells(batchid);
   options=struct('rasterfs',fs,'includeprestim',1);
   for ii=1:length(cfd)
      spkfile=[cfd(ii).path,cfd(ii).respfile];
      options.channel=cfd(ii).channum;
      options.unit=cfd(ii).unit;
      if batchid==294
         options.runclass='VOC';
      end
      if ismember(batchid, [301,304])
         options.tag_masks={'Reference'};
      end
      loadspikeraster(spkfile,options);
      
      parmfile=[cfd(ii).stimpath,cfd(ii).stimfile];
      poptions=options;
      poptions.pupil=1;
      poptions.pupil_median=1;
      poptions.pupil_deblink=1;
      poptions.pupil_offset=0.75;
      loadevpraster(parmfile,poptions);
      
      poptions.pupil_derivative='pos';
      loadevpraster(parmfile,poptions);
      
      poptions.pupil_derivative='neg';
      loadevpraster(parmfile,poptions);
      
   end
   
elseif uselsr
   % go through each cell in the batch and generate export file
   for cellidx=1:length(celldata)
      cellid=celldata(cellidx).cellid;
      outfile=export_cellfile(batchid,cellid,fs,stimfmt,chancount,per_trial,all_cells,1);
      load(outfile);
      fprintf('saving RDT data in simplified format\n');
      [pp,bb]=fileparts(outfile);
      for ii=1:length(data)
         [pps,bbs]=fileparts(data(ii).fn_param);

         % -break each stim into stim, stim1, stim2, save state variables
         % single file for single experiment, even if there are multiple
         % cells
         stimfile_out=sprintf('%s%s%s_b%d_stim_pertrial-%s-fs%d-ch%d-incps1.mat',...
            pp,filesep,bbs,batchid,stimfmt,fs,chancount);
         stimfile=data(ii).fn_param;
         stimparam=data(ii).stimparam;
         stim1=data(ii).stim(:,:,:,1);
         stim2=data(ii).stim(:,:,:,2);
         stim=data(ii).stim(:,:,:,3);
         repeating_phase=data(ii).state(:,:,:,1);
         singlestream=data(ii).state(:,:,:,2);
         targetid=data(ii).state(:,:,:,3);
         fs=data(ii).stimfs;
         tags=data(ii).tags;
         filestate=data(ii).filestate;
         save(stimfile_out,'stim','stim1','stim2','stimparam','stimfile',...
            'repeating_phase','singlestream','targetid','fs','tags','filestate');
         
         % save spike data in a separate file, same directory, but with
         % cellid as part of a unique filename
         respfile=sprintf('%s%s%s_%s_b%d_resp_pertrial_fs%d_tags-Reference_run-all_prestim-1_correctTrials_psth-1.mat',...
            pp,filesep,bbs,cellid,batchid,fs);
         r=data(ii).resp_raster;
         spkfile=data(ii).fn_spike;
         isolation=data(ii).isolation;
         fs=data(ii).respfs;
         save(respfile,'r','cellid','isolation','spkfile','fs');
         
      end
      fprintf('saved %d dataset(s) for cellid %s\n\n',length(data),cellid);
   end
else
   % go through each cell in the batch and generate export file
   for cellidx=1:length(celldata)
      cellid=celldata(cellidx).cellid;
      export_cellfile(batchid,cellid,fs,stimfmt,chancount,per_trial,all_cells,0);
   end
end
