%function res=spn_tuning_match(cellid,rawid/batchid,verbose)
%   
function res=spn_tuning_match2(cellid,batchid,verbose)
    
    if ~exist('verbose','var'),
        verbose=1;
    end
    
    dbopen;
    
    if iscell(cellid),
       cellids=cellid;
    else
       cellids={cellid};
    end
    modelnames={'env100_log2_dep1pcno_fir15_zsoft_fit05h'};
    try
       strfs=get_strf_from_model(batchid,cellids,modelnames);
       if ~isempty(strfs),
          res=sum(strfs{1}(:,1:5),2)./sum(sum(abs(strfs{1}(:,1:5))));
       end
    catch
       res=spn_tuning_match(cellid,batchid,verbose);
    end
    
    
