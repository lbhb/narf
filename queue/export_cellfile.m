function outfile=export_cellfile(batchid,cellid,fs,stimfmt,chancount,per_trial,all_cells,FORCEREGENERATE,EXPORTFORMAT)

RDT_batches=[269,273,284,285];

if ~exist('FORCEREGENERATE','var'),
   % if 1, regenerate cache file even if it already exists
   FORCEREGENERATE=0;
end
if ~exist('per_trial','var'),
   % if 1, load responses per trial, don't save stimulus, just stimulus
   % event times
   per_trial=0;
end
if ~exist('EXPORTFORMAT','var')
   EXPORTFORMAT='full';
end
demopath='/auto/data/code/nems_demo_data/';
rootpath='/auto/data/code/nems_in_cache/';
outpath=sprintf('%sbatch%03d/',rootpath,batchid);
if ~exist(outpath,'dir')
   fprintf('creating directory %s\n',outpath);
   mkdir(rootpath,sprintf('batch%03d',batchid));
end

dbopen(1);

if ~exist('cellid','var')
   celldata=mysql(['SELECT * FROM sRunData WHERE batch=',num2str(batchid)]);
else
   celldata=mysql(['SELECT * FROM sRunData WHERE batch=',num2str(batchid),...
      ' AND cellid="',cellid,'"']);
end

if ~exist('fs','var')
   fs=200;
end

if ~exist('stimfmt','var')
   if batchid==283
      stimfmt='envelope';
   else
      stimfmt='ozgf';
   end
end
if ~exist('chancount','var'),
   if batchid==283 || batchid==296,
      chancount=2;
      
   else
      chancount=24;
      
   end
end

stimfs=fs;
respfs=fs;
data=struct();

for cellidx=1:length(celldata),
   cellid=celldata(cellidx).cellid;
   
   %cellfiledata=dbgetscellfile('cellid',cellid,'runclassid',8);
   % hard-coded for only center-surround data
   %[cellfiledata,times,params]=cellfiletimes(cellid,batchid);
   cellfiledata=dbbatchcells(batchid,cellid);
   filecount=length(cellfiledata);
   isolation=cellfiledata(1).isolation;
   
   if per_trial,
      ptstring='_pertrial';
   else
      ptstring='';
   end
   if strcmpi(stimfmt,'parm') || strcmpi(stimfmt,'envelope') || strcmpi(stimfmt,'none'),
      cstring='';
   else
      cstring=sprintf('_c%d',chancount);
   end
   outfile=sprintf('%s%s_b%d%s_%s%s_fs%d.mat',outpath,cellid,batchid,ptstring,stimfmt,cstring,fs);
   
   if ~FORCEREGENERATE && exist(outfile,'file') && ~strcmpi(EXPORTFORMAT,'demo')
      fprintf('Cache file %s already exists\n',outfile);
      
   elseif strcmpi(EXPORTFORMAT,'full') || strcmpi(EXPORTFORMAT,'demo')
      keepfile=zeros(filecount,1);
      for ff=1:filecount,
         if batchid==263 || isempty(findstr('VOC_VOC',cellfiledata(ff).stimfile)),
            keepfile(ff)=1;
         end
      end
      cellfiledata=cellfiledata(find(keepfile));
      filecount=length(cellfiledata);
      activecount=0;
      for ff=1:filecount

         data(ff).fn_spike=[cellfiledata(ff).path cellfiledata(ff).respfile];
         data(ff).fn_param=[cellfiledata(ff).stimpath cellfiledata(ff).stimfile];
         LoadMFile(data(ff).fn_param);
         baphyparm=dbReadData(cellfiledata(ff).rawid);
         if cellfiledata(ff).pupil==2
            PUPIL=1;
         else
            PUPIL=0;
         end
         
         data(ff).cellid=cellid;
         data(ff).isolation=isolation;
         data(ff).stimfs=stimfs;
         data(ff).respfs=respfs;
         data(ff).stimchancount=chancount;
         data(ff).stimfmt=stimfmt;
         data(ff).stimparam=baphyparm;
         
         if batchid==267 && exptparams.TrialObject.ReferenceHandle.Subsets==5, 
             data(ff).estfile=0;
         elseif batchid==267,
             data(ff).estfile=1;
         end
         
         %       data(ff).zvector=[zeros(round(ref.PreStimSilence.*data(ff).stimfs),1);
         %          ones(round(ref.Duration.*data(ff).stimfs),1);
         %          zeros(round(ref.PostStimSilence.*data(ff).stimfs),1)];
         %       data(ff).zvector=repmat(data(ff).zvector,[ref.MaxIndex,1]);
         
         options=struct();
         options.channel=cellfiledata(ff).channum;
         options.unit=cellfiledata(ff).unit;
         options.rasterfs=data(ff).respfs;
         options.psthonly=-1;
         options.includeprestim=1;
         if ismember(batchid,[230,301,295,297,298,302,303,304])
            options.tag_masks={'Reference'};
         end
         if batchid==294
            options.runclass='VOC';
         end
         ref=exptparams.TrialObject.ReferenceHandle;
         SoundHandle = 'ReferenceHandle';
         if (batchid==294 || batchid==299) && ~strcmpi(ref.descriptor,'FerretVocal'),
            ref=exptparams.TrialObject.TargetHandle;
            SoundHandle = 'TargetHandle';
         end
        
         if batchid==296
            options.tag_masks={'SPECIAL-TRIAL'};
            [data(ff).resp_raster,rtags,rtrials]=loadspikeraster(data(ff).fn_spike,options);
            Frequencies=exptparams.TrialObject.ReferenceHandle.Frequencies;
            s=size(data(ff).resp_raster);
            stim=zeros(2,s(1),s(2));
            [t,trial,Note,toff] = evtimes(exptevents,'Stim ,*');
            
            disp('converting events to envelopes')
            for evidx=1:length(t),
               s=strsep(Note{evidx},',');
               if ~isnumeric(s{2}),
                  s=strsep(s{2},'+');
               else
                  s=s(2);
               end
               dd=abs(Frequencies-s{1});
               fidx=find(dd==min(dd), 1 );
               bin_on=round(t(evidx).*fs);
               bin_off=round(toff(evidx).*fs)-1;
               stim(fidx,bin_on:bin_off,trial(evidx))=1;
            end
            data(ff).stim=stim;
            data(ff).resp_raster=permute(data(ff).resp_raster,[1 3 2]);
            
         elseif batchid==298
            
            options.tag_masks={'Reference'};
            [ref_raster,rtags,rtrials]=loadspikeraster(data(ff).fn_spike,options);
            options.tag_masks={'Target'};
            [tar_raster,ttags,ttrials]=loadspikeraster(data(ff).fn_spike,options);
            
            data(ff).resp_raster=merge_rasters(ref_raster,tar_raster,3);
            data(ff).tags.tags={rtags{:} ttags{:}};
            if strcmpi(stimfmt,'none'),
                data(ff).stim=[];
            else
                [data(ff).stim,data(ff).tags]=loadstimfrombaphy(data(ff).fn_param,[],[],...
                   stimfmt,stimfs,chancount,0,1);
                data(ff).tags.tags=rtags;
            end
         elseif ~isempty(findstr('VOC_VOC',cellfiledata(ff).stimfile))
            % clean + noisy ref/tar
            options.tag_masks={'Reference1'};
            [ref1_raster,r1tags,r1trials]=loadspikeraster(data(ff).fn_spike,options);
            options.tag_masks={'Reference2'};
            [ref2_raster,r2tags,r2trials]=loadspikeraster(data(ff).fn_spike,options);
            data(ff).resp_raster=merge_rasters(ref1_raster,ref2_raster,3);
            %data(ff).tags.tags={rtags{:} ttags{:}};
            
            [stim1,tags1]=loadstimfrombaphy(data(ff).fn_param,[],[],...
               stimfmt,stimfs,chancount,0,1,'ReferenceHandle');
            [stim2,tags2]=loadstimfrombaphy(data(ff).fn_param,[],[],...
               stimfmt,stimfs,chancount,0,1,'TargetHandle');
            data(ff).stim=merge_rasters(stim1,stim2,3);
            data(ff).tags=tags1;
            
            tags=cat(2,r1tags,r2tags);
            data(ff).tags.tags=tags;
            
            
         elseif ismember(batchid,RDT_batches) && per_trial
            
            options.tag_masks={'SPECIAL-TRIAL'};
            options.includeincorrect=1;
            [data(ff).resp_raster,~,stimtrials]=loadspikeraster(data(ff).fn_spike,options);
            
            stimoptions=struct('filtfmt',stimfmt,'fsout',stimfs,...
               'chancount',chancount);
            %truncatetargets=getparm(options,'truncatetargets',1);
            [data(ff).stim,stimparams]=loadstimbytrial(data(ff).fn_param,stimoptions);
            data(ff).resp_raster=data(ff).resp_raster(1:size(data(ff).stim,2),:,:);
            
            % insert dummy repetition dimension since this is per-trial
            % data
            data(ff).resp_raster=permute(data(ff).resp_raster,[1 4 2 3]);

            
            data(ff).tags=stimparams;
            
            state=zeros([size(data(ff).resp_raster),3]);
            single_stream_trials=find(stimparams.BigStimMatrix(1,2,:)==-1);
            state(:,:,single_stream_trials,2)=1;
            trialcount=size(data(ff).resp_raster,3);
            prebins=baphyparm.Trial_PreTrialSilence*stimfs;
            samplebins=baphyparm.Ref_Duration*stimfs;
            for trialidx=1:trialcount
               rslot=min(find(diff(stimparams.BigStimMatrix(:,1,trialidx))==0));
               rbin=prebins+rslot*samplebins+1;
               state(rbin:end,:,trialidx,1)=1;
               tarslot=max(find(stimparams.BigStimMatrix(:,1,trialidx)>0));
               state(:,:,trialidx,3)=stimparams.BigStimMatrix(tarslot,1,trialidx);
            end
            data(ff).state=state;
            data(ff).tags.state_names={'repeating_phase','single_stream','targetid'};
            
%             trialidx=3;
%             figure;
%             subplot(2,1,1);
%             imagesc(data(ff).stim(:,:,trialidx,1));
%             subplot(2,1,2);
%             plot(squeeze(state(:,trialidx,:)));
%             axis tight
%             set(gca,'Ylim',[-0.1 1.1]);
            
            
         elseif all_cells || batchid==299
            sql=['SELECT * FROM gRunClass WHERE id=',num2str(cellfiledata(ff).runclassid)];
            rcdata=mysql(sql);
            options.runclass=rcdata.name;
            
            toptions=options;
            %toptions.tag_masks={'Reference2'};
            toptions.channel=[];
            toptions.unit=[];
            [data(ff).resp_raster,rtags,rtrials,~,~,cellids]=loadsiteraster(data(ff).fn_spike,toptions);
            
%             if isempty(data(ff).resp_raster),
%                toptions.tag_masks={'Reference'};
%                [data(ff).resp_raster,rtags,rtrials,~,~,cellids]=loadsiteraster(data(ff).fn_spike,toptions);
%             end
            
            data(ff).cellids=cellids;
            
            % first see if stim was in target
            if batchid==294 || batchid==299,
               [data(ff).stim,data(ff).tags]=loadstimfrombaphy(data(ff).fn_param,[],[],...
                  stimfmt,stimfs,chancount,0,1,'TargetHandle');
            else
               [data(ff).stim,data(ff).tags]=loadstimfrombaphy(data(ff).fn_param,[],[],...
                  stimfmt,stimfs,chancount,0,1,'ReferenceHandle');
            end
            %options.tag_masks=toptions.tag_masks; % propogate to poptions
            
         elseif ~strcmpi(stimfmt,'none'),
            [data(ff).resp_raster,rtags,rtrials]=loadspikeraster(data(ff).fn_spike,options);
            
            % load stim with pre/post-stimsilence
            %spnw=loadstimfrombaphy(data(ff).fn_param,[],[],'wav',16000,1,0,1);
            [data(ff).stim,data(ff).tags]=loadstimfrombaphy(data(ff).fn_param,[],[],...
               stimfmt,stimfs,chancount,0,1,SoundHandle);
            data(ff).tags.tags=rtags;
         else
            [data(ff).resp_raster,rtags,rtrials]=loadspikeraster(data(ff).fn_spike,options);
            
            data(ff).stim=[];
            data(ff).tags.tags=rtags;
         end
         
         if per_trial && ~ismember(batchid,RDT_batches)
            options.tag_masks={'SPECIAL-TRIAL'};
            options.includeincorrect=1;
            if all_cells || batchid==299
               toptions.tag_masks={'SPECIAL-TRIAL'};
               toptions.includeincorrect=1;
               [alt_resp,~,stimtrials,~,~,cellids]=loadsiteraster(data(ff).fn_spike,toptions);
            else
               [alt_resp,~,stimtrials]=loadspikeraster(data(ff).fn_spike,options);
            end
            
            [t,trial,Note]=evtimes(exptevents,'PreStimSilence*');
            evcount=length(t);
            stimids=zeros(evcount,1);
            stimtrials=zeros(evcount,1);
            stimtimes=zeros(evcount,1);
            keepidx=zeros(evcount,1);
            for ii=1:evcount
                %s=strsep(Note{ii},',',1);
                %s=strtrim(s{2});
                stimidx=find(strcmp(data(ff).tags.tags,Note{ii}));
                if ~isempty(stimidx)
                    stimids(ii)=stimidx;
                    stimtimes(ii)=t(ii);
                    stimtrials(ii)=trial(ii);
                    keepidx(ii)=1;
                else
                    fprintf('Adding stim tag %s\n',Note{ii});
                    data(ff).tags.tags{end+1}=Note{ii};
                    stimids(ii)=length(data(ff).tags.tags);
                    stimtimes(ii)=t(ii);
                    stimtrials(ii)=trial(ii);
                    keepidx(ii)=1;
                end
                
                % nan-out target periods?
                s=strsep(Note{ii},',',1);
                s=strtrim(s{3});
                if ~isempty(findstr(s,'Target')),
                    %fprintf('naning target on trial %d\n',trial(ii));
                    stimbin=round(stimtimes(ii).*options.rasterfs)+1;
                    alt_resp(stimbin:end,stimtrials(ii))=nan;
                end
                
            end
            [t,trial]=evtimes(exptevents,'LICK*');
            evcount=length(t);
            for ii=1:evcount        
                stimbin=round(t(ii).*options.rasterfs)+1;
                alt_resp(stimbin:end,trial(ii))=nan;
            end
            
%             stimids=zeros(size(alt_resp,2),1);
%             keepidx=zeros(size(stimids));
%             for ii=1:length(keepidx),
%                [rep,st]=find(rtrials==ii);
%                if ~isempty(st),
%                   stimids(ii)=st;
%                   stimtrials(ii)=ii;
%                   keepidx(ii)=1;
%                end
%             end
%
%            alt_resp(:,find(~keepidx),:)=nan;

            %truncate nans at end of all trials
            rm=nanmean(alt_resp(:,:),2);
            rm=max(find(~isnan(rm)));
            alt_resp=alt_resp(1:rm,:,:,:);

            alt_resp=permute(alt_resp,[1 4 2 3]);
            
            data(ff).resp_raster=alt_resp;
            data(ff).stimids=stimids(find(keepidx));
            data(ff).stimids=data(ff).stimids-min(data(ff).stimids)+1;
            data(ff).stimtrials=stimtrials(find(keepidx));
            data(ff).stimtimes=stimtimes(find(keepidx));
         end
         
         
         % file-wide state
         if batchid==296
            % SSA jittered vs. non-jittered stim times
            if isfield(exptparams.TrialObject.ReferenceHandle, 'Jitter') &&...
                  strcmpi(strtrim(exptparams.TrialObject.ReferenceHandle.Jitter),'On'),
               data(ff).filestate=1;
            else
               data(ff).filestate=0;
            end
            
         elseif batchid==263
            % set tags to reflect SNR
            tags=data(ff).tags.tags;
            if ~isfield(data(ff).stimparam,'Ref_SNR') || data(ff).stimparam.Ref_SNR>=100
               snr_str='Inf dB';
            else
               snr_str=sprintf('%d dB',data(ff).stimparam.Ref_SNR);
            end
            try
               if data(ff).stimparam.Tar_SNR>=100
                  snr_str2='Inf dB';
               else
                  snr_str2=sprintf('%d dB',data(ff).stimparam.Tar_SNR);
               end
            catch
               snr_str2='Reference';
            end
            
            for jj=1:length(tags)
               if ~isempty(tags{jj})
                  tags{jj}=strrep(tags{jj},'Reference1',snr_str);
                  tags{jj}=strrep(tags{jj},'Reference2',snr_str2);
                  tags{jj}=strrep(tags{jj},'Reference',snr_str);
               end
            end
            data(ff).tags.tags=tags;
            
         elseif ismember(batchid,[298,301,302,303,304])
            % BVT or PTD (TIN)  active vs. passive
            if isfield(exptparams,'BehaveObjectClass') && ...
               strcmpi(exptparams.BehaveObjectClass,'Passive'),
               data(ff).filestate=0;  % passive
            else
               % increment counter to distinguish different active files
               activecount=activecount+1;
               data(ff).filestate=activecount;
            end
            
         else
            % everything else, no file-wide state info
            data(ff).filestate=0;
         end
         
         % extra parameter data
         if ismember(batchid,RDT_batches)
            data(ff).tags.PreStimSilence=baphyparm.Trial_PreTrialSilence;
            data(ff).tags.Duration=ref.Duration;
            data(ff).tags.PostStimSilence=baphyparm.Trial_PostTrialSilence;
         else
            data(ff).tags.PreStimSilence=ref.PreStimSilence;
            data(ff).tags.Duration=ref.Duration;
            data(ff).tags.PostStimSilence=ref.PostStimSilence;
         end         
         
         % remove nan-only stimuli
         keepidx=find(~isnan(data(ff).resp_raster(1,1,:,1)));
         data(ff).resp_raster=data(ff).resp_raster(:,:,keepidx,:);
         if ~per_trial && ~isempty(data(ff).stim) && ...
               size(data(ff).stim,3)>length(keepidx)
            data(ff).stim=data(ff).stim(:,:,keepidx);
            
            try
               data(ff).tags.tags=data(ff).tags.tags(keepidx);
            catch
               disp('could not trim nan-trial tags');
            end
         end
         
         if PUPIL
            poptions=options;
            poptions.pupil=1;
            poptions.pupil_median=1;
            poptions.pupil_offset=0.75;
            data(ff).tags.state_names={'pupil'};
            
            if batchid==296
               [data(ff).pupil]=loadevpraster(data(ff).fn_param,poptions);
               % reshape per-trial matrix to have "repetition" dimension
               data(ff).pupil=permute(data(ff).pupil,[1 3 2]);            
            elseif batchid==298 && ~per_trial
               poptions.tag_masks={'Reference'};
               [r_pupil]=loadevpraster(data(ff).fn_param,poptions);
               poptions.tag_masks={'Target'};
               [t_pupil]=loadevpraster(data(ff).fn_spike,poptions);
               data(ff).pupil=merge_rasters(r_pupil,t_pupil,3);
            else
               p1=loadevpraster(data(ff).fn_param,poptions);
               poptions.pupil_derivative='pos';
               p2=loadevpraster(data(ff).fn_param,poptions);
               poptions.pupil_derivative='neg';
               p3=loadevpraster(data(ff).fn_param,poptions);
               data(ff).pupil=cat(4,p1,p2,p3);
               data(ff).tags.state_names={'pupil','pupilDp','pupilDn'};
            end
            if per_trial
               data(ff).pupil=permute(data(ff).pupil,[1 3 2 4]);
            end
            if size(data(ff).pupil,3)<max(keepidx)
                s=size(data(ff).pupil);
                s(3)=max(keepidx)-size(data(ff).pupil,3);
                data(ff).pupil=cat(3,data(ff).pupil,nan(s));
            end
            
            data(ff).pupil=data(ff).pupil(:,:,keepidx,:);
            
            if per_trial
               % go through each cell and pad with zeros out to end of valid
               % pupil
               if size(data(ff).pupil,1)>size(data(ff).resp_raster,1)
                   data(ff).pupil=data(ff).pupil(1:size(data(ff).resp_raster,1),:,:,:);
               end
               validp=~isnan(data(ff).pupil(:,:,:,1));
               
               % this is deprecated?
               if size(validp,1)>size(data(ff).resp_raster,1)
                  s=size(data(ff).resp_raster);
                  s(1)=size(validp,1)-size(data(ff).resp_raster,1);
                  data(ff).resp_raster=cat(1,data(ff).resp_raster,nan(s));
               end
               
               % fill in zeros at end of trial when there were no spikes
               % but data was recorded?
               % BROKEN
               %for cid=1:size(data(ff).resp_raster,4)
               %   tr=data(ff).resp_raster(:,:,:,cid);
               %   validr=~isnan(tr);
               %   tr(validp & ~validr)=0;
               %   data(ff).resp_raster(:,:,:,cid)=tr;
               %end
            end
            data(ff).state=data(ff).pupil;
         end
      end
      
      if strcmpi(EXPORTFORMAT,'full')
         
         fprintf('exporting to %s\n',outfile);
         save(outfile,'cellid','data');
      else
         fprintf('Exporting simple demo files to %s\n',demopath);
         d=dbget('gRunClass',cellfiledata(1).runclassid);
         runclass=d.name;
         for ff=1:filecount
            if filecount>1
               fcstr=sprintf('_f%d',ff);
            else
               fcstr='';
            end
            stimfile=sprintf('%s%s_%s_stim_%s%s_fs%d%s.mat',demopath,cellid,runclass,stimfmt,cstring,fs,fcstr);
            respfile=sprintf('%s%s_%s_resp_fs%d%s.mat',demopath,cellid,runclass,fs,fcstr);
            pupilfile=sprintf('%s%s_%s_pupil_fs%d%s.mat',demopath,cellid,runclass,fs,fcstr);
            raster=permute(data(ff).resp_raster,[2,3,1]);
            psth=nanmean(raster,1);
            stim=permute(data(ff).stim,[1,3,2]);
            save(stimfile,'stim');
            save(respfile,'raster','psth');
            if isfield(data(ff),'pupil')
               pupil=permute(data(ff).pupil,[2,3,1]);
               save(pupilfile,'pupil');
            end
         end
      end
      
   elseif strcmpi(EXPORTFORMAT,'simple')
      % old format. not used any more??
      for ff=1:filecount
         fn_param=[cellfiledata(ff).stimpath cellfiledata(ff).stimfile];
         fn_spike=[cellfiledata(ff).path cellfiledata(ff).respfile];
         
         if ~strcmpi(stimfmt,'none')
            [stim,stim_params]=loadstimfrombaphy(fn_param,[],[],...
               stimfmt,stimfs,chancount,0,1);
         else
            stim=[];
            stim_params=struct();
         end
         
         LoadMFile(fn_param);
         
         ref=exptparams.TrialObject.ReferenceHandle;
         stim_params.StimType=exptparams.TrialObject.ReferenceClass;
         stim_params.PreStimSilence=ref.PreStimSilence;
         stim_params.Duration=ref.Duration;
         stim_params.PostStimSilence=ref.PostStimSilence;
         stim_params.StimFmt=stimfmt;
         stim_params.ChanCount=chancount;
         
         options=struct();
         options.channel=cellfiledata(ff).channum;
         options.unit=cellfiledata(ff).unit;
         options.rasterfs=respfs;
         options.psthonly=-1;
         options.includeprestim=1;
         [resp,rtags]=loadspikeraster(fn_spike,options);
         
         % remove empty (all NaN) stimuli
         keepidx= ~isnan(resp(1,1,:));
         resp=resp(:,:,keepidx);
         stim=stim(:,:,keepidx);
         bb=basename(fn_param);
         outfile=sprintf('%s%s_c%d_u%d.mat',outpath,bb,options.channel,...
            options.unit);
         save(outfile,'cellid','resp','stim','stim_params','isolation');
      end
   else
      error('EXPTFORMAT not specified');
      
   end
   
end

% $$$ dai007a-a1  0.242      0.312
% $$$ dai007a-a2  0.485      0.499
% $$$ dai007a-b1  0.457  <   0.639
% $$$ dai007a-c1  0.303      0.288
% $$$ dai007a-c2  0.120      0.109
% $$$ dai007a-d1  0.368  <   0.493
% $$$ dai007b-a1  0.103  <   0.184
% $$$ dai007b-b1  0.344      0.391


