% function sout=strmerge(s,delim[=','])
%
% created SVD 3/15/17
%
% 
function sout=strmerge(s,delim)

if ~iscell(s),
   sout=s;
   return
end

sout=[];
if isempty(s),
   return
end
if ~exist('delim','var'),
   delim=',';
end

sout=ifnum2str(s{1});
for ii=2:length(s),
   sout=strcat(sout,delim,ifnum2str(s{ii}));
end



