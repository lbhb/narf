function plotpath = plot_model_summary(do_save)
% plotpath = plot_model_summary()
% plotpath = plot_model_summary(do_save)
% 
% Plots the model currently loaded in NARF and saves a PNG image at the
% appropriate path. Does NOT do anything else, so make sure your model is
% ready to be plotted.
%
% ARGUMENTS: do_save - true by default
%  
% RETURNS:
%    plotpath    The path to the PNG file that was just generated.
%
% Example:
%   load_model('/path/to/mymodel.mat');
%   calc_xxx(1);
%   plot_model_summary();

global META XXX STACK NARF_SAVED_IMAGES_PATH;
if(nargin==0)
    do_save=1;
end
lb = 50;  % Left border
rb = 20;  % Right border
bb = 30;  % Bottom border
th = 130; % text height
ph = 130; % Plot height

w = 500;  % Pixels

vspace = 0.2; % Relative sizes
hspace = 0.05; 

% Scan through the STACK looking for things to .auto_plot
ap = [];
for ii = 1:length(STACK)
    m = STACK{ii}{1};
    if isfield(m, 'auto_plot') && ~isempty(m.auto_plot),
        ap(end+1) = ii;
    end
end
nplots = length(ap);

disp('plot_model_summary: counted number of plots');
randn(100,1);

% Create a new, invisible figure
h = nplots*ph + th;
fig = figure('Menubar', 'figure', 'Resize','off', 'Visible', 'on', ...
             'Units','pixels', 'Position', [50 50 w+20 h]);

disp('plot_model_summary: counted number of plots');
randn(100,1);

% Call the auto-plot functions
for ii = 1:nplots
    idx = ap(ii);
    m = STACK{idx}{1};
    
    plotfn = m.auto_plot;
    
    pos=[lb (nplots-ii)*ph+bb w-lb-rb ph-bb];
    ax = axes('Parent', fig, 'Units', 'pixels', 'Position', pos);
    
    fns = fieldnames(XXX{idx+1}.dat);
    if ~isempty(XXX{idx+1}.test_set),
       first_val_idx=find(strcmp(fns,XXX{idx+1}.test_set{1}));
       sel.stimfile=fns{first_val_idx};
    else
       sel.stimfile = fns{1};
    end
   % fns = fieldnames(XXX{idx+1}.dat);
   % sel.stimfile = fns{1};
    sel.chan_idx = 1;
    sel.stim_idx = 1;
unfcs=unique(XXX{idx+1}.filecodes);
if any(strcmp(unfcs,'CN1'))
    sel(2).stimfile = XXX{idx+1}.test_set{1};
    sel(2).chan_idx = 1;
    sel(2).stim_idx = 3;
elseif any(strcmp(unfcs,'C1')) && any(strcmp(unfcs,'N1'))
    if isequal(size(XXX{idx+1}.dat.(XXX{idx+1}.test_set{1}).resp),size(XXX{idx+1}.dat.(XXX{idx+1}.test_set{2}).resp))
        sel(2).stimfile = XXX{idx+1}.test_set{2};
        sel(2).chan_idx = 1;
        sel(2).stim_idx = 1;
    else
        % Only plot clean. Most (all?) of these were collected with no
        % separate noisy validation data.
    end
end
fprintf('plot_model_summary: plotting for mod %d/%s\n',ap(ii),m.name);
    plotfn(sel, STACK(1:idx), XXX(1:idx+1));
    if(strcmp(STACK{idx}{1}.name,'mean_squared_error'))
        legend(gca,'hide')
    end
    fprintf('plot_model_summary: done plotting for mod %d/%s\n',ap(ii),m.name);
    fprintf('plot_model_summary: formatted %d\n',ap(ii));
    randn(100,1);
end

% TEXT AT TOP
if ~isfield(META, 'batch')
    META.batch = 0;
end

% Print the text at the top
axtt = axes('Parent', fig , 'Units','pixels', ...
    'Position', [lb nplots*ph+bb w-lb-rb th-bb]);
set(gca,'xtick',[]); set(gca,'xticklabel',[]);
set(gca,'ytick',[]); set(gca,'yticklabel',[]);
ax_text  = text('Interpreter', 'none', 'Position', [0.0, 0.45], ...
   'String', sprintf('Batch:     %d\nCellid:    %s\nModel:     %s\nTrain Set: %s\nTest Set:  %s\nTrain r:   %.5f\nTest r:    %.5f', ...
                       META.batch, XXX{1}.cellid, META.modelname, ...
                       [XXX{1}.training_set{:}], [XXX{1}.test_set{:}], ...
                       XXX{end}.score_train_corr, ...
                       XXX{end}.score_test_corr));
axis off;

disp('plot_model_summary: done with text');
randn(100,1);

if(do_save)
    % Save the file
    celldir = [NARF_SAVED_IMAGES_PATH filesep num2str(META.batch) filesep XXX{1}.cellid];
    if ~exist(celldir)
        mkdir(celldir);
        unix(['chmod 777 ' celldir]);
    end
    
    pngfile = [celldir filesep META.modelfile '.png'];
    set(gcf,'PaperPositionMode','auto');
    set(gcf,'InvertHardcopy','off');
    set(0,'defaultTextFontName', 'Arial');
    print(fig, pngfile, '-dpng');
    unix(['chmod 777 ' pngfile]);
    
    % SVD- Don't close figure so that it gets saved by queuerun.m as well
    %close(fig);
    
    plotpath = pngfile;
else
    plotpath=[];
end