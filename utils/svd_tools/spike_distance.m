% function [mMEtau,eMEtau,tau]=spike_distance(r,fs);
%
% von Rossum metric
%
% mMEtau = 2 X tau matrix.  Top row: within stimulus MSE (<1?)
%                           Bottome row: between stimulus MSE (~1)
% tau sampled logrithmically from 2^-1 to 2^(ceil(log2(fs/2)))
%
function [mMEtau,eMEtau,tau]=spike_distance(r,fs);

repcount=size(r,2);
stimcount=size(r,3);

xcN=200;

%tau=2.^(-1:1:floor(log2(size(r,1))-2));
tau=2.^(-1:1:ceil(log2(fs./2)));
taucount=length(tau);
tfilt=cell(taucount,1);
for tt=1:taucount
    %tfilt{tt}=ones(tau(tt),1)./tau(tt);
    mm=ceil(min(tau(tt).*3,size(r,1)));
    xx=(-mm:mm)';
    tfilt{tt}=exp(-abs(xx)./tau(tt));
    tfilt{tt}=tfilt{tt}./sum(tfilt{tt});
end

mMEtau=ones(2,taucount);
eMEtau=ones(2,taucount);
fprintf('tau:');
for tt=1:taucount,
    fprintf(' (%.1f)',tau(tt));
    rsm=zeros(size(r)).*nan;
    
    for s1=1:stimcount,
        mnnan=max(find(~isnan(r(:,1,s1))));
        rsm(1:mnnan,:,s1)=conv2(r(1:mnnan,:,s1),tfilt{tt},'same');
    end
    
    tmsein=[];
    tmseout=[];
    for s1=1:stimcount,
        for t1=1:repcount-1,
            r1=rsm(:,t1,s1);
            r2=rsm(:,(t1+1):end,s1);
            rnorm=nanstd(r1).*sqrt(2);
            if rnorm,
                r1=repmat(r1,[1 size(r2,2)]);
                
                e=nanstd(r1-r2)./rnorm;
                tmsein=cat(2,tmsein,e);
            else
                tmsein=cat(2,tmsein,ones(1,size(r2,2)));
            end
        end
        for t1=1:repcount,
            for s2=[1:(s1-1) (s1+1):stimcount],
                r1=rsm(:,t1,s1);
                r2=rsm(:,:,s2);
                rnorm=nanstd(r1).*sqrt(2);
                if rnorm,
                    r1=repmat(r1,[1 size(r2,2)]);
                    
                    e=nanstd(r1-r2)./rnorm;
                    tmseout=cat(2,tmseout,e);
                else
                    tmseout=cat(2,tmseout,ones(1,size(r2,2)));
                end
            end
        end
    end
    [mMEtau(1,tt),eMEtau(1,tt)]=jackmeanerr(tmsein');
    [mMEtau(2,tt),eMEtau(2,tt)]=jackmeanerr(tmseout');
    
end
fprintf('\n');
