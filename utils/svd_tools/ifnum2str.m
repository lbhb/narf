function a = ifnum2str (b);
% function a = ifnum2str (b);
% 
% this simple function is num2str, but works also when the input is a
% numeric, which in that case its not changed.

% svd, ripped off ifstr2num, mar 2017

if ischar(b)
    a = b;
else
    a = num2str(b);
end    