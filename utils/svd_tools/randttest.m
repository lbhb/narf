% function [p,m,s]=randttest(x1,x2,n,tail,DOMEDIAN,showdetails);
%
% permutation test to substitute for standard t-test for significant 
% difference between mean (or median) of vectors x1 and x2
%
% n - number of randomizations (default 100)
% tail - 0 - two tailed (default)
%       -1 - test mean(x2) < mean(x1)  ie, x2<x1
%        1 - test mean(x2) > mean(x1)  ie, x2>x1
% DOMEDIAN - if 1, test for difference between medians of x1 and x2
%            (default, 0, means test for difference between means)
% showdetails - if 1, plot histogram of results (default 0)
%
% SVD 2016-10-03
%
function [p,m,s]=randttest(x1,x2,n,tail,DOMEDIAN,showdetails)

if isempty(x1) || isempty(x2),
   m=0;
   p=1;
   return
end
if length(x1)==1,
   m=x2-x1;
   p=1;
   return
end

if ~exist('n','var'),
   n=100;
end
if ~exist('tail','var'),
   tail=0;
end
if ~exist('DOMEDIAN','var'),
   DOMEDIAN=0;
end
if ~exist('showdetails','var'),
   showdetails=0;
end

xall=[x1(:);x2(:)];
dcount1=length(x1(:));
dcount2=length(x2(:));

mdiff=zeros(n,1);

for ii=1:n,
   xshuff=shuffle(xall);
   if DOMEDIAN,
       mdiff(ii)=median(xshuff(dcount1+1:end))-median(xshuff(1:dcount1));
   else
       mdiff(ii)=mean(xshuff(dcount1+1:end))-mean(xshuff(1:dcount1));
   end
end
if DOMEDIAN,
    m=median(x2(:))-median(x1(:));
else
    m=mean(x2(:))-mean(x1(:));
end

if tail==0,
   mlist=sort(-abs([mdiff; m]));
   bmin=max(find(mlist<=-abs(m)));
elseif tail==-1, % mean x2 < mean x1
   mlist=sort([mdiff; m]);
   bmin=max(find(mlist<=m));
elseif tail==1,
   mlist=sort(-[mdiff; m]);
   bmin=min(find(mlist>=-m));
end

s=std(mdiff);
p=bmin/(n+1);

if showdetails,
    xx=linspace(min(xall),max(xall),floor(length(xall)./5));
    
    n1=hist(x1,xx);
    n2=hist(x2,xx);
    bar(xx(:),[n1(:) n2(:)]);
    title(sprintf('median x1: %.3f x2: %.3f, p<%.4f',median(x1),median(x2),p));
    axis square
end

%keyboard



% SHUFFLE Randomly shuffle the elements in an array.
%    S = SHUFFLE(A) rearranges the elements in A randomly.
%
%    See also RAND, RANDN, SORT.

function shuffled_array = shuffle (input_array)

     [y,ii]=   sort(rand(size(input_array)));

     shuffled_array = input_array(ii);


