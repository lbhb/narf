function [mMI,eMI,mMEtau,eMEtau,mXC,eXC,f,C]=spike_coherence(r,fs);

% addpath ~/code/toolbox/chronux_2_0/spectral_analysis/helper
% addpath ~/code/toolbox/chronux_2_0/spectral_analysis/pointbinned

params.tapers=[3 5];
params.Fs=fs;

repcount=size(r,2);
stimcount=size(r,3);

xcN=200;
MIin=zeros(xcN,1);
MIout=zeros(xcN,1);
xcin=zeros(xcN,1);
xcout=zeros(xcN,1);
msein=ones(xcN,1);
mseout=ones(xcN,1);

tau=2.^(0:1:floor(log2(size(r,1))));
taucount=length(tau);
tfilt=cell(taucount,1);
for tt=1:taucount
    %tfilt{tt}=ones(tau(tt),1)./tau(tt);
    mm=min(tau(tt).*3,size(r,1));
    xx=-mm:mm;
    tfilt{tt}=exp(-abs(xx)./tau(tt));
    tfilt{tt}=tfilt{tt}./sum(tfilt{tt});
end

msetau=ones(taucount,2,xcN);

Cin=[];
Cout=[];
for nn=1:xcN
    t1=ceil(rand*repcount);
    t2=ceil(rand*(repcount-1));
    if t2>=t1, t2=t2+1; end
    s1=ceil(rand*stimcount);
    s2=ceil(rand*(stimcount-1));
    if s2>=s1, s2=s2+1; end
    
    [c,~,~,~,~,f]=coherencypb(r(:,t1,s1),r(:,t2,s1),params);
    Cin=cat(2,Cin,c);
    MIin(nn)=-mean(log2(1-c.^2));
    if var(r(:,t1,s1))>0 && var(r(:,t2,s1))>0,
        xcin(nn)=xcov(r(:,t1,s1),r(:,t2,s1),0,'coeff');
        msein(nn)=std(r(:,t1,s1)-r(:,t2,s1))./...
                  sqrt((std(r(:,t1,s1)).*std(r(:,t2,s1))).*2);
        for tt=1:taucount,
            tr=conv2([r(:,t1,s1) r(:,t2,s1)],tfilt{tt},'same');
            msetau(tt,1,nn)=std(tr(:,1)-tr(:,2))./...
                  sqrt((std(tr(:,1)).*std(tr(:,2))).*2);
        end
    end
    if stimcount>1,
        [c,~,~,~,~,f]=coherencypb(r(:,t1,s1),r(:,t2,s2),params);
        Cout=cat(2,Cout,c);
        MIout(nn)=-mean(log2(1-c.^2));
        if var(r(:,t1,s1))>0 && var(r(:,t2,s2))>0,
            xcout(nn)=xcov(r(:,t1,s1),r(:,t2,s2),0,'coeff');
            mseout(nn)=std(r(:,t1,s1)-r(:,t2,s2))./...
                      sqrt((std(r(:,t1,s1)).*std(r(:,t2,s2))).*2);
            for tt=1:taucount,
                tr=conv2([r(:,t1,s1) r(:,t2,s2)],tfilt{tt},'same');
                msetau(tt,2,nn)=std(tr(:,1)-tr(:,2))./...
                    sqrt((std(tr(:,1)).*std(tr(:,2))).*2);
            end
        end
    end
end

mMEtau=zeros(size(msetau,2),size(msetau,1));
eMEtau=zeros(size(msetau,2),size(msetau,1));
for tt=1:taucount,
    for s=1:2,
        [mMEtau(s,tt),eMEtau(s,tt)]=jackmeanerr(squeeze(msetau(tt,s,:)));
    end 
end
mXC=zeros(2,1);
eXC=zeros(2,1);
mMI=zeros(2,1);
eMI=zeros(2,1);
mME=zeros(2,1);
eME=zeros(2,1);
[mXC(1),eXC(1)]=jackmeanerr(xcin);
[mXC(2),eXC(2)]=jackmeanerr(xcout);
[mMI(1),eMI(1)]=jackmeanerr(MIin);
[mMI(2),eMI(2)]=jackmeanerr(MIout);
[mME(1),eME(1)]=jackmeanerr(msein);
[mME(2),eME(2)]=jackmeanerr(mseout);
C=cat(3,Cin,Cout);