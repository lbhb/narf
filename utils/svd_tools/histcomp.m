% function [p,m]=histcomp(d1,d2,n1a,n1b,sunits,axisrange,DOMEDIAN,nbins);
%
% axisrange=[minx maxx miny maxy] or [minx maxx] and autoscaling of y axis
%
function [p,m]=histcomp(d1,d2,n1a,n1b,sunits,axisrange,DOMEDIAN,nbins,colorsign)

if ~exist('axisrange','var'),
   diffmax=max(abs([d1(:); d2(:)]));
   axisrange=[-diffmax diffmax];
end
if ~exist('sunits','var'),
   sunits=[];
end
if ~exist('DOMEDIAN','var'),
   DOMEDIAN=0;
end
if ~exist('nbins','var'),
    nbins=12;
end
if ~exist('colorsign','var'),
    colorsign=0;
end
colUpShade=[255 80 80]./255;
colDownShade=[0 0 215]./255;

% remove invalid values
d1=d1(~isnan(d1));
d2=d2(~isnan(d2));


cla
TTAIL=0;
TREP=2500;
% [p,m]=randttest(d1,zeros(size(d1)),TREP,TTAIL,1);
% [pall,m]=randttest([d1;d2],zeros(size([d1;d2])),TREP,TTAIL,1);
% [p,m]=randpairtest(d1,zeros(size(d1)),TREP,TTAIL,'median');
% [pall,m]=randpairtest([d1;d2],zeros(size([d1;d2])),TREP,TTAIL);
[p]=signtest(d1);
[pall]=signtest([d1;d2]);
if DOMEDIAN,
   mall=median([d1(:);d2(:)]);
   m1=median(d1(:));
   if ~isempty(d2(:))
      m2=median(d2(:));
   else
      m2=0;
   end
   stat='med';
else
   mall=mean([d1(:);d2(:)]);
   m1=mean(d1(:));
   if ~isempty(d2(:))
      m2=mean(d2(:));
   else
      m2=0;
   end
   stat='mean';
end

diffrange=linspace(axisrange(1),axisrange(2),nbins+1);
if colorsign,
   data(1).d=d1(d1>=0);
   data(2).d=d1(d1<0);
   data(3).d=d2;
   hb=pophist(data,diffrange);
   set(hb(1),'FaceColor',colUpShade);
   set(hb(2),'FaceColor',colDownShade);
   set(hb(3),'FaceColor',[1 1 1]);
   
else
   data(1).d=d1;
   data(2).d=d2;
   pophist(data,diffrange);
end

ht=title(sprintf('%s v %s',n1a,n1b));
hx=xlabel(sprintf('diff (%s)',sunits));
if colorsign,
   s=sprintf('count(up: %d,dn: %d,NS: %d)',length(data(1).d),length(data(2).d),length(data(3).d));
else
   s=sprintf('count(sig: %d,NS: %d)',length(data(1).d),length(data(2).d));
end
hy=ylabel(s);

set(ht,'FontSize',6);
set(hx,'FontSize',6);
set(hy,'FontSize',6);
set(gca,'FontSize',6);

if length(axisrange)==4,
   axis(axisrange);
   aa=axis;
else
   aa=axis;
   axis([axisrange(1:2) aa(3:4)]);
end
axis square
box off

s=sprintf('%s all %.2f\n(%d)p<%.3f\n%s %.2f\n(%d)p<%.3f',...
   stat,mall,length(d1)+length(d2),pall,n1a,m1,length(d1),p);
text(axisrange(1),aa(4),s,'VerticalAlignment','top',...
   'HorizontalAlignment','left','Fontsize',6);
