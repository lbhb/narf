function [b,Yest,berr]=nested_regress(Y,X,jackcount)

if ~exist('jackcount','var'), jackcount=20; end

stimlen=size(X,1);
stepsize=stimlen/jackcount;
chancount=size(X,2);

jb=zeros(size(X,2)+1,jackcount);

Yest=zeros(size(Y));

for jj=1:jackcount,
   fprintf('%d ',jj);
   vii=round((jj-1).*stepsize+1):round(jj.*stepsize);
   eii=[1:round((jj-1).*stepsize) round(jj.*stepsize+1):stimlen];
   
   vY=Y(vii,:);
   vX=[X(vii,:) ones(size(vY))];
   eY=Y(eii,:);
   eX=[X(eii,:) ones(size(eY))];
   
   jb(:,jj)=regress(eY,eX);
   
   Yest(vii)=vX*jb(:,jj);
   
end
fprintf(' Done\n');
b=mean(jb,2);
berr=std(jb,0,2).*sqrt(jackcount-1);
