
%cfd=dbgetscellfile('cellid','TTL','TrialObjectClass','RefTarOpt','area','A1');
%cfd=cat(1,dbgetscellfile('cellid','TTL','TrialObjectClass','RefTarOpt','area','A1'),...
%   dbgetscellfile('cellid','TTL','TrialObjectClass','RefTarOpt','area','AC'));
%cfd=dbgetscellfile('runclass','SPN','area','A1');
cfd=dbgetscellfile('runclass','VOC','area','A1');

fnstr='elec';
%fnstr='chan';

widths=zeros(size(cfd));
widths2=zeros(size(cfd));
medisis=zeros(size(cfd));
keep=zeros(size(cfd));
for ii=1:length(cfd),
   cellid=cfd(ii).cellid;
   rawid=cfd(ii).rawid;
   isolation=cfd(ii).isolation;
   
   sql=sprintf(['SELECT * FROM gSingleRaw WHERE rawid=%d AND cellid="%s"',...
      ' AND not(isnull(spikewidth2_ms))'],rawid,cellid);
   sdata=mysql(sql);
   if ~isempty(sdata),
      widths(ii)=sdata.spikewidth_ms;
      widths2(ii)=sdata.spikewidth2_ms;
      medisis(ii)=sdata.peak_isi_ms;
      
      keep(ii)=1;
      
   elseif isolation>90,
      
      rawdata=mysql(['select * from gDataRaw where id=',num2str(rawid)]);
      
      evpfile=[rawdata.resppath,rawdata.respfileevp];
      [pp,bb,ee]=fileparts(evpfile);
      bbpref=strsep(bb,'_');
      bbpref=bbpref{1};
      checkrawevp=[pp filesep 'raw' filesep bb '.001.1' ee];
      if exist(checkrawevp,'file')  
         evpfile=checkrawevp;  
      end
      checktgzevp=[pp filesep 'raw' filesep bbpref '.tgz'];
      if exist(checktgzevp,'file'),
         evpfile=checktgzevp;
      end
      
      if exist(evpfile,'file'),
         rS=evpread(evpfile,'spikechans',cfd(ii).channum,'filterstyle','none');
         
         spkfile=[cfd(ii).path cfd(ii).respfile];
         spkdata=load(spkfile);
         sortresults=spkdata.sortinfo{cfd(ii).channum}{1}(cfd(ii).unit);
         
         spktimes=sortresults.unitSpikes(2,:)'+rS.STrialidx(sortresults.unitSpikes(1,:));
         isidata=[];
         spikes=sortresults.unitSpikes;
         for trialidx=1:max(spikes(1,:)),
            isidata=cat(2,isidata,diff(spikes(2,spikes(1,:)==trialidx))./10);
         end
         
         
         sfigure(1);
         clf
         
         chunk=zeros(61,length(spktimes));
         for aa=1:length(spktimes),
            mm=rS.Spike(spktimes(aa)+(-20:40));
            chunk(:,aa)=mm-mean(mm);
         end
         mm=mean(chunk,2);
         
         ymm=zeros(size(chunk,2),2);
         for jj=1:size(chunk,2),
            mm=chunk(:,jj);
            ymm(jj,1)=min(find(mm==min(mm)));
            ymm(jj,2)=max(find(mm==max(mm)));
         end
         w2=mean(abs(diff(ymm,[],2)))./25000*1000;
         
         mm=mean(chunk,2);
         maxo = max(mm);
         mino = min(mm);
         mm = mm / (maxo-mino);
         mm=round(mm*1000)./1000;
         
         ymin=min(find(mm==min(mm)));
         ymax=max(find(mm==max(mm)));
         
         width=abs(ymax-ymin)./25000*1000;
         
         widths(ii)=width;
         widths2(ii)=w2;
         
         keep(ii)=1;
         
         subplot(3,1,1);
         plot(mm);
         title(sprintf('%s - %s - iso %.1f -width=%.1f',...
            cellid,bb,cfd(ii).isolation,width),'Interpreter','none');
         
         h=subplot(3,1,2);
         xx=linspace(0,median(isidata)*3,75);
         nn=hist(isidata,xx);
         nn_sm=gsmooth(nn(1:(end-1)),1);
         nn_sm(end+1)=0;
         
         maxii=min(find(nn_sm==max(nn_sm(1:(end-3)))));
         medisis(ii)=xx(maxii);
         bar(xx,nn);
         hold on;
         plot(xx,nn_sm,'r');
         plot(medisis(ii),nn_sm(maxii));
         hold off
         
         title(sprintf('peak ISI=%.1f ms',medisis(ii)));
         
         
         h=subplot(3,1,3);
         parms=dbReadData(rawid);
         options=struct('rasterfs',1000,'psthfs',50,'usesorted',1,...
            'psth',1,'datause','Collapse both');
         options.PreStimSilence=parms.Ref_PreStimSilence;
         options.PostStimSilence=parms.Ref_PostStimSilence;
         mfile=[cfd(ii).stimpath cfd(ii).stimfile];
         channel=cfd(ii).channum;
         unit=cfd(ii).unit;
         %raster_online(mfile,channel,unit,h,options);
         %  title(sprintf('%s - %s(%d) - iso: %.1f',cfd(ii).cellid,...
         %     basename(mfile),rawid,cfd(ii).isolation));
         
         %opto_raster(cfd(ii).rawid,cellid,h);
         
         
         sfigure(2);
         clf
         subplot(2,2,1);
         kk=find(keep);
         plot(widths(kk),widths2(kk),'.');
         
         subplot(2,2,2);
         plot(widths(kk),medisis(kk),'.');
         
         subplot(2,1,2);
         ww=linspace(0,1.2,15);
         nn=hist([widths(kk) widths2(kk)],ww);
         plot(ww,nn);
         ylabel('number of cells');
         xlabel('spike width');
         legend('width avg','avg width');
         drawnow
         
         sql=sprintf(['UPDATE gSingleRaw SET waveform="%s",peak_isi_ms=%.2f,',...
            'spikewidth_ms=%.2f,spikewidth2_ms=%.2f',...
            ' WHERE cellid="%s" and rawid=%d'],...
            mat2str(mm),medisis(ii),width,w2,cellid,rawid);
         [~,aff]=mysql(sql);
         fprintf('%s\n%d rows affected\n',aff);
         if aff>1,
            keyboard
         end
      end
   end
end

