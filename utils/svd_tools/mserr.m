% function e=mserr(fitdata,obsdata);
function e=mserr(fitdata,obsdata,meansub)

if ~exist('meansub','var'),
   meansub=0;
end
if meansub,
   fitdata=fitdata-nanmean(fitdata(:));
   obsdata=obsdata-nanmean(obsdata(:));
end

d=sum(sum(sum(abs(fitdata-obsdata).^2)));
s=sum(sum(sum(abs(fitdata).^2)));
e=d/s;
