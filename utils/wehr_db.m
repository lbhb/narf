function [celllist,files]=wehr_db()

envpath='/auto/data/daq/wehr/soundfiles/sourcefiles/';
basepath='/auto/users/svd/data/wehr/SpNoise_Data/';

basepathcc='/auto/data/daq/wehr/data/Current Clamp Files (SPNoise)/';
basepathcc2='/auto/data/daq/wehr/data/Current Clamp Files (SPNoise)/';
basepathcc3='/auto/data/daq/wehr/data/Current Clamp Files (SPNoise)/';

basepathvc='/auto/data/daq/wehr/data/Voltage Clamp Files (SPNoise)/';
basepathvc2='/auto/data/daq/wehr/data/Voltage Clamp Files (SPNoise)/';
basepathvc3='/auto/data/daq/wehr/data/Voltage Clamp Files (SPNoise)/ExcCurrentsOnly/';

basepathec='/auto/data/daq/wehr/data/Awake Recordings/SP Noise/';

basepathca='/auto/data/daq/wehr/data/LooseCellAttached/SPNoise/';

basepathoe='/auto/data/daq/wehr/data/OEoutfiles/Awake/SPN/';

files=struct();
files.out121012_002_002.file=[basepathcc 'out121012-002-002.mat'];
files.out121012_002_002.respfmt=0;
files.out121412_007_002.file=[basepathcc 'out121412-007-002.mat'];
files.out121412_007_002.respfmt=0;
files.out121812_002_003.file=[basepathcc 'out121812-002-003.mat'];
files.out121812_002_003.respfmt=0;
files.out121812T_002_005.file=[basepathcc 'out121812-002-005.mat'];
files.out121812T_002_005.respfmt=0;
files.out121812_004_003.file=[basepathcc 'out121812-004-003.mat'];
files.out121812_004_003.respfmt=0;
files.out121812T_004_005.file=[basepathcc 'out121812-004-005.mat'];
files.out121812T_004_005.respfmt=0;
files.out122012_003_001.file=[basepathcc 'out122012-003-001.mat'];
files.out122012_003_001.respfmt=0;
files.out012213_007_002.file=[basepathcc 'out012213-007-002.mat'];
files.out012213_007_002.respfmt=0;
files.out031113_002_002.file=[basepathcc 'out031113-002-002.mat'];
files.out031113_002_002.respfmt=0;
files.out040213_003_002.file=[basepathcc 'out040213-003-002.mat'];
files.out040213_003_002.respfmt=0;
files.out040213_004_001.file=[basepathcc 'out040213-004-001.mat'];
files.out040213_004_001.respfmt=0;
files.out051113_003_004.file=[basepathcc 'out051113-003-004.mat'];
files.out051113_003_004.respfmt=0;
files.out051113_003_009.file=[basepathcc 'out051113-003-009.mat'];
files.out051113_003_009.respfmt=0;
files.out051113_004_003.file=[basepathcc 'out051113-004-003.mat'];
files.out051113_004_003.respfmt=0;
files.out061913_002_002.file=[basepathcc 'out061913-002-002.mat'];
files.out061913_002_002.respfmt=0;
files.out062113_005_001.file=[basepathcc 'out062113-005-001.mat'];
files.out062113_005_001.respfmt=0;

files.out121112_004_003_E.file=[basepathvc 'out121112-004-003.mat'];
files.out121112_004_003_E.respfmt=1;
files.out121112_004_003_I.file=[basepathvc 'out121112-004-003.mat'];
files.out121112_004_003_I.respfmt=2;
files.out121112T_004_005_E.file=[basepathvc 'out121112-004-005.mat'];
files.out121112T_004_005_E.respfmt=1;
files.out121112T_004_005_I.file=[basepathvc 'out121112-004-005.mat'];
files.out121112T_004_005_I.respfmt=2;
files.out122012_002_001_E.file=[basepathvc 'out122012-002-001.mat'];
files.out122012_002_001_E.respfmt=1;
files.out122012_002_001_I.file=[basepathvc 'out122012-002-001.mat'];
files.out122012_002_001_I.respfmt=2;
files.out122012_007_001_E.file=[basepathvc 'out122012-007-001.mat'];
files.out122012_007_001_E.respfmt=1;
files.out122012_007_001_I.file=[basepathvc 'out122012-007-001.mat'];
files.out122012_007_001_I.respfmt=2;

files.out022313_005_002_E.file=[basepathvc 'out022313-005-002.mat'];
files.out022313_005_002_E.respfmt=1;
files.out022313_005_002_I.file=[basepathvc 'out022313-005-002.mat'];
files.out022313_005_002_I.respfmt=2;

files.out022313_008_001_E.file=[basepathvc 'out022313-008-001.mat'];
files.out022313_008_001_E.respfmt=1;
files.out022313_008_001_I.file=[basepathvc 'out022313-008-001.mat'];
files.out022313_008_001_I.respfmt=2;

files.out052213_003_001_E.file=[basepathvc 'out052213-003-001.mat'];
files.out052213_003_001_E.respfmt=1;
files.out052213_003_001_I.file=[basepathvc 'out052213-003-001.mat'];
files.out052213_003_001_I.respfmt=2;

files.out052213_004_001_E.file=[basepathvc 'out052213-004-001.mat'];
files.out052213_004_001_E.respfmt=1;
files.out052213_004_001_I.file=[basepathvc 'out052213-004-001.mat'];
files.out052213_004_001_I.respfmt=2;

files.out071113_003_002_E.file=[basepathvc 'out071113-003-002.mat'];
files.out071113_003_002_E.respfmt=1;
files.out071113_003_002_I.file=[basepathvc 'out071113-003-002.mat'];
files.out071113_003_002_I.respfmt=2;

files.out071113_004_001_E.file=[basepathvc 'out071113-004-001.mat'];
files.out071113_004_001_E.respfmt=1;
files.out071113_004_001_I.file=[basepathvc 'out071113-004-001.mat'];
files.out071113_004_001_I.respfmt=2;

files.out082613_002_002_E.file=[basepathvc 'out082613-002-002.mat'];
files.out082613_002_002_E.respfmt=1;
files.out082613_002_002_I.file=[basepathvc 'out082613-002-002.mat'];
files.out082613_002_002_I.respfmt=2;

files.out082613_003_001_E.file=[basepathvc 'out082613-003-001.mat'];
files.out082613_003_001_E.respfmt=1;
files.out082613_003_001_I.file=[basepathvc 'out082613-003-001.mat'];
files.out082613_003_001_I.respfmt=2;

files.out082713_005_002_E.file=[basepathvc 'out082713-005-002.mat'];
files.out082713_005_002_E.respfmt=1;
files.out082713_005_002_I.file=[basepathvc 'out082713-005-002.mat'];
files.out082713_005_002_I.respfmt=2;

files.out082813_002_003_E.file=[basepathvc 'out082813-002-003.mat'];
files.out082813_002_003_E.respfmt=1;
files.out082813_002_003_I.file=[basepathvc 'out082813-002-003.mat'];
files.out082813_002_003_I.respfmt=2;

files.out121812_003_002_E.file=[basepathvc 'out121812-003-002.mat'];
files.out121812_003_002_E.respfmt=1;
files.out121812_003_002_I.file=[basepathvc 'out121812-003-002.mat'];
files.out121812_003_002_I.respfmt=2;

files.out021714_002_001.file=[basepathcc2 'out021714-002-001.mat'];
files.out021714_002_001.respfmt=0;
files.out021714_004_002.file=[basepathcc2 'out021714-004-002.mat'];
files.out021714_004_002.respfmt=0;
files.out021714_005_001.file=[basepathcc2 'out021714-005-001.mat'];
files.out021714_005_001.respfmt=0;
files.out021714T_005_003.file=[basepathcc2 'out021714-005-003.mat'];
files.out021714T_005_003.respfmt=0;
files.out021914_003_004.file=[basepathcc2 'out021914-003-004.mat'];
files.out021914_003_004.respfmt=0;
files.out031914_002_002.file=[basepathcc2 'out031914-002-002.mat'];
files.out031914_002_002.respfmt=0;
files.out031914T_002_005.file=[basepathcc2 'out031914-002-005.mat'];
files.out031914T_002_005.respfmt=0;
files.out032014T_002_002.file=[basepathcc2 'out032014-002-002.mat'];
files.out032014T_002_002.respfmt=0;
files.out032014_004_006.file=[basepathcc2 'out032014-004-006.mat'];
files.out032014_004_006.respfmt=0;

files.out111713_004_003.file=[basepathcc3 'out111713-004-003.mat'];
files.out111713_004_003.respfmt=0;
files.out112013_001_002.file=[basepathcc3 'out112013-001-002.mat'];
files.out112013_001_002.respfmt=0;
files.out112113_005_005.file=[basepathcc3 'out112113-005-005.mat'];
files.out112113_005_005.respfmt=0;
files.out112113_007_002.file=[basepathcc3 'out112113-007-002.mat'];
files.out112113_007_002.respfmt=0;
files.out112313_002_002.file=[basepathcc3 'out112313-002-002.mat'];
files.out112313_002_002.respfmt=0;
files.out112313_003_002.file=[basepathcc3 'out112313-003-002.mat'];
files.out112313_003_002.respfmt=0;
files.out112313_006_002.file=[basepathcc3 'out112313-006-002.mat'];
files.out112313_006_002.respfmt=0;

files.out011714_002_005_E.file=[basepathvc2 'out011714-002-005.mat'];
files.out011714_002_005_E.respfmt=1;
files.out011714_002_005_I.file=[basepathvc2 'out011714-002-005.mat'];
files.out011714_002_005_I.respfmt=2;
files.out012814_002_003_E.file=[basepathvc2 'out012814-002-003.mat'];
files.out012814_002_003_E.respfmt=1;
files.out012814_002_003_I.file=[basepathvc2 'out012814-002-003.mat'];
files.out012814_002_003_I.respfmt=2;
files.out012914_002_004_E.file=[basepathvc2 'out012914-002-004.mat'];
files.out012914_002_004_E.respfmt=1;
files.out012914_002_004_I.file=[basepathvc2 'out012914-002-004.mat'];
files.out012914_002_004_I.respfmt=2;
files.out021714_004_003_E.file=[basepathvc2 'out021714-004-003.mat'];
files.out021714_004_003_E.respfmt=1;
files.out021714_004_003_I.file=[basepathvc2 'out021714-004-003.mat'];
files.out021714_004_003_I.respfmt=2;
files.out032014T_004_003_E.file=[basepathvc2 'out032014-004-003.mat'];
files.out032014T_004_003_E.respfmt=1;
files.out032014T_004_003_I.file=[basepathvc2 'out032014-004-003.mat'];
files.out032014T_004_003_I.respfmt=2;
%files._E.file=[basepathvc2 ''];
%files._E.respfmt=1;
%files._I.file=[basepathvc2 ''];
%files._I.respfmt=2;

% E only cells
files.out011714T_002_002_E.file=[basepathvc3 'out011714-002-002.mat'];
files.out011714T_002_002_E.respfmt=1;
files.out012014_004_003_E.file=[basepathvc3 'out012014-004-003.mat'];
files.out012014_004_003_E.respfmt=1;
files.out021714_002_003_E.file=[basepathvc3 'out021714-002-003.mat'];
files.out021714_002_003_E.respfmt=1;
files.out031814_002_004_E.file=[basepathvc3 'out031814-002-004.mat'];
files.out031814_002_004_E.respfmt=1;
files.out031914_002_004_E.file=[basepathvc3 'out031914-002-004.mat'];
files.out031914_002_004_E.respfmt=1;

files.out071614_002_003_t2_cell2.file=[basepathec 'out071614-002-003_t2_cell2_.mat'];
files.out071614_002_003_t2_cell2.respfmt=3;
files.out071714_001_003_t5_cell1.file=[basepathec 'out071714-001-003_t5_cell1_.mat'];
files.out071714_001_003_t5_cell1.respfmt=3;
files.out071714_001_003_t5_cell2.file=[basepathec 'out071714-001-003_t5_cell2_.mat'];
files.out071714_001_003_t5_cell2.respfmt=3;
files.out071714_001_003_t6_cell1.file=[basepathec 'out071714-001-003_t6_cell1_.mat'];
files.out071714_001_003_t6_cell1.respfmt=3;
files.out071714_001_003_t6_cell2.file=[basepathec 'out071714-001-003_t6_cell2_.mat'];
files.out071714_001_003_t6_cell2.respfmt=3;
files.out071714_001_003_t6_cell3.file=[basepathec 'out071714-001-003_t6_cell3_.mat'];
files.out071714_001_003_t6_cell3.respfmt=3;
files.out071814_001_002_t6_cell1.file=[basepathec 'out071814-001-002_t6_cell1_.mat'];
files.out071814_001_002_t6_cell1.respfmt=3;
files.out071814_001_002_t6_cell2.file=[basepathec 'out071814-001-002_t6_cell2_.mat'];
files.out071814_001_002_t6_cell2.respfmt=3;
files.out071814_001_002_t6_cell3.file=[basepathec 'out071814-001-002_t6_cell3_.mat'];
files.out071814_001_002_t6_cell3.respfmt=3;
files.out071814_001_002_t7_cell1.file=[basepathec 'out071814-001-002_t7_cell1_.mat'];
files.out071814_001_002_t7_cell1.respfmt=3;
files.out071814_001_002_t7_cell2.file=[basepathec 'out071814-001-002_t7_cell2_.mat'];
files.out071814_001_002_t7_cell2.respfmt=3;
files.out071814_001_002_t8_cell1.file=[basepathec 'out071814-001-002_t8_cell1_.mat'];
files.out071814_001_002_t8_cell1.respfmt=3;
files.out071814_001_002_t8_cell2.file=[basepathec 'out071814-001-002_t8_cell2_.mat'];
files.out071814_001_002_t8_cell2.respfmt=3;
files.out071814_001_003_t6_cell1.file=[basepathec 'out071814-001-003_t6_cell1_.mat'];
files.out071814_001_003_t6_cell1.respfmt=3;
files.out071814_001_003_t6_cell2.file=[basepathec 'out071814-001-003_t6_cell2_.mat'];
files.out071814_001_003_t6_cell2.respfmt=3;
files.out071814_001_003_t6_cell3.file=[basepathec 'out071814-001-003_t6_cell3_.mat'];
files.out071814_001_003_t6_cell3.respfmt=3;
files.out071814_001_003_t7_cell1.file=[basepathec 'out071814-001-003_t7_cell1_.mat'];
files.out071814_001_003_t7_cell1.respfmt=3;
files.out071814_001_003_t7_cell2.file=[basepathec 'out071814-001-003_t7_cell2_.mat'];
files.out071814_001_003_t7_cell2.respfmt=3;
files.out071814_001_003_t8_cell1.file=[basepathec 'out071814-001-003_t8_cell1_.mat'];
files.out071814_001_003_t8_cell1.respfmt=3;
files.out071814_001_003_t8_cell2.file=[basepathec 'out071814-001-003_t8_cell2_.mat'];
files.out071814_001_003_t8_cell2.respfmt=3;
files.out071814_003_002_t5_cell1.file=[basepathec 'out071814-003-002_t5_cell1_.mat'];
files.out071814_003_002_t5_cell1.respfmt=3;

files.out011714_003_002.file=[basepathca 'out011714-003-002.mat'];
files.out011714_003_002.respfmt=4;
files.out021714_001_003.file=[basepathca 'out021714-001-003.mat'];
files.out021714_001_003.respfmt=4;
files.out021914_001_007.file=[basepathca 'out021914-001-007.mat'];
files.out021914_001_007.respfmt=4;
files.out021914_002_002.file=[basepathca 'out021914-002-002.mat'];
files.out021914_002_002.respfmt=4;
files.out022014_003_002.file=[basepathca 'out022014-003-002.mat'];
files.out022014_003_002.respfmt=4;

files.out022313_005_003.file=[basepathcc 'out022313-005-003.mat'];
files.out022313_005_003.respfmt=0;
files.out032014_004_002.file=[basepathcc 'out032014-004-002.mat'];
files.out032014_004_002.respfmt=0;
files.out040213_003_003_E.file=[basepathvc 'out040213-003-003.mat'];
files.out040213_003_003_E.respfmt=1;
files.out040213_003_003_I.file=[basepathvc 'out040213-003-003.mat'];
files.out040213_003_003_I.respfmt=2;
files.out051113_003_010.file=[basepathcc 'out051113-003-010.mat'];
files.out051113_003_010.respfmt=0;

files.out111713_004_004_E.file=[basepathvc 'out111713-004-004.mat'];
files.out111713_004_004_E.respfmt=1;
files.out111713_004_004_I.file=[basepathvc 'out111713-004-004.mat'];
files.out111713_004_004_I.respfmt=2;
files.out112013_001_003_E.file=[basepathvc3 'out112013-001-003.mat'];
files.out112013_001_003_E.respfmt=1;
files.out112113_005_003_E.file=[basepathvc3 'out112113-005-003.mat'];
files.out112113_005_003_E.respfmt=1;
files.out121412_007_001_E.file=[basepathvc3 'out121412-007-001.mat'];
files.out121412_007_001_E.respfmt=1;
files.out121812_004_004_E.file=[basepathvc3 'out121812-004-004.mat'];
files.out121812_004_004_E.respfmt=1;
files.out122012_003_002.file=[basepathcc 'out122012-003-002.mat'];
files.out122012_003_002.respfmt=0;

%basepathoe='/auto/data/daq/wehr/data/OEoutfiles/Awake/SPN/';
files.out102814_001_002_t1_cell1.file=[basepathoe '001_out102814-001-002_t1_cell1_.mat'];
files.out102814_001_002_t1_cell1.respfmt=3;
files.out102814_001_002_t1_cell2.file=[basepathoe '002_out102814-001-002_t1_cell2_.mat'];
files.out102814_001_002_t1_cell2.respfmt=3;
files.out102814_001_002_t3_cell1.file=[basepathoe '003_out102814-001-002_t3_cell1_.mat'];
files.out102814_001_002_t3_cell1.respfmt=3;
files.out102814_001_002_t4_cell1.file=[basepathoe '004_out102814-001-002_t4_cell1_.mat'];
files.out102814_001_002_t4_cell1.respfmt=3;
files.out102814_001_002_t4_cell2.file=[basepathoe '005_out102814-001-002_t4_cell2_.mat'];
files.out102814_001_002_t4_cell2.respfmt=3;
files.out100814_001_003_t3_cell2.file=[basepathoe '006_out100814-001-003_t3_cell2_.mat'];
files.out100814_001_003_t3_cell2.respfmt=3;
files.out100814_001_003_t3_cell3.file=[basepathoe '007_out100814-001-003_t3_cell3_.mat'];
files.out100814_001_003_t3_cell3.respfmt=3;
files.out092514_001_004_t2_cell1.file=[basepathoe '008_out092514-001-004_t2_cell1_.mat'];
files.out092514_001_004_t2_cell1.respfmt=3;
files.out092514_001_004_t2_cell2.file=[basepathoe '009_out092514-001-004_t2_cell2_.mat'];
files.out092514_001_004_t2_cell2.respfmt=3;
files.out092514_001_004_t3_cell1.file=[basepathoe '010_out092514-001-004_t3_cell1_.mat'];
files.out092514_001_004_t3_cell1.respfmt=3;
files.out092514_001_004_t8_cell1.file=[basepathoe '011_out092514-001-004_t8_cell1_.mat'];
files.out092514_001_004_t8_cell1.respfmt=3;
files.out092514_001_004_t8_cell3.file=[basepathoe '012_out092514-001-004_t8_cell3_.mat'];
files.out092514_001_004_t8_cell3.respfmt=3;

% $$$ files..file=[basepathcc ''];
% $$$ files..respfmt=0;
% $$$ files..file=[basepathcc ''];
% $$$ files..respfmt=0;

celllist=fieldnames(files);

