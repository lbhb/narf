function [cellids,good] = filter_cells(batch, cellids, criterea, usecache)
% cells = filter_low_snr_cells(batch, cellids, criterea)
% 
% If criterea is a struct:
% Filters out cellids for each field in criterea that have:
%   metric < criterea_field
% for each cell, the metic is taken as the minium across est/val and condition (ex noise/clean)
%       min_snr_index is (SNR^2 * repcount)
%           Generally, threshold values are like this
%               0.2  very good cells
%               0.1  good cells
%               0.03 Top 50% (for batch 267)
%
%       min_isolation is isolation
%               95  very good cells
%               85  decent cells
%               <85 not good
%
% cells = filter_low_snr_cells(batch, cellids, min_snr_index_threshold)
%    filters by min_snr_index only

if ~exist('usecache', 'var')
    usecache = true;
end
if(~isstruct(criterea))
    criterea.min_snr_index=criterea;
end
criterea_names=fieldnames(criterea);
criterea_names(cellfun(@(x)criterea.(x)<=0,criterea_names))=[];%don't need to load criterea less than or equal to 0
if isempty(criterea_names)
   return
end

dbopen;
good=true(size(cellids));
for jj = 1:length(cellids)
    cellid = cellids{jj};    
    
    if usecache
        compute_metrics=false;
        sql = ['select * from NarfBatches where batch=' num2str(batch) ...
               ' and cellid="' cellid '"'];
        thisbatch = mysql(sql);
        if isempty(thisbatch)
            good(jj)=false;
            warning(['No results found for cellid ',cellid]);
            continue
        elseif length(thisbatch) > 1
            fprintf('Multiple results found for cellid %s\n', cellid);
            thisbatch=thisbatch(1);
            continue;
        end
        missing_fields=~ismember(criterea_names,fieldnames(thisbatch));
        missing_fields(cellfun(@(x)isempty(thisbatch.(x)),criterea_names(~missing_fields)))=true; %count empty fields as missing
        if any(missing_fields)
            compute_metrics=true;
            if(sum(missing_fields)==1)
                if(strcmp(criterea_names(missing_fields),'min_snr'))
                    thisbatch.min_snr=min(thisbatch.val_snr,thisbatch.est_snr);
                    compute_metrics=false;
                end
            end
        else
            compute_metrics=false;
        end
%         snr_est = ret(1).est_snr;
%         snr_val = ret(1).val_snr;
%         estreps = ret(1).est_reps;
%         valreps = ret(1).val_reps;
    else
        compute_metrics=true;
    end
    
    if(compute_metrics)
        repstransform=true;
        [val_snr, est_snr, z_val, z_est,iso_val,iso_est] = db_get_snr(cellid,batch,repstransform);
        thisbatch.min_isolation=min(iso_val,iso_est);
        
        if(repstransform)
            thisbatch.min_snr_index=min(val_snr,est_snr);
            sql=sprintf(['UPDATE NarfBatches SET min_snr_index=%.5f,min_isolation=%.1f',...
                ' WHERE cellid="%s" AND batch=%d'],...
                thisbatch.min_snr_index,thisbatch.min_isolation,cellid,batch);
        else
            sql=sprintf(['UPDATE NarfBatches SET val_snr=%.5f,est_snr=%.5f,min_isolation=%.1f',...
                ' WHERE cellid="%s" AND batch=%d'],...
                thisbatch.val_snr,thisbatch.est_snr,thisbatch.min_isolation,cellid,batch);
        end
        mysql(sql);
    end
        % All of this is called inside db_get_snr now:
        %s = request_celldb_batch(batch, cellid); 
%         est = dbgetscellfile('cellid', cellid, 'rawid', s{1}.training_rawid);
%         estreps = est(1).reps;
%         val = dbgetscellfile('cellid', cellid, 'rawid', s{1}.test_rawid);
%         valreps = val(1).reps;
    
    
    for i=1:length(criterea_names)
        if(thisbatch.(criterea_names{i})<=criterea.(criterea_names{i}))
            good(jj)=false;
            break
        end
    end
       
%     if(repstransform)
%         if isempty(snr_est) || (snr_est > threshold && snr_val > threshold)
%             cells{end+1} = cellid;
%         end
%     else
%         if isempty(snr_est) || (snr_est^2 * estreps > threshold && snr_val^2 * valreps > threshold)
%             cells{end+1} = cellid;
%         end
%     end
end
cellids(~good)=[];