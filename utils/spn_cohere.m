function plotpath = spn_cohere(stack,xxx,meta,sel_results)
    
    global META XXX STACK NARF_SAVED_IMAGES_PATH;
    
    SPECIAL_FORMAT=1;
    PLOT_STIM=1;
    
    modelcount=length(sel_results);
    
    stacks=cell(modelcount,1);
    xxxs=cell(modelcount,1);
    metas=cell(modelcount,1);
    maxplots=0;
    jpgnames={};
    n=0;
    cellids={};
    fh=figure;
    for midx=1:modelcount,
        modelpath=char(sel_results(midx).modelpath);
        fprintf('loading %s\n',modelpath);
        load_model(modelpath);
        stacks{midx}=STACK;
        xxxs{midx}=XXX;
        metas{midx}=META;
        
        [~, mod_idxs] = find_modules(STACK,'fir_filter');
        if isempty(mod_idxs),
           disp('no fir filter');
        else
           
           update_xxx(2);
           
           firmod=mod_idxs{1};
           f=XXX{2}.training_set{1};
           
           stim=XXX{2}.dat.(f).stim;
           pred=XXX{end}.dat.(f).stim;
           resp=XXX{end}.dat.(f).respavg;
           stim_filtered=XXX{firmod+1}.dat.(f).stim_filtered;
           e=sum(abs(stim(:,:,2)-stim(:,:,1)),1);
           eps=max(e)./1000;
           
           repcount=sum(~isnan(resp(1,:,:)),3);
           cohere_stim=find(e<eps);
           incohere_stim=find(e>eps);
           if isempty(cohere_stim),
              disp('no coherent stim');
           else
              
              maxrep=min(max(repcount(cohere_stim)),max(repcount(incohere_stim)));
              cohere_stim=find(e<eps & repcount>0 & repcount<=maxrep);
              incohere_stim=find(e>eps & repcount>0 & repcount<=maxrep);
              incohere_stim=incohere_stim(1:length(cohere_stim));
              
              n=n+1;
              cellids{end+1}=sel_results(midx).cellid;
              
              cp=pred(:,cohere_stim);
              cr=resp(:,cohere_stim);
              ip=pred(:,incohere_stim);
              ir=resp(:,incohere_stim);
              cE(n,1)=sqrt(mean((cp(:)-cr(:)).^2));
              iE(n,1)=sqrt(mean((ip(:)-ir(:)).^2));
              cX(n,1)=xcorr(cp(:),cr(:),0,'coef');
              iX(n,1)=xcorr(ip(:),ir(:),0,'coef');
              
              sfigure(fh);
              clf
              
              subplot(3,1,1);
              plot([cp(:) cr(:)]);
              
              subplot(3,1,2);
              plot([ip(:) ir(:)]);
              
              subplot(3,2,5);
              plot([0 30],[0 30],'k--');
              hold on
              plot(cE,iE,'.');
              hold off
              axis tight square
              
              subplot(3,2,6);
              plot([0 1],[0 1],'k--');
              hold on
              plot(cX,iX,'.');
              hold off
              axis tight square
              drawnow
           end
        end
    end
    disp('done');
    
        
