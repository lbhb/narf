function y = nl_sig_5PL(phi, z)
% The reparameterized 5 PL (5 parameter-logistic) sigmoid function, used
% frequently in dose-response curves. This is an extension of the 4 PL 
% which allows assymetric curves.
% (see http://onlinelibrary.wiley.com/doi/10.1002/cem.1218/pdf )
% (and http://www.raab.me.uk/esbatech/papers/5pl.pdf )
%   All the parameters should be in [0,1]:
%   => phi(1) is the baserate
%   => phi(2) is the peakrate (baserate<peakrate is allowed)
%   => phi(3) is the input that triggers the mean firing rate
%   => phi(4) is the curvature parameter (higher values are closer to step functions)
%   => phi(5) is the assymetry parameter (lower values are more assymetric)

    baserate = phi(1)*200;
    if baserate < 0,
        baserate = 0;
    elseif baserate > 200
        baserate = 200;
    end
    
    peakrate = phi(2)*200;
    if peakrate < 0,
        peakrate = 0;
    elseif peakrate > 200
        peakrate = 200;
    end
    
    C = phi(3);
    if C < 0,
        C = 0;
    elseif C > 1
        C = 1;
    end
    
    curvature = exp(9*phi(4)+1); % repam into exp(1) --- exp(10)
    if curvature < exp(1),
        curvature = exp(1);
    elseif curvature > exp(10)
        curvature = exp(10);
    end
    
    assymetry = exp(-5*phi(5)); % repam into exp(-5) --- exp(1)
    if assymetry < exp(-5),
        assymetry = exp(-5);
    elseif assymetry > 1
        assymetry = 1;
    end
    
    z(z<0) = 0;

    y = peakrate - (peakrate-baserate) ./ (1 + (2^(1/assymetry) -1 ) * ( (z/C).^curvature ) ).^assymetry;

end