function coefs = fir_gamma(phi, N)
    [N_chans, N_parms] = size(phi);
    
    if N_parms ~= 4
        error('fir_gamma needs exactly 4 parameters per channel');
    end
    
    delay = phi(:,1); 
    scale = phi(:,2); 
    p1    = phi(:,3);
    p2    = phi(:,4);   

    coefs = zeros(N);
    
    if ~any(p1 < 0) && ~any(p2 < 0) && ~any(delay < 0)
        t=0:(N(2)-1);        
        for c = 1:N_chans
            x = t - delay(c);
            x(x<0) = 0;
            coefs(c,:) = scale(c) .* pdf('gamma', x, p1(c), p2(c));
        end
    end         
end
