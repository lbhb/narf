function [x,bi] = excise(x)
% x = excise(x)
% Excises all rows of matrix x which contain a NaN at any position. 
% For higher dimensional matrices, excises a hyper-row (dimensions 2 to N)
% if there is a NAN anywhere.

if isvector(x)
    bi=~isnan(x);
    x = x(bi);
elseif ndims(x) == 2
    bi=any(isnan(x)');
    x(bi,:) = [];
elseif ndims(x) == 3    
    [a,b,c] = size(x);
    y = reshape(x, a, b*c); 
    bi=any(isnan(y)');
    y(bi,:) = [];   
    x = reshape(y, [], b, c);
else
    error('Not sure how to excise that!');
end

