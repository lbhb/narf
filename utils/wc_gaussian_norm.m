% function weights = wc_gaussian_norm(phi, N_inputs)
%
% phi Channels X parameters.  gaussian without trying to map to
% real frequencies
%   phi(:,1) = center (octaves above f0--200hz)
%   phi(:,2) = standard deviation (octaves)
%   phi(:,3) = offset from zero for all frequencies

function weights = wc_gaussian_norm(phi, N_inputs)
    [N_chans, N_parms] = size(phi);
    
    if N_parms < 2 || N_parms > 3
        error('WC_GAUSSIAN needs 2-3 parameters per channel');
    end
    
    spacing = (log(20000) - log(200)) / N_inputs;
    mu  = phi(:, 1)./spacing;     % Center of gaussian in kHz
    sig = phi(:, 2)./spacing;     % Gaussian STDDEV (as if kHz were linear)    
    if N_parms == 3
        offset = phi(:, 3);  % Offset amount
    else
        offset = zeros(1, N_chans);
    end
    
    weights = zeros(N_inputs, N_chans);
    
    for c = 1:N_chans
        % Otherwise, position the gaussian in the right place
        weights(:, c) = gauss1([mu(c), sig(c)], 1:N_inputs) + offset(c);
    end
end
