function ax = plot_complexity(data, modelnames, metric_name)
% A plot of model complexity (number of parameters) vs FOV remaining
        
%if ~any(strcmp(metric_name, {'r_ceiling', 'r_test', 'r_fit', 'r_test - r_fit', ...
%                'mse_test', }))
%    error('I only understand correlation metrics right now!');
%end
    
% Compute the means and standard error of each model
d_nparm  = round(nanmean(data(:,:,1)));  % Means should result in no change
D = data(:,:,2);
D(D==0) = NaN;
d_metric = D;
%d_fittime = nanmean(data(:,:,3));

disp('Computing mean');
d_means = nanmean(d_metric);
%disp('Computing median');
%d_means = nanmedian(d_metric);
d_count = sum(~isnan(d_metric));
%d_stddev = nanstd(d_metric);
d_stddev = nanstd(d_metric-repmat(d_metric(:,1),[1 size(d_metric,2)]));
d_stderr = d_stddev ./ sqrt(d_count);
len = length(modelnames);

figure('Name', 'Complexity Plot', 'NumberTitle', 'off', 'Position', [10 10 1000 1000]);
ax = axes();

% UGLY HACK TO ADD NUMBER OF PARMS FOR INITIAL FILTERS
for ii = 1:len
    if regexp(modelnames{ii}, '^but', 'once')
        d_nparm(ii) = d_nparm(ii) + 3;
    end
    if regexp(modelnames{ii}, '^ell', 'once')
        d_nparm(ii) = d_nparm(ii) + 3;
    end
    if regexp(modelnames{ii}, '^gam', 'once')
        d_nparm(ii) = d_nparm(ii) + 2;
    end
    if regexp(modelnames{ii}, '^apgt\d\dx\d', 'once')
        d_nparm(ii) = d_nparm(ii) + 2;
    end        
    if regexp(modelnames{ii}, '^ozgf\d\dx\d', 'once')
        d_nparm(ii) = d_nparm(ii) + 3;
    end
    if regexp(modelnames{ii}, '^szgf\d\dx\d', 'once')
        d_nparm(ii) = d_nparm(ii) + 3;
    end
end
names = shorten_modelnames(modelnames);
groups=zeros(1,length(names));
for nn=1:length(groups),
   if ~groups(nn),
      groups(nn)=max(groups)+1;
      ff=find(groups==0);
      for ii=ff,
         if length(names{nn})==length(names{ii}) &&...
               sum(names{nn}~=names{ii})==1,
            diffidx=find(names{nn}~=names{ii});
            if names{nn}(diffidx)>='0' & names{nn}(diffidx)<='9',
               groups(ii)=groups(nn);
            end
         end
      end
   end
end

% Scatter plot with text labels
hold on;

%jitter = randn(size(d_means));
jitter = zeros(size(d_means));

PLOT_LABELS=1;
PLOT_FRONTIER=1;
PLOT_ERROR_BARS=0;

for gg=unique(groups),
   ff=find(groups==gg);
   x=d_nparm(ff);
   yc = d_means(ff);
   if PLOT_FRONTIER,
      plot(x,yc, '-', 'Color', [0.7 0.7 0.7], 'LineWidth', 0.5);
   else
      plot(x,yc,  'Linewidth', 2, 'Linestyle', '-', 'Color', pickcolor(gg));
   end
end

for pass = 1:2
    for ii = 1:len
        name = names{ii};
        yc = d_means(ii);
        yt = yc + d_stderr(ii); 
        yb = yc - d_stderr(ii);
        x = d_nparm(ii) + 0.1*jitter(ii);                
        
        if pass == 1
           if PLOT_FRONTIER,
              plot(x,yc, '.', 'Color', pickcolor(groups(ii)),'MarkerSize',20);
           else
              if PLOT_ERROR_BARS,
                 line([x+0.1 x-0.1 x x x-0.1 x+0.1], [yt, yt, yt, yb, yb, yb], 'Linewidth', 2, 'Linestyle', '-', 'Color', pickcolor(groups(ii)));
              end
              plot(x,yc, 'Linestyle', '.', 'Color', [0 0 0]);
           end
        else
           if 0,
              %fittime = d_fittime(ii);
              %text(x, yc, sprintf('%s [%0.1f]', name, fittime/60));
              text(x(end), yc(end), name{end});
           end
        end
    end
end

if PLOT_FRONTIER,
   xf=unique(d_nparm);
   yf=zeros(size(xf));
   for ii=1:length(xf),
      yf(ii)=max(d_means(d_nparm<=xf(ii)));
   end
   keepidx=find([1 diff(yf)]);
   xf=xf(keepidx);
   yf=yf(keepidx);
   plot(xf,yf,'LineWidth',2,'Color',[0.4 0.4 0.4]);
end

if PLOT_LABELS,
   for gg=unique(groups),
      ff=max(find(groups==gg));
      x=d_nparm(ff) + 0.1*jitter(ff);
      yc = d_means(ff);
      name=names{ff};
      %fittime = d_fittime(ii);
      %text(x, yc, sprintf('%s [%0.1f]', name, fittime/60));
      ht=text(x, yc, name, 'Color', pickcolor(gg));
   end
end

aa=axis;
axis([0 aa(2:4)]);

hold off
title('Pareto Plot');
xlabel('Number of parameters');
ylabel(sprintf('%s', metric_name), 'interpreter', 'none');
% xticks(1:10:ceil(max(d_nparm(:))));

set(gcf,'PaperPosition',[0.25 1.5 8 8]);

fprintf('Total models: %d\n', length(d_means));

end