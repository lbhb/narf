function ax = plot_bar_pretty(data, modelnames, show_datapoints,do_median,show_bar_labels)
%ax = plot_bar_pretty(data, modelnames, show_datapoints)

if(nargin<4) do_median=0; end
if(nargin<5) show_bar_labels=1; end
DO_SORT=0; % LAS: added option to not sort for side-by-side comparisons
DO_SIGLINES=1;

if ~exist('show_datapoints', 'var')
    show_datapoints = true;
    %show_datapoints = false
end

barwidth = 0.8;

% Sort data by mean to make significance algorithm work
% data = abs(data);
Dunsortedmean = nanmean(data,1);
Dunsortedmedian = nanmedian(data,1);
Dunsortedcount = sum(~isnan(data),1);
if do_median
    mfun=@nanmedian;
else
    mfun=@nanmean;
end

D = [mfun(data,1)' data'];
[sD, idxs] = sortrows(D, -1);
if(DO_SORT)
    sig_idxs=1:size(data,2);
    label_idxs=idxs;
    data = sD(:, 2:end)';
else
    sig_idxs=idxs;
    label_idxs=1:size(data,2);
end

Dmean = mfun(data,1);

Dcount = sum(~isnan(data));
Dstddev = sqrt(nanvar(data,[],1));
Dstderr = Dstddev ./ sqrt(Dcount);
len = length(modelnames);

fig = figure('Name', 'Bar Plot', 'NumberTitle', 'off', ...
    'Position', [10 10 1200 80+len*25],'PaperPosition',[0.5 0.5 8 len*.3+0.8]);
hold on;
bar(1:len, Dmean, barwidth, 'r');
errorbar(1:len, Dmean, Dstderr, 'k.', 'LineWidth',2);

ax = gca;
set(gca,'XTick', 1:len);
if show_datapoints
    xmin = min(data(:));
    xmax = max(data(:));
    xrng = xmax-xmin;
    % axis([0 len+0.75 -0.1 max(data(:))*1.05]);
    axis([0 len+0.75 xmin-0.1*xrng xmax+(0.1*xrng)]);
    % Draw the jittered data points
    for ii = 1:len
        a = data(:, ii);
        jitter = -0.15*barwidth+0.3*barwidth*rand(size(a));
        plot(ii*ones(size(a))+jitter, a, 'k.'); % Plot jittered values
    end
else
    xmin = min(0, min(Dmean-Dstderr));
    xmax = max(0, max(Dmean+Dstderr));
    xrng = xmax-xmin;
    axis([0 len+0.75 xmin-0.1*xrng xmax+(0.05*xrng)]);
    %axis([0 len+0.75 -0.1 max(Dmean+Dstderr)*1.05]);
end

match=true;
commonstartlen=0;
model1=modelnames{1};
while match,
   commonstartlen=commonstartlen+1;
   if commonstartlen>=length(model1),
      match=false;
   else
      A = cellfun(@(x)(length(x)>commonstartlen &...
         strcmpi(x(1:(commonstartlen+1)),model1(1:(commonstartlen+1)))),modelnames);
      if sum(A)~=length(modelnames),
         match=false;
      end
   end
end

commonfinishlen=0;
match=true;
while match,
   A = cellfun(@(x)(length(x)>commonfinishlen &...
      strcmpi(x((end-commonfinishlen):end),model1((end-commonfinishlen):end))),modelnames);
   if sum(A)~=length(modelnames),
      match=false;
   else
      commonfinishlen=commonfinishlen+1;
   end
end

thelabels = {};
for ii = 1:len
    mn=sprintf('%0.3f',Dunsortedmean(ii));
    md=sprintf('%0.3f',Dunsortedmedian(ii));
    if commonstartlen>1 || commonfinishlen>1,
       thelabels{ii} = [ '(' num2str(Dunsortedcount(ii)) ')[' ...
          mn '/' md ']--' modelnames{ii}((commonstartlen+1):(end-commonfinishlen)) '--' ];
    else
       thelabels{ii} = [ '(' num2str(Dunsortedcount(ii)) ')[' ...
          mn '/' md ']' modelnames{ii} ];
    end
end
max_label_length=max(cellfun(@(x)length(x),thelabels(label_idxs))); %Luke: pad end of label strings for left alignment (easier to read)
%set(gca,'XTickLabel',cellfun(@(x)[x,repmat(' ',1,max_label_length-length(x))],thelabels(label_idxs),'Uni',0))
%set(gca,'XTickLabel', thelabels(label_idxs));
%set(gca,'CameraUpVector',[-1,0,0]);
set(gca,'View',[90 90]) %Luke: makes "y" (to MATLAB, x in plot) tick labels more spaced than using CamerUpVector

% Draw the significance lines
if(DO_SIGLINES)
    sep = 0.1 / (len-1);
    for ii=1:len-1
        a = data(:, sig_idxs(ii));
        z = 0;
        for jj = ii+1:len
            b = data(:, sig_idxs(jj));
            d=a-b;
            d=d(~isnan(d));
            if ~isempty(d),
                % Paired T test
                p = randttest(d, zeros(size(d)), 2000, 0);
                %p = signtest(d)
                %[~,p2] = ttest(a,b);
            else
                p=1;
            end
            if p < 0.001 && z < 3
                ls = '-';
                z = 3;
                r = -5/6*sep - sep*ii + sep;
                l = -6/6*sep - sep*ii + sep;
            elseif p < 0.01 && z < 2
                ls = '--';
                z = 2;
                r = -3/6*sep - sep*ii + sep;
                l = -4/6*sep - sep*ii + sep;
            elseif p < 0.05 && z < 1
                ls = ':';
                z = 1;
                r = -1/6*sep - sep*ii + sep;
                l = -2/6*sep - sep*ii + sep;
            else
                continue;
            end
            aa=axis;
            if aa(3)>l,
                axis([aa(1:2) l*1.1 aa(4)]);
            end
            line(sig_idxs([ii ii jj jj]), [r l l r], 'LineWidth', 2, 'Color', [0 0 0], 'LineStyle', ls);
        end
    end
end
%axpos=get(gca,'Position');
set(gca,'XTickLabel',[]);
axpos=[0.05 0.15 0.9 0.75];
set(gca,'Position',axpos);
%xlabel('temp')
%hxLabel = get(gca,'XLabel');
%set(hxLabel,'Units','data');
%xLabelPosition = get(hxLabel,'Position');
%y = 0;
XTick = get(gca,'XTick');
y=zeros(length(XTick),1);
fs = get(gca,'fontsize');
hText=text(XTick, y, thelabels(label_idxs),'fontsize',fs,'VerticalAlignment','middle','Interpreter','none');
if commonstartlen>1 || commonfinishlen>1,
   title(['Avg performance: ' modelnames{1}(1:commonstartlen) '--XX--' modelnames{1}((end-commonfinishlen+1):end)],'fontsize',fs,'Interpreter','none');
else
   title('Average population performance');
end
%xlabel('')
% ext=cell2mat(arrayfun(@(x)get(x,'extent'),hText,'Uni',0));
% yl=get(gca,'YLim');
% tl=get(gca,'TickLength');
%p=get(gca,'Position');
%y=max(-1*max(ext(1:(end-1),3))+yl(1)-tl(1),-p(1)-0.08);
% for i=1:length(hText)
%     p=get(hText(i),'Position');
%     p(2)=y;
%     set(hText(i),'Position',p)
% end

% svd commented out these LAS changes -- replaced by above?
% if(show_bar_labels)
%     hText = text(XTick, y, strrep(thelabels(label_idxs),'_','\_'),'fontsize',fs,'VerticalAlignment','middle');
%     xlabel('')
%     ext=cell2mat(arrayfun(@(x)get(x,'extent'),hText,'Uni',0));
% 
% yl=get(gca,'YLim');
% tl=get(gca,'TickLength');
% y=-1*max(ext(:,3))+yl(1)-tl(1);
% for i=1:length(hText)
%     p=get(hText(i),'Position');
%     p(2)=y;
%     set(hText(i),'Position',p)
% end
% else
%     xlabel('')
% end
% leave enough room for the title
%p=get(gca,'Position');
%p(4)=p(4)-.08;
%set(gca,'Position',p);
hold off;
end


