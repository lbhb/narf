function v = flatten_field(dat, sfs, field, trial_code)
% v = flatten_field(dat, sfs, field trial_code[=all])
%
% Concatenates each matricies found under 'dat.(sf).(field)', where sf is
% each of the elements of cell array sfs. Returns a single long vector.
% Intended to be used for quickly creating a 'resp' or 'stim' type vector.
%
% ARGUMENTS:
%    dat    A structure
%    sfs    Structure fieldnames to be used
%    field  The field to be extracted
%
% RETURNS:
%    v      A single long vector containing all the values
%
% NOTE: Relies on the default order of fields in each matrix being
% organized such that the first dimenson is time. 

if ~exist('trial_code','var'),
   trial_code=0;
end

% Count number of elements for each 'field' entry
lens = zeros(length(sfs), 1);
for ii = 1:length(sfs),
   if ~trial_code, % whole file
      lens(ii) = numel(dat.(sfs{ii}).(field));
   else
      tc=dat.(sfs{ii}).trial_code;
      %tc=tc(1:size(dat.(sfs{ii}).(field),2));
      lens(ii) = numel(dat.(sfs{ii}).(field)(:,tc==trial_code,:));
   end
end

% Create and fill the row vector
v = zeros(sum(lens), 1);
jj = 1;
for ii = 1:length(sfs),
   if ~trial_code, % whole file
      lens(ii) = numel(dat.(sfs{ii}).(field));
      v(jj:jj+lens(ii)-1) = reshape(dat.(sfs{ii}).(field), [], 1);
   else
      tc=dat.(sfs{ii}).trial_code;
      %tc=tc(1:size(dat.(sfs{ii}).(field),2));
      v(jj:jj+lens(ii)-1) = reshape(dat.(sfs{ii}).(field)(:,tc==trial_code,:), [], 1);
   end 
   jj=jj+lens(ii);
end