function y = nl_sig_betacdf(phi, z)
% The Beta C.D.F
% C.D.F have especially good properties of starting at 0 and ending at 1
% for inputs in [0,1], which is the case for normalized filters
% In particular, the Beta CDF has a great panel of possible shapes, making
% it suitable to model different transfer functions.

% this procedure allows "reversed" transfer function

% shape1: high values shift the sigmoid to the left
% shape2: high values are closer to a step function

    baserate = phi(1)*200;
    if baserate < 0,
        baserate = 0;
    elseif baserate > 200
        baserate = 200;
    end
    
    peakrate = phi(2)*200;
    if peakrate < 0,
        peakrate = 0;
    elseif peakrate > 200
        peakrate = 200;
    end
    
    shape1 = exp(phi(3)*4.5+0.5); % repam into exp(0.5) --- exp(5)
    if shape1 < exp(0.5),
        shape1 = exp(0.5);
    elseif shape1 > exp(5)
        shape1 = exp(5);
    end
    
    shape2 = exp(phi(4)*11-4); % repam into exp(-4) --- exp(7)
    if shape2 < exp(-4),
        shape2 = exp(-4);
    elseif shape2 > exp(7)
        shape2 = exp(7);
    end
    
    z(z<0) = 0;
    z(z>1) = 1;
    
    y = baserate + (peakrate-baserate) * betacdf(z, shape1, shape2); 
    
end