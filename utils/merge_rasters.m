function c=merge_rasters(a,b,dim)

if ~exist('dim','var'),
   dim=3;
end

if numel(a)==0,
   c=b;
   return
elseif numel(b)==0,
   c=a;
   return
end

if dim~=1,
   dd=size(a,1)-size(b,1);
   if dd<0
      a=cat(1,a,nan(-dd,size(a,2),size(a,3)));
   elseif dd>0,
      b=cat(1,b,nan(dd,size(b,2),size(b,3)));
   end
end

if dim~=2,
   dd=size(a,2)-size(b,2);
   if dd<0
      a=cat(2,a,nan(size(a,1),-dd,size(a,3)));
   elseif dd>0,
      b=cat(2,b,nan(size(b,1),dd,size(b,3)));
   end
end

if dim~=3,
   dd=size(a,3)-size(b,3);
   if dd<0
      a=cat(2,a,nan(size(a,1),size(a,2),-dd));
   elseif dd>0,
      b=cat(2,b,nan(size(b,1),size(b,2),dd));
   end
end

c=cat(dim,a,b);
