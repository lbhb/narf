% function y=nanimag2zero(x)
%
% convert NaN and imaginary entries of x to zero
%
function y=nanimag2zero(x)
    
    y=real(x);
    y(isnan(y) | isinf(y))=0;
    