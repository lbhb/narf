function enqueue_batch(batch, modulekeys, force_rerun)
% function enqueue_batch(batch, modulekeys, force_rerun[=1])
%
% ARGUMENTS:
%    batch  
%    modulekeys     {'env100', 'log2', 'etc'}
%    force_rerun    When true, jobs are added to queue even if a matching
%                   model is found to exist in NarfResults mysql table.
%
% RETURNS: nothing
%    

if ~exist('force_rerun','var'),
    force_rerun=1;
end

ret=request_celldb_batch(batch);

for ii=1:length(ret),
   enqueue_single_model(batch,ret{ii}.cellid,modulekeys,...
                        ret{ii}.training_set,ret{ii}.test_set,ret{ii}.filecode,force_rerun);
end

