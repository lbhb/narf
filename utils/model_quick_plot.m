function model_quick_plot(cellid,batchid,modelname);

dbopen;
mpath='/auto/data/code/saved_models/';
sel_results=struct();
sel_results.modelpath=sprintf('%s%d/%s/%d_%s_%s.mat',mpath,batchid,...
                              cellid,batchid,cellid,modelname);

plot_model_comparison([],[],[],sel_results);
drawnow
