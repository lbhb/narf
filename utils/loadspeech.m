% function stim=loadspeech(parmfile,startbin[=0],stopbin[=end],[filtfmt=none],[saf=100],[chancount=30],[forceregen=0],[includeprestim=0]);
%
%
% created SVD 2006-01-13
%
function [stim,ff]=loadspeech(parmfile,startbin,stopbin,filtfmt,saf,chancount,forceregen,includeprestim)

if ~exist('filtfmt','var'),
   filtfmt='none';
end
if ~exist('saf','var'),
   if strcmpi(filtfmt,'none'),
      saf=16000;
   else
      saf = 1000;
   end
end
if ~exist('chancount','var'),
   chancount = 30;
end
if strcmpi(filtfmt,'none') & chancount>1,
   chancount=1;
   disp('chancount forced to 1 because not filtering'); 
end   
if ~exist('forceregen','var'),
   forceregen = 0;
end

parmfile=strrep(parmfile,'/',filesep);
[bb,pp]=basename(parmfile);

% newf==0: old format
% newf==1: new format
newf = ~isempty(findstr(bb,'_'));

% figure out runclass
if newf,
   runclass=strsep(bb,'.',1);
   runclass=strsep(runclass{1},'_');
   runclass=runclass{end};
else
   runclass=strsep(bb,'.',1);
   runclass=strsep(runclass{1},'-');
   runclass=runclass{end};
end


switch upper(runclass),
 case {'SP1','SPC'},
  spidx=1;
  ll=3;
  s=Speech;
 case 'SP2',
  spidx=2;
  ll=4;
  s=Speech;
 case 'SP3',
  spidx=3;
  ll=4;
  s=Speech;
 case 'SP4',
  spidx=4;
  ll=4;
  s=Speech;
 case 'VOC',
  spidx=1;
  ll=3;
  s=FerretVocal;
  
 otherwise
  
  disp('Huh? Unknown runclass');
  keyboard
end

s=set(s,'SubSets',spidx);
s=set(s,'Duration',ll);
sents=get(s,'Names');
scount=length(sents);

filecount=get(s,'MaxIndex');
f=get(s,'SamplingRate');
tbinsize=1000/saf;
tbincount=ll.*saf;
if includeprestim, % includeprestim disabled for old data
   incstr='.incps1';
else
   incstr='';
end

if strcmp(computer,'PCWIN'),
   preprocfile=sprintf('%s\\%s.filt%s.samp%dHz.%dw%s.mat',...
                       'v:\\data\\tstim',runclass,...
                       filtfmt,saf,chancount,incstr);
else
   preprocfile=sprintf('%s/data/tstim/%s.filt%s.samp%dHz.%dw%s.mat',...
                       getenv('HOME'),runclass,...
                       filtfmt,saf,chancount,incstr);
end
ff=[];
%preprocfile

if exist(preprocfile,'file') && ~forceregen
   
   fprintf('loading saved %s stimulus spectrogram for %s\n',...
           filtfmt,bb);
   
   % load pregenerated stim
   load(preprocfile);
   
else
   fprintf('loadspeech: %s not found. generating stimulus spectrogram for %s\n',preprocfile,bb);
   
   % load some globals for use by wav2aud
   if ~strcmpi(filtfmt,'none') & ~strcmpi(filtfmt,'ozgf') & ...
         ~strcmpi(filtfmt,'lyonpassiveear'),
      loadload;
   end
   
   fprintf('converting from waveform ')
   fsin=f;
   fsout=saf;
   if strcmp(filtfmt,'audspectrogram'),
      fsadjust=log2(get(s,'SamplingRate')/16000);
      
      if fsout==100,
         param = [10 10 0.25 fsadjust]; % 10ms samples, compression 0.25
      elseif fsout==200,
         param = [5 5 .25 fsadjust]; % 5ms samples, compression 0.25
      end
      aud = audspectrogram(s,param,4,1);  % decimate to 30channels.
      
      stim = cat(3,aud{:});
      tstimparam=[];
   else
      % standard wav2spectral method
      for ii=1:filecount
         wav=waveform(s,ii);
         fprintf('.');
         
         [tstim,tstimparam]=wav2spectral(wav,filtfmt,fsin,fsout,chancount);
         if ii==1,
            stim=zeros(size(tstim,1),size(tstim,2),filecount);
         end
         stim(:,:,ii)=tstim;
         
         %keyboard
      end
      
   end
   fprintf('\n');
   if includeprestim,
      disp('includeprestim: assuming 0.4 sec fixed...');
      stim=cat(1,zeros(round(fsout*0.4),size(stim,2),size(stim,3)),...
               stim);
   end
   
   stimparam=tstimparam;
   % don't need to reshape raw waveform (filter=none) or save it
   if ~strcmpi(filtfmt,'none') && ~strcmpi(filtfmt,'audspectrogram'),
      stim=permute(stim,[2,1,3]);
   end
   if ~strcmpi(filtfmt,'none'),
      save(preprocfile,'stim','stimparam');
      fprintf('saving filtered stim to %s\n',preprocfile);
   end
end

if ~exist('startbin','var') || isempty(startbin) || startbin==0,
    return
    %startbin=1;
end
if ~exist('stopbin','var') || isempty(stopbin) || stopbin==0,
   stopbin=size(stim,2).*size(stim,3);
elseif stopbin>size(stim,2).*size(stim,3),
   stopbin=size(stim,2).*size(stim,3);
end

stim=stim(:,startbin:stopbin);
