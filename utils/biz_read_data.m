% function [respout,stimout]=biz_read_data(respfile,stimfile);
%
% example
% stimpath=['/auto/data/daq/Bizley/FerretSpeech/Level 17/Dec14/']
% stimfile=[stimpath 'bData-8_12_2014 level17_N 11_18.mat'];
% resppath='/auto/data/daq/Bizley/raw/';
% respfile=[dataroot '8_12_2014 level17_N 11_18_SU2_ev_SU2_ev/Chan_05.mat'];
% plus ozg file=[resppath 'FemaleSounds_ozgf100.mat'];
%
function [respout,stimout]=biz_read_data(respfile,stimfile);


global NARF_DEBUG NARF_DEBUG_FIGURE

pp=fileparts(fileparts(respfile));
sgfile=[pp filesep 'FemaleSounds_ozgf100.mat'];
load(sgfile);
wavfs=100;
chancount=size(stim{1}{1},1);

load(respfile);
spikebins=round(t.*wavfs);

load(stimfile);

trialset=unique(bData(:,6));
TrialCount=0;
for trialidx=trialset(:)',
    TrialCount=TrialCount+1;
    startrow=min(find(bData(:,6)==trialidx));
    stoprow=max(find(bData(:,6)==trialidx));
    
    startbin=round(bData(startrow,1).*wavfs);
    stopbin=round(bData(stoprow,2).*wavfs);
    
    wavtrial=zeros(chancount,stopbin-startbin+1);
    for ii=startrow:stoprow,
        sample=stim{bData(ii,3)}{bData(ii,4)};
        wavtrial(:,round(bData(ii,1)*wavfs-startbin)+(1:size(sample,2)))=sample;
    end
    
    if trialidx==trialset(1),
        stimout=wavtrial;
        respout=zeros(size(wavtrial,1),1);
    elseif size(stimout,2)>size(wavtrial,2),
        stimout=cat(3,stimout,zeros(size(stimout,1),size(stimout,2)));
        stimout(:,1:size(wavtrial,2),end)=wavtrial;
        respout=cat(2,respout,nan(size(respout,1),1));
    else
        stimout=cat(2,stimout,zeros(size(stimout,1),size(wavtrial,2)-size(stimout,2),size(stimout,3)));
        stimout=cat(3,stimout,wavtrial);
        respout=cat(1,respout,nan(size(wavtrial,2)-size(respout,1),size(respout,2)));
        respout=cat(2,respout,nan(size(respout,1),1));
    end
    respout(1:(stopbin-startbin),TrialCount)=0;
    thistrialspikebins=spikebins(spikebins>=startbin & spikebins<stopbin);
    for jj=1:length(thistrialspikebins),
        respout(thistrialspikebins(jj)-startbin+1,TrialCount)=...
            respout(thistrialspikebins(jj)-startbin+1,TrialCount)+1;
    end
    if NARF_DEBUG,
        if isempty(NARF_DEBUG_FIGURE),
            NARF_DEBUG_FIGURE=figure;
        end
        sfigure(NARF_DEBUG_FIGURE);
        clf
        subplot(2,1,1);
        if chancount==1,
            plot(wavtrial);
        else
            imagesc(stimout(:,:,TrialCount));
            axis xy;
        end
        title(sprintf('trial %d (actual %d)',TrialCount,trialidx));
        
        subplot(2,1,2);
        plot(respout(:,TrialCount));
        drawnow
        
        keyboard
    end
    
end

