function bw_oct=wc_gaussian_bwk_to_bwoct(phi)
    %phi = [mk,bwk]
    bw_oct=abs(phi(2))/10*log2(1000*phi(1)/200);