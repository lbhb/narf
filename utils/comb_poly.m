function y = comb_poly(p, varargin)
% This function performs the polynomial combination of varargin{...} with
% the coefficients stored in p, with:
% p = [a1 P11 P12 ...;
%      a2 P21 P22 ...
%           ....      ]
%
% the result is:
% a1 * varargin{1} .^ P11 .* varargin{2} .^ P12 * ...
%   + a2 * varargin(1} .^ P21 .* varargin{2} .^ P22 * ...
%   + ...

% sample polynom and data to test:
% p = [3 1 0 0; 0.5 2 4 0; 1 0 1 1];
% data = [10 11 12 14; 1 2 3 4; 0.1 0.2 0.3 0.4];
% output should be approx [80 1001 5869 25132]

% sample polynom and data to test:
% p = [-1 1 0; 1 0 1];
% data = [10 11 12 14; 1 2 3 4];
% output should be approx [-9 -9 -9 -10]

% smart way: halfway there, not working for now
% vars = permute(reshape(repmat(data,1,size(p,1)), [size(p, 1), size(data,2), size(data,1)]),[3 1 2]);
% result = sum(prod([p(:, 1) realpow(vars, p(:,2:end)) ], 2), 1);

if numel(varargin) ~= size(p, 2) - 1
    error('uncompatible sizes!\n');
end

% dumb way: build the expression as a string and execute it with eval:
expr = '';
for i=1:size(p, 1)
    expr = [expr ' + p(' num2str(i) ',1)'];
    for j=1:numel(varargin)
%         expr = [expr ' .* data(' num2str(j) ',:) .^ p(' num2str(i) ',' num2str(j+1) ')'];
        expr = [expr ' .* varargin{' num2str(j) '} .^ p(' num2str(i) ',' num2str(j+1) ')'];
    end
end

y = eval(expr);

end