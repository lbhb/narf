function delete_all_module_guis(fh)
% delete_all_module_guis()
%
% Deletes any gui handles, widgets, etc from NARFGUI. This is important to do
% whenever the STACK is about to be destroyed, because otherwise you can
% lose access to the GUI handles and they will persist even when you want
% to create newer, replacement GUI widgets later.
%


global NARFGUI;

for ii = 1:length(NARFGUI)
    delete_module_gui(ii,fh);
end

if(length(NARFGUI)>1)
    if(ishandle(NARFGUI{1}.fn_panel))
        fh_old=ancestor(NARFGUI{1}.fn_panel,'figure');
        ud=get(fh_old,'UserData');
        if(~isempty(ud))
            set(ud.handles.container_slider,'Enable','off');
        end
    end
end

NARFGUI = {};