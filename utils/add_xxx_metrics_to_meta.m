function meta=add_xxx_metrics_to_meta(xxx,meta)

% either define the fields you want
fns={'score_train_corr_byC','score_test_corr_byC','score_train_corr_byC_all','score_test_corr_byC_all'};


% or just keep everything, it doesn't cost much and it could make things easier later
fns=setdiff(fieldnames(xxx{end}),fieldnames(xxx{2}));
%except not this...
rm_fns={'testvals','trainvals'};
fns(ismember(fns,rm_fns))=[];

for i=1:length(fns)
    meta.xxx_metrics.(fns{i})=xxx{end}.(fns{i});
end

