% function weights = wc_gaussian(phi, N_inputs)
%
% phi Channels X parameters.  
%   phi(:,1) = center freq of Gaussian for each wight channel in KHz
%   phi(:,2) = standard deviation of Gaussian in KHz(?)
%
function coefs = fir_dexp(phi, N)
    [N_chans, N_parms] = size(phi);
    
    if N_parms ~= 6 
        error('FIR_DEXP needs exactly 6 parameters per channel');
    end
    
    lat1=phi(:,1);
    tau1=abs(phi(:,2));
    A1=phi(:,3);
    lat2=phi(:,4);
    tau2=abs(phi(:,5));
    A2=phi(:,6);
    
    coefs = zeros(N);
    
    t=0:(N(2)-1);
    for c = 1:N_chans
        coefs(c,:)=A1(c)*(exp(-tau1(c)*(t-lat1(c))) - exp(-tau1(c)*5*(t-lat1(c)))).*(t-lat1(c)>0)+...
           A2(c)*(exp(-tau2(c)*(t-lat2(c))) - exp(-tau2(c)*5*(t-lat2(c)))).*(t-lat2(c)>0);
    end
end
