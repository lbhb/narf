function sout=file_string_cleanup(sin)

badchars={'/','\','*','+','-','.',' '};

sout=basename(sin);
while ismember(sout(1),badchars),
    sout=sout(2:end);
end
while ismember(sout(end),badchars),
    sout=sout(1:(end-1));
end
for ii=1:length(badchars),
    sout=strrep(sout,badchars{ii},'_');
end
if sout(1)>='0' && sout(1)<='9',
    sout=['F',sout];
end
