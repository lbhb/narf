function delete_module_gui(idx,fh)
% delete_module_gui()
%
% Deletes any gui handles, widgets, etc from the module at NARFGUI index
% number idx.
%
% No arguments or return values. Use purely for side effects.

global NARFGUI;

ancestor(NARFGUI{1}.fn_panel,'figure')

del_fields={'plot_axes','plot_popup','plot_panel','fn_apply','fn_table','fn_popup','fn_panel','fn_recalc','fn_replot'};


for i=1:length(del_fields)
    try
        if(ancestor(NARFGUI{idx}.(del_fields{i}),'figure')==fh)
            %only delete if axes is part of the active modelpane figure
            delete(NARFGUI{idx}.(del_fields{i}));
        else
            %otherwise, disable all callbacks
            recursive_call_back_remove(NARFGUI{idx}.(del_fields{i}));
        end
    catch err
        a=2;
        % Do nothing if a delete failed
    end
end


if isfield(NARFGUI{idx}, 'plot_gui')
    NARFGUI{idx} = rmfield(NARFGUI{idx}, 'plot_gui');
end
end

function recursive_call_back_remove(h)
switch get(h,'Type')
    case 'uicontrol'
        set(h,'CallBack',[]);
        set(h,'Enable','off');
    case {'axes','line','image','text','uipanel','uitable'}
        set(h,'ButtonDownFcn',[]);
    otherwise
        a=2;
end
ch=get(h,'Children');
for i=1:length(ch)
    recursive_call_back_remove(ch(i));
end
end