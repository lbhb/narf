narf_set_path
dbopen;

batch=266;

modelname1='fb18ch100_lognn_wcg04_condap3z1_dexp_fit23c';
modelname2='fb18ch100_lognn_wcg04_condap3z1_dexp_fit23d';
modelname1='fb18ch100_lognn_wcg03_ap3z1_dexp_fit23c';
modelname2='fb18ch100_lognn_wcg05_ap3z1_dexp_fit23c';
modelname1='fb18ch100_lognn_wcg02_adp2pcv2ptl_ap3z1_dexp_fit23c';
modelname2='fb18ch100_lognn_wcg03_adp2pcv2ptl_ap3z1_dexp_fit23c';
modelname1='fb18ch100_lognn_wcg03_adp1pc_ap3z1_dexp_fit23c';
modelname2='fb18ch100_lognn_wcg03_adp1pc_ap3z1_dexp_fit23d';
modelname1='fb18ch100_lognn_wcg03_adp2pcv2ptl_ap3z1_dexp_fit23c';
modelname2='fb18ch100_lognn_wcg03_adp2pcv2ptl_ap3z1_dexp_fit23d';
modelname1='fb18ch100_lognn_wcg03_adp1pc_ap3z1_dexp_fit05v';
modelname2='fb18ch100_lognn_wcg05_adp1pc_ap3z1_dexp_fit05v';

snr_string='NarfBatches.est_snr*NarfBatches.est_snr*NarfBatches.est_reps';
sql=['SELECT NarfResults.*,',snr_string,' as est_snr',...
     ' FROM NarfResults INNER JOIN NarfBatches',...
     ' ON (NarfResults.cellid=NarfBatches.cellid AND NarfResults.batch=NarfBatches.batch)',...
     ' WHERE NarfResults.batch=',num2str(batch),' and modelname="',modelname1,'" order by cellid'];
cd1=mysql(sql);
sql=['SELECT NarfResults.*,',snr_string,' as est_snr',...
     ' FROM NarfResults INNER JOIN NarfBatches',...
     ' ON (NarfResults.cellid=NarfBatches.cellid AND NarfResults.batch=NarfBatches.batch)',...
     ' WHERE NarfResults.batch=',num2str(batch),' and modelname="',modelname2,'" order by cellid'];
cd2=mysql(sql);

d=[cat(1,cd1.est_snr) cat(1,cd1.r_ceiling) cat(1,cd2.r_ceiling)];

b=[sort(d(:,1)); max(d(:,1))];
bcount=6;
bb=b(round(linspace(1,length(b),bcount+1)));

for ii=1:bcount,
   ff=find(d(:,1)>=bb(ii) & d(:,1)<bb(ii+1));
   fprintf('%d: %.3f  %.3f  %.3f\n',length(ff),mean(d(ff,:)));
end



