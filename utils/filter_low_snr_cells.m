function cells = filter_low_snr_cells(batch, cellids, threshold, usecache)
% cells = filter_low_snr_cells(batch, cellids, threshold)
% 
% Filters out cellids that have fit or estimation data set values that are
%       (SNR^2 * repcount) < threshold
%
% Generally, threshold values are like this
%       0.2  very good cells
%       0.1  good cells
%       0.03 Top 50% (for batch 267)

if ~exist('usecache', 'var')
    usecache = true;
end

if threshold<=0,
   cells=cellids;
   return
end

dbopen;
cells = {};
for jj = 1:length(cellids)
    cellid = cellids{jj};    
    if usecache
        sql = ['select * from NarfBatches where batch=' num2str(batch) ...
               ' and cellid="' cellid '"'];
        ret = mysql(sql);
        if length(ret) ~= 1
            fprintf('Bad number of results found for cellid %s\n', cellid);
            continue;
        end
        snr_est = ret(1).est_snr;
        snr_val = ret(1).val_snr;
        estreps = ret(1).est_reps;
        valreps = ret(1).val_reps;
    else
        [snr_val, snr_est, z_val, z_est, sec_val, sec_est] = db_get_snr(cellid,batch);
        s = request_celldb_batch(batch, cellid);
        est = dbgetscellfile('cellid', cellid, 'rawid', s{1}.training_rawid);
        estreps = est(1).reps;
        val = dbgetscellfile('cellid', cellid, 'rawid', s{1}.test_rawid);
        valreps = val(1).reps;
    end
    
    % snr of exactly 0 suggests that it hasn't been measured
    if isempty(snr_est) || (snr_est^2 * estreps > threshold && snr_val^2 * valreps > threshold)
        %if isempty(snr_est) || snr_est==0 || (snr_est > threshold && snr_val > threshold)
        cells{end+1} = cellid;
    end  
end