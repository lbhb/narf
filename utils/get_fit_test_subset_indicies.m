function keepidx=get_fit_test_subset_indicies(repcount,datasubset,cfd,batch,fcs,Ltags,Ltags1)


if batch==261,
    SUBTORC=1;
else
    SUBTORC=0;
end
parms=dbReadData(cfd.rawid);
do_validx=true;
if strcmpi(fcs,'CN1') && ...
        isfield(parms,'Ref_RepIdx') && isfield(parms,'Tar_RepIdx')
    if isequal(parms.Tar_RepIdx,parms.Ref_RepIdx)
        if(~isempty(parms.Ref_RepIdx))
            vfrac=prod(parms.Ref_RepIdx) / Ltags;
            validx=1:parms.Ref_RepIdx;
            if(~isempty(Ltags1))
                validx=[validx,(1:parms.Ref_RepIdx)+Ltags1];
            end
            do_validx=false;
        end
    end
elseif batch~=288 && isfield(parms,'Ref_RepIdx') && ~isempty(parms.Ref_RepIdx),
    vfrac=prod(parms.Ref_RepIdx) / Ltags;
    if vfrac > 0,
        validx=1:parms.Ref_RepIdx;
        do_validx=false;
    end
end

if(do_validx)
    if ismember(batch,[263,288]),
        vfrac=0.2;
    elseif ismember(batch,[271 272 259 278 283]),
        vfrac=0.15;
    else
        vfrac=0.1;
    end
    
    %if max(repcount)>2,
    %    validx=min(find(repcount==max(repcount)));
    %else
    [ff,ii]=sort(repcount,'descend');
    ff=cumsum(ff)./sum(ff);
    validx=ii(1:min(find(ff>=vfrac)));
end
%end

if SUBTORC && datasubset==2,
    
    keepidx=1;
    % KLUDGE ALERT! convert stim to envelope
    %resp(1:round(0.5.*mdl.raw_stim_fs),:,:)=nan;
    stim=mean(stim,3);
elseif SUBTORC && datasubset==1,
    if size(stim,2)>5,
        keepidx=[3 6];
    else
        keepidx=3;
    end
    % KLUDGE ALERT! convert stim to envelope
    %resp(1:round(0.25.*mdl.raw_stim_fs),:,:)=nan;
    stim=mean(stim,3);
    
elseif datasubset==2
    keepidx=validx;
else
    keepidx=setdiff(find(repcount>0),validx);
end