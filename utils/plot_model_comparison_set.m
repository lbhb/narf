function plotpath = plot_model_comparison_set(stack,xxx,meta,sel_results)
    
cellids={sel_results.cellid};
uc=unique(cellids);

for ii=1:length(uc),
   ff=find(strcmp(cellids,uc{ii}));
   plotpath=plot_model_comparison(stack,xxx,meta,sel_results(ff));
   fh=gcf;
   fprintf('saving to current path: %s\n',pwd);
   eval(plotpath);
   
   %set(gcf,'PaperPosition',[0.25    0.25   8 10.5],'PaperOrientation','portrait');
   set(gcf,'PaperOrientation','portrait');
   plottiff=strrep(plotpath,'dpdf','dtiff');
   plottiff=strrep(plottiff,'pdf','tif');
   eval(plottiff);
   close(fh);
end
