% function weights = wc_gaussian(phi, N_inputs)
%
% phi Channels X parameters.  
%   phi(:,1) = center freq of Gaussian for each wight channel in KHz
%   phi(:,2) = standard deviation of Gaussian in KHz(?)
%   phi(:,3) = offset from zero for all frequencies

function weights = wc_gaussian(phi, N_inputs)
    [N_chans, N_parms] = size(phi);
    
    if N_parms < 2 || N_parms > 3
        error('WC_GAUSSIAN needs 2-3 parameters per channel');
    end
    
    mu_khz  = abs(phi(:, 1));     % Center of gaussian in kHz
    filter_sign=sign(phi(:,1));
    sig_khz = phi(:, 2);     % Gaussian STDDEV (as if kHz were linear)    
    if N_parms == 3
        offset = phi(:, 3);  % Offset amount
    else
        offset = zeros(1, N_chans);
    end
    
    weights = zeros(N_inputs, N_chans);
    
    for c = 1:N_chans
        if (mu_khz(c) < 0.2 || mu_khz(c) > 20)
            % If you go below 200Hz or above 20000Hz, make the prediction terrible
            weights(:, c ) = ones(N_inputs, 1);
        else
            % Otherwise, position the gaussian in the right place
            %sig_khz=1;mu_khz=.200*2^(4*log2(20000/200)/N_inputs);
            
            spacing = (log(20000) - log(200)) / N_inputs;
            mu = (log(mu_khz(c)*1000) - log(200)) / spacing;
            sigma = (sig_khz(c)/10) * mu;
            weights(:, c) = gauss1([mu, sigma], 1:N_inputs).*...
               filter_sign(c) + offset(c);
            
             %figure;plot(1:.1:N_inputs,gauss1([mu,sigma], 1:.1:N_inputs))
            if(0)
               %currently std changes with mean
               sig_khz=1;mu_khz=.200*2^(2*log2(20000/200)/N_inputs);
               spacing = (log(20000) - log(200)) / N_inputs;
               mu = (log(mu_khz(c)*1000) - log(200)) / spacing;
               sigma = (sig_khz(c)/10) * mu;
               figure;plot(1:.1:N_inputs,gauss1([mu,sigma], 1:.1:N_inputs))
               
               sig_khz=1;mu_khz=.200*2^(4*log2(20000/200)/N_inputs);
               mu = (log(mu_khz(c)*1000) - log(200)) / spacing;
               sigma = (sig_khz(c)/10) * mu;
               hold on;plot(1:.1:N_inputs,gauss1([mu,sigma], 1:.1:N_inputs),'r')
               
               %define bandwidth in octaves instead?
               sig_oct=.1;mu_khz=.200*2^(2*log2(20000/200)/N_inputs);
               spacing = (log2(20000) - log2(200)) / N_inputs; % octaves per bin
               mu = (log2(mu_khz(c)*1000) - log2(200)) / spacing; %bin
               sigma = sig_oct(c)/spacing; %bin
               figure;plot(1:.1:N_inputs,gauss1([mu,sigma], 1:.1:N_inputs))
               
               sig_oct=.1;mu_khz=.200*2^(4*log2(20000/200)/N_inputs);
               spacing = (log2(20000) - log2(200)) / N_inputs; % octaves per bin
               mu = (log2(mu_khz(c)*1000) - log2(200)) / spacing; %bin
               sigma = sig_oct(c)/spacing; %bin
               hold on;plot(1:.1:N_inputs,gauss1([mu,sigma], 1:.1:N_inputs),'r')
               
               
               %check converiosn
               %sig_khz=1;mu_khz=.200*2^(4*log2(20000/200)/N_inputs);
               spacing = (log(20000) - log(200)) / N_inputs;
               mu = (log(mu_khz(c)*1000) - log(200)) / spacing;
               sigma = (sig_khz(c)/10) * mu;
               sig_oct=wc_gaussian_bwk_to_bwoct([mu_khz,sig_khz]);
               spacing = (log2(20000) - log2(200)) / N_inputs; % octaves per bin
               mu2 = (log2(mu_khz(c)*1000) - log2(200)) / spacing; %bin
               sigma2 = sig_oct(c)/spacing; %bin
            
            end
        end
    end
end
