function [nmse, penalty, val_nmse] = pm_nmse_uselessnesspenalty()
% pm_nmse_uselessnesspenalty()
%
% Performance metric: Normalized MSE with a penalty of unused stimulus in
% the context of signal combination based on their value (i.e. when a
% stimulus is bigger, then it is more used, which is the case, for example:
% in weighted average with positive coefficients, in softmax, or in max)
%
% RETURNS:
%    nmse      Mean Squared Error
%    penalty   A penalty term occuring if one signal is inferior to all
%              others more than 90% of the time in the training set

global XXX STACK;

nmse = XXX{end}.score_train_nmse; 
val_nmse = XXX{end}.score_test_nmse; 
sf = XXX{end}.training_set{:};
penalty = 0;

[comb_mods, comb_idxs] = find_modules(STACK, 'combine_fields');

for c = 1:length(comb_idxs)
    mod = comb_mods{c}{:};
    xxx_i = comb_idxs{c} + 1;
    for s1 = 1:length(mod.inputs)
        stim_name1 = mod.inputs{s1};
        dominated = zeros(size(XXX{xxx_i}.dat.(sf).(stim_name1)));
        for s2 = 1:length(mod.inputs)
            if s1 == s2
                continue
            end
            stim_name2 = mod.inputs{s2};
            % update the timesteps in which s1 is dominated by any other
            % stimulus s2
            dominated = boolean(dominated + ( XXX{xxx_i}.dat.(sf).(stim_name1) < XXX{xxx_i}.dat.(sf).(stim_name2) ));
        end
%         proportion = sum(sum(sum(dominated))) / numel(dominated);
%         proportion = mean(mean(dominated));
        proportion = mean(mean(dominated( (50):(end-50), : ))); % we consider only meaningful periods (i.e. when there is actually a stimulus)
        if proportion > 0.9
            % the stimulus is dominated more than 5 % of the time
            penalty = penalty + proportion * 100; % amplify that proportion
        end
    end
    penalty = penalty / length(mod.inputs); % normalize the penalty by the number of stimulus
end