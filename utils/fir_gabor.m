% function coefs = fir_gabor(phi, N)
%
% skewed Gabor temporal RF from DeAngelis et al 1999
%   lat1=phi(:,1);
%    tau1=abs(phi(:,2));
%    A1=phi(:,3);
%    
%    tf=phi(:,4);
%    phase=phi(:,5);
% N= number of samples output
%
function coefs = fir_gabor(phi, N)
    N_chans=N(1);
    N_parms=size(phi,2);
    
    if N_parms ~= 5 
        error('FIR_GABOR needs exactly 5 parameters per channel');
    end
    
    lat1=phi(:,1);
    tau1=abs(phi(:,2));
    A1=phi(:,3);
    
    tf=phi(:,4);
    phase=phi(:,5);
    
    coefs = zeros(N_chans,N(2));
    
    t=0:(N(2)-1);
    for c = 1:N_chans
        coefs(c,:)=(t-lat1(c)>0) .* A1(c).*exp(-(t-lat1(c)/tau1(c))) .*...
            cos(tf(c)./10 .* (t-lat1(c)) + phase(c));
    end
end
