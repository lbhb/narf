function y = comb_max(p, varargin)
% This function performs the max between varargin{...}

if (isnan(p))
    % perform a regular max
    y = varargin{1};
    for i=2:numel(varargin)
        y = max(y, varargin{i});
    end
else
    p = max(min(p,10),-10); % bound p in [-10,10]
    % perform a soft-max with parameter p
    yd = exp(p*varargin{1});
    yn = yd .* varargin{1};
    for i=2:numel(varargin)
        tmp = exp(p*varargin{i});
        yd = yd + tmp;
        yn = yn + tmp .* varargin{i};
    end
    y = yn ./ yd;
end
end