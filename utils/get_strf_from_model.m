function strfs=get_strf_from_model(batch,cellids,modelnames);
   
global META;

    function strf = extract_strf(x)
        global STACK;
        w = [];
        h = [];
        % Take the first channel weights and FIR filter that exist
        for ii = 1:length(STACK)
            m = STACK{ii}{1};
            if strcmp(m.name, 'weight_channels') && isempty(w),
               if isfield(m, 'phi') && isfield(m, 'phifn') && ~isempty(m.phifn)
                  w = m.phifn(m.phi, 24);
               else
                  w = m.weights;
               end
            end
            if strcmp(m.name, 'pole_zeros') && isempty(h),
               % all parameters
               p = {};
               z = {};
               for ii = 1:m.n_inputs
                  p{ii} = m.poles(ii, :);
                  z{ii} = m.zeros(ii, :);
               end
               sys = zpk(z, p, m.gains);
               sys.InputDelay = abs(m.delays) / 1000; % (milliseconds)
               h = impulse(sys, linspace(0, 0.05, 26));  
               h = squeeze(h)';
               
            end
            if strcmp(m.name, 'fir_filter') && isempty(h),
               if isfield(m, 'phi') && isfield(m, 'phifn') && ~isempty(m.phifn)
                  h = m.phifn(m.phi, [m.num_dims m.num_coefs]);
               else
                  h = m.coefs;
               end
            end
        end
                
        if isempty(w)
            w = [1];
        else
            % TODO: Proper normalization uses the stimulus power
            % w = w./repmat(sum(w), size(w,1), 1);  % Normalize wrongly
            w = w; % Don't normalize
        end
        
        %w = w./max(abs(w(:))).*max(abs(strf(:)));
        %h = h./max(abs(h(:))).*max(abs(strf(:)));
        strf = w * h;
        
    end

nullfn = @(x) 0;
[strfs, ~, ~] = load_model_batch(batch, cellids, modelnames, ...
                       @extract_strf, nullfn);

end

