function zgthresh()

global MODULES XXX;

append_module(MODULES.nonlinearity.mdl(struct('fit_fields', {{'phi'}}, ...
                                              'phi', [0 1], ...
                                              'nlfn', @nl_zerothresh)));
