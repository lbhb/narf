function fit22()
global STACK;

function fn = make_subfitter(del)
    function [a,b,c,d] = subfitter(prev_opts)
        
        % Determine which module is being fit.
        module_being_fit = '';
        for kk = 1:length(STACK)
            if isfield(STACK{kk}{1}, 'fit_fields') && ...
                    ~isempty(STACK{kk}{1}.fit_fields)
                module_being_fit = STACK{kk}{1}.name;
                break;
            end
        end
        
        % If the module is FIR-like, we should boost on it
        if (strcmp(module_being_fit, 'fir_filter') || ...
                strcmp(module_being_fit, 'weight_channels')) && ...
                isempty(STACK{kk}{1}.phifn),
            if exist('prev_opts', 'var')
                [a,b,c,d] = fit_boo(prev_opts);
            else
                [a,b,c,d] = fit_boo('StopAtAbsScoreDelta', del, ...
                    'StopAtStepSize', 10^-6,...
                    'StepAnyway', true, ...%   'ScaleStepSize', true, ...
                    'StopAtStepNumber', 1, ...
                    'StepGrowth', 1.1);
            end
        else
            % Otherwise, use a good NL fitter
            [a,b,c,d] = fit_scaat('StopAtAbsScoreDelta', del, ...                
                'InitStepSize', min(100, max(1.0, 1000/del)), ...
                'StopAtStepNumber', 10);
            
            % Otherwise, we should use fminsearch
            %[a,b,c,d] = fit_fminsearch(optimset('MaxIter', 1000, ...
            %    'MaxFunEvals', 1000, ...
            %    'TolFun', del, ...
            %    'TolX', del));
        end
        
    end
    phi = pack_fittables(STACK)';
    fprintf('del=%.7e\n',del);
    fprintf('phi=[');
    fprintf('%0.3f ', phi(:));
    fprintf(']\n')
    fn = @subfitter;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% disp('Removing static spike NL for quick fit');
% [nl_mods,nl_idx] = find_modules(STACK, 'nonlinearity', false);
% if strcmp(func2str(STACK{nl_idx{end}}{1}.nlfn),'nl_dcg'),
%     nl_mods=nl_mods(1:(end-1));
%     nl_idx=nl_idx(1:(end-1));
% end 
% nl_save=nl_mods{end};
% nl_idx=nl_idx{end};
% for jj=1:length(STACK{nl_idx}),
%     STACK{nl_idx}{jj}.nlfn=@nl_dummy;
%     STACK{nl_idx}{jj}=rmfield(STACK{nl_idx}{jj},'fit_fields');
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Do a limited early search without an NL
scale=10^-2;
stop_at=10^-6;
while(scale > stop_at)
        for mm = 1:2
            if mm == 1
                fprintf('\n\nMSE!MSE!MSE!MSE!MSE!MSE!MSE!MSE!MSE!MSE!MSE!MSE!\n');
                semse();
            elseif mm == 2
                fprintf('\n\nCOHERENCE!COHERENCE!COHERENCE!COHERENCE!COHERENCE!\n');
                coher();
            else
                fprintf('\n\nNLOGL!NLOGL!NLOGL!NLOGL!NLOGL!NLOGL!NLOGL!NLOGL!\n');
                llpoisson();            
            end
            fit_iteratively(make_subfitter(scale), ...
                create_term_fn('StopAtAbsScoreDelta', scale));
            pop_module();
        end
    scale = scale * 0.8;
end
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Restore the NL
% 
% if strcmpi(func2str(nl_save{1}.nlfn),'nl_dexp'),    
%     pop_module();  % remove MSE module
%     pop_module();  % remove dexp
%     dexp();    
% else
%     disp('Restoring static spike NL for final fit');
%     STACK{nl_idx}=nl_save; % restore original NL function
% end
% update_xxx(2);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now fit everything

% 
% % Now do multiobjective fitting
% scale=10^-2;
% stop_at=10^-6;
% while(scale > stop_at)
%     fit_iteratively(make_subfitter(scale), create_term_fn('StopAtAbsScoreDelta', scale));
%     scale = scale * 0.8;
% end


end
