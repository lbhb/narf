function coher()

global MODULES META STACK;

mods = find_modules(STACK, 'coherence', true);
if isempty(mods)    
    append_module(MODULES.coherence);   
end

META.perf_metric = @pm_cohere;
