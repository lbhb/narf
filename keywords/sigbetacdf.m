function sigbetacdf()

global MODULES XXX;

ff=XXX{end}.training_set{1};
maxresp = max(XXX{end}.dat.(ff).stim(:));
minresp = min(XXX{end}.dat.(ff).stim(:));

append_module(MODULES.normalize_output.mdl());

append_module(MODULES.nonlinearity.mdl(...
    struct('fit_fields', {{'phi'}}, ...
           'fit_constraints', {{struct( ...
                                     'var', 'phi', ...
                                     'lower', [0   0   0   0], ...
                                     'upper', [1   1   1   1], ...
                                     'fitter', 'custom')}}, ...
           'phi', [minresp/200 maxresp/200 0.1 0.45], ...
           'nlfn', @nl_sig_betacdf)));


       
fitSubstack();