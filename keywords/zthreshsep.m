function zthreshsep()

global MODULES XXX;

meanstim = nanmean(flatten_field(XXX{end}.dat,XXX{end}.training_set,'stim'));
stdstim = nanstd(flatten_field(XXX{end}.dat,XXX{end}.training_set,'stim'));
mm=meanstim-stdstim*2;
meanresp = nanmean(flatten_field(XXX{end}.dat,XXX{end}.training_set,'respavg'));
stdresp = nanstd(flatten_field(XXX{end}.dat,XXX{end}.training_set,'respavg'));
rr=meanresp-stdresp*2;

append_module(MODULES.nonlinearity.mdl(struct('fit_fields', {{'phi'}}, ...
   'input_stim','stim_filtered',...
   'phi', [0 1 0], ...
   'nlfn', @nl_zerothresh)));
fitSubstack();
