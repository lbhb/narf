% function fit05u()
% 
% Some sort of hybrid fitter?

function fit05u5()

    global STACK
    
MaxStepsPerIteration=10;
StepGrowth=2;

function fn = make_subfitter(del)
    function [a,b,c,d] = subfitter(prev_opts)    
    
        % Detect whether the fittables are in a FIR block or not    
        module_being_fit = '';
        for kk = 1:length(STACK)
            if isfield(STACK{kk}{1}, 'fit_fields') && ~isempty(STACK{kk}{1}.fit_fields)
                module_being_fit = STACK{kk}{1}.name;
                break;
            end
        end
    
        if strcmp(module_being_fit, 'fir_filter') || ...
                (strcmp(module_being_fit, 'weight_channels') && ...
                    isempty(STACK{kk}{1}.phifn))
            if exist('prev_opts', 'var')
                [a,b,c,d] = fit_boo(prev_opts);
            else
                         
                [a,b,c,d] = fit_boo('StopAtAbsScoreDelta', del, ...
                                    'StopAtStepSize', 10^-6,...
                                    'StopAtStepNumber', MaxStepsPerIteration, ...
                                    'StepAnyway', true, ...
                                    'StepGrowth', StepGrowth);
            end
        else
            if exist('prev_opts', 'var')
                [a,b,c,d] = fit_scaat(prev_opts);
            else
                [a,b,c,d] = fit_scaat('StopAtAbsScoreDelta', del, ...                                      
                                      'InitStepSize', 1.0, ...
                                      'StopAtStepNumber', 2*MaxStepsPerIteration);
            end
        end
    end
    phi = pack_fittables(STACK)';
    fprintf('phi=[');
    fprintf('%0.3f ', phi(:));
    fprintf(']\n');    
    fn = @subfitter;   
end

function fn = make_booster(del)
    function [a,b,c,d] = boostit(prev_opts)
        if exist('prev_opts', 'var')
            [a,b,c,d] = fit_boo(prev_opts);
        else
            [a,b,c,d] = fit_boo('StopAtAbsScoreDelta', del, ...
                                'StopAtStepSize', 10^-6,...                                
                                'StopAtStepNumber', MaxStepsPerIteration, ...
                                'StepAnyway', true, ...    
                                'StepGrowth', StepGrowth);
        end
    end
    fn = @boostit;
end

disp('Removing static spike NL for quick fit');
[nl_mods,nl_idx] = find_modules(STACK, 'nonlinearity', false);
if strcmp(func2str(STACK{nl_idx{end}}{1}.nlfn),'nl_dcg'),
    nl_mods=nl_mods(1:(end-1));
    nl_idx=nl_idx(1:(end-1));
end
nl_save=nl_mods{end};
nl_idx=nl_idx{end};

for jj=1:length(STACK{nl_idx}),
    STACK{nl_idx}{jj}.nlfn=@nl_dummy;
    STACK{nl_idx}{jj}=rmfield(STACK{nl_idx}{jj},'fit_fields');
end

% Instead of the NL, place a WC at the end.
wc01();
semse();

% Run sequence of progressively tighter fits on model w/o NL
fit_boo('StopAtAbsScoreDelta', 10^-2.0, ...
        'StepAnyway', true, ...
        'StepGrowth', StepGrowth);
fit_iteratively(make_booster(10^-3),  create_term_fn('StopAtAbsScoreDelta', 10^-3));
fit_iteratively(make_booster(10^-3.5), create_term_fn('StopAtAbsScoreDelta', 10^-3.5));
fit_iteratively(make_booster(10^-4),  create_term_fn('StopAtAbsScoreDelta', 10^-4));
fit_iteratively(make_booster(10^-4.5),  create_term_fn('StopAtAbsScoreDelta', 10^-4.5));

if strcmpi(func2str(nl_save{1}.nlfn),'nl_dexp'),    
    disp('Restoring DEXP NL for final fit');   
    pop_module();  % remove MSE module
    pop_module();  % remove WC01 module
    pop_module();  % remove dexp
    dexp(); % Restore DEXP  
    semse(); % Restore MSE
else
    fprintf('WARNING: I only have been tested with DEXP right now');
    disp('Restoring static spike NL for final fit');
    STACK{nl_idx}=nl_save; % restore original NL function
end
update_xxx(2);

% now run through the series of fits again with static NL included.
steps = [10^-3, 10^-3.5 10^-4 10^-4.5 10^-5];
for ii = 1:length(steps)
    del = steps(ii);
    % fit_iteratively(make_booster(del),  create_term_fn('StopAtAbsScoreDelta', del));
    fit_iteratively(make_subfitter(del/10), create_term_fn('StopAtAbsScoreDelta', del));
end
   
end

