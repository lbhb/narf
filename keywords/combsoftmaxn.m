function combsoftmaxn()

% soft-max operation

global MODULES STACK XXX;

inputs = {'stim1', 'stim2'};

% inputs should be in stim1, stim2
% this outputs to stim
append_module(MODULES.combine_fields.mdl(struct('inputs', {inputs}, ...
    'fit_fields', {{'combination_p'}}, ...
    'combination_fn', 'comb_max', ...
    'combination_p', 0.5)));

update_xxx(1)

% fitSubstack();
