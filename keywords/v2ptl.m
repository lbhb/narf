function v2ptl()
% partial second-order expansion.  Only keep products of different
% channels (x1 * x2), not channels with themselves (x1 * x1)

global MODULES XXX;

append_module(MODULES.add_nth_order_terms);

fns = fieldnames(XXX{end}.dat);
[T, S, C] = size(XXX{end}.dat.(fns{1}).stim);
if C==3,
    keep_channels=[1 2 3];
elseif C==10,
    keep_channels=[1:4 6 9];
    %keep_channels=[6 9];
elseif C==21,
    keep_channels=[1:6 9 14 18];
end

append_module(MODULES.subsample_channels.mdl(...
    struct('keep_channels',keep_channels)));

