% function fit05adepperfile()
%
% use SEMSE rather than standard MSE
function fit05adepperfile()

global STACK XXX

disp('NOW FITTING DEP STAGES PER FILE');

% Remove any correlation at end of stack
if strcmp(STACK{end}{1}.name, 'correlation')
    STACK = STACK(1:end-1);
    XXX = XXX(1:end-1);
end

% find STACK entries that contain linear filters
[~, fir_idxs] = find_modules(STACK, 'depression_filter_bank', false);

if ~isempty(fir_idxs),
    % now fit linear filter stages per-filecode
    split_stack(fir_idxs{1},fir_idxs{end});
end

fit05a;


