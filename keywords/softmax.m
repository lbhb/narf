function softmax()

% soft-max operation, with fittable p weighting coefficient

global MODULES STACK XXX FLATXXX;

inputs = {'stim'};

if strcmp(STACK{end}{1}.name, 'mean_squared_error')
    pop_module();  % trim the mean_squared_error module from the stack
end
if strcmp(STACK{end}{1}.name, 'normalize_channels')
    post_norm=1;
    pop_module();  % trim the normalization module if there
                   % (pole-zero models)
else
    post_norm=0;
end

for l = length(STACK):-1:2,
    if strcmp(STACK{l}{1}.name, 'pole_zeros')
        % and make sure that "sum_channels" is false
        STACK{l}{1}.sum_channels = false;
        break;
   elseif strcmp(STACK{l}{1}.name, 'fir_filter')
        % replace the output by "stim_filtered"
        STACK{l}{1}.sum_channels = 0;
        STACK{l}{1}.baseline=repmat(STACK{l}{1}.baseline,...
                                    [1 size(STACK{l}{1}.coefs,1)]);
        break;
    end
end

XXX = XXX(1:2);
FLATXXX = FLATXXX(1:2);
update_xxx(1);

% this outputs to stim
append_module(MODULES.combine_fields.mdl(struct('inputs', {inputs}, ...
    'fit_fields', {{'combination_p'}}, ...
    'combination_fn', 'comb_max', ...
    'combination_p', 0.00)));
% 0.05 for \combination_p\ corresponds to a "soft" softmax. For a harder
% softmax, use 0.2 or 0.5

if post_norm,
    append_module(MODULES.normalize_channels);
end

update_xxx(1)

% fitSubstack();
