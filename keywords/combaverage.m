function combaverage()

% simple average between two stimuli, without any fitting of the weights

global MODULES;

% inputs should be in stim1, stim2
% this outputs to stim
append_module(MODULES.combine_fields.mdl(struct(...
    'combination_p', [0.5 1 0; 0.5 0 1])));

