function pupgain()

global MODULES XXX;

meanpupil = nanmean(flatten_field(XXX{end}.dat,XXX{end}.training_set,'pupil'));
meanpred = nanmean(flatten_field(XXX{end}.dat,XXX{end}.training_set,'stim'));
fit_fields={{'offset'}};
%fit_fields={{'gain','offset'}};

% default inputs are already stim1, stim2
% default output already stim
append_module(MODULES.pupil_gain.mdl(struct(...
   'gain',[meanpred 0],...
   'offset',[meanpupil 0],...
   'fit_fields', fit_fields,...
   'bincount', 5,...
   'smoothwindow',0)));

