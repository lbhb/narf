function adpo2()
% second order STP, only preserve product of STP channels (discard individuals)
% normal intial conditions. start with slightly weaker STP
% parameters than for adp2pc.  Also no offset, since that doesn't
% seem to help in the adp2pcnov2ptl vs adp2pcv2ptl comparison

global MODULES XXX;

append_module(MODULES.normalize_channels.mdl(struct('force_positive', true)));

sf=XXX{end}.training_set{1};
chan_count=size(XXX{end}.dat.(sf).stim,3);
init_str=repmat([1 2],[1 chan_count]);
init_tau=repmat([10 20],[1 chan_count]);
init_offset=zeros(1,chan_count);

append_module(MODULES.depression_filter_bank.mdl(...
                    struct('strength', init_str, ...
                           'tau',      init_tau, ...
                           'offset_in',init_offset,...
                           'tau_norm',  100,...
                           'facil_on', 1, ...
                           'per_channel', 1, ...
                           'fit_fields', {{'strength', 'tau'}})));

fns = fieldnames(XXX{end}.dat);
[T, S, C] = size(XXX{end}.dat.(fns{1}).stim);
if C==1,
    keep_channels=[1];
elseif C==2,
    keep_channels=[1 2];
elseif C==4,
    keep_channels=[1 2; 3 4];
elseif C==6,
    keep_channels=[1 2; 3 4; 5 6];
elseif C==8,
    keep_channels=[1 2; 3 4; 5 6; 7 8];
end

append_module(MODULES.subsample_channels.mdl(...
    struct('keep_channels',keep_channels)));

