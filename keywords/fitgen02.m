function fitgen02()

% same as fitgen05, but without any prior/posterior optimization method
% (only evolution) and with a tailored fitness function penalizing unused
% submodels

global GA_PhiHistory GA_FLATXXXHistory GA_FLATXXXPointer GA_MaskFittable GA_LowerBound GA_UpperBound MODULES STACK META FLATXXX XXX;

mods = find_modules(STACK, 'split_stim', true);

if ~isempty(mods)
    % we have a split: use the penalized version of semse
    mods = find_modules(STACK, 'mean_squared_error', true);
    if isempty(mods)
        append_module(MODULES.mean_squared_error.mdl(struct('norm_by_se', 0.5)));
    end
    META.perf_metric = @pm_nmse_uselessnesspenalty;
else
    % we don't: use normal semse
    semse();
end

% genetic algorithm parameters
PopSize = 100; % 100
TolFun = 1e-6; % 1e-6 is not enough?
Gen = 10000; % 10000
StallGen = 200;
RunNumber = 4;

    function [termcond, term_score, n_iters, term_phi] = two_dimensional_fitter_loop(fittername, highlevel_fn)
        
        phi_init = pack_fittables(STACK);
        
        if isempty(phi_init)
            fprintf('Skipping because there are no parameters to fit.\n');
            term_cond = NaN;
            term_score = NaN;
            n_iters = 0;
            return
        end
        
        % initialization of the StimHistory and StackHistory
        GA_PhiHistory = Inf((size(GA_MaskFittable,1)-1)*PopSize, length(phi_init));
        GA_FLATXXXHistory = cell((size(GA_MaskFittable,1)-1)*PopSize,1);
        GA_FLATXXXPointer = 1;
        
        n_iters = 0;
        start_depth = find_fit_start_depth(STACK);
        fprintf('The STACK computation start depth is %d\n', start_depth);
        
        function score_diversity = my_obj_fn(phi2)
            
            n_sol = size(phi2,1);
            n_params = size(phi2,2);
            score_diversity = Inf(n_sol,2);
            
            for phi_i=1:n_sol
                
                unpack_fittables(phi2(phi_i,:));
                
%                 % check for already computed stimulus
%                 s = start_depth-1;
%                 for c=(size(GA_MaskFittable,1)-1):-1:1
%                     [~, last_param_position] = find(GA_MaskFittable(c+1,:),1);
%                     cols = 1:(last_param_position - 1);
%                     phi = Inf(1,n_params);
%                     phi(cols) = phi2(phi_i,cols);
%                     [is_stored, stored_index] = ismember(phi,GA_PhiHistory,'rows');
%                     if is_stored
%                         s = GA_MaskFittable(c,last_param_position - 1);
%                         FLATXXX{s+1}.dat = GA_FLATXXXHistory{stored_index};
%                         XXX{s+1}.dat = GA_FLATXXXHistory{stored_index};
%                         break
%                     end
%                 end
                
                % force the computation of the whole stack
                s = start_depth-1;
                FLATXXX = FLATXXX(1:(s+1));
                XXX = XXX(1:(s+1));

                update_xxx(s+1);
                
%                 % try to update the cached copies
%                 for c=(size(GA_MaskFittable,1)-1):-1:1
%                     [~, last_param_position] = find(GA_MaskFittable(c+1,:),1);
%                     cols = 1:(last_param_position - 1);
%                     phi = Inf(1,n_params);
%                     phi(cols) = phi2(phi_i,cols);
%                     if ~ismember(phi,GA_PhiHistory,'rows');
%                         s = GA_MaskFittable(c,last_param_position - 1);
%                         GA_FLATXXXHistory{GA_FLATXXXPointer} = FLATXXX{s+1}.dat;
%                         GA_PhiHistory(GA_FLATXXXPointer,:) = phi;
%                         GA_FLATXXXPointer = GA_FLATXXXPointer + 1;
%                         if GA_FLATXXXPointer > length(GA_FLATXXXHistory)
%                             GA_FLATXXXPointer = 1;
%                         end
%                     end
%                 end
                
                [m, p] = META.perf_metric();
                score_diversity(phi_i,1) = m + p;
                
            end
            
            if (n_sol>1)
                dists = squareform(pdist(phi2));
                score_diversity(:,2) = -min(dists+max(max(dists))*eye(n_sol));
            end
            
            % Print 1 progress dot for every 5 iterations no matter what
            if isequal(mod(n_iters, 5), 0) %% && ~bequiet
                fprintf('.');
            end
            
            n_iters = n_iters + n_sol;
        end
        
        fprintf('Fitting %d variables with %s:\n', length(phi_init), fittername);
        
        [X, FVAL, termcond] = highlevel_fn(@my_obj_fn, phi_init);
        
        [term_score, minvali] = min(FVAL(:,1));
        term_phi = X(minvali,:);
        
        unpack_fittables(term_phi);
        calc_xxx(start_depth);
        
        fprintf('Complete fit with %d objective function evaluations.\n', n_iters);
        fprintf('----------------------------------------------------------------------\n');
        
    end

    function print_stats_search(best_indivs, best_origins, best_scores)
        fprintf('\n\n\n\n\n\n\n\n\n\n\nSUMMARY TABLE:\n');
        for ii=1:(24+size(best_indivs,2)*12)
            fprintf('-');
        end
        [~,best] = sort(best_scores);
        for ii=1:numel(best_origins)
            if ii==best(1)
                fprintf('\n| * |');
            else
                fprintf('\n|   |');
            end
            letters = min(numel(best_origins{ii}), 12);
            fprintf('%s | ',best_origins{ii}(1:letters));
            fprintf('%4.2f',best_scores(ii));
            for jj=1:size(best_indivs,2)
                fprintf(' | %+7.2E',best_indivs(ii,jj));
            end
            %             fprintf('\n');
        end
        fprintf('\n');
        for ii=1:(24+size(best_indivs,2)*12)
            fprintf('-');
        end
        fprintf('\n');
    end


% initialization of all the global variables used by genetic algorithms and
% prefitrnd3
phi0 = pack_fittables(STACK)';
initialize_GA_GLOBALS(phi0)

best_indivs = [];
best_origins = {};
best_scores = [];
best_score = Inf;

% now do the optimization RunNumber times
for run=1:RunNumber
    % Unpack the initial stack
    unpack_fittables(phi0);calc_xxx(1);
    
    [termcond, term_score, n_iters, term_phi] = two_dimensional_fitter_loop('gagamultiobj()', ...
        @(obj_fn, phi_init) ga_gamultiobj(obj_fn, length(phi_init), [], [], [], [], [], [], ...
        gaoptimset('TolFun', TolFun, ...
        'PopulationSize', PopSize, ...
        'Generations', Gen, ...
        'Vectorized', 'on', ...
        'StallGenLimit', StallGen, ...
        'ParetoFraction', 0.1, ...
        'MutationFcn', @ga_mutation, ...
        'CrossoverFraction', 0.2, ...%     'SelectionFcn', @selectiontournament, ...
        'CrossoverFcn', @ga_crossover, ...
        'DistanceMeasureFcn', @ga_distancecrowding, ...
        'InitialPopulation', [rand(PopSize,length(phi_init)).*repmat(GA_UpperBound-GA_LowerBound, PopSize,1) + repmat(GA_LowerBound, PopSize,1)])));
%         'InitialPopulation', [phi0; rand(PopSize-1,length(phi_init)).*repmat(GA_UpperBound-GA_LowerBound, PopSize-1,1) + repmat(GA_LowerBound, PopSize-1,1)])));
    
    unpack_fittables(term_phi);
    
    % compute the last score - update if necessary
    phi_candidate = pack_fittables(STACK)';
    calc_xxx(1);
    [m, p] = META.perf_metric();
    origin = ['MOEA ' num2str(run) '        '];
    if best_score > m+p
        % we have a winner
        best_indiv = phi_candidate;
        best_score = m+p;
        best_origin = origin;
    end
    
    best_indivs = [best_indivs; phi_candidate];
    best_scores = [best_scores m+p];
    best_origins= [best_origins origin];
    print_stats_search(best_indivs, best_origins, best_scores);
    
end

% now the last round: using all the previously found optimal solutions
% with the additional "phi0" default parameterization, just in case it
% happens to be decent

% Unpack the initial stack
unpack_fittables(phi0);calc_xxx(1);
[termcond, term_score, n_iters, term_phi] = two_dimensional_fitter_loop('gagamultiobj()', ...
    @(obj_fn, phi_init) ga_gamultiobj(obj_fn, length(phi_init), [], [], [], [], [], [], ...
    gaoptimset('TolFun', TolFun, ...
    'PopulationSize', PopSize, ...
    'Generations', Gen, ...
    'Vectorized', 'on', ...
    'StallGenLimit', StallGen, ...
    'ParetoFraction', 0.5, ... % higher elitism to keep as long as possible the previous optimal solutions
    'MutationFcn', @ga_mutation, ...
    'CrossoverFraction', 0.2, ...
    'CrossoverFcn', @ga_crossover, ...
    'DistanceMeasureFcn', @ga_distancecrowding, ...
    'InitialPopulation', [phi0; best_indivs; rand(PopSize - size(best_indivs,1) - 1,length(phi0)).*repmat(GA_UpperBound-GA_LowerBound, PopSize - size(best_indivs,1) - 1,1) + repmat(GA_LowerBound, PopSize - size(best_indivs,1) - 1,1)])));

unpack_fittables(term_phi);

% compute the last score - update if necessary
phi_candidate = pack_fittables(STACK)';
calc_xxx(1);
[m, p] = META.perf_metric();
origin = 'MOEA FINAL     ';
if best_score > m+p
    % we have a winner
    best_indiv = phi_candidate;
    best_score = m+p;
    best_origin = origin';
end

best_indivs = [best_indivs; phi_candidate];
best_scores = [best_scores m+p];
best_origins= [best_origins origin];

print_stats_search(best_indivs, best_origins, best_scores);

unpack_fittables(best_indiv);calc_xxx(1);


end