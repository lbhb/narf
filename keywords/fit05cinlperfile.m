% function fit05cinlperfile()
%
% use SEMSE rather than standard MSE
% svd: copied over from fit05cnlperfile 2016-09-14
%
function fit05cinlperfile()

global STACK XXX

disp('Splitting at first nonlinearity in stack (should be input nonlinearity), merging right after, and fitting using fit05c');

% Remove any correlation at end of stack
if strcmp(STACK{end}{1}.name, 'correlation')
    STACK = STACK(1:end-1);
    XXX = XXX(1:end-1);
end

% find first STACK entry with fit_fields
[~, mod_idxs] = find_modules(STACK, 'nonlinearity', false);
if ~isempty(mod_idxs),
    ii=mod_idxs{1};    
    % Then boost on each file individually
    split_stack(ii,ii);
end

fit05c;


