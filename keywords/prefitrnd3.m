function prefitrnd3()
% JL - June 2015

% RATIONALE:
% with low-parametric kernels, the default starting position may lead to a
% local minimum quickly (with bad luck and wrong initial changes of step
% size, the starting position may even be the local minimum!

% STUDIED SOLUTION:
% Trying 10 different random starting positions to perform a preliminary
% search. Keeping the best position for a real search, to happen later on.


global STACK;

semse()


    function [term_cond, term_score,  n_iters, options] = quick_search()
        [term_cond, term_score,  n_iters, options] = ...
            fit_boo('StepSize', 1, ...
            'StepGrowth', 1.1, ...
            'StepShrink', 0.5, ...
            'StopAtStepNumber', 10);
    end

% % initialization: a first rough search with the default starting position
% initial_stack = STACK;
% [~, term_score,  ~, ~] = quick_search();

% % we don't want the starting position
initial_stack = STACK;
term_score = Inf;


% random search: try 10 different positions
for i=1:10
    % Store the previous state of the STACK and revert to default
    cached_stack = STACK;
    STACK = initial_stack;
    
    % Randomize variables in the fit fields
    for kk = 1:length(STACK)
        if isfield(cached_stack{kk}{1}, 'fit_fields') && isfield(cached_stack{kk}{1}, 'fit_constraints')
            for ll = 1:length(STACK{kk}{:})
                for vv = 1:numel(STACK{kk}{ll}.fit_constraints)
                    variable = STACK{kk}{ll}.fit_constraints{vv}.var;
                    lower_bound = STACK{kk}{ll}.fit_constraints{vv}.lower;
                    upper_bound = STACK{kk}{ll}.fit_constraints{vv}.upper;
                    STACK{kk}{ll}.(variable) = lower_bound + rand(size(lower_bound)) .* (upper_bound - lower_bound);
                end
            end
        end
    end
    
    [~,new_term_score,~,~] = quick_search();
    
    fprintf('======================================================================\n');
    fprintf('======================================================================\n');
    fprintf('=================       Finished prelim search num %d       ==========\n', i);
    
    if term_score < new_term_score,
        % no improvement => revert
        STACK = cached_stack;
        fprintf('=================                                           ==========\n');
        fprintf('=================                                           ==========\n');
        fprintf('=================        Reverting to old solution          ==========\n');
        fprintf('======================================================================\n');
        fprintf('======================================================================\n');
        
    else
        % improvement => keep new (do nothing but update the best score)
        term_score = new_term_score;
        fprintf('=================                                           ==========\n');
        fprintf('=================                                           ==========\n');
        fprintf('=================           Keeping new solution            ==========\n');
        fprintf('======================================================================\n');
        fprintf('======================================================================\n');
        
    end
end

% pop_module();  % trim the mean_squared_error module from the stack

end