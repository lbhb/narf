function siglogify()

global STACK XXX;

% Cache the relevant STACK and remove fittable things
[~,nl_idx] = find_modules(STACK, 'nonlinearity', false);
nl_idx=nl_idx{end};
stack_cached = STACK(1:nl_idx-1);
for jj=1:nl_idx,
    if isfield(STACK{jj}{1}, 'fit_fields')
        STACK{jj}{1} = rmfield(STACK{jj}{1},'fit_fields');
    end
end

% Remove trailing modules
while (length(STACK) >= nl_idx)
    pop_module();
end

% Append siglog and fit
siglog();
fit09c();

% Refit with the whole model
STACK(1:nl_idx-1) = stack_cached;
fit09c();
