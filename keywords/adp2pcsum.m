function adp2pcsum()
% 2 adp synapses per channel, mulitplied back together. so total
% channel count doesn't change

global MODULES STACK XXX;

append_module(MODULES.normalize_channels.mdl(struct('force_positive', true)));

sf=XXX{end}.training_set{1};
chan_count=size(XXX{end}.dat.(sf).stim,3);
init_str=repmat([1 1],[1 chan_count]);
init_tau=repmat([2 30],[1 chan_count]);
init_offset=zeros(1,chan_count);

append_module(MODULES.depression_filter_bank.mdl(...
                    struct('strength', init_str, ...
                           'tau',      init_tau, ...
                           'offset_in',init_offset,...
                           'tau_norm',  100,...
                           'facil_on', 1, ...
                           'per_channel', 1, ...
                           'fit_fields', {{'strength','tau'}})));

signal = 'stim';
n_output_chans = 2;

% Compute number of input channels
fns = fieldnames(XXX{end}.dat);
n_input_chans = size(XXX{end}.dat.(fns{1}).(signal), 3);

SF=1;

w_init=[1 0; 1 0; 0 1; 0 1];
%if n_input_chans>n_output_chans,
%    w_init=SF*ones(n_input_chans, n_output_chans);
%else
%    w_init=SF*[eye(n_input_chans) ...
%               ones(n_input_chans,n_output_chans-n_input_chans)];
%end

append_module(MODULES.weight_channels.mdl(...
       struct('weights', w_init, ...
              'y_offset', zeros(n_output_chans, 1))));
