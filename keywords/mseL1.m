function mseL1()

global MODULES META STACK;

[~,wc]=find_modules(STACK,'weight_channels');
wckeep={};
for ii=cat(2,wc{:}),
   if isempty(STACK{ii}{1}.phifn),
       wckeep{end+1}=ii;
   end
end
wc=wckeep;

[~,fir_mod]=find_modules(STACK,'fir_filter');
[~,mse_mod]=find_modules(STACK,'mean_squared_error');

if ~isempty(mse_mod),
   STACK{mse_mod{1}}{1}.output='score';
   STACK{mse_mod{1}}{1}.L1Frac=0.001;
   STACK{mse_mod{1}}{1}.L1Modules=[cat(2,wc{:}) cat(2,fir_mod{:})];
   %STACK{mse_mod{1}}{1}.L1Modules=[cat(2,fir_mod{:})];
else
   append_module(MODULES.mean_squared_error.mdl(struct('output', 'score','L1Frac',0.001,...
      'L1Modules',[cat(2,wc{:}) cat(2,fir_mod{:})])));
   %append_module(MODULES.mean_squared_error.mdl(struct('output', 'score','L1Frac',0.001,...
   %   'L1Modules',[cat(2,fir_mod{:})])));
end
%append_module(MODULES.correlation);

META.perf_metric = @pm_nmse;
