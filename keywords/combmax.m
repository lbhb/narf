function combmax()

% simple max operation

global MODULES;

% inputs should be in stim1, stim2
% this outputs to stim
append_module(MODULES.combine_fields.mdl(struct(...
    'combination_fn', 'comb_max', ...
    'combination_p', NaN)));

fitSubstack();
