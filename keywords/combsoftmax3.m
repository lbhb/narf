function combsoftmax()

% soft-max operation

global MODULES STACK XXX;

inputs = {'stim1', 'stim2'};

% update_xxx(1,2);
% 
% [mods, mod_idxs] = find_modules(STACK, 'split_stim', false);
% 
% if ~isempty(mods)
%     update_xxx(1,2);
% 
%     mod = mods{end}{1};
%     start_idx = mod_idxs{end}+1;
%     end_idx = length(STACK);
%     
%     start_idx2 = start_idx;
%     end_idx2 = end_idx;
%     nb_modules = end_idx - start_idx + 1;
%     for ii = 1:numel(mod.outputs)
%         if ii>1
%             % need to duplicate the last bunch of modules
%             STACK = [STACK(1:end_idx2), STACK(start_idx:end_idx)];
%             %             for jj = 1:nb_modules
%             %                 append_module(STACK{start_idx+jj-1}{1});
%             %
%             % %                 if isfield(to_add, 'auto_init')
%             % %                     STACK{l} = {to_add.auto_init(STACK, XXX(1:l))};
%             % %                 else
%             % %                     STACK{l} = {to_add};
%             % %                 end
%             % %                 update_xxx(1,l);
%             %             end
%             start_idx2 = start_idx2 + nb_modules;
%             end_idx2 = end_idx2 + nb_modules;
%         end
%         for jj = 1:nb_modules
%             l = start_idx2+jj-1;
%             % we start by changing the input/output to put the splitted
%             % stims
%             STACK{l}{1}.input = mod.outputs{ii};
%             STACK{l}{1}.input_stim = mod.outputs{ii};
%             STACK{l}{1}.output = mod.outputs{ii};
%             STACK{l}{1}.modifies = {mod.outputs{ii}, 'stim'}; % quick and dirty: should do it properly
%             STACK{l}{1}.required = {mod.outputs{ii}, 'stim', 'stim_time', 'respavg'}; % quick and dirty: should do it properly
%             % is auto-initialization wanted?
%             if isfield(STACK{l}{1}, 'auto_init')
%                 % yes: we do the auto-initialization after computing the right XXX:
%                 update_xxx(2,l);
%                 STACK{l} = {STACK{l}{1}.auto_init(STACK(1:l), XXX(1:l))};
%                 % but then, because auto-initialization can mess things up,
%                 % we have to make sure that the input/output are still well set:
%                 STACK{l}{1}.input = mod.outputs{ii};
%                 STACK{l}{1}.input_stim = mod.outputs{ii};
%                 STACK{l}{1}.output = mod.outputs{ii};
%                 STACK{l}{1}.modifies = {mod.outputs{ii}, 'stim'}; % quick and dirty: should do it properly
%                 STACK{l}{1}.required = {mod.outputs{ii}, 'stim', 'stim_time', 'respavg'}; % quick and dirty: should do it properly 
%             end
%         end
%     end
%     
%     inputs = mod.outputs;
% end
% 
% update_xxx(1);

% inputs should be in stim1, stim2
% this outputs to stim
append_module(MODULES.combine_fields.mdl(struct('inputs', {inputs}, ...
    'combination_fn', 'comb_max', ...
    'combination_p', 0.5)));
% 0.05 for \combination_p\ corresponds to a "soft" softmax. For a harder
% softmax, use 0.2 or 0.5

update_xxx(1)

% fitSubstack();
