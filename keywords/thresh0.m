%function thresh0()
%
% hard threshold at zero, no fit.
%
function thresh0()

global MODULES;

append_module(MODULES.nonlinearity.mdl(struct('phi', [0], ...
                                              'nlfn', @nl_zerothresh)));
