function fir15pn()

global MODULES XXX STACK;

fir_num_coefs=15;

append_module(MODULES.normalize_channels.mdl(struct('force_positive', true)));

meanresp = nanmean(flatten_field(XXX{end}.dat,XXX{end}.training_set,'respavg'));
append_module(MODULES.fir_filter.mdl(struct('num_coefs', fir_num_coefs, ...
                                'baseline',meanresp,...
                                'fit_fields', {{'coefs','baseline'}}, ...
                                 'fit_constraints', {{struct( ...
                                                       'var', 'coefs', ...
                                                       'lower', -50, ...
                                                       'upper', 50), ...
                                                     struct( ...
                                                       'var', 'baseline', ...
                                                       'lower', -50, ...
                                                       'upper', 50)}} ...
                                )));

fitSubstack([],10^-2);

channel_count=STACK{end}{1}.num_dims;
mc=ceil(channel_count/2);
coefs=STACK{end}{1}.coefs;
ff=min(mc*2,channel_count);
coefs((mc+1):ff,:)=-coefs(1:(ff-mc),:)./2;
STACK{end}{1}.coefs=coefs;
STACK{end}{1}.baseline=repmat(STACK{end}{1}.baseline./channel_count.*2,channel_count,1);
STACK{end}{1}.rectify_channels=1;

fitSubstack([],10^-2);

