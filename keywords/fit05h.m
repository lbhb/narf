% function fit05h()
% 
% semse instead of nmse, still stopping at 1e-5, first fit without
% final NL
%
function fit05h()

    global STACK XXX
    
semse();

MaxStepsPerIteration=10;
StepGrowth=1.1;

function [a,b,c,d] = step_until_10neg3(prev_opts)
    if exist('prev_opts', 'var')
        [a,b,c,d] = fit_boo(prev_opts);
    else
        [a,b,c,d] = fit_boo('StopAtAbsScoreDelta', 10^-3, ...
                            'StopAtStepSize', 10^-6,...
                            'StepAnyway', true, ...
                            'StopAtStepNumber', MaxStepsPerIteration, ...
                            'StepGrowth', StepGrowth);
    end
end

function [a,b,c,d] = step_until_10neg35(prev_opts)
    if exist('prev_opts', 'var')
        [a,b,c,d] = fit_boo(prev_opts);
    else
        [a,b,c,d] = fit_boo('StopAtAbsScoreDelta', 10^-3.5, ...
                            'StopAtStepSize', 10^-6,...
                            'StepAnyway', true, ...
                            'StopAtStepNumber', MaxStepsPerIteration, ...
                            'StepGrowth', StepGrowth);
    end
end

function [a,b,c,d] = step_until_10neg4(prev_opts)
    if exist('prev_opts', 'var')
        [a,b,c,d] = fit_boo(prev_opts);
    else
        [a,b,c,d] = fit_boo('StopAtAbsScoreDelta', 10^-4, ...
                            'StopAtStepSize', 10^-7,...
                            'StepAnyway', true, ...
                            'StopAtStepNumber', MaxStepsPerIteration, ...
                            'StepGrowth', StepGrowth);
    end
end

function [a,b,c,d] = step_until_10neg45(prev_opts)
    if exist('prev_opts', 'var')
        [a,b,c,d] = fit_boo(prev_opts);
    else
        [a,b,c,d] = fit_boo('StopAtAbsScoreDelta', 10^-4.5, ...
                            'StopAtStepSize', 10^-7,...
                            'StepAnyway', true, ...
                            'StopAtStepNumber', MaxStepsPerIteration, ...
                            'StepGrowth', StepGrowth);
    end
end

function [a,b,c,d] = step_until_10neg5(prev_opts)
    if exist('prev_opts', 'var')
        [a,b,c,d] = fit_boo(prev_opts);
    else
        [a,b,c,d] = fit_boo('StopAtAbsScoreDelta', 10^-5, ...
                            'StepAnyway', true, ...
                            'StopAtStepNumber', MaxStepsPerIteration, ...
                            'StepGrowth', StepGrowth);
    end
end

function [a,b,c,d] = step_until_10neg55(prev_opts)
    if exist('prev_opts', 'var')
        [a,b,c,d] = fit_boo(prev_opts);
    else
        [a,b,c,d] = fit_boo('StopAtAbsScoreDelta', 10^-5.5, ...
                            'StepAnyway', true, ...
                            'StopAtStepNumber', MaxStepsPerIteration, ...
                            'StepGrowth', StepGrowth);
    end
end

function [a,b,c,d] = step_until_10neg6(prev_opts)
    if exist('prev_opts', 'var')
        [a,b,c,d] = fit_boo(prev_opts);
    else
        [a,b,c,d] = fit_boo('StopAtAbsScoreDelta', 10^-6, ...
                            'StepAnyway', true, ...
                            'StopAtStepNumber', MaxStepsPerIteration, ...
                            'StepGrowth', StepGrowth);
    end
end

disp('Removing static spike NL for quick fit');
[nl_mods,nl_idx] = find_modules(STACK, 'nonlinearity', false);
if strcmp(func2str(STACK{nl_idx{end}}{1}.nlfn),'nl_dcg'),
    nl_mods=nl_mods(1:(end-1));
    nl_idx=nl_idx(1:(end-1));
end
nl_save=nl_mods{end};
nl_idx=nl_idx{end};

for jj=1:length(STACK{nl_idx}),
    if isfield(STACK{nl_idx}{jj},'fit_fields'),
        STACK{nl_idx}{jj}.nlfn=@nl_dummy;
        STACK{nl_idx}{jj}.input_stim='stim';
        STACK{nl_idx}{jj}=rmfield(STACK{nl_idx}{jj},'fit_fields');
    end
end

[sm_mods,sm_idx] = find_modules(STACK, 'combine_fields', false);

if ~isempty(sm_idx),
    disp('Fixing combine_fields module for quick fit');
    sm_save=sm_mods{end};
    sm_idx=sm_idx{end};
    
    for jj=1:length(STACK{sm_idx}),
        if isfield(STACK{sm_idx}{jj},'fit_fields'),
            STACK{sm_idx}{jj}=rmfield(STACK{sm_idx}{jj},'fit_fields');
        end
    end
else
    sm_idx=[];
end

[~,pup_idx] = find_modules(STACK, 'pupil_gain', false);
if ~isempty(pup_idx),
   pup_idx=pup_idx{end};
   disp('Removing pupil_gain mod for first-round fit');
   for jj=1:length(STACK{pup_idx}),
      pup_fields=STACK{pup_idx}{jj}.fit_fields;
      STACK{pup_idx}{jj}=rmfield(STACK{pup_idx}{jj},'fit_fields');
   end
else
   pup_idx=[];
end

% now run sequence of progressively tighter fits on model without
% spike NL, and don't run quite so deep.
fit_boo('StopAtAbsScoreDelta', 10^-2.0, ...
        'StepAnyway', true, ...
        'StepGrowth', StepGrowth);

fit_iteratively(@step_until_10neg3, ...
                create_term_fn('StopAtAbsScoreDelta', 10^-3));

[~,fir_idx] = find_modules(STACK, 'fir_filter', true);
if isempty(fir_idx),
   [~,fir_idx] = find_modules(STACK, 'pole_zeros', true);
end
if ~isempty(fir_idx) && STACK{fir_idx}{1}.reset_filter_during_fit,
   STACK{fir_idx}{1}=STACK{fir_idx}{1}.auto_init(STACK, XXX(1:fir_idx));
   fit_iteratively(@step_until_10neg35, ...
      create_term_fn('StopAtAbsScoreDelta', 10^-3.5),0,fir_idx);
   STACK{fir_idx}{1}.reset_filter_during_fit=1;
else
   fit_iteratively(@step_until_10neg35, ...
      create_term_fn('StopAtAbsScoreDelta', 10^-3.5));
end

% if ~isempty(fir_idx) && STACK{fir_idx}{1}.reset_filter_during_fit,
%    STACK{fir_idx}{1}=STACK{fir_idx}{1}.auto_init(STACK, XXX(1:fir_idx));
%    fit_iteratively(@step_until_10neg4, ...
%       create_term_fn('StopAtAbsScoreDelta', 10^-4),0,fir_idx);
%    STACK{fir_idx}{1}.reset_filter_during_fit=1;
% else
   fit_iteratively(@step_until_10neg4, ...
      create_term_fn('StopAtAbsScoreDelta', 10^-4));
% end

if 0 && strcmpi(func2str(nl_save{1}.nlfn),'nl_sig_logistic'),
    disp('Reinitializing static spike NL for final fit');
    pop_module();  % remove MSE module
    pop_module();  % remove siglog
    siglogs();
    % restore MSE module
    semse();
elseif strcmpi(func2str(nl_save{1}.nlfn),'nl_dexp'),    
    STACK{nl_idx}=nl_save; % restore original NL function

    % re-calculate initial conditions for NL based on model fit so far
    ff=XXX{nl_idx}.training_set{1};
    meanresp=nanmean(XXX{nl_idx}.dat.(ff).respavg(:));
    meanpred=nanmean(XXX{nl_idx}.dat.(ff).stim(:));
    resprange = max(XXX{nl_idx}.dat.(ff).respavg(:))-min(XXX{nl_idx}.dat.(ff).respavg(:));
    curvature = 1 / resprange;
    STACK{nl_idx}{1}.phi=[0 meanresp*2 meanpred curvature];
    
else
    disp('Restoring static spike NL for final fit');
    STACK{nl_idx}=nl_save; % restore original NL function
end

if ~isempty(sm_idx),
    disp('But first refining with just NL and no combine_fields');
    update_xxx(2);
    fit_iteratively(@step_until_10neg3, ...    
                    create_term_fn('StopAtAbsScoreDelta', 10^-3));

    fit_iteratively(@step_until_10neg35, ...
                    create_term_fn('StopAtAbsScoreDelta', 10^-3.5));

    fit_iteratively(@step_until_10neg4, ...
                    create_term_fn('StopAtAbsScoreDelta', 10^-4));

    fit_iteratively(@step_until_10neg45, ...
                    create_term_fn('StopAtAbsScoreDelta', 10^-4.5));

    disp('Restoring combine_fields module for final fit');
    STACK{sm_idx}=sm_save;
end

[~,pup_idx] = find_modules(STACK, 'pupil_gain', false);
if ~isempty(pup_idx),
    pup_idx=pup_idx{1};
    disp('Restoring pupil_gain mod for second-round fit');
    ff=XXX{nl_idx}.training_set{1};
    meanpupil=nanmean(XXX{nl_idx}.dat.(ff).pupil(:));
    meanpred=nanmean(XXX{nl_idx}.dat.(ff).stim(:));
    for jj=1:length(STACK{pup_idx}),
        STACK{pup_idx}{jj}.fit_fields=pup_fields;
        STACK{pup_idx}{jj}.gain=[meanpred 0];
    end
end

update_xxx(2);

%
% now run through the series of fits again with static NL included.
fit_iteratively(@step_until_10neg3, ...    
                create_term_fn('StopAtAbsScoreDelta', 10^-3));

fit_iteratively(@step_until_10neg35, ...
                create_term_fn('StopAtAbsScoreDelta', 10^-3.5));

fit_iteratively(@step_until_10neg4, ...
                create_term_fn('StopAtAbsScoreDelta', 10^-4));

fit_iteratively(@step_until_10neg45, ...
                create_term_fn('StopAtAbsScoreDelta', 10^-4.5));

fit_iteratively(@step_until_10neg5, ...
                create_term_fn('StopAtAbsScoreDelta', 10^-5));

%fit_iteratively(@step_until_10neg55, ...
%                create_term_fn());

%fit_iteratively(@step_until_10neg6, ...
%                create_term_fn());
end

