function llpoisson_shr()

global MODULES META STACK;

mods = find_modules(STACK, 'likelihood_poisson', true);
if isempty(mods)
    append_module(MODULES.likelihood_poisson.mdl(...
        struct('norm_by_se', 0.5)));    
end

META.perf_metric = @pm_nlogl;
