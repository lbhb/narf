function model_bar_comp(sel_batch, cellids, sel_models)

sel_metric='r_test';

[data, cellids] = compute_data_matrix({sel_metric,'r_floor'});
if isempty(data)
   return;
end

do_median=1;
if(do_median)
   str='Median';
else
   str='Mean';
end

ax = plot_bar_pretty(data, sel_models, false,do_median);
if(length(cellids)==1)
   str2=cellids{1};
else
   str2=[str,'/StdErr of'];
end
t=ylabel(ax, sprintf(['Performance: ',str2,' %s (Batch %d, %s)'],...
   sel_metric, sel_batch, datestr(now,'dd-mmm-yyyy HH:MM')), 'interpreter', 'none','VerticalAlignment','middle');

end
