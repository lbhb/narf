function fitset=merge_models(batch, cellids, modelnames)

global XXX STACK META


% this version makes the usual scaling of weights so the sum is equal to 1
    function x = format_coef(x)
        x = abs(x);
        x = x/sum(x);
    end

% this version constrains the coef to be higher than (roughly) 1/2n
%     function x = format_coef(x)
%         x = abs(x);
%         x = x/sum(x);
%         x = x + 1/length(x);
%         x = x/sum(x);
%     end

    function score = getfit(x, exp_train, mdl_train)
        predic = zeros(size(exp_train));
        x = format_coef(x);
        for m=1:size(mdl_train,2),
            predic = predic + mdl_train(:,m)*x(m);
        end
        score = nanmean((predic - exp_train).^2);
        score = score / (nanvar(exp_train)+(score==0));
    end


    function [nmse, cor, score] = compute_score(mdl_set, exp_set)
        score = nanmean((mdl_set - exp_set).^2);
        nmse = score / (nanvar(exp_set)+(score==0));
        RR = corrcoef(excise([mdl_set exp_set]));
        cor = RR(2,1);
    end


    function K = get_parameters_nb(stack)
        K = 0;
        for iii = 1:length(stack)
            mmm = stack{iii};
            nsplits = length(mmm);
            for kk = 1:nsplits
                m = mmm{kk};
                if isfield(m, 'fit_fields')
                    for jj = 1:length(m.fit_fields),
                        p = m.fit_fields{jj};
                        field = m.(p);
                        K = K + numel(field);
                    end
                end
            end
        end
    end


Options = '';

Response = questdlg('Perform Stacking? (this is time consuming)', 'Pick one', 'No', 'Leave-One-Out', 'Bootstrap', 'No');
if strcmp(Response, 'Leave-One-Out')
    doStack = 1;
    Options = [Options '_loo'];
elseif strcmp(Response, 'Bootstrap')
    doStack = 2;
    Options = [Options '_bootsrap'];
else
    doStack = 0;
    Options = [Options '_nostack'];
end

if strcmp(questdlg('Output CSV files? (this is time consuming)', 'Pick one', 'No', 'Yes', 'No'), 'Yes')
    doOutputCsv = 1;
else
    doOutputCsv = 0;
end

if nargout==0,
    [FileName,PathName,~] = ...
        uiputfile(['model_merge_', num2str(batch), Options, '.csv'],'Save to...');
    if ~FileName,
        disp('Canceled save.');
        return;
    else
        fprintf('Saving csv to %s%s\n',PathName,FileName);
        fid = fopen([PathName FileName],'w');
        fprintf(fid,'CELL_ID MODEL TRAINING_SCORE TESTING_SCORE TRAINING_CORR TESTING_CORR COEF W_STACK W_AIC W_AICC W_BIC W_APC\n');
    end
end


stack_extractor = @(x) x;
meta_extractor = @(x) x;
[stacks, metas, x0s,preds] = load_model_batch(batch, cellids, modelnames, ...
    stack_extractor, meta_extractor);
cellcount=length(cellids);
modelcount=length(modelnames);


optimization_set = 'training_set';
testing_set = 'test_set';
% optimization_set = 'test_set';
% testing_set = 'training_set';


for cc=150:cellcount,
    fprintf('********\nDoing cell %d\n********\n',cc);
    
    % STEP 1:
    % we extract the original recording along with the model predictions
    train_nmse = zeros(modelcount,1);
    train_cor = zeros(modelcount,1);
    AIC = zeros(modelcount,1);
    AICc = zeros(modelcount,1);
    BIC = zeros(modelcount,1);
    APC = zeros(modelcount,1);
    
    for mm=1:modelcount,
        fprintf('********\nDoing model %d\n********\n',mm);
        
        XXX={x0s{mm,cc}};
        STACK=stacks{mm,cc};
        META=metas{mm,cc};
        calc_xxx(1);
        xxx = XXX{end};
        if mm==1,
            % we get the original stimulus only in the query for the first model
            exp_train = flatten_field(xxx.dat, xxx.(optimization_set), 'respavg');
            mdl_train = zeros(length(flatten_field(xxx.dat, xxx.(optimization_set), 'stim')), modelcount);
            n = numel(exp_train);
        end
        
        if (size(mdl_train(:,mm)) == size(flatten_field(xxx.dat, xxx.(optimization_set), 'stim'))),
            mdl_train(:,mm) = flatten_field(xxx.dat, xxx.(optimization_set), 'stim');
            [nmse, cor, MSE] = compute_score(mdl_train(:,mm), exp_train);
            train_nmse(mm) = nmse;
            train_cor(mm) = cor;
            k = get_parameters_nb(STACK);
            AIC(mm) = n*log(MSE) + 2*k;
            AICc(mm) = AIC(mm) + (2*k*(k+1)) / (n-k-1);
            BIC(mm) = n*log(MSE) + k*log(n);
            APC(mm) = (n*MSE) * (1 + k/n) / (n-k);
        else
            fprintf('\n\n#######################\nBUG in model %s !!!\n#######################\n\n', META.modelname);
        end
    end
    
    
    % OPTIONAL STEP 1.5
    % Output the model predictions in a .csv for processing somewhere else
    if (doOutputCsv)
        fprintf(1,'\nSTARTING WRITING CSV FILE... ');
        fid_tmp = fopen([PathName FileName(1:(end-4)) '_cell' num2str(cc) '.csv'],'w');
        % output the header
        for mm=1:modelcount,
            fprintf(fid_tmp, 'MODEL_%d ', mm);
        end
        fprintf(fid_tmp, 'REAL\n');
        % output the data
        for nn=1:n
            for mm=1:modelcount,
                fprintf(fid_tmp, '%f ', mdl_train(nn,mm));
            end
            fprintf(fid_tmp, '%f\n', exp_train(nn));
        end
        fprintf(1,'DONE WITH WRITING\n');
    end
    
    % STEP 2
    % 2.1: we get the fixed weights from the model indicators
    delta_AIC = AIC - min(AIC);
    delta_AICc = AICc - min(AICc);
    delta_BIC = BIC - min(BIC);
    delta_APC = APC - min(APC);
    w_AIC = exp(-delta_AIC/2) / sum(exp(-delta_AIC/2));
    w_AICc = exp(-delta_AICc/2) / sum(exp(-delta_AICc/2));
    w_BIC = exp(-delta_BIC/2) / sum(exp(-delta_BIC/2));
    w_APC = exp(-delta_APC/2) / sum(exp(-delta_APC/2));
    
    % 2.2: we optimize the weights so to maximize the merged nmse in training
    % The following optimizes from different good starting guesses and keeps the overall best solution
    starts = eye(modelcount);
    best_coefs = fminsearch(@(x) getfit(x, exp_train, mdl_train), repmat(1/modelcount,modelcount,1));
    best_coefs = format_coef(best_coefs);
    best_perf = getfit(best_coefs, exp_train, mdl_train);
    for start=1:modelcount
        coefs = fminsearch(@(x) getfit(x, exp_train, mdl_train), starts(start,:));
        coefs = format_coef(coefs);
        perf = getfit(coefs, exp_train, mdl_train);
        if (perf < best_perf)
            best_coefs = coefs;
            best_perf = perf;
        end
    end
    coefs = best_coefs;
    
    if (doStack == 1)
        % 2.3 a: we optimize the weights so to maximize the merged nmse in
        % training, on a resampled _leave-one-out collection of datasets_
        n_stims = 0;
        for optim_set=1:numel(xxx.(optimization_set))
            n_stims = n_stims + size(xxx.dat.(xxx.(optimization_set){optim_set}).respavg,2);
        end
        n_timesteps = n / n_stims;
        ave_best_coefs = zeros(n_stims,modelcount);
        for ci = 1:n_stims
            fprintf(1,'.');
            % The following optimizes from different good starting guesses and keeps the overall best solution
            starts = eye(modelcount);
            loo = ones(size(exp_train));
            loo(  (1+(ci-1)*n_timesteps):((ci)*n_timesteps)  ) = 0;
            loo = boolean(loo);
            best_coefs = fminsearch(@(x) getfit(x, exp_train(loo), mdl_train(loo,:)), repmat(1/modelcount,modelcount,1));
            best_coefs = format_coef(best_coefs);
            best_perf = getfit(best_coefs, exp_train(loo), mdl_train(loo,:));
            for start=1:modelcount
                coefs2 = fminsearch(@(x) getfit(x, exp_train(loo), mdl_train(loo,:)), starts(start,:));
                coefs2 = format_coef(coefs2);
                perf = getfit(coefs2, exp_train(loo), mdl_train(loo,:));
                if (perf < best_perf)
                    best_coefs = coefs2;
                    best_perf = perf;
                end
            end
            ave_best_coefs(ci,:) = best_coefs;
        end
        stacked_coefs = mean(ave_best_coefs,1);
        
    elseif (doStack == 2)
        % 2.3 b: we optimize the weights so to maximize the merged nmse in
        % training, on a _bootstraped collection of datasets_
        rep = 100; % 100 takes a few hours to process 176 cells with 4 models
        ave_best_coefs = zeros(rep,modelcount);
        for ci = 1:rep
            fprintf(1,'.');
            % The following optimizes from different good starting guesses and keeps the overall best solution
            starts = eye(modelcount);
            loo = randi(n,round(0.95*n),1);
            best_coefs = fminsearch(@(x) getfit(x, exp_train(loo), mdl_train(loo,:)), repmat(1/modelcount,modelcount,1));
            best_coefs = format_coef(best_coefs);
            best_perf = getfit(best_coefs, exp_train(loo), mdl_train(loo,:));
            for start=1:modelcount
                coefs2 = fminsearch(@(x) getfit(x, exp_train(loo), mdl_train(loo,:)), starts(start,:));
                coefs2 = format_coef(coefs2);
                perf = getfit(coefs2, exp_train(loo), mdl_train(loo,:));
                if (perf < best_perf)
                    best_coefs = coefs2;
                    best_perf = perf;
                end
            end
            ave_best_coefs(ci,:) = best_coefs;
        end
        stacked_coefs = mean(ave_best_coefs,1);
    end
    
    
    
    
    % STEP 3
    % we compute the combined predictions with the optimized weights
    test_nmse = zeros(modelcount,1);
    test_cor = zeros(modelcount,1);
    for mm=1:modelcount,
        XXX={x0s{mm,cc}};
        STACK=stacks{mm,cc};
        META=metas{mm,cc};
        calc_xxx(1);
        xxx = XXX{end};
        if mm==1
            % we get the original stimulus only in the query for the first model
            exp_test = flatten_field(xxx.dat, xxx.(testing_set), 'respavg');
            mdl_test = zeros(length(flatten_field(xxx.dat, xxx.(testing_set), 'stim')), modelcount);
            merge_test = zeros(length(flatten_field(xxx.dat, xxx.(testing_set), 'stim')), 1);
            merge_train = zeros(length(flatten_field(xxx.dat, xxx.(optimization_set), 'stim')), 1);
            if (doStack)
                merge_test_STACKING = zeros(length(flatten_field(xxx.dat, xxx.(testing_set), 'stim')), 1);
                merge_train_STACKING = zeros(length(flatten_field(xxx.dat, xxx.(optimization_set), 'stim')), 1);
            end
            merge_test_AIC = zeros(length(flatten_field(xxx.dat, xxx.(testing_set), 'stim')), 1);
            merge_test_AICc = zeros(length(flatten_field(xxx.dat, xxx.(testing_set), 'stim')), 1);
            merge_test_BIC = zeros(length(flatten_field(xxx.dat, xxx.(testing_set), 'stim')), 1);
            merge_test_APC = zeros(length(flatten_field(xxx.dat, xxx.(testing_set), 'stim')), 1);
            merge_train_AIC = zeros(length(flatten_field(xxx.dat, xxx.(optimization_set), 'stim')), 1);
            merge_train_AICc = zeros(length(flatten_field(xxx.dat, xxx.(optimization_set), 'stim')), 1);
            merge_train_BIC = zeros(length(flatten_field(xxx.dat, xxx.(optimization_set), 'stim')), 1);
            merge_train_APC = zeros(length(flatten_field(xxx.dat, xxx.(optimization_set), 'stim')), 1);
        end
        mdl_test(:,mm) = flatten_field(xxx.dat, xxx.(testing_set), 'stim');
        [nmse, cor] = compute_score(mdl_test(:,mm), exp_test);
        test_nmse(mm) = nmse;
        test_cor(mm) = cor;
        
        merge_test = merge_test + coefs(mm) * mdl_test(:,mm);
        merge_train = merge_train + coefs(mm) * mdl_train(:,mm);
        if (doStack)
            merge_test_STACKING = merge_test_STACKING + stacked_coefs(mm) * mdl_test(:,mm);
            merge_train_STACKING = merge_train_STACKING + stacked_coefs(mm) * mdl_train(:,mm);
        end
        merge_test_AIC = merge_test_AIC + w_AIC(mm) * mdl_test(:,mm);
        merge_test_AICc = merge_test_AICc + w_AICc(mm) * mdl_test(:,mm);
        merge_test_BIC = merge_test_BIC + w_BIC(mm) * mdl_test(:,mm);
        merge_test_APC = merge_test_APC + w_APC(mm) * mdl_test(:,mm);
        merge_train_AIC = merge_train_AIC + w_AIC(mm) * mdl_train(:,mm);
        merge_train_AICc = merge_train_AICc + w_AICc(mm) * mdl_train(:,mm);
        merge_train_BIC = merge_train_BIC + w_BIC(mm) * mdl_train(:,mm);
        merge_train_APC = merge_train_APC + w_APC(mm) * mdl_train(:,mm);
    end
    
    [train_nmse2, train_cor2] = compute_score(merge_train, exp_train);
    [test_nmse2, test_cor2] = compute_score(merge_test, exp_test);
    if (doStack)
        [train_nmse2_STACKING, train_cor2_STACKING] = compute_score(merge_train_STACKING, exp_train);
        [test_nmse2_STACKING, test_cor2_STACKING] = compute_score(merge_test_STACKING, exp_test);
    else
        train_nmse2_STACKING=-1;
        train_cor2_STACKING=-1;
        test_nmse2_STACKING=-1;
        test_cor2_STACKING=-1;
    end
    [train_nmse2_AIC, train_cor2_AIC] = compute_score(merge_train_AIC, exp_train);
    [train_nmse2_AICc, train_cor2_AICc] = compute_score(merge_train_AICc, exp_train);
    [train_nmse2_BIC, train_cor2_BIC] = compute_score(merge_train_BIC, exp_train);
    [train_nmse2_APC, train_cor2_APC] = compute_score(merge_train_APC, exp_train);
    [test_nmse2_AIC, test_cor2_AIC] = compute_score(merge_test_AIC, exp_test);
    [test_nmse2_AICc, test_cor2_AICc] = compute_score(merge_test_AICc, exp_test);
    [test_nmse2_BIC, test_cor2_BIC] = compute_score(merge_test_BIC, exp_test);
    [test_nmse2_APC, test_cor2_APC] = compute_score(merge_test_APC, exp_test);
    
    for mm = 1:modelcount
        fprintf(fid,'%s %s %f %f %f %f %f %f %f %f %f %f\n', cellids{cc} , modelnames{mm}, train_nmse(mm), test_nmse(mm), train_cor(mm), test_cor(mm), ...
            coefs(mm), stacked_coefs(mm), w_AIC(mm), w_AICc(mm), w_BIC(mm), w_APC(mm));
    end
    fprintf(fid,'%s merged_model %f %f %f %f NA NA NA NA NA\n', cellids{cc}, train_nmse2, test_nmse2, train_cor2, test_cor2);
    fprintf(fid,'%s merged_model_STACKING %f %f %f %f NA NA NA NA NA\n', cellids{cc}, train_nmse2_STACKING, test_nmse2_STACKING, train_cor2_STACKING, test_cor2_STACKING);
    fprintf(fid,'%s merged_model_AIC %f %f %f %f NA NA NA NA NA\n', cellids{cc}, train_nmse2_AIC, test_nmse2_AIC, train_cor2_AIC, test_cor2_AIC);
    fprintf(fid,'%s merged_model_AICc %f %f %f %f NA NA NA NA NA\n', cellids{cc}, train_nmse2_AICc, test_nmse2_AICc, train_cor2_AICc, test_cor2_AICc);
    fprintf(fid,'%s merged_model_BIC %f %f %f %f NA NA NA NA NA\n', cellids{cc}, train_nmse2_BIC, test_nmse2_BIC, train_cor2_BIC, test_cor2_BIC);
    fprintf(fid,'%s merged_model_APC %f %f %f %f NA NA NA NA NA\n', cellids{cc}, train_nmse2_APC, test_nmse2_APC, train_cor2_APC, test_cor2_APC);
    
end

fclose(fid);

end

