function plot_wc_around_BFs(batch, cellids, modelnames)
% Plots the WC coefs centered around the BF (largest WC weight) 

cellids = filter_low_snr_cells(batch, cellids, 0.03);

    function centered_weights = extract_wc_around_BFs(~)
        global STACK;
        
        % OPTION 1: Spectral analysis
        %mods = find_modules(STACK, 'weight_channels'); % Spectral
        %w = mods{1}{1}.weights; 
        
        % OPTION 2: Temporal analysis
        mods = find_modules(STACK, 'fir_filter');
        w = mods{1}{1}.coefs';
       
        [N, M] = size(w);
        
        centered_weights = zeros(9,M);         
        
        % ----------------------------------------------------------------
        % OPTION ONE: Center the weights at the best frequency
      
        for jj = 1:M
            idx = find(abs(w(:,jj)) == max(abs(w(:,jj))), 1);
    
            % Extract +/-4 coefs
            for ii = -4:4
                if idx+ii >= 1 && idx+ii <= N
                    centered_weights(ii+5,jj) = w(idx+ii, jj);
                else
                    centered_weights(ii+5,jj) = nan;
                end
            end
            
            % Smooth weights unfairly
            % centered_weights(:, jj) = filter2([1 4 1]', centered_weights(:,jj), 'same');
                
            % Normalize to the BF
            %centered_weights = centered_weights ./ centered_weights(5);
        
            % Make the sign of the BF always positive
            centered_weights(:,jj) = centered_weights(:,jj) ./ sign(centered_weights(5, jj));

            % Normalize by the variance
            centered_weights(:,jj) = centered_weights(:,jj) ./ nanstd(centered_weights(:,jj));
            
            % Use Z score as normalization
            %centered_weights(:, jj) = zscore(centered_weights(:,jj))
                        
        end        
        
        % ----------------------------------------------------------------
        % OPTION TWO: Use all the weights, without recentering
        % (Showed we have more data at some freqs than others, but was hard
        % to derive many other conclusions from this)
        
        %centered_weights = w;
        %centered_weights = centered_weights ./ repmat(nanstd(centered_weights), size(w, 1), 1);
        
        % ----------------------------------------------------------------
    end

stack_extractor = @extract_wc_around_BFs;

[params, ~, ~] = load_model_batch(batch, cellids, modelnames, ...
    stack_extractor);

data = cell2mat(params(~cellfun('isempty', params)))';

pos = repmat(1:size(data,2), size(data,1), 1);
jitter = -0.15+0.3*rand(size(pos));

data2 = excise(data); % Alternative #1: Use only the "completely valid" rows
% data2 = ... % Alternative #2: Replace NANs with the mirrored values

data2(any(isinf(data2)'),:) = [];
%pc = princomp(data2);
pc = princomp(zscore(data2));

% Project data2 onto the principal components
[U, S, V] = svd(data2);
data3 = data2 * V;

figure('Name', 'WCs Around BF', 'NumberTitle', 'off', 'Position', [20 50 900 450]);

title('Centered weights around BF');

subplot(2,4,1);
hold on;
plot(pos + jitter, data, 'k.');
mu = nanmean(data2);
sig = nanstd(data2);
% plot(pos, mu, 'r-');
errorbar(1:9, mu, sig, sig, 'xr', 'LineWidth', 3);
d = find_best_smooth_gauss(mu');
plot(1:.01:9, mu(5).*gausswin(length(1:0.01:9), d), 'LineWidth', 3, 'Color', 'b');
hold off;
xlabel('Relative Index Around BF');
ylabel('Weight Strength');

% subplot(2,4,1);
% hold on;
% bar(pos(1,:)-5, mean(data2));
% %histfit(data2(:), length(pos(1,:)), 'normal');
% d = fitdist(data2(:), 'normal');
% plot(-4:.01:4, sum(mean(data2).^2) * pdf('norm', -4:.01:4, 0, sqrt(d.sigma)), 'LineWidth', 3, 'Color', 'r');
% hold off;
% xlabel('Relative Index Around BF');
% ylabel('Mean Weight Strength');
% 
% subplot(2,4,2);
% hold on
% %plot(1:9, mu - mu(5).*gausswin(length(1:0.01:9), d), 'k-');% 
% subplot(2,4,4);
% hold on;
% xx = data3(:, 1);
% yy = data3(:, 2);
% %yy = yy(xx > 0);
% %xx = xx(xx > 0);
% yy(xx<0) = -yy(xx<0);
% xx(xx<0) = -xx(xx<0);
% 
% scatter(xx, yy,'k.');
% p = polyfit(xx, yy, 1);
% plot(sort(xx), p(1).*sort(xx) + p(2), 'LineWidth', 3, 'Color', 'r');
% hold off;
% xlabel('Principal Component #1');
% ylabel('Principal Component #2');

% %mu = nanmean(data2);
% %sig = nanstd(data2);
% % plot(pos, mu, 'r-');
% %errorbar(1:9, mu, sig, sig, 'xr', 'LineWidth', 3);
% %d = find_best_smooth_gauss(mu');
% xlabel('Relative Index Around BF');
% ylabel('Residual');
% title('Principal Component #1');
% hold off;
% % 

subplot(2,4,2);
hold on
bar(pos(1,:)-5, pc(:,1));
%d = fitdist(pc(:,1), 'normal');
%gg = pdf('norm', -4:.01:4, 0, sqrt(d.sigma));
%plot(-4:.01:4, gg, 'LineWidth', 3, 'Color', 'r');
textLoc(sprintf('Eig: %f', S(1,1) / sum(S(:))), 'NorthWest');
xlabel('Relative Index Around BF');
ylabel('Spectral Weight Strength');
title('Principal Component #1');
hold off;

subplot(2,4,3);
bar(pos(1,:)-5, pc(:,2));
textLoc(sprintf('Eig: %f', S(2,2) / sum(S(:))), 'NorthWest');
xlabel('Relative Index Around BF');
ylabel('Spectral Weight Strength');
title('Principal Component #2');

% subplot(3,2,5);
% bar(pos(1,:)-5, pc(:,3));
% textLoc(sprintf('Eigenvalue: %f', S(3,3)), 'NorthWest');
% xlabel('Relative Index Around BF');
% ylabel('Principal Component #3');
% 
% subplot(2,4,4);
% hold on;
% xx = data3(:, 1);
% yy = data3(:, 2);
% %yy = yy(xx > 0);
% %xx = xx(xx > 0);
% yy(xx<0) = -yy(xx<0);
% xx(xx<0) = -xx(xx<0);
% 
% scatter(xx, yy,'k.');
% p = polyfit(xx, yy, 1);
% plot(sort(xx), p(1).*sort(xx) + p(2), 'LineWidth', 3, 'Color', 'r');
% hold off;
% xlabel('Principal Component #1');
% ylabel('Principal Component #2');

%figure('Name', 'Can Morlets Fit', 'NumberTitle', 'off', 'Position', [20 50 900 300]);

% Pick five points along the best fit line
xx = sort(xx);
x1 = xx(1);
y1 = p(1)*x1 + p(2);

x2 = xx(floor(0.33*length(xx)));
y2 = p(1)*x2 + p(2);

x3 = xx(floor(0.66*length(xx)));
y3 = p(1)*x3 + p(2);

x4 = xx(end);
y4 = p(1)*x4 + p(2);

    function ret = find_best_smooth_gauss(vec)   
        function err = cost_fn(phi)
            weights = gausswin(9, phi);
            %weights = weights / sum(weights.^2);    
            weights = vec(5).*weights;
            err = sum((weights - vec).^2);
        end                
        
        opts = saoptimset('MaxIter', 90000, ...
                      'MaxFunEvals', 90000, ...
                      'InitialTemperature', 0.5, ...
                      'AnnealingFcn', @annealingboltz, ...
                      'TemperatureFcn', @temperatureboltz, ...
                      'Display', 'diagnose', ...
                      'TolFun', 10^-9);
                  
        ret = simulannealbnd(@cost_fn, [3], [], [], opts);
        ret = fminsearch(@cost_fn, ret);
        
    end

    function ret = find_best_gauss(vec)   
        function err = cost_fn(phi)
            weights = wc_gaussian(phi, 9);
            %weights = weights / sum(weights.^2);    
            weights = weights ./ max(abs(weights(:)));  
            err = sum((weights - vec).^2);
        end                
        
        opts = saoptimset('MaxIter', 90000, ...
                      'MaxFunEvals', 90000, ...
                      'InitialTemperature', 0.5, ...
                      'AnnealingFcn', @annealingboltz, ...
                      'TemperatureFcn', @temperatureboltz, ...
                      'Display', 'diagnose', ...
                      'TolFun', 10^-9);
                  
        ret = simulannealbnd(@cost_fn, [.25 2], [], [], opts);
        ret = simulannealbnd(@cost_fn, [.25 2], [], [], opts);
        ret = fminsearch(@cost_fn, ret);
        
    end

    function ret = find_best_morlet(vec)   
        function err = morlet_cost_fn(phi)
            weights = wc_morlet(phi, 9);
            %weights = weights / sum(weights.^2);
            weights = weights ./ max(abs(weights(:))); 
            err = sum((weights - vec).^2);
        end                
        
        opts = saoptimset('MaxIter', 90000, ...
                      'MaxFunEvals', 90000, ...
                      'InitialTemperature', 0.5, ...
                      'AnnealingFcn', @annealingboltz, ...
                      'TemperatureFcn', @temperatureboltz, ...
                      'Display', 'diagnose', ...
                      'TolFun', 10^-9);
                  
        ret = simulannealbnd(@morlet_cost_fn, [1 7 0.1], [], [], opts);
        ret = simulannealbnd(@morlet_cost_fn, [1 7 0.1], [], [], opts);
        ret = fminsearch(@morlet_cost_fn, ret)
        
    end

    function plotexample(xin, yin)
        rng = -4:4;
        smth= -4:0.1:4;
        vec = pc(:,1)*xin + pc(:, 2)*yin;    
        %vec = vec ./ sum(vec(:).^2);       
        vec = vec ./ max(abs(vec(:)));  
        hold on;
        bar(rng, vec);        
        gv = find_best_gauss(vec)
        mv = find_best_morlet(vec);
        %vec = vec ./ sum(vec(:).^2);
        plot(rng, wc_gaussian(gv, 9), 'LineWidth', 3, 'Color', 'r', 'LineStyle', '-');        
        %plot(smth, gauss1([0, sqrt(gv(2))], smth), 'LineWidth', 3, 'Color', 'm', 'LineStyle', '--');
        plot(rng, wc_morlet(mv, 9), 'LineWidth', 3, 'Color', 'g', 'LineStyle', '--');          
        hold off;
        axis([-5 5 -0.25 1]);
        xlabel('Relative Index Around BF');        
        ylabel('Weight Strength');
    end

%subplot(2,4,5);
%plotexample(x1,y1);
%
%subplot(2,4,6);
%plotexample(x2,y2);
%
%subplot(2,4,7);
%plotexample(x3,y3);
%
%subplot(2,4,8);
%plotexample(x4,y4);
%
end
