function plot_data_quality(batch, cellids, modelnames)
% Looks for a connection between data quality and model performance

metric = 'r_fit';

% batches = {267, 266, 264};
batches = {267};

% Load the modelnames and add them to the data
testdata = nan(length(batches), length(modelnames), 500);
fitdata  = nan(length(batches), length(modelnames), 500);
n_cells = 0;
for bb = 1:length(batches)
    batch = batches{bb};
    
    sql = ['SELECT distinct cellid from sRunData WHERE batch=', num2str(batch)];
	ret = mysql(sql);
    cellids = {ret.cellid};
    
    for jj = 1:length(cellids)
        % TEMPORARY HACK: Remove troublesome cells causing request celldb
        % to crash
        if any(strcmp(cellids{jj}, {'daf076e-b1', 'daf077b-a1', 'daf077b-d1', 'dai035a-c1'}))
            continue;
        end
        s = request_celldb_batch(batch, cellids{jj});
        v_fit = dbgetscellfile('cellid', cellids{jj}, 'rawid', s{1}.training_rawid);
        v_test = dbgetscellfile('cellid', cellids{jj}, 'rawid', s{1}.test_rawid);
        
        [snr_val,snr_est, z_val, z_est,~,~, sec_val, sec_est]=db_get_snr(cellids{jj},batch);  
        % Crop out the bad cells with very low SNR
        if isempty(snr_val) || isempty(snr_est)%  || snr_est < 0.1 || snr_val < 0.1
            continue;
        end
        n_cells = n_cells + 1;
        
        for ii = 1:length(modelnames)                   
            sql = ['SELECT * FROM NarfResults WHERE batch=' num2str(batch)];
            sql = [sql ' AND modelname="' modelnames{ii} '" AND cellid="' cellids{jj} '"'];
            r = mysql(sql);
            
            if length(v_test) ~= 1
                v_test = v_test(1);
                fprintf('WARNING!!!!');
            end
            if length(v_fit) ~= 1
                fprintf('WARNING!!!!');
                v_fit = v_fit(1);
            end
            
            if isempty(v_test.isolation)
                v_test.isolation = NaN;
            end
            
            if isempty(v_fit.isolation)
                v_fit.isolation = NaN;
            end      
            
           % THEMPORARY HACK: Remove an outlier
           %if v_fit.reps == 1 && v_test.reps == 20
           %     continue;
           % end               
%                      
%          % THEMPORARY HACK: Remove outliers
           %if (v_fit.reps * snr_est < 0.4)
           %    continue;
           %end               
                     
            if ~isempty(snr_val) && ~isempty(snr_est)
                fitdata(bb,ii,jj,7) = snr_est;
                testdata(bb,ii,jj,7) = snr_val;           
                fitdata(bb,ii,jj,8) = z_est;
                testdata(bb,ii,jj,8) = z_val;         
                fitdata(bb,ii,jj,9) = sec_est;
                testdata(bb,ii,jj,9) = sec_val;  
            end                        
            
            fitdata(bb,ii,jj,1) = v_fit.reps;
            fitdata(bb,ii,jj,2) = v_fit.spikes;
            fitdata(bb,ii,jj,3) = v_fit.isolation;
            fitdata(bb,ii,jj,4) = v_fit.spikes / v_fit.reps;
            
            if ~isempty(r)
                fitdata(bb,ii,jj,5) = r(1).r_test / r(1).r_ceiling;
                fitdata(bb,ii,jj,6) = r(1).r_fit.^2;
            end
            
            testdata(bb,ii,jj,1) = v_test.reps;
            testdata(bb,ii,jj,2) = v_test.spikes;
            testdata(bb,ii,jj,3) = v_test.isolation;
            testdata(bb,ii,jj,4) = v_test.spikes / v_test.reps;
            
            if ~isempty(r)
                testdata(bb,ii,jj,5) = r(1).r_test / r(1).r_ceiling;
                %testdata(bb,ii,jj,6) = r(1).r_test .^2;
                testdata(bb,ii,jj,6) = r(1).r_ceiling .^2;
            end
            
        end
    end
end

% Popup plot of low and high SNR data
%lowsnr = testdata(:, ;
%figure;
%bar(testdata(:,


testdata = reshape(testdata, [], 9);
fitdata = reshape(fitdata, [], 9);

testdata = excise(testdata);
fitdata = excise(fitdata);

    function plotit(x, y)
                hold on
                plot(x, y, 'k.');
                d = sortrows([x y]);
                gf = gausswin(2*ceil(length(x)/10));
                gf = gf / sum(gf);
                plot(iconv(d(:, 1), gf), iconv(d(:, 2), gf), 'r-');
                p = polyfit(x,y,1);title('FitSNR vs TestSNR')
                plot(x, p(1)*x + p(2), 'g-');
        
                rr = corr(x,y);
                textLoc(sprintf('r=%f', rr), 'North');
        
                hold off;
        
        
%         %figure; 
%         ix = 1./x;
%         iy = 1./y;
%         p = polyfit(ix,iy,1); 
%         hold on;
%         plot(ix, iy, 'k.'); 
%         plot (ix, p(1).*ix + p(2), 'r-');
%         title('FitSNR vs TestSNR')
%         xlabel('1/(T*SNR^2)');
%         ylabel('1/(R_{ceil}^2)');
%         p
%         hold off;
    
    end

figure;
% %%%%%%%%%%%%%%%

% Does SNR determine R^2? 
subplot(3,4,1);
plotit(fitdata(:, 7), fitdata(:, 6));
title('FitSNR vs Rfit^2');
textLoc(sprintf('N=%d', n_cells), 'NorthWest');

%% Does fit rep count affect r_fit?
%subplot(3,4,2);
%plotit(fitdata(:, 1), fitdata(:, 6));
%title('FitReps vs Rfit^2');

% Does a changing reps ratio clearly affect Rfit?
subplot(3,4,2);
plotit(testdata(:,1) ./ fitdata(:, 1), fitdata(:, 6));
title('testreps/FitReps vs Rfit^2');

%subplot(3,4,3);
%plotit(fitdata(:, 7) .* fitdata(:, 1), fitdata(:, 6));
%title('FitSNR * FitReps vs Rfit^2');

% Does repcount clearly affect SNR?
%subplot(3,4,3);
%plotit(fitdata(:, 1), fitdata(:, 7));
%title('FitReps vs FitSNR');

% Do few fit reps hurt Rfit?
subplot(3,4,3);
plotit(fitdata(:, 9) ./ fitdata(:, 1), fitdata(:, 6));
title('FitSecs/FitReps vs Rfit');

% Is most of the data good for both data sets?
%subplot(3,4,4);
%plotit(fitdata(:, 7), testdata(:, 7));
%title('FitSNR vs TestSNR');

% Do few fit reps hurt Rfit?
subplot(3,4,4);
plotit(fitdata(:, 9) ./ fitdata(:, 1), testdata(:, 6));
title('FitSecs/FitReps vs Rtest');

%%%%%%%%%%%%%%%

subplot(3,4,5);
plotit(fitdata(:, 7), testdata(:, 6));
title('FitSNR vs Rtest^2');

subplot(3,4,6);
plotit(testdata(:,7), testdata(:, 6));
title('TestSNR vs Rtest^2');
% 
% subplot(3,4,7);
% plotit(sqrt(fitdata(:, 7) .* testdata(:,7)), testdata(:, 6));
% title('sqrt(FitSNR * TestSNR) vs Rtest^2');


subplot(3,4,7);
plotit(fitdata(:, 7) .* fitdata(:,1) .* fitdata(:,9), testdata(:, 6));
title('FitSNR * FitReps * FitSecs vs Rtest^2');

subplot(3,4,8);
plotit(fitdata(:, 7) .* fitdata(:, 1), testdata(:, 6));
title('FitSNR .* FitReps vs Rtest^2');

%%%%%%%%%%%%%%%

subplot(3,4,9);
plotit(fitdata(:, 7).^2 .* fitdata(:,1), testdata(:, 6));
title('(FitSNR)^2 * FitReps vs Rtest^2');

subplot(3,4,10);
plotit(testdata(:, 7) .* testdata(:,1), testdata(:, 6));
title('TestSNR * TestReps vs Rtest^2');

subplot(3,4,11);
plotit(sqrt(fitdata(:, 7) .* testdata(:, 7) .* fitdata(:,1)), testdata(:, 6));
title('sqrt(FitSNR * TestSNR * FitReps) vs Rtest^2');

subplot(3,4,12);
plotit(sqrt(fitdata(:, 7) .* testdata(:, 7) .* fitdata(:,1) .* testdata(:,1)), testdata(:, 6));
title('sqrt(FitSNR * TestSNR * FitReps * TestReps) vs Rtest^2');


%%%%%%%%%%%%%%%
% 
% subplot(4,3,7);
% plotit(fitdata(:, 2) ./ fitdata(:, 9), fitdata(:, 6));
% title('Fit Spikes/sec vs Rfit');
% 
% subplot(4,3,8);
% plotit(fitdata(:, 2) ./ fitdata(:, 1), fitdata(:, 6));
% title('Fit Spikes/rep vs Rfit');
% 
% subplot(4,3,9);
% plotit(fitdata(:, 7) ./ fitdata(:, 1), fitdata(:, 6));
% title('Fit SNR/rep vs Rfit');

%%%%%%%%%%%%%%%
% 
% 
% subplot(4,3,10);
% plotit(fitdata(:, 7) .* fitdata(:, 9), fitdata(:, 6));
% title('SNR * sec vs Rfit');
% 
% subplot(4,3,11);
% plotit(fitdata(:, 7) .* fitdata(:, 1), fitdata(:, 6));
% title('SNR * reps vs Rfit');
% 
% subplot(4,3,12);
% plotit(fitdata(:, 7) .* fitdata(:, 2), fitdata(:, 6));
% title('SNR * spikes vs Rfit');
% 
% 

% subplot(4,3,1);
% plotit(fitdata(:, 8), fitdata(:, 6));
% title('Z_est vs Rfit');
% 
% subplot(4,3,2);
% plotit(fitdata(:, 8), testdata(:, 6));
% title('Z_est vs Rtest');
% 
% subplot(4,3,3);
% plotit(testdata(:, 8), testdata(:, 6));
% title('Test Reps vs Rtest');

%%%%%%%%%%%%%%%
% subplot(4,3,4);
% plotit(fitdata(:, 2), fitdata(:, 6));
% title('Fit Spikes vs Rfit');
% 
% subplot(4,3,5);
% plotit(fitdata(:, 2), testdata(:, 6));
% title('Fit Spikes vs Rtest');
% 
% subplot(4,3,6);
% plotit(testdata(:, 2), testdata(:, 6));
% title('Test Spikes vs Rtest');

%%%%%%%%%%%%%%%
% subplot(4,3,7);
% plotit(fitdata(:, 3), fitdata(:, 6));
% title('Fit Isolation vs Rfit');
% 
% subplot(4,3,8);
% plotit(fitdata(:, 3), testdata(:, 6));
% title('Fit Isolation vs Rtest');
% 
% subplot(4,3,9);
% plotit(testdata(:, 3), testdata(:, 6));
% title('Test Isolation vs Rtest');
% 
% 
% subplot(4,3,7);
% plotit(fitdata(:, 9), fitdata(:, 6));
% title('Est info vs Rfit');
% 
% subplot(4,3,8);
% plotit(fitdata(:, 9), testdata(:, 6));
% title('Est Info vs Rtest');
% 
% subplot(4,3,9);
% plotit(testdata(:, 9), testdata(:, 6));
% title('Val Info vs Rtest');

%%%%%%%%%%%%%%%%
% 
% subplot(4,3,10);
% plotit(fitdata(:, 7), fitdata(:, 6));
% title('Fit SNR vs Rfit');
% 
% subplot(4,3,11);
% plotit(fitdata(:, 7), testdata(:, 6));
% title('Fit SNR vs Rtest');
% 
% subplot(4,3,12);
% plotit(testdata(:, 7), testdata(:, 6));
% title('Test SNR vs Rtest');

%subplot(4,3,11);
%mesh(fitdata(:, 1), fitdata(:, 2), zeros(sizeof(testdata(:,6))), testdata(:, 6));
%title('Trial CC Ratio vs Rtest');
%%%%%%%%%%%%%%%

%plot_scatter(reshape(data, [], 6), {'reps', 'spikes', 'isolation', 'spkrate', 'ratio', 'r fit'});
%plot_scatter(reshape(data, [], 6), {'reps', 'spikes', 'isolation', 'spkrate', 'ratio', 'r fit'});

end
