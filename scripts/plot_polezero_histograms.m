function plot_polezero_histograms(batch, cellids, modelnames)

    function dat = pack_pzstuff(~)
        global STACK;
        dat = [];
        for iii = 1:length(STACK)
            mm = STACK{iii};
            nsplits = length(mm);
            
            if nsplits > 1
                error('I only understand non-split pz models right now');
            end
            
            m = mm{1};
            
            if ~strcmp(m.name, 'pole_zeros')
                continue;
            end 
            
            if ~isempty(dat)
                error('Why are there two PZ models!?');
            end
          
            dat.poles = m.poles;
            dat.zeros = m.zeros;
            dat.gains = m.gains; 
            dat.delays = abs(m.delays);
        end        
    end

batches = {264, 266, 267}
for 

[params, ~, ~] = load_model_batch(batch, cellids, modelnames, @pack_pzstuff);

% Build collections from the data
xpoles = [];
xzeros = [];
xgains = [];
xdelays = [];

for ii = 1:length(params)
    xpoles = cat(3, xpoles, params{ii}.poles);
    xzeros = cat(2, xzeros, params{ii}.zeros);
    xgains = cat(2, xgains, params{ii}.gains');
    xdelays = cat(2, xdelays, params{ii}.delays);    
end

poles = sort(xpoles(:));
zeros = sort(xzeros(:));
gains = sort(xgains(:));
delays = sort(xdelays(:));

% Optionally, crop to the middle 90%
cropit = @(x) x(floor(0.05*length(x)):ceil(0.95*length(x)));

if 1
    poles = poles(floor(0.03*length(poles)):end);
    zeros = zeros(floor(0.02*length(zeros)):ceil(0.99*length(gains)));
    gains = gains(floor(0.02*length(gains)):ceil(0.98*length(gains)));
    delays = delays(1:ceil(0.98*length(delays)));
end

figure('Name', ['Pole Zero Histograms of ' cell2mat(modelnames(1))], 'NumberTitle', 'off', 'Position', [20 50 900 900]);
subplot(1, 3, 1);
hist(poles, 50);
xlabel('Poles, 3-100%');

subplot(1, 3, 2);
hist(zeros, 50);
xlabel('Zeros, 2-99%');

subplot(1, 3, 3);
hist(delays, 50);
xlabel('Delays, 0-98%');

%subplot(2, 2, 4);
%plot(abs(xgains(:)), squeeze(xpoles(1,:)), 'bx', abs(xgains(:)), xzeros(:), 'ro');
%
%plot(squeeze(xpoles(:,1,:)), xzeros(:), 'ro', squeeze(xpoles(:,2,:)), xzeros(:), 'bo' );
%xlabel('Gains');
%ylabel('Poles (x) and Zeros (o)');


end
