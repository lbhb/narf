function dump_performance(batch, cellids, modelnames)

global narf_dump_data narf_dump_cellids

dbopen;                

%fieldstoget={'r_floor', 'r_test',};
fieldstoget={'r_test','r_floor'};

data = nan(length(cellids), length(modelnames),length(fieldstoget));        
for ii = 1:length(modelnames),
    for jj = 1:length(cellids)
        sql = ['SELECT * FROM NarfResults WHERE batch=' num2str(batch) ''];
        sql = [sql ' AND cellid in (' interleave_commas(cellids(jj)) ')'];
        sql = [sql ' AND modelname in (' interleave_commas(modelnames(ii)) ')'];
        ret = mysql(sql); 
        
        if isempty(ret) 
            continue;
        end
        if length(ret) > 1
            error('More than one matching model found!');
        end
        
        for kk = 1:length(fieldstoget)
            data(jj, ii, kk) = ret(1).(fieldstoget{kk});
        end
    end
end

disp('KEY:');
for jj=1:length(fieldstoget),
   for ii=1:length(modelnames),
      fprintf('%d: %s - %s\n',ii*(jj-1)*length(modelnames),modelnames{ii},fieldstoget{jj});
   end
end
fprintf('\n%-25s','CellID');
for ii=1:length(modelnames)*length(fieldstoget),
    fprintf(',%8d',ii);
end
fprintf('\n');

for cc=1:length(cellids),
    fprintf('%-25s',cellids{cc});
    fprintf(',%8.3f',data(cc,:,:));
    fprintf('\n');
end

narf_dump_data=data;
narf_dump_cellids=cellids;