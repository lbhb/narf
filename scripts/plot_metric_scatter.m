function plot_metric_scatter(batch, cellids, modelnames)
% Makes a large complexity plot showing all valid models for the current
% batch

% Get the number of cells in the batch
cells = request_celldb_batch(batch);
N_cells = length(cells);
%metrics = {'r_test', 'mse_test', 'mi_test', 'nlogl_test', 'cohere_test'};
metrics = {'r_fit', 'mse_fit', 'mi_fit', 'nlogl_fit', 'cohere_fit'};


% Load the modelnames and add them to the data IFF they are complete
% in both r_ceil and n_parms
data = nan(N_cells * length(modelnames), length(metrics));

% Set this to true if you want the POPULATION AVERAGE
USEPOPAVG = false;


for ii = 1:length(modelnames)
    sql = ['SELECT * FROM NarfResults WHERE batch=' num2str(batch)];
    sql = [sql ' AND modelname="' modelnames{ii} '"'];
    results = mysql(sql);    
    mm = [];
    for jj = 1:length(metrics)
        if USEPOPAVG
            mm = cat(2, mm,  mean([results.(metrics{jj}) ]'));
        else
            mm = cat(2, mm,  [results.(metrics{jj}) ]');
        end
    end
    data = [data; mm];
end

figure;
plot_scatter_funky(data, metrics);
end

function plot_scatter_funky(X, names)
plot_marginals = false;
autoscale = false;

X = excise(X);

% Set plot borders and sizes
tmargin = 0.1;
lmargin = 0.1;
rmargin = 0.1;
bmargin = 0.1;

% Set text position on each subplot, from 0 to 1
textx = 0.05;  % 0.1=Left
texty = 0.95;  % 0.9=Top

axmin = -0.2;
axmax = 0.9;
axmin = min(min(X(:)), 0);
axmax = max(X(:));

bins = linspace(axmin, axmax, 20);

[Npoints, Nsets] = size(X);

if not(exist('names')) || length(names) ~= Nsets
    error('I need as many names as the size of the y dimension!');
end

names = shorten_modelnames(names);

R = corrcoef(X);

w = (1.0 - lmargin - rmargin) / (Nsets - 1);    % Plot width
h = (1.0 - tmargin - bmargin) / (Nsets - 1);    % Plot height

for i = 1:Nsets
    for j = 1:Nsets;
        
        % Calculate draw location
        if i < j
            subplot('Position', [(lmargin+(i-1)*w) (bmargin + (Nsets-j)*h) w h]);
        else
            continue;
        end
        
        % Use the 5-95% range for plotting data (ignore outliers)
        xs = sort(X(:,i));
        ys = sort(X(:,j));
        bot = ceil(size(X,1)*0.05);
        top = ceil(size(X,1)*0.95);
        %axis([xs(bot), xs(top), ys(bot), ys(top)]);
        
        hold on;
        plot(X(:,i),X(:,j), 'k.');
                
        xx = X(:,i);
        yy = X(:,j);        
        t = min(xx):0.01:max(xx);
        
        % For finding the equations
        p = polyfit(xx,yy,1);
        %plot(t, p(1).*t.^2 + p(2).*t + p(3), 'r--');
        plot(t, p(1).*t + p(2), 'r--');
        %textLoc(sprintf('y = %0.3fx^2 + %0.3fx + %0.3f\n', p(1), p(2), p(3)), 'NorthEast');
        textLoc(sprintf('y = %0.3fx + %0.3f\n', p(1), p(2)), 'NorthEast');
         pearson = corrcoef(xx, yy);
        textLoc(sprintf('Corr = %0.3f\n', pearson(2,1)), 'NorthWest');
                
        if (i==1 && j==2)
            plot(t, -t.^2 +1, 'r-');
            textLoc('NMSE = -r^2 + 1', 'NorthEast');
        elseif (i==1 && j==3)
%             plot(t, .70*t.^2+.007, 'r-');            
%             plot(t, .71*t.^2+.007, 'b-');
%             plot(t, .72*t.^2+.007, 'g-');
%             textLoc('MI = 2/3 r^2 + 0.07', 'NorthEast');
        elseif (i==1 && j==5)
            plot(t, 0.40.*t.^2 + .042.*t + (0.02), 'r-');                        
            textLoc('gamma = 0.4r^2 + 0.042*r + 0.02', 'NorthEast');
        elseif (i==2 && j==3)
            plot(t, 0.73 * (1 - t) + 0.007, 'r-');
            textLoc('MI = 0.73 (1-NMSE) + 0.007' , 'NorthEast');
        elseif (i==2 && j==4)
            plot(t, -0.565 * (1-t) - .565, 'r-');
            textLoc('NLOGL = -0.565 (1-NMSE) - .565', 'NorthEast');
        elseif (i==2 && j==5)
            plot(t, 0.541 * (1 - t) + .023, 'r-');
            textLoc('gamma = 0.541 (1-NMSE) + .023', 'NorthEast');
        end
            
        % Turn off tick labels unless in the bottom or leftmost rows
        if j == Nsets 
            hl = xlabel(sprintf('%s\n\nmean:%.3f\nmed:%.3f', names{i}, ...
                                nanmean(X(:,i)),nanmedian(X(:,i))));
            set(hl,'interpreter','none');
        else
            set(gca, 'XTickLabel', '');
        end
        
        if i == 1 
            hl = ylabel(sprintf('%s\n\nmean:%.3f\nmed:%.3f', names{j}, ...
                                nanmean(X(:,j)),nanmedian(X(:,j))));
            set(hl,'interpreter','none');
        else
            set(gca, 'YTickLabel', '');
        end
        
        hold off;
    end
end
end