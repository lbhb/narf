
global XXX

sf=XXX{1}.test_set{1};

XXX{xid}.dat.(sf)

figure;

subplot(3,1,1);
stimid=1;
xid=4;
plot(XXX{xid}.dat.(sf).stim(:,:,stimid));
legend('center','surround');
title('input stimulus envelopes');

xid=6;
subplot(3,1,2);
plot(XXX{xid}.dat.(sf).stim(:,:,stimid));
legend('center','surround');
title('post-depression');

subplot(3,1,3);
xid=9;
plot(XXX{xid}.dat.(sf).stim(:,stimid),'b');
hold on
plot(XXX{xid}.dat.(sf).respavg(:,stimid),'k');
hold off
legend('pred','actual resp');


figure;

subplot(3,1,1);
stimid=1;
xid=4;
plot(XXX{xid}.dat.(sf).stim(:,:,stimid));
legend('center','surround');
title('input stimulus envelopes');


subplot(3,1,3);
xid=7;
plot(XXX{xid}.dat.(sf).stim(:,stimid),'b');
hold on
plot(XXX{xid}.dat.(sf).respavg(:,stimid),'k');
hold off
legend('pred','actual resp');


