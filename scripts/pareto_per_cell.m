function pareto_per_cell(batch, cellids, modelnames)
% compute pareto frontier for each neuron

metric = 'r_ceiling';

if isempty(batch) || isempty(cellids) || isempty(modelnames)
    disp('Error: need to select cellids and models.');
    return;
end
dbopen;

fieldstoget={metric,'n_parms'};
data = nan(length(cellids), length(modelnames),length(fieldstoget));
modelcount=length(modelnames);

for ii = 1:length(modelnames)
   %fprintf('%d: %s\n', ii,modelnames{ii});
   fprintf('%d ',ii);
   for jj = 1:length(cellids)
      sql = ['SELECT * FROM NarfResults WHERE batch=' num2str(batch) ''];
      sql = [sql ' AND cellid in (' interleave_commas(cellids(jj)) ')'];
      sql = [sql ' AND modelname in (' interleave_commas(modelnames(ii)) ')'];
      ret = mysql(sql);
      
      if isempty(ret)
         continue;
      end
      
      if length(ret) > 1
         fprintf('More than one matching model found!');
         keyboard;
      end
      
      for kk = 1:length(fieldstoget)
         v = ret(1).(fieldstoget{kk});
          
         if isempty(v)
            v = nan;
         end
         data(jj, ii, kk) = v;
      end
   end
end
disp('done');

% load SNRs
snr=zeros(length(cellids),2);
for jj = 1:length(cellids)
    sql=['SELECT * from NarfBatches',...
         ' WHERE batch=',num2str(batch),' AND cellid="',cellids{jj},'"'];
    snrdata=mysql(sql);
    snr(jj,:)=[snrdata.est_snr snrdata.val_snr];
end

% pareto plot
d=data(:,:,1);
md=median(d,2);
d=d-repmat(md,[1 size(d,2)]);
rd=max(d,[],2)-min(d,[],2);
rd=std(d,0,2);
d=d./repmat(rd,[1 size(d,2)]);
%d=[d (md-mean(md))./std(md) (rd-mean(rd))./std(rd)];
d=[d (md-mean(md))./std(md)];

% compute pareto plot for each cell
minparms=min(data(1,:,2));
maxparms=max(data(1,:,2));
ml=zeros(maxparms,length(cellids));
for jj = 1:length(cellids)
    
    for ii=minparms:maxparms,
        ml(ii,jj)=max(d(jj,data(jj,:,2)<=ii,1));
    end
end

if batch==267,
   cluster_count=3;
elseif batch==259,
   cluster_count=4;
end  
   
Z = linkage(d,'ward','euclidean');
c = cluster(Z,'maxclust',cluster_count);
%crosstab(c)

% sort by mean performance
mxc=mean(data(:,:,1),1);
[smxc,ssi]=sort(mxc);
sd=data(:,ssi);
smn=modelnames(ssi);
for ii=1:modelcount,
   fprintf('%d: %.3f %s\n',ii,smxc(ii),smn{ii});
end

if batch==267,
   % r_ceiling VOC
   mi=[find(strcmpi(smn,'fb18ch100_lognn_wcg01_ap4z1_dexp_fit05v')) ...
      find(strcmpi(smn,'fb18ch100_lognn_wcg01_adp1pc_ap4z1_dexp_fit05v')) ...
      find(strcmpi(smn,'fb18ch100_lognn_wcg04_ap4z1_dexp_fit05v')) ...
      find(strcmpi(smn,'fb18ch100_lognn_wcg04_adp1pc_ap4z1_dexp_fit05v'))];
   %mi=[find(strcmpi(smn,'fb18ch100_lognn_wcg01_ap3z1_dexp_fit05v')) ...
   %   find(strcmpi(smn,'fb18ch100_lognn_wcg01_adp2pc_ap3z1_dexp_fit05v')) ...
   %   find(strcmpi(smn,'fb18ch100_lognn_wcg05_ap3z1_dexp_fit05v')) ...
   %   find(strcmpi(smn,'fb18ch100_lognn_wcg05_adp2pc_ap3z1_dexp_fit05v'))];
   %mi=[4 19 39];  % r_test VOC
   strip='fb18ch100_lognn_';
elseif batch==259,
   % r_ceiling SPN
   mi=[find(strcmpi(smn,'env100_logn_wc01c_fir15_siglog100_fit05h_fit05c')) ...
      find(strcmpi(smn,'env100_logn_wc01c_adp2pc_fir15_siglog100_fit05h_fit05c')) ...
      find(strcmpi(smn,'env100_logn_wc04c_fir15_siglog100_fit05h_fit05c')) ...
      find(strcmpi(smn,'env100_logn_wc04c_adp2pc_fir15_siglog100_fit05h_fit05c'))];
   strip='_siglog100_fit05h_fit05c';
end

cvar=zeros(cluster_count,1);
cmean=zeros(cluster_count,modelcount);
cmodelmin=cell(cluster_count,1);
cmodelmax=cell(cluster_count,1);
for ii=1:cluster_count,
   cmean(ii,:)=mean(d(c==ii,1:modelcount));
   cmodelmin{ii}=modelnames{min(find(cmean(ii,:)==min(cmean(ii,:))))};
   cmodelmax{ii}=modelnames{min(find(cmean(ii,:)==max(cmean(ii,:))))};
   cvar(ii)=var(cmean(ii,:));
end
c1=min(find(cvar==max(cvar)));
cvar(c1)=0;
c2=min(find(cvar==max(cvar)));
if length(mi)<4,
   mi=[find(strcmpi(smn,cmodelmin{c1})) find(strcmpi(smn,cmodelmax{c1})) ...
      find(strcmpi(smn,cmodelmin{c2})) find(strcmpi(smn,cmodelmax{c2}))];
end

figure; 

[u,s,v]=svd(d);

subplot(3,2,2);
dendrogram(Z);


dproj=d*v;
modelproj=d'*u;
subplot(3,1,2);
plot(v(ssi,1:3),'LineWidth',2);
legend('1','2','3');

%colors={'b','g','r','c','k'};
colors=[0 0 1; 1 0 0; 0 1 0; 0 .5 .5; 0 0 0];

ls={};
for ii=1:cluster_count,
   fprintf('Cluster %d. n=%d snr=%.3f\n',ii,length(find(c==ii)),mean(snr(c==ii,1)));
   
   ls{ii}=sprintf('C%d: n=%d snr=%.2f\n',ii,length(find(c==ii)),mean(snr(c==ii,1)));
    subplot(3,2,1);
    %plot(minparms:maxparms,mean(ml(minparms:maxparms,c==ii),2),'-','Color',colors(ii,:));
    plot(median(sd(c==ii,:)),'-','Color',colors(ii,:));
    hold on
    
    subplot(3,2,5);
    plot(dproj(c==ii,1),dproj(c==ii,2),'.','Color',colors(ii,:));
    hold on
    subplot(3,2,6);
    plot(dproj(c==ii,2),dproj(c==ii,3),'.','Color',colors(ii,:));
    hold on
end

subplot(3,2,1);
title(sprintf('Batch %d',batch));
plot(mean(sd(:,:)),'k');
legend(ls);
%axis([minparms-1 maxparms+1 min(d(:)) max(d(:))]);
%xlabel('param count');
%ylabel('best model performance');
hold off

subplot(3,2,5);
hold off
subplot(3,2,6);
hold off


figure;
mxc=zeros(length(mi),cluster_count);
for ii=1:cluster_count,
   mxc(:,ii)=mean(sd(c==ii,mi));
   
   subplot(2,2,1);
   plot(sd(c==ii,mi(1)),sd(c==ii,mi(2)),'.','Color',colors(ii,:));
   hold on
   subplot(2,2,2);
   plot(sd(c==ii,mi(3)),sd(c==ii,mi(4)),'.','Color',colors(ii,:));
   hold on
   
   subplot(2,2,4);
   plot(sd(c==ii,mi(3))-sd(c==ii,mi(1)),sd(c==ii,mi(4))-sd(c==ii,mi(3)),'.','Color',colors(ii,:));
   hold on
   
   fprintf('Cluster %d:',ii);
   for jj=find(c==ii)',
      fprintf(' %s',cellids{jj});
   end
   fprintf('\n');
end

subplot(2,2,1);
title(sprintf('Batch %d',batch));
plot([-0.1 1],[-0.1 1],'k--');
xlabel(strrep(smn{mi(1)},strip,''),'Interpreter','None');
ylabel(strrep(smn{mi(2)},strip,''),'Interpreter','None');
axis tight square
hold off
legend(ls,'Location','southeast');

subplot(2,2,2);
plot([-0.1 1],[-0.1 1],'k--');
xlabel(strrep(smn{mi(3)},strip,''),'Interpreter','None');
ylabel(strrep(smn{mi(4)},strip,''),'Interpreter','None');
axis tight square
hold off

subplot(2,2,3);
hb=bar(mxc);
colormap(colors(1:cluster_count,:));

subplot(2,2,4);
plot([-0.5 0.7],[-0.5 0.7],'k--');
xlabel('spectral complexity improvement');
ylabel('adp improvement');
axis tight square
hold off



keyboard



