function plot_data_bars(batch, cellids, modelnames)
% Looks for a connection between data quality and model performance
% 
% modelnames = {'fb18ch100_lognn_wc01_fir15_siglog100_fit05h_fit05c', ...
%               'fb18ch100_lognn_wc02_fir15_siglog100_fit05h_fit05c', ...
%               'fb18ch100_lognn_wc03_fir15_siglog100_fit05h_fit05c', ...
%               'fb18ch100_lognn_wc04_fir15_siglog100_fit05h_fit05c', ...
%               'fb18ch100_lognn_wcg01_fir15_siglog100_fit05h_fit05c', ...
%               'fb18ch100_lognn_wcg02_fir15_siglog100_fit05h_fit05c', ...
%               'fb18ch100_lognn_wcg03_fir15_siglog100_fit05h_fit05c', ...
%               'fb18ch100_lognn_wcg04_fir15_siglog100_fit05h_fit05c', ...
%               'fb18ch100_lognn_wcg01_fir15p_siglog100_fit05h_fit19g', ...
%               'fb18ch100_lognn_wcg02_fir15p_siglog100_fit05h_fit19g', ...
%               'fb18ch100_lognn_wcg03_fir15p_siglog100_fit05h_fit19g', ...
%               'fb18ch100_lognn_wcg04_fir15p_siglog100_fit05h_fit19g', ...
%               'fb18ch100_lognn_wcg01_ap3z1_dexp_fit09c', ...
%               'fb18ch100_lognn_wcg02_ap3z1_dexp_fit09c', ...
%               'fb18ch100_lognn_wcg03_ap3z1_dexp_fit09c', ...
%               'fb18ch100_lognn_wcg04_ap3z1_dexp_fit09c', ...
%               'fb18ch100_lognn_wcg05_ap3z1_dexp_fit09c', ...                   
%               'fb18ch100_lognn_wcg01_ap1z0_dexp_fit09c', ...
%               'fb18ch100_lognn_wcg02_ap1z0_dexp_fit09c', ...
%               'fb18ch100_lognn_wcg03_ap1z0_dexp_fit09c', ...
%               'fb18ch100_lognn_wcg04_ap1z0_dexp_fit09c', ...
%               'fb18ch100_lognn_wcg05_ap1z0_dexp_fit09c', ...
%               'fb18ch100_lognn_wcg06_ap1z0_dexp_fit09c'};

% SNR Thresholds for "ok" data and "good" data
% >0.07 gets 2/3rds
% >0.10 is the 50% point for the population
% >0.20 gets the top few cells

% These are good for SNR
snr_thresh_ok   = 0.04;
snr_thresh_good = 0.12;

% These are good for total info
info_thresh_ok   = -1;
info_thresh_good = 0.04;

metric = 'r_ceiling';

batches = {267, 267, 264};

% Load the modelnames and add them to the data
% There are several 3 groups of data quality, and up to 500 cells/batch
data = nan(length(batches), 3, length(modelnames), 500);

for bb = 1:length(batches)
    batch = batches{bb};
    
    sql = ['SELECT distinct cellid from sRunData WHERE batch=', num2str(batch)];
	ret = mysql(sql);
    cellids = {ret.cellid};   
    
    for jj = 1:length(cellids)
        cellid = cellids{jj};       
        
        [snr_val, snr_est, z_val, z_est, ~,~,sec_val, sec_est] = db_get_snr(cellid,batch); 
        s = request_celldb_batch(batch, cellid); 
        vvv = dbgetscellfile('cellid', cellid, 'rawid', s{1}.training_rawid);        
        nreps = vvv.reps;
        
        % If we don't know SNR, drop it
        if isempty(snr_val) || isempty(snr_est) 
            fprintf('Skipping %s becasue of no SNR\n', cellid);
            continue;
        end        
        
%         % Decide which "quality group" to put results in
%         if snr_est > snr_thresh_good && snr_val > snr_thresh_good
%             group = 3;
%         elseif snr_est > snr_thresh_ok && snr_val > snr_thresh_ok
%             group = 2;
%         else 
%             group = 1;
%         end

        % Using "Total information" may be a better metric
        if snr_est^2 * nreps > info_thresh_good && snr_val^2 * nreps > info_thresh_good
            group = 3;
        elseif snr_est^2 * nreps > info_thresh_ok && snr_val^2 * nreps > info_thresh_ok
            group = 2;
        else 
            group = 1;
        end
        
        for ii = 1:length(modelnames)                   
            model = modelnames{ii};
            sql = ['SELECT * FROM NarfResults WHERE batch=' num2str(batch)];
            sql = [sql ' AND modelname="' modelnames{ii} '" AND cellid="' cellids{jj} '"'];
            r = mysql(sql);                        
            if length(r) > 1 
                r = r(1);
            end
            if isempty(r)
                continue;
            end
            data(bb, group, ii, jj) = r.(metric);                    
        end
    end

    % Plot the models in bad, ok, and good data
    figure('Name', 'Models by Data Quality', 'NumberTitle', 'off', ...
                     'Position', [10 10 1200 900]); 
        
    barbarbar(squeeze(data(bb, :, :, :)), modelnames);     
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function barbarbar(data, models)    

        Dcount = sum(~isnan(data), 3);
        Dstddev = sqrt(nanvar(data, [], 3));
        Dstderr = Dstddev ./ sqrt(Dcount);
        Dmean = nanmean(data, 3);
        len = length(models);
        
        barwidth = 0.8;
        
        hold on;        
        
        % Plot the bars
        bar(Dmean(3,:), barwidth, 'c');
        bar(Dmean(2,:), barwidth, 'g');
        bar(Dmean(1,:), barwidth, 'r');
        
        % Error bars        
        errorbar(1:len, Dmean(1,:), Dstderr(1,:), 'k.', 'LineWidth',2);            
        errorbar(1:len, Dmean(2,:), Dstderr(2,:), 'k.', 'LineWidth',2);            
        errorbar(1:len, Dmean(3,:), Dstderr(3,:), 'k.', 'LineWidth',2);            
        
        % Values
        for ii = 1:3
            for jj = 1:size(Dmean, 2)
                text(jj, Dmean(ii,jj) + Dstderr(ii,jj), sprintf(' %0.3f', Dmean(ii,jj)));
            end
        end
        
        ax = gca;
        set(gca,'XTick', 1:len+3);    
        
        % Plot jittered scatter data at end
        a = nanmean(squeeze(data(1, :, :)));
        jitter = -0.15*barwidth+0.3*barwidth*rand(size(a));                
        plot((len+1)*ones(size(a))+jitter, a, 'LineStyle', '.', 'Color', [1 0 0]);      
        
        a = nanmean(squeeze(data(2, :, :)));
        jitter = -0.15*barwidth+0.3*barwidth*rand(size(a));           
        plot((len+2)*ones(size(a))+jitter, a, 'LineStyle', '.', 'Color', [0 1 0]);     
        
        a = nanmean(squeeze(data(3, :, :)));
        jitter = -0.15*barwidth+0.3*barwidth*rand(size(a));           
        plot((len+3)*ones(size(a))+jitter, a, 'LineStyle', '.', 'Color', [0 0 1]); 
                
        % Add Labels and rotate
        labelz = shorten_modelnames(models);
        set(gca,'XTickLabel', cat(1, labelz, {'Bad Data'; 'OK Data'; 'Good Data'}));
        set(gca,'CameraUpVector',[-1,0,0]);
        
        %title(sprintf('Batch %d: R_{ceiling} for Various SNR limits (Bad<%0.3f, Good >%0.3f)(N_{bad}=%d, N_{OK}=%d, N_{good}=%d)', batch, snr_thresh_ok, snr_thresh_good, Dcount(1), Dcount(2), Dcount(3)));
        title(sprintf('Batch %d: R_{ceiling} for Various InfoSNR limits (Bad<%0.3f, Good >%0.3f)(N_{bad}=%d, N_{OK}=%d, N_{good}=%d)', batch, info_thresh_ok, info_thresh_good, Dcount(1), Dcount(2), Dcount(3)));
        hold off;
        
end

    function plotcurves(x, y1, y2, y3)
        hold on
        plot(x, y1, 'r.');
        plot(x, y2, 'b.');
        plot(x, y3, 'g.');
        d = sortrows([x y]);
        gf = gausswin(2*ceil(length(x)/10));
        gf = gf / sum(gf);
        plot(iconv(d(:, 1), gf), iconv(d(:, 2), gf), 'r-');        
        p = polyfit(x,y,1);title('FitSNR vs TestSNR')
        plot(x, p(1)*x + p(2), 'g-');
        
        legend('267', '266', '264');        
        hold off;
    end

end
