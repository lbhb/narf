function fitset=dump_dep_parms(batch, cellids, modelnames)

    function ret = getit(x, idx)
        if isempty(x)
            ret = nan;
        else
            ret = x(idx);
        end
    end
    
    function ret = getfitval(x)
        if ~isfield(x, 'r_fit') || ~isfield(x, 'r_test')
            ret = [nan nan];
        else
            ret = [x.r_fit x.r_test];
        end
    end

stack_extractor = @(x) x;
meta_extractor = @getfitval;
% stacks is modelnames X cellid
disp('Loading models...');
[stacks, metas, x0s,preds] = load_model_batch(batch, cellids, modelnames, ...
                                        stack_extractor, meta_extractor);
cellcount=length(cellids);
modelcount=length(modelnames);

keepcell=zeros(cellcount,1);
for cc=1:cellcount,
   if size(x0s,1)>1 && ~isempty(x0s{2,cc}),
        keepcell(cc)=1;
   elseif ~isempty(x0s{1,cc}),
        keepcell(cc)=1;
    end
end
kk=find(keepcell);
cellids=cellids(kk);
cellcount=length(cellids);
x0s=x0s(:,kk);
preds=preds(:,kk);
stacks=stacks(:,kk);
metas=metas(:,kk);

depstr0 = nan(3,length(cellids), length(modelnames));        
deptau0 = nan(3,length(cellids), length(modelnames));        
depmag0 = nan(3,length(cellids), length(modelnames));        

fitset=struct();
for cc=1:cellcount,
    for mm=1:modelcount,
        fitset(cc,mm).cellid=cellids{cc};
        fitset(cc,mm).modelname=modelnames{mm};
        fitset(cc,mm).training_set=x0s{mm,cc}.training_set;
        fitset(cc,mm).test_set=x0s{mm,cc}.test_set;
        fitset(cc,mm).filecodes=x0s{mm,cc}.filecodes;
        
        fitset(cc,mm).r_test=preds{mm,cc}(1);
        fitset(cc,mm).r_fit=preds{mm,cc}(2);
        if length(preds{mm,cc})>2,
            fitset(cc,mm).r_active=preds{mm,cc}(3);
        else
            fitset(cc,mm).r_active=nan;
        end
        unique_codes = unique(x0s{mm,cc}.filecodes);
        
        % depression calculations - magnitude of effect on cartoon stimulus
        
        [mods] = find_modules(stacks{mm,cc}, 'depression_filter_bank', 1);
        mod=mods{1};
        
        if mod.facil_on
            depstr=mod.strength;
            deptau=mod.tau;
        else
            depstr=abs(mod.strength);
            deptau=abs(mod.tau);
        end
        fs=100;
        stim=[zeros(fs./2,1);ones(fs,1)./2;zeros(fs,1);
            ones(round(fs./10),1);zeros(round(fs./10),1);
            ones(round(fs./10),1);zeros(7.*fs./10,1)]';
        stimmax=max(stim(:));
        tau_norm=100;
        str_norm=100;
        tresp=depression_bank(stim,depstr./ ...
            str_norm,deptau./fs*tau_norm,1);
        depmag=zeros(1,size(tresp,1));
        for jj=1:size(tresp,1)
            depmag(jj)=1-sum(tresp(jj,:))./sum(stim);
        end
        
        depstr0(1:length(depstr),cc,mm)=depstr./str_norm;
        deptau0(1:length(deptau),cc,mm)=deptau./tau_norm.*1000;
        depmag0(1:length(depmag),cc,mm)=depmag;
   end
end

disp('KEY:');
for ii=1:length(modelnames),
    fprintf('%d: %s\n',ii,modelnames{ii});
end

fprintf('\n%-25s','CellID');
col_heads={'magC1','magC2','magC3','tauC1','tauC2','tauC3'};
for ii=1:length(col_heads),
    fprintf(',%8s',col_heads{ii});
end
fprintf('\n');

for cc=1:length(cellids),
    fprintf('%-25s',cellids{cc});
    fprintf(',%8.3f,%8.3f,%8.3f',depmag0(:,cc,1));
    fprintf(',%8.3f,%8.3f,%8.3f',deptau0(:,cc,1));
    fprintf('\n');
end


end

