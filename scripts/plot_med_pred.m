function plot_med_pred(batch, cellids, modelnames)

metric = 'r_ceiling';

if isempty(batch) || isempty(cellids) || isempty(modelnames)
    disp('Error: need to select cellids and models.');
    return;
end
dbopen;

fieldstoget={metric};
data = nan(length(cellids), length(modelnames),length(fieldstoget));

for ii = 1:length(modelnames)
   fprintf('Querying DB for model: %s\n', modelnames{ii});
   for jj = 1:length(cellids)
      sql = ['SELECT * FROM NarfResults WHERE batch=' num2str(batch) ''];
      sql = [sql ' AND cellid in (' interleave_commas(cellids(jj)) ')'];
      sql = [sql ' AND modelname in (' interleave_commas(modelnames(ii)) ')'];
      ret = mysql(sql);
      
      if isempty(ret)
         continue;
      end
      
      if length(ret) > 1
         fprintf('More than one matching model found!');
         keyboard;
      end
      
      for kk = 1:length(fieldstoget)
         v = ret(1).(fieldstoget{kk});
          
         if isempty(v)
            v = nan;
         end
         data(jj, ii, kk) = v;
      end
   end
end

med_pred=nanmedian(data(:,:,1),1);

figure;
subplot(2,2,1);
bar(med_pred);
title(sprintf('batch %d n=%d cells',batch,size(data,1)));

for ii=1:length(modelnames),
   if ii>1,
      p=randpairtest(data(:,ii-1),data(:,ii),1000,1,'median');
      text(0.5,-.1-.1*ii,...
         sprintf('%d: %s (%.3f, p<%.3f)',ii,modelnames{ii},med_pred(ii),p),...
         'Interpreter','none');
   else
      text(0.5,-.1-.1*ii,...
         sprintf('%d: %s (%.3f)',ii,modelnames{ii},med_pred(ii)),...
         'Interpreter','none');
   end
end
