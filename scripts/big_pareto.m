% big_pareto

% Pareto plot for all models in a batch

if 1,
    % between-area nat comparison
    DOMEDIAN=1;
    batchid=272;
    corrmax=0.8;
    snrthresh=0.0005;
    miniso=75;
    clin=[0.4 0.4 0.4];
    clinv=[0 0 0];
    cnl=[0.9 .5 .5];
    cnlv=[0.9 .1 .1];
    maxparms=175;
elseif 0,
    % for PLoS CB paper:
    DOMEDIAN=0;
    batchid=267;
    corrmax=0.6;
    snrthresh=0;
    clin=[0.4 0.4 0.4];
    clinv=[0 0 0];
    cnl=[0.9 .5 .5];
    cnlv=[0.9 .1 .1];
    maxparms=350;
elseif 0,
    % for R01 proposal:
    DOMEDIAN=1;
    batchid=266;  % or 267
    corrmax=0.7;
    snrthresh=0.01;
    clin=[0.5 0.5 0.5];
    clinv=[0.5 0.5 0.5]
    cnl=[0 0 0];
    cnlv=[0 0 0];
    maxparms=350;
elseif 1,
    % SPN two channel
    DOMEDIAN=0;
    batchid=259;
    corrmax=0.9;
    snrthresh=0.01;
    clin=[0.4 0.4 0.4];
    clinv=[0 0 0];
    cnl=[0.9 .5 .5];
    cnlv=[0.9 .1 .1];
    maxparms=100;
end

dbopen;
if miniso>0,
   iso_str=[' AND min_isolation>',num2str(miniso)];
else
   iso_str='';
end

%(snr_est^2 * estreps > threshold && snr_val^2 * valreps > threshold)
if snrthresh>0,
   snr_str=[' AND est_snr*est_snr*est_reps>',num2str(snrthresh),...
         ' AND val_snr*val_snr*val_reps>',num2str(snrthresh)];
else
   snr_str='';
end
if snrthresh>0 || miniso>0,
    sql=['SELECT modelname,avg(n_parms) as n_parms,avg(r_ceiling) as rc,',...
         'avg(r_test) as rt,avg(r_fit) as rf,',...
         ' count(NarfResults.id) as n_cells',...
         ' FROM NarfResults INNER JOIN NarfBatches ON',...
         ' (NarfResults.cellid=NarfBatches.cellid AND',...
         ' NarfResults.batch=NarfBatches.batch)',...
         ' WHERE NarfBatches.batch=',num2str(batchid),...
         snr_str,iso_str,...
         ' GROUP BY modelname'];
else
    sql=['SELECT modelname,avg(n_parms) as n_parms,avg(r_ceiling) as rc,',...
         'avg(r_test) as rt,avg(r_fit) as rf,count(id) as n_cells',...
         ' FROM NarfResults WHERE batch=',num2str(batchid),...
         ' GROUP BY modelname'];
end
pdata=mysql(sql);


lincounter=0;
fitv=zeros(length(pdata),1);
for ii=1:length(pdata),
    pdata(ii).modelname=char(pdata(ii).modelname);
    pdata(ii).n_parms=ifstr2num(pdata(ii).n_parms);
    if isempty(pdata(ii).n_parms), 
        pdata(ii).n_parms=0;
    end
    
    if length(pdata(ii).n_parms)~=1,
        fprintf('ii=%d np=%d\n',[ii length(pdata(ii).n_parms)]);
    end
    
    mn=pdata(ii).modelname;
    if ~isempty(regexp(mn,'fit05v$')),
        fitv(ii)=1;
    end
    if regexp(mn,'(trunc)'),
        % skip : trunc data
        pdata(ii).n_parms=0;
    elseif regexp(mn,'(adp|dep|volt|ifn|combsoft|cond)'),
        % skip : nl
        pdata(ii).linear=0;
    else
        %if regexp(mn,'(lognn|lognbn|logn|log2|logfree|wc[0-9][0-9]|wcg[0-9][0-9]|wcm[0-9][0-9])_fir'),
        lincounter=lincounter+1;
        fprintf('%d. lin: %s (%.3f)\n',lincounter,mn,pdata(ii).rc);
        pdata(ii).linear=1;
        
    end
    
    if isempty(regexp(mn,'fb18ch')),
        pdata(ii).alt_filterbank=1;
    else
        pdata(ii).alt_filterbank=0;
    end
    if isempty(regexp(mn,'lognn|lognbn|logn')),
        pdata(ii).alt_compress=1;
    else
        pdata(ii).alt_compress=0;
    end
    if isempty(regexp(mn,'dexp')),
        pdata(ii).alt_outnl=1;
    else
        pdata(ii).alt_outnl=0;
    end
    if isempty(regexp(mn,'fit05c|fit05v|fit09c')),
        pdata(ii).alt_fitter=1;
    else
        pdata(ii).alt_fitter=0;
    end
    
end

% only include full dataset and ones with n_parms recorded properly
n_parms=cat(1,pdata.n_parms);
n_cells=cat(1,pdata.n_cells);
maxn=max(n_cells);
r_fit=cat(1,pdata.rf);
if batchid==266 && maxn==134,
    maxn=130;
end

ff=find(n_cells==maxn & n_parms>0); % & (n_parms>30 | r_fit<.33));

pdata=pdata(ff);
n_parms=cat(1,pdata.n_parms);
r_ceil=cat(1,pdata.rc);
r_fit=cat(1,pdata.rf);
linear=cat(1,pdata.linear);
afb=cat(1,pdata.alt_filterbank);
acp=cat(1,pdata.alt_compress);
anl=cat(1,pdata.alt_outnl);
aft=cat(1,pdata.alt_fitter);
fitv=fitv(ff);

if DOMEDIAN,
    fprintf('DOMEDIAN==1: Calculating median for each model...\n');
    for ii=1:length(pdata),
        if snrthresh>0,
            sql=['SELECT r_fit,r_ceiling FROM NarfResults',...
                 ' INNER JOIN NarfBatches ON',...
                 ' (NarfResults.cellid=NarfBatches.cellid AND',...
                 ' NarfResults.batch=NarfBatches.batch)',...
                 ' WHERE NarfResults.modelname="',pdata(ii).modelname,'"',...
                 ' AND est_snr*est_snr*est_reps>',num2str(snrthresh),...
                 ' AND val_snr*val_snr*val_reps>',num2str(snrthresh),...
                 ' AND NarfResults.batch=',num2str(batchid)];
        else
            sql=['SELECT r_fit,r_ceiling FROM NarfResults',...
                 ' WHERE modelname="',pdata(ii).modelname,'"',...
                 ' AND batch=',num2str(batchid)];
        end
        
        mdata=mysql(sql);
        pdata(ii).med_r_ceil=median(cat(1,mdata.r_ceiling));
        pdata(ii).med_r_fit=median(cat(1,mdata.r_fit));
        
    end
    r_ceil=cat(1,pdata.med_r_ceil);
    r_fit=cat(1,pdata.med_r_fit);
end



msize=4;

figure;
for dataidx=1:2,
    x=n_parms;
    if dataidx==1,
        y=r_fit;
        stitle='r fit';
        ymax=0.5;
    else
        y=r_ceil;
        stitle='r ceiling';
        ymax=corrmax;
    end
    lin=linear;
    fv=fitv;
    
    x(x>300)=randn(size(find(x>300)))*2+300;
    
    xl=x(find(lin & ~fv));
    yl=y(find(lin & ~fv));
    xnl=x(find(~lin & ~fv));
    ynl=y(find(~lin & ~fv));
    xlv=x(find(lin & fv));
    ylv=y(find(lin & fv));
    xnlv=x(find(~lin & fv));
    ynlv=y(find(~lin & fv));
    
    ml=zeros(maxparms,1);
    mnl=zeros(maxparms,1);
    for ii=1:maxparms,
        if sum(lin & x<=ii)>0
            ml(ii)=max(y(lin & x<=ii));
        end
        if sum(~lin & x<=ii)>0
            mnl(ii)=max(y(~lin & x<=ii));
        end
    end
    %ml=gsmooth(ml,2);
    %mnl=gsmooth(mnl,2);
    %ml=ml(2:end);
    %mnl=mnl(2:end);
    
    
    subplot(2,3,dataidx*3-2);
    lt50=find(xnl<=50);
    plot(xnl(lt50),ynl(lt50),'.','Color',cnl);
    hold on
    lt50=find(xl<=50);
    plot(xl(lt50),yl(lt50),'.','Color',clin);
    lt50=find(xnlv<=50);
    plot(xnlv(lt50),ynlv(lt50),'.','Color',cnlv);
    lt50=find(xlv<=50);
    ht=plot(xlv(lt50),ylv(lt50),'.','Color',clinv);
    plot(mnl(1:50),'Color',cnlv,'LineWidth',.5);
    plot(ml(1:50),'Color',clinv,'LineWidth',.5);
    hold off
    aa=axis;
    axis([aa(1:2) 0 ymax]);
    
    subplot(2,3,dataidx*3-1);
    minx=0;
    gt50=find(xnl>minx);
    plot(xnl(gt50),ynl(gt50),'.','Color',cnl);
    hold on
    gt50=find(xl>minx);
    plot(xl(gt50),yl(gt50),'.','Color',clin);
    gt50=find(xnlv>minx);
    plot(xnlv(gt50),ynlv(gt50),'.','Color',cnlv);
    gt50=find(xlv>minx);
    %plot(xlv(gt50),ylv(gt50),'o','Color',[0.1 .1 .9],...
    %     'MarkerFaceColor',[0.1 .1 .9],'Markersize',msize);
    plot(xlv(gt50),ylv(gt50),'.','Color',clinv);
    plot((minx+1):length(mnl),mnl((minx+1):end),'Color',cnlv,'LineWidth',.5);
    plot((minx+1):length(ml),ml((minx+1):end),'Color',clinv,'LineWidth',.5);
    hold off
    aa=axis;
    axis([0 maxparms 0 ymax]);
    title(sprintf('batch %d max l=%.3f(%d) nl=%.3f(%d) snr>%.3f n=%d',...
                  batchid,max([yl;ylv]),length([yl;ylv]),max(y),length(y),snrthresh,maxn));
    
    n_best_lin=find(ml>=max(ml).*0.975,1);
    n_best_nl=find(mnl>=max(mnl).*0.975,1);
    xlabel(sprintf('nbestlin=%d nbestnl=%d',n_best_lin,n_best_nl));
    
    subplot(2,3,dataidx*3);
    minx=0;
    gt50=find(xl>minx);
    plot(xl(gt50),yl(gt50),'.','Color',clin);
    hold on
    gt50=find(xlv>minx);
    plot(xlv(gt50),ylv(gt50),'.','Color',clinv);
    plot((minx+1):length(ml),ml((minx+1):end),'Color',clinv,'LineWidth',.5);
    hold off
    aa=axis;
    axis([0 maxparms 0 ymax]);
end

set(gcf,'PaperPosition',[.25 2.5 7 4]);

if 0 && batchid~=259,
    figure;
    modeltypes=[sum(afb & ~acp & ~anl & linear) ...
                sum(~afb & acp & ~anl & linear) ...
                sum(~afb & ~acp & anl & linear) ...
                sum(afb & acp & ~anl & linear) ...
                sum(~afb & acp & anl & linear) ...
                sum(afb & ~acp & anl & linear) ...
                sum(afb & acp & anl & linear) ...
                sum(~afb & ~acp & ~anl & aft & linear) ...
                sum(~afb & ~acp & ~anl & ~aft & linear)];
    labelstr={'Filterbank','Compression','Output NL','Filterbank+Compression',...
              'Compression+Output NL','Filterbank+Output NL',...
              'Filterbank+Compression+OutputNL',...
              'Fit algorithm',...
              'Linear filter'};
    barh(modeltypes,'FaceColor',[0 0 0]);
    for ii=1:length(labelstr),
        text(5,ii,1,labelstr{ii},...
             'VerticalAlignment','middle','Color',[1 1 1]);
    end
    xlabel('Number of architectures');
end




return
fid=fopen('/auto/users/svd/docs/current/lowdim_strf/big_model_list_v2.txt','w');

ll=find(lin);
sset=[-fv(ll) n_parms(ll)];
[~,orderidx]=sortrows(sset);
orderidx=ll(orderidx);

cc=0;
for ii=orderidx(:)',
    cc=cc+1;
    fprintf(fid, '%d\t%.0f\t%.3f\t%.3f\t%.3f\t%s\n',...
            cc,...
            pdata(ii).n_parms,pdata(ii).rf,pdata(ii).rt,pdata(ii).rc,...
            pdata(ii).modelname);
end
fclose(fid);
