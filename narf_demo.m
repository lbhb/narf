
global BAPHYHOME NARF_PATH
BAPHYHOME='X'; % SVD hack to disable baphy dependency!

narf_set_path;

batch=0;
cellid='bbl031f-a1';
kw={'loadbasic','lognn','wcg02','fir15','siglog100','fit05h'};
training_set={[NARF_PATH filesep 'demo' filesep 'bbl031f11_p_NAT_c1_u1.mat_est']};
test_set={[NARF_PATH filesep 'demo' filesep 'bbl031f11_p_NAT_c1_u1.mat_val']};
filecodes={''};

success=fit_single_model(batch, cellid, kw, training_set, test_set, filecodes);

if success,
   disp('Model successfully fit!  Run narf_modelpane() to inspect.');
   disp('Or explore the global variables XXX STACK and META');
end

global STACK XXX META

for ii=1:length(STACK),
   fprintf('Module %d: %s\n',ii,STACK{ii}{1}.name');
   if isfield(STACK{ii}{1},'fit_fields'),
      fprintf(' fit fields: %s\n',strmerge(STACK{ii}{1}.fit_fields));
   else
      disp(' no fit fields');
   end
end

