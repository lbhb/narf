function narf_set_path()   

    % Back out of keywords directory if happened to have crashed there.
    [pp,ppb]=fileparts(pwd);
    if strcmpi(ppb,'keywords'),
        cd('..');
    end
    
    global NARF_PATH NARF_MODULES_PATH NARF_SAVED_MODELS_PATH ...
           NARF_SAVED_IMAGES_PATH NARF_SCRIPTS_PATH NARF_LIBS_PATH...
           NARF_KEYWORDS_PATH NARF_FITTERS_PATH NARF_MEMOIZATION_PATH;
    NARF_PATH = fileparts(which('narf_set_path'));   
    NARF_FITTERS_PATH = [NARF_PATH filesep 'fitters'];
    NARF_KEYWORDS_PATH = [NARF_PATH filesep 'keywords'];
    NARF_MODULES_PATH = [NARF_PATH filesep 'modules'];
    % lbhb in-house defaults
    NARF_SAVED_MODELS_PATH   = '/auto/data/code/saved_models';
    NARF_SAVED_IMAGES_PATH   = '/auto/data/code/saved_images';
    NARF_MEMOIZATION_PATH    = '/auto/data/code/memoization';
    if ~exist(NARF_SAVED_MODELS_PATH,'dir'),
       NARF_SAVED_MODELS_PATH   = [NARF_PATH filesep 'saved_models'];
       NARF_SAVED_IMAGES_PATH   = [NARF_PATH filesep 'saved_images'];
       NARF_MEMOIZATION_PATH    = '';
    end
    
    NARF_SCRIPTS_PATH = [NARF_PATH filesep 'scripts'];
    NARF_LIBS_PATH = [NARF_PATH filesep 'libs'];
    
    warning off MATLAB:dispatcher:nameConflict;
    addpath(NARF_MODULES_PATH, ...
            NARF_FITTERS_PATH, ...
            NARF_LIBS_PATH, ...        
            [NARF_PATH filesep 'methods'], ...
            [NARF_PATH filesep 'queue'], ...    
            [NARF_PATH filesep 'utils'], ...
            [NARF_PATH filesep 'utils' filesep 'svd_tools'], ...
            [NARF_PATH filesep 'utils' filesep 'nsltools']);

    addpath(NARF_PATH);
    global BAPHYHOME
    if isempty(BAPHYHOME),
        baphy_set_path
    end
    warning on MATLAB:dispatcher:nameConflict;        
end
