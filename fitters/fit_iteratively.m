function [termcond, s, n_iters] = fit_iteratively(fitter,outer_loop_term_fn, reverse, first_start)

global STACK XXX FLATXXX META;
if ~exist('reverse','var'),
    reverse=0;
end
if ~exist('first_start','var'),
    first_start=1;
end

phi_init = pack_fittables(STACK);

if isempty(phi_init)
    fprintf('Skipping because there are no parameters to fit.\n');
    termcond = NaN;  
    return 
end

if ~exist('fitter', 'var')
    error('You should provide a fitter to fit_iteratively();'); 
end

if ~exist('outer_loop_term_fn', 'var')
    outer_loop_term_fn = create_term_fn('StopAtStepNumber', 10);
end

cached_stack = STACK;
prev_opts = {}; 

n = 1;
s = nan;
d = nan;
n_iters = 0;

fprintf('*********************BEGINNING ITERATIVE FIT**************************\n');

% Terminate if the previous position is exactly the same as this one
termcond = outer_loop_term_fn(n, s, d, n_iters);
while ~termcond || (first_start>0 && n<=2),
    fprintf('Beginning Iterative Loop Iteration #%d\n', n);

    if evalin('caller','exist(''SubsampleFrac'',''var'')'),
       SubsampleFrac=evalin('caller','SubsampleFrac');
       if SubsampleFrac<1,
         fprintf('Subsampling %.2f of the fit data.\n',SubsampleFrac);
         
         % remove part of the fit set for stochastic gradient descent
         savedat=XXX{2}.dat;
         XXX=XXX(1:2);
         fns = fieldnames(savedat);
         for ii = 1:length(fns)
            sf=fns{ii};
      
            % Compute the size of the filter
            [T, S, C] = size(savedat.(sf).stim);
            kk=shuffle(1:S);
            kk=kk(1:round(S.*SubsampleFrac));
            XXX{2}.dat.(sf).stim=savedat.(sf).stim(:,kk,:);
            XXX{2}.dat.(sf).resp=savedat.(sf).resp(:,kk,:);
            XXX{2}.dat.(sf).respavg=savedat.(sf).respavg(:,kk);
            if isfield(XXX{2}.dat.(sf),'pupil'),
               XXX{2}.dat.(sf).pupil=savedat.(sf).pupil(:,kk);
            end
            XXX{2}.dat.(sf).trial_code=savedat.(sf).trial_code(kk);
         end
         FLATXXX=XXX;
         update_xxx(2);
         if ~isnan(s),
           s=META.perf_metric()
         end
         %XXX{end}.dat.(sf)
       end
    else 
       SubsampleFrac=1;
    end

    if n_iters==0 && first_start>1,
        jjkeys=first_start:length(STACK);
    elseif reverse,
        jjkeys=length(STACK):-1:1;
    else
        jjkeys=1:length(STACK);
    end
    for jj = jjkeys,
        if ~isfield(cached_stack{jj}{1}, 'fit_fields')
            prev_opts{jj} = nan; % Just a stupid placeholder
            continue;
        end
        for mm=1:length(STACK{jj}),
            fprintf('Running fitter on only module %d:%d (%s)\n', ...
                    jj, mm, STACK{jj}{mm}.name);
        
            % Erase all fit fields except the normal one
            for kk = 1:length(STACK)
                for ll=1:length(STACK{kk}),
                    if ~isfield(cached_stack{kk}{ll}, 'fit_fields') || ...
                            (jj==kk && mm==ll),
                        continue;
                    else
                        STACK{kk}{ll}.fit_fields = {};
                    end
                end
            end
            
            % Run the fitter once, passing it the previous opts if they exist.       
            if length(prev_opts) < jj || isempty(prev_opts{jj}),
                [~, s_new, iters, prev_opts{jj}] = fitter();
            else
                [~, s_new, iters, prev_opts{jj}] = fitter(prev_opts{jj});
            end 
            
            if isnan(s)
                s = s_new;   % Just the first time, initialize it
            end
            
            % Restore the fit fields
            for kk = 1:length(STACK)
                if ~isfield(cached_stack{kk}{1}, 'fit_fields')
                    continue;
                else
                    for ll = 1:length(STACK{kk}(:))
                        STACK{kk}{ll}.fit_fields = cached_stack{kk}{ll}.fit_fields;
                    end
                end
            end
        end
    end
    
    n_iters = n_iters + iters;
    if isnan(s)
        d = s_new;
    else
        d = s - s_new;
    end
    s = s_new;
    
    fprintf('Ending Iterative loop %d. Score: %e, S_delta: %e\n', n, s, d);
    fprintf('======================================================================\n');  
    n = n + 1;
    termcond = outer_loop_term_fn(n, s, d, n_iters);
    if n>1000,
       fprintf('Hard coded too many iterations\n');
       termcond=1;
    end
    
    if SubsampleFrac<1,
       % restore full training set following subsampling during boosting
       %XXX{2}.dat.(sf)
       disp('restoring XXX');
       XXX=XXX(1:2);
       XXX{2}.dat=savedat;
       update_xxx(2);
    end

    % tell the queue daemon that job is still alive
    dbtickqueue;
end
