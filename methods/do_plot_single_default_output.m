function do_plot_single_default_output(sels, stack, xxx)
    [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));    
    yfields=mdls{1}.output;% original behavior
    yfields=cellfun(@(x)x.output,mdls,'Uni',0); %mod to get plots for split models
    if length(unique(yfields))>1
        for k=1:length(sels)
            if(size(xouts{k}.dat.(sels(k).stimfile).(yfields{k}),3)<sels(k).chan_idx)
                sels(k).chan_idx=1;
            end
            out_ind=xouts{k}.dat.(sels(k).stimfile).trial_code(sels(k).stim_idx);
            inds=xouts{k}.dat.(sels(k).stimfile).trial_code==out_ind;
           sels(k).stim_idx=sum(inds(1:sels(k).stim_idx));
            do_plot(xouts, mdls{1}.time, yfields{out_ind}, ...
                sels(k), 'Time [s]', 'Output [-]','Color',pickcolor(k));
        end
    else
        do_plot(xouts, mdls{1}.time, yfields{1}, ...
                sels, 'Time [s]', 'Output [-]');
    end
end
