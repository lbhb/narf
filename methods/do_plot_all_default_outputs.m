function do_plot_all_default_outputs(sels, stack, xxx)
[mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));
[sels.chan_idx] = deal([]); % when chan_idx is empty, do_plot plots all channels

outputs=cellfun(@(x)x.output,mdls,'Uni',0);
h=[];
leg=cell(0);
if length(unique(outputs))>1 
    for k=1:length(mdls)
        for m=1:length(sels)
            sel=sels(m);
            inds=xouts{k}.dat.(sel.stimfile).trial_code==k;
            if(xouts{k}.dat.(sel.stimfile).trial_code(sel.stim_idx)~=k)
                continue
            else
                sel.stim_idx=sum(inds(1:sel.stim_idx));
                if(length(sels)>1)
                    vars={'Color',pickcolor(k)};
                else
                    vars={};
                end
                [ht,legt]=do_plot(xouts(k), mdls{k}.time, mdls{k}.output,sel, 'Time [s]', 'Output [-]',vars{:});
                h(end+1:end+length(ht))=ht;
                leg(end+1:end+length(legt))=legt;
            end
        end
    end
    lh = legend(h, leg, 'Location', 'NorthWest');
    set(lh,'Interpreter', 'none');
else
    do_plot(xouts, mdls{1}.time, mdls{1}.output,sels, 'Time [s]', 'Output [-]');
end
end
