function [handles,leg]=do_plot(xxs, xfield, yfield, sels, xlab, ylab,varargin)
% do_plot(xxs, xfield, yfield, sels, xlab, ylab)
% xxs is a cell array of default inputs to a module's .fn function
% xfield (what to use as x axis indexes)
% yfield (what to use as y data. If it's a cell array, multiple things will be plotted)
% sels.stimfile must be a single string
% sels.stim_idx must be a scalar
% sels.chan_idx can be a scalar, vector, or an empty vector to plot everything
% xlab  x label
% ylab  y axis label

%leg_location='NorthWest';
leg_location='Best';

if ~isfield(sels, 'stimfile') || ~isfield(sels, 'stim_idx') || ...
        ~isfield(sels, 'chan_idx')
    error('sels has 3 required fields: stimfile, stim_idx, chan_idx');    
end

if ~iscell(yfield)
    yfields = {yfield};
else
    yfields = yfield;
end

n_yfields = length(yfields);
if(length(sels)==1&&n_yfields==1)
    if(length(xxs)==1)
        color_by='yfields';%mean yfields and channels
        linetype_by='xxxs';
    else
        color_by='xxxs';
        linetype_by='yfields';
    end
elseif(length(sels)==1)
    color_by='yfields';
    linetype_by='xxxs';
elseif(length(xxs)==1&&length(sels)>1&&n_yfields==1)
    color_by='sels';
    linetype_by='available';
elseif(length(xxs)==1&&length(sels)>1&&n_yfields>1)
    color_by='sels';
    linetype_by='yfields';
elseif(length(xxs)>1&&length(sels)>1&&n_yfields==1)
    color_by='sels';
    linetype_by='xxxs';
elseif(length(xxs)>1&&length(sels)>1&&n_yfields>1)
    error('Multiple sels, xxs and y_fields, what to do?')
else
    error('How did I get here?')
end
  
if(1)
    color_shade_by=linetype_by;
    linetype_by='none';
    ls='-';lw=1;
else
    color_shade_by='none';
end


hold on;
leg = {};
handles = [];
plotted_data=0;
leg_nb = 0;
for ii = 1:length(xxs)
  for kk=1:length(sels)  
    % Skip unless all fields are in existence
    if  ~isfield(xxs{ii}.dat, sels(kk).stimfile) || ...
            ~isfield(xxs{ii}.dat.(sels(kk).stimfile), xfield) || ...
            ~all(isfield(xxs{ii}.dat.(sels(kk).stimfile), yfields)) ||...
            isempty(xxs{ii}.dat.(sels(kk).stimfile).(yfields{1})),
        continue;
    end
    
    % Otherwise, plot all the selected channels   
    
    
    
    switch linetype_by
        case 'xxxs';
            [ls, n_linestyles] = pickline(ii);
            lw = ceil(ii / n_linestyles);
        case 'sels'
            [ls, n_linestyles] = pickline(kk);
            lw = ceil(kk / n_linestyles);
        case 'yfields'
            %do later
        otherwise    
            error('')
    end
    
    switch color_by
        case 'yfields'
            ci = 1;%switched later
        case 'sels'
            ci=kk;
        case 'xxxs'
            ci = ii;
        otherwise
            error('')
    end
    
    ydata=[];
    valididx=[];
    for yi = 1:n_yfields
        yfield = yfields{yi};
        
        chansize = 1:size(xxs{ii}.dat.(sels(kk).stimfile).(yfield), 3);
        
        if isempty(sels(kk).chan_idx)
            sels(kk).chan_idx = chansize;
        end
        if(any(sels(kk).chan_idx>1) && length(chansize)==1)
            sels(kk).chan_idx=1;
        end
        for qq = 1:length(sels(kk).chan_idx)
            jj = sels(kk).chan_idx(qq);
            leg_nb = leg_nb + 1;
            if isempty(ydata),
               ydata=squeeze(xxs{ii}.dat.(sels(kk).stimfile).(yfield)(:, sels(kk).stim_idx, jj));
            else
                try
                    ydata(:,yi,qq)=squeeze(xxs{ii}.dat.(sels(kk).stimfile).(yfield)(:, sels(kk).stim_idx, jj));
                catch err
                    fprintf(1,'Could not print channel %d of stimulus %s. Are you trying to plot stimuli with different channel number?\n', jj, yfield);
                    leg_nb = leg_nb - 1;
                end
            end
        end
    end
    invalididx=sum(sum(isnan(ydata),2),3)>0;
    ydata(invalididx,:,:)=nan;
    
    for yi = 1:n_yfields
        yfield = yfields{yi};
        
        chansize = 1:size(xxs{ii}.dat.(sels(kk).stimfile).(yfield), 3);
        
        if isempty(sels(kk).chan_idx) || max(sels(kk).chan_idx)>max(chansize),
           selchans=chansize;
        else
           selchans=sels(kk).chan_idx;
        end
        if(strcmp(linetype_by,'yfields'))
            [ls, n_linestyles] = pickline(yi);
            lw = ceil(yi / n_linestyles);
        end
        for qq = 1:length(selchans)
            jj = sels(kk).chan_idx(qq);
            c = pickcolor(ci);
            if(strcmp(color_shade_by,'yfields'))
                c_hsv=rgb2hsv(c);
                %c_hsv(2)=min(c_hsv(2)+.3*(yi-1),1);
                c_hsv(1)=mod(c_hsv(1)+.1*(yi-1),1);
                c_hsv(3)=min(c_hsv(3)+.4*(yi-1),1);
                c=hsv2rgb(c_hsv);
            elseif(strcmp(color_shade_by,'available')&&length(selchans)>1)    
                c_hsv=rgb2hsv(c);
                %c_hsv(2)=min(c_hsv(2)+.3*(yi-1),1);
                c_hsv(1)=mod(c_hsv(1)+.1*(qq-1),1);
                c_hsv(3)=min(c_hsv(3)+.4*(qq-1),1);
                c=hsv2rgb(c_hsv);
            end
            if(strcmp(color_by,'yfields'))
                ci = ci + 1;
            end
            
            % Special case: if there is only one signal possible, make it black
            if (1 == length(selchans)) && (n_yfields == 1) && strcmp(color_by,'y_fields')
                c = [0, 0, 0];
            end
            hold on;
            h = plot(xxs{ii}.dat.(sels(kk).stimfile).(xfield)(:), ...
                ydata(:,yi,qq), ...
                'Color', c, 'LineStyle', ls, 'LineWidth', lw,varargin{:});
            hold off;
            handles(end+1) = h;
            
            if length(xxs)==1 && length(sels)==1,
                leg{end+1} = [yfield '/CH' num2str(jj)];
            elseif length(xxs)>1 && length(sels)==1,
                leg{end+1}=[yfield '/' xxs{ii}.unique_codes{ii} '/CH' num2str(jj)];
            elseif length(xxs)==1 && length(sels)>1,
                leg{end+1}=[yfield '/SS' num2str(kk) '/CH' num2str(jj)];
            elseif length(xxs)>1 && length(sels)>1,
                leg{end+1}=[yfield '/' xxs{ii}.unique_codes{ii} '/SS' num2str(kk) '/CH' num2str(jj)];
            else
                error('Why am I here?')
            end
        end
    end
    plotted_data=1;
  end
end

if plotted_data,
   lh = legend(handles, leg{1:leg_nb}, 'Location', leg_location);
   set(lh,'Interpreter', 'none');
   
   do_xlabel(xlab);
   do_ylabel(ylab);

   axis tight;
end

hold off;
