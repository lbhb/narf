function varargout=do_plot_scatter(sels, xxxs, field1, field2, n_plotpts,varargin)
%do_plot_scatter(sel, xxxs, field1, field2, n_plotpts)
%do_plot_scatter(sel, xxxs, field1, field2, n_plotpts,varargin)
% Plots two fields vs each other
% n_plotpts determines how many points will be plotted.
% If not provided, all points will be plotted.
% If you want a smoother plot, try n_plotpts=100
% Desired plot attributes can be added in varargin

% LAS: added varargin 2016-7-26

hold on;

% These variables will hold the limits of ALL plots
xmin = 0;
xmax = 10;
ymin = 0;
ymax = 10;

if(length(sels)==1)
    color_by='xxxs';
    linetype_by='none';
elseif(length(xxxs)==1&&length(sels)>1)
    color_by='sels';
    linetype_by='xxxs';
elseif(length(xxxs)>1&&length(sels)>1)
    color_by='sels';
    linetype_by='xxxs';
else
    error('How did I get here?')
end


ci=0;
for ii = 1:length(xxxs)
    for jj=1:length(sels)
        if(jj>1 && strcmp(sels(jj).stimfile,sels(1).stimfile))
            continue
        end
        switch color_by
            case 'xxxs'
                ci=ii;
            case 'sels'
                ci=jj;
        end
        x = xxxs{ii};
        
        if ~isfield(x.dat, sels(jj).stimfile)
            % Do nothing if there is no matching selected stimfile
            continue;
        end
        
        dat = x.dat.(sels(jj).stimfile);
        
        inds1=1:size(dat.(field1),2);
        inds2=1:size(dat.(field2),2);
        do_tc=true;
        if(~isempty(strcmp(field1,'stim')))
            if(length(field1)>4)
                stimi=str2double(field1(5:end));
                inds2=find(dat.trial_code==stimi);
                do_tc=false;
            end
        end
        if(isfield(sels(jj),'trial_code') && do_tc)
            if(~isempty(sels(jj).trial_code))
                inds2=find(dat.trial_code==sels(jj).trial_code);
                inds1=inds2;
            end
        end
        [T1, S1, C1] = size(dat.(field1)(:,inds1,:));
        [T2, S2, C2] = size(dat.(field2)(:,inds2,:));
        
        if C2 ~= 1
            error('I can only accomodate a single channel in input2');
        end
        
        if S1==0 || S2==0,
            disp('empty scatter plot');
        elseif ~isequal([T1 S1], [T2 S2])
            cla;
            text(0.35, 0.5, 'ERROR! Inputs of unequal size cannot be scatter plotted.');
            axis([-10, 100, -10 100]);
            return
        else
            
            for cc = 1:C1
                tmp = dat.(field1)(:,inds1,cc);
                tmp2= dat.(field2)(:,inds2,:);
                D = [tmp(:) tmp2(:)];
                D = D(~isnan(D(:,1)) & ~isnan(D(:,2)),:);
                
                if exist('n_plotpts', 'var') && ~isnan(n_plotpts) && n_plotpts > 0
                    D = sortrows(D);
                    D = conv_fn(D, 1, @nanmean, max(1,ceil(size(D, 1)/n_plotpts)), 0);
                end
                
                c = pickcolor(ci);
                if C1 == 1 && length(sels)==1
                    c = [0 0 0];
                end
                
                plot(D(:,1), D(:,2), 'Color', c, 'Marker', '.', 'LineStyle', 'none',varargin{:});
                if(nargout>0)
                   varargout{1}{jj,cc}=D;
                end
            end
            axis tight;
            v = axis;
            xmin = min(xmin, v(1));
            xmax = max(xmax, v(2));
            ymin = min(xmin, v(3));
            ymax = max(xmax, v(4));
        end
    end
end

axis([xmin, xmax, ymin, ymax]);
hold off;